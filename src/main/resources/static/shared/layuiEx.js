(function (moduleName4Html, global, factory) {
    if (typeof module !== 'undefined' && typeof exports === 'object') {//CommonJS(NodeJS)
        module.exports = factory(global);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {//AMD   CMD
        define(function () { factory(global); });
    } else {//ES(Browser)
        global[moduleName4Html] = factory(global);
    }
}("layuiEx", this, function (global) {
    'use strict';

    var layuiEx = {
        tableRender: function (url, tableId, pageId, cols, reqData, callback) {
            if (!reqData) {
                reqData = { page: 1, pageSize: 10 }
            }
            shared.ajaxGet(url, reqData, function (data) {
                layui.table.render({
                    elem: '#' + tableId,
                    cols: [cols],
                    even: true,//颜色交替
                    skin: 'line',//去除纵向分割线
                    data: data.list,
                    limit: reqData.pageSize,
                    height: data.list.length > 10 ? 450 : 431,
                    done: function (res, curr, count) {
                        $('table').css('width', '100%');
                        //这里设置表格的宽度为100%
                    },
                });

                layui.laypage.render({
                    elem: pageId
                    , count: data.count
                    , limit: reqData.pageSize
                    , limits: [10, 20, 50, 100]
                    , curr: reqData.page
                    , layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
                    jump: function (obj, first) {
                        if (!first) {
                            reqData.page = obj.curr;
                            reqData.pageSize = obj.limit;
                            layuiEx.tableRender(url, tableId, pageId, cols, reqData, callback);
                        }
                    }
                });

                if (callback) {
                    callback(data);
                }
            });
        },


        tableSort: function (url, tableId, pageId, cols, orderBy, reqData, callback) {
            if (!reqData) {
                reqData = { page: 1, pageSize: 10 }
            }
            var upload = layer.msg('加载中...', {
                icon: 16,
                shade: 0.2,
                time: false
            });
            shared.ajaxGet(url, reqData, function (data) {
                layui.table.render({
                    elem: '#' + tableId,
                    cols: [cols],
                    even: true,//颜色交替
                    skin: 'line',//去除纵向分割线
                    data: data.list,
                    initSort: orderBy,
                    limit: reqData.pageSize,
                    height: data.list.length > 10 ? 450 : 431,
                    done: function (res, curr, count) {
                        layer.close(upload);
                        $('table').css('width', '100%');
                        //这里设置表格的宽度为100%
                    },
                });

                layui.laypage.render({
                    elem: pageId
                    , count: data.count
                    , limit: reqData.pageSize
                    , limits: [10, 20, 50, 100]
                    , curr: reqData.page
                    , initSort: orderBy
                    , layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
                    jump: function (obj, first) {
                        if (!first) {
                            reqData.page = obj.curr;
                            reqData.pageSize = obj.limit;
                            layuiEx.tableSort(url, tableId, pageId, cols, orderBy, reqData, callback);
                        }
                    }
                });

                if (callback) {
                    callback(data);
                }
            });
        }
    };

    return layuiEx;
}));
