(function (moduleName4Html, global, factory) {
    if (typeof module !== 'undefined' && typeof exports === 'object') {//CommonJS(NodeJS)
        module.exports = factory(global);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {//AMD   CMD
        define( function () { factory(global); } );
    } else {//ES(Browser)
        global[moduleName4Html] = factory(global);
    }
}("shared", this, function (global) {
    'use strict';

    var shared = {
        useLayout: function (selectedClassName, optionalSelector) {
            $.ajaxSetup({ async: false });
            $("#header").load("/shared/header.html", function () {
                var url = window.location.pathname
                if (url === "/") {
                    url = "/index.html";
                }
                if (optionalSelector) {
                    $('#header '  + 'a[ href="' + optionalSelector + '"]').parent().toggleClass(selectedClassName);
                } else {
                    $('#header a[ href="' + url + '"]').parent().toggleClass(selectedClassName);
                }
            });
            // 收藏弹窗
            $("#saveClassify").load("/shared/savePop.html");
            // 高级检索
            $("#advanceSearch").load("/shared/advanceSearch.html");
            $("#search").load("/shared/search.html");
            
            $("#footer").load("/shared/footer.html");
            //屏幕宽度
            var rollWidth=$(window)[0].innerWidth-$(window).width();
            $("body").css("width",$(window)[0].screen.width-rollWidth);
            $.ajaxSetup({ async: true });
        },

        ajaxCallback: function (jResult, callback) {
            if (!jResult) {
                alert("服务器响应异常");
                return;
            }
            if (jResult.code === undefined) {
                callback(jResult);
            } else if (jResult.code === 200) {
                callback(jResult.data);
            } else if (jResult.code === 732) {
                callback(jResult.data);
            } else if (jResult.code === 1001) {
                shared.setCookie('userinfo', '');
                shared.setCookie('mydx_token', '');
               // alert('登录信息过期，请重新登录！');
               // window.location.href = "/login.html";
            }else if (jResult.code === 1200) {
                //修改密码成功
                callback(jResult.data);
            }else {
                alert(jResult.msg ? jResult.msg : "未知错误");
            }
        },

        ajaxPost:function (serverRelUrl, dataObj, callback, setupObj) {
            $.ajax($.extend({
                //async : false,
                type: "POST",
                url: config.server + serverRelUrl,
                data: typeof dataObj === 'object' ? JSON.stringify(dataObj) : dataObj,
                headers: {
                    mydx_token: shared.getCookie('mydx_token'),
                 },
                //  xhrFields: {
                //     withCredentials: true
                //  },
                crossDomain: true,
                cache:false,
                contentType: 'application/json',
                //contentType: 'text/html',
                dataType: "json",
                success: function (jResult) {
                    shared.ajaxCallback(jResult, callback);
                
                },
                error: function (p1,p2,p3) {
                    // console.log(JSON.stringify(p1))
                    // console.log(p2)
                    // console.log(p3)
                    // console.log("ajaxPost error")
                    // console.log('token',shared.getCookie('token'))
                }

            }, setupObj));
        },

        ajaxGetSync:function (serverRelUrl, dataObj, callback, setupObj) {
            $.ajax($.extend({
                url: config.server + serverRelUrl,
                headers: {
                    mydx_token: shared.getCookie('mydx_token')
                },
                async: false,
                crossDomain: true,
                cache:false,
                //data: typeof dataObj === 'object' ? JSON.stringify(dataObj) : dataObj,
                data: dataObj,
                //  dataType: "json",
                success: function (jResult) {
                    shared.ajaxCallback(jResult, callback)
                },
                error: function (p1,p2,p3) {
                    // console.log(JSON.stringify(p1))
                    // console.log(p2)
                    // console.log(p3)
                    // console.log("ajaxGet error")
                    // console.log(666666666667)
                }
            }, setupObj));
        },
        ajaxGet:function (serverRelUrl, dataObj, callback, setupObj) {
            $.ajax($.extend({
                url: config.server + serverRelUrl,
                headers: {
                    mydx_token: shared.getCookie('mydx_token')
                },
                crossDomain: true,
                cache:false,
                //data: typeof dataObj === 'object' ? JSON.stringify(dataObj) : dataObj,
                data: dataObj,
              //  dataType: "json",
                success: function (jResult) {
                    shared.ajaxCallback(jResult, callback)
                },
                error: function (p1,p2,p3) {
                    // console.log(JSON.stringify(p1))
                    // console.log(p2)
                    // console.log(p3)
                    // console.log("ajaxGet error")
                    // console.log(666666666667)
                }
            }, setupObj));
        },
        // cookie设置参数值
        setCookie:function(name, value){
            $.cookie(name, value, { path: '/' });  
        },
        //获取cookie值
        getCookie:function(name){
            return $.cookie(name); 
        }
    }

    return shared;
}));

