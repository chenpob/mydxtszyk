(function (moduleName4Html, global, factory) {
    if (typeof module !== 'undefined' && typeof exports === 'object') {//CommonJS(NodeJS)
        module.exports = factory(global);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {//AMD   CMD
        define( function () { factory(global); } );
    } else {//ES(Browser)
        global[moduleName4Html] = factory(global);
    }
}("config", this, function (global) {
    'use strict';
    var realServer = "http://218.89.178.103:9000";
    var testServer = "http://172.16.218.125:8288";
    var selfServer = "http://127.0.0.1:9000";
    var chengYuServer = "http://172.16.218.163:8288";
    var ZhouZhiPengServer= "http://172.16.218.174:8100";
    var XuTianServer = "http://172.16.218.35:8288";
    var ChengWenServer = "http://172.16.218.43:8288";
    var server =selfServer;

    var config = {
        server: server,
        json: {
          index_server:[
            {name:'期刊',code:'CJFDTOTAL'},
            {name:'博硕',code:'CDFDTOTAL'},
            {name:'会议',code:'CPFDLAST2019_06,IPFDTOTAL'},
            {name:'报纸',code:'CCNDTOTAL'},
            {name:'中国专利',code:'SCPD'},
            {name:'海外专利',code:'SOPD'},
            {name:'科技成果',code:'SNAD'}
            //   {name:'期刊',code:'SYHJLAST2020'},
            //   {name:'博硕',code:'SYHALAST2020'},
            //   {name:'会议',code:'SYHOLAST2020'},
            //   {name:'报纸',code:'SYHNLAST2020'},
            //   {name:'中国专利',code:'SYHZLAST2020'},
            //   {name:'海外专利',code:'SYHYLAST2020'},
            //   {name:'科技成果',code:'SYHLLAST2020'}
            ]
        },
        cnkiTop:[{name:'文献',code:'SYSTZK'},
                 {name:'期刊',code:'SYHJ'},
                 {name:'博硕士',code:'SYSTBS'},
                 {name:'会议',code:'SYSTHY'},
                 {name:'报纸',code:'SYHN'},
                 {name:'中国专利',code:'SYHZ'},
                 {name:'海外专利',code:'SYHO'},
                 {name:'科技成果',code:'SYHA'},
                 {name:'党建知识' ,code:'http://10.89.145.250/hyindex/djsw/'}
                ],
        // 分页每页显示条数
        selectArr:[
            {value:'主题',text:'主题'},
            {value:'题名',text:'题名'},
            // {value:'作者',text:'作者'},
            // {value:'关键词',text:'关键词'}
        ],
        selectwwqkArr:[
            {value:'主题',text:'主题'},
            {value:'题名',text:'题名'},
            // {value:'作者',text:'作者'},
             {value:'关键词',text:'关键词'}
        ],
        selectPerArr:[{value:'题名',text:'题名'}],
        selectSystemArr:[{value:'标题',text:'标题'}],
        //后台登录
        // loginUrl: "/adminUserInfo/login",
        loginUrl: "/admin/login",
        // 首页情报资讯
        getMenue:'/qbzx/getSortName',
        getQBZXBySortName:'/qbzx/getQBZXBySortName',
        //首页知识服务
         //获取知识服务下拉框数据
        getSearchItem:'/knowledge/getSearchItem',
        getQBZXByTableName:'/yqtIndex/getQBZXByTableName',
        //科研档案  get 参数:无
        getKYDASort:'/yqtIndex/getKYDASort',
        // get  参数:sortCode
        getKYDABySort_Index:'/yqtIndex/getKYDABySortName', 
         // 期刊阅读
        getQKYD_Index:'/yqtIndex/getQKYD_Index',
        getDBCodeFromFile:'/qkyd/getDBCodeFromFile',
        // 外文数据
        getWWSJ:'/yqtIndex/getWWSJ',
        fzww:'http://10.89.143.134:8000/Usp/apabi_usp/?pid=book.detail&metaid={1}&dt=META020&cult=CN',
        kns:'http://10.89.145.250/kns/brief/result.aspx?dbprefix=SYSTZK&kw=&korder=2&other=&sel=1&NaviDatabaseName=SYST_042_CLS&NaviField=%e8%a1%8c%e4%b8%9a%e5%88%86%e7%b1%bb%e4%bb%a3%e7%a0%81&systemno={1}&DSCode={2}',
        //外部资源
        getOutResourceDetail:'http://10.89.145.250/KCMS/detail/detail.aspx?dbcode=&dbname={0}&filename={1}&pcode=SYST',
        Domain:"http://10.89.145.250",
        cnkiMore:'http://10.89.145.250/kns/brief/result.aspx',
        getCover:'/qbzx/getCover',
        getPngSource:'/qbzx/getPngSource',
        //知识服务整合检索
        complexSearch:'/knowledge/complexSearch',
        // 左侧导航栏
        knowledgeGetIndex:'/knowledge/getIndex',
        //情报资讯
        getSortName:'/qbzx/getSortName',
        //获取外文期刊显示项的接口get  
        getSort_English:'/qkyd/getSort_English',
        //获取中文期刊显示项的接口  
        getYear_Chinese:'/qkyd/getYear_Chinese',
        //期刊数据
        complexSearchQKYD:'/qkyd/complexSearchQKYD',
       // post   参数:sysId
        getQKYDIndex:'/qkyd/getQKYD_WWsj_Index',
        //获取收藏列表
        getCollectionList:'/personCenter/getCollectionList',
        // 新增收藏
        insertCollection:'/personCenter/insertCollection',
        // 删除收藏
        deleteCollection:'/personCenter/deleteCollection',
        //期刊插入浏览量
        QKYD_BrowseRecords:'/qkyd/QKYD_BrowseRecords',
        //期刊下载 
        common_onlineDownload:'/qkyd/common_onlineDownload',
        //预览:
        common_onlineView:'/qkyd/common_onlineView',
        // 增加下载量
        insertDown:'/personCenter/insertDown',
        //下载列表
        getDownList:'/personCenter/getDownList',
        //删除下载
        deleteDown:'/personCenter/deleteDown',
        // 预览量
        insertView:'/personCenter/insertView',
        //预览列表
        getViewList:'//personCenter/getViewList',
        //删除预览
        deleteView:'/personCenter/deleteView',
        // 登录
        login:'/adminUserInfo/login',
        //修改密码
        updatePassword:'/adminUserInfo/initPwd',
        // 知识详情
        getIndex:'/yqtIndex/getIndex',
        // 获取提示词
        searchMention:'/knowledge/getCueWord',
        // 注册
        registUser:'/adminUserInfo/registUser',
        //统计下载、检索，浏览
        InsertLog:'/adminLog/InsertLog',
        // 通知公告
        showIndexPageSystemNotice:'/systemNotice/showIndexPageSystemNotice',
        // 更多
        querySystemNotice:'/systemNotice/querySystemNotice',
        // 详情页
        systemNoticeInfo:'/systemNotice/systemNoticeInfo',
        //通知公告
        getNoticeSort:'/admin/notice/getNoticeSort',
        // 新增收藏分类
        addSaveClassify:'/collectClassify/insertCollectClassify',
        // 修改收藏分类
        updateSaveClassify:'/collectClassify/updateCollectClassify',
        // 删除收藏分类
        deleteSaveClassify:'/collectClassify/deleteCollectClassify',
        // 获取所有收藏分类
        showSaveClassify:'/collectClassify/getCollectClassifyList',
        // 收藏数据
        // 添加收藏数据
        insertCollectData:'/CollectData/insertCollectData',
        // 删除收藏数据
        deleteCollectData:'/CollectData/deleteCollectData',
        //科研档案
        complexSearch_kyda:'/kyda/complexSearch_kyda',
        // 科技情报
        getClsTree:'/kjqb/getClsTree',
        // 情报科技数据列表
        complexSearch_qbkj:'/kjqb/complexSearch_qbkj',
        // 勘探技术等
        getCNKIResource_Sort:'/yqtIndex/getCNKIResource_Sort',
        //cnki
        getCNKIResource:'/yqtIndex/getCNKIResource',

        // 外文原刊列表
        complexSearch_wwsj:'/qkyd/complexSearchWWSJ',
        //期刊刊名
        getQKKM:'/qkyd/getQKKM',
        getFZWWDMETAID:'/qkyd/getFZWWDMETAID'
    };

    return config;
}));

