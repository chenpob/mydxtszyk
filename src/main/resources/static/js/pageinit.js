
function des(n, t, i, r, u) {
    var it = [16843776, 0, 65536, 16843780, 16842756, 66564, 4, 65536, 1024, 16843776, 16843780, 1024, 16778244, 16842756, 16777216, 4, 1028, 16778240, 16778240, 66560, 66560, 16842752, 16842752, 16778244, 65540, 16777220, 16777220, 65540, 0, 1028, 66564, 16777216, 65536, 16843780, 4, 16842752, 16843776, 16777216, 16777216, 1024, 16842756, 65536, 66560, 16777220, 1024, 4, 16778244, 66564, 16843780, 65540, 16842752, 16778244, 16777220, 1028, 66564, 16843776, 1028, 16778240, 16778240, 0, 65540, 66560, 0, 16842756],
    rt = [ - 2146402272, -2147450880, 32768, 1081376, 1048576, 32, -2146435040, -2147450848, -2147483616, -2146402272, -2146402304, -2147483648, -2147450880, 1048576, 32, -2146435040, 1081344, 1048608, -2147450848, 0, -2147483648, 32768, 1081376, -2146435072, 1048608, -2147483616, 0, 1081344, 32800, -2146402304, -2146435072, 32800, 0, 1081376, -2146435040, 1048576, -2147450848, -2146435072, -2146402304, 32768, -2146435072, -2147450880, 32, -2146402272, 1081376, 32, 32768, -2147483648, 32800, -2146402304, 1048576, -2147483616, 1048608, -2147450848, -2147483616, 1048608, 1081344, 0, -2147450880, 32800, -2147483648, -2146435040, -2146402272, 1081344],
    ut = [520, 134349312, 0, 134348808, 134218240, 0, 131592, 134218240, 131080, 134217736, 134217736, 131072, 134349320, 131080, 134348800, 520, 134217728, 8, 134349312, 512, 131584, 134348800, 134348808, 131592, 134218248, 131584, 131072, 134218248, 8, 134349320, 512, 134217728, 134349312, 134217728, 131080, 520, 131072, 134349312, 134218240, 0, 512, 131080, 134349320, 134218240, 134217736, 512, 0, 134348808, 134218248, 131072, 134217728, 134349320, 8, 131592, 131584, 134217736, 134348800, 134218248, 520, 134348800, 131592, 8, 134348808, 131584],
    ft = [8396801, 8321, 8321, 128, 8396928, 8388737, 8388609, 8193, 0, 8396800, 8396800, 8396929, 129, 0, 8388736, 8388609, 1, 8192, 8388608, 8396801, 128, 8388608, 8193, 8320, 8388737, 1, 8320, 8388736, 8192, 8396928, 8396929, 129, 8388736, 8388609, 8396800, 8396929, 129, 0, 0, 8396800, 8320, 8388736, 8388737, 1, 8396801, 8321, 8321, 128, 8396929, 129, 1, 8192, 8388609, 8193, 8396928, 8388737, 8193, 8320, 8388608, 8396801, 128, 8388608, 8192, 8396928],
    et = [256, 34078976, 34078720, 1107296512, 524288, 256, 1073741824, 34078720, 1074266368, 524288, 33554688, 1074266368, 1107296512, 1107820544, 524544, 1073741824, 33554432, 1074266112, 1074266112, 0, 1073742080, 1107820800, 1107820800, 33554688, 1107820544, 1073742080, 0, 1107296256, 34078976, 33554432, 1107296256, 524544, 524288, 1107296512, 256, 33554432, 1073741824, 34078720, 1107296512, 1074266368, 33554688, 1073741824, 1107820544, 34078976, 1074266368, 256, 33554432, 1107820544, 1107820800, 524544, 1107296256, 1107820800, 34078720, 0, 1074266112, 1107296256, 524544, 33554688, 1073742080, 524288, 0, 1074266112, 34078976, 1073742080],
    ot = [536870928, 541065216, 16384, 541081616, 541065216, 16, 541081616, 4194304, 536887296, 4210704, 4194304, 536870928, 4194320, 536887296, 536870912, 16400, 0, 4194320, 536887312, 16384, 4210688, 536887312, 16, 541065232, 541065232, 0, 4210704, 541081600, 16400, 4210688, 541081600, 536870912, 536887296, 16, 541065232, 4210688, 541081616, 4194304, 16400, 536870928, 4194304, 536887296, 536870912, 16400, 536870928, 541081616, 4210688, 541065216, 4210704, 541081600, 0, 541065232, 16, 16384, 541065216, 4210704, 16384, 4194320, 536887312, 0, 541081600, 536870912, 4194320, 536887312],
    st = [2097152, 69206018, 67110914, 0, 2048, 67110914, 2099202, 69208064, 69208066, 2097152, 0, 67108866, 2, 67108864, 69206018, 2050, 67110912, 2099202, 2097154, 67110912, 67108866, 69206016, 69208064, 2097154, 69206016, 2048, 2050, 69208066, 2099200, 2, 67108864, 2099200, 67108864, 2099200, 2097152, 67110914, 67110914, 69206018, 69206018, 2, 2097154, 67108864, 67110912, 2097152, 69208064, 2050, 2099202, 69208064, 2050, 67108866, 69208066, 69206016, 2099200, 0, 2, 69208066, 0, 2099202, 69206016, 2048, 67108866, 67110912, 2048, 2097154],
    ht = [268439616, 4096, 262144, 268701760, 268435456, 268439616, 64, 268435456, 262208, 268697600, 268701760, 266240, 268701696, 266304, 4096, 64, 268697600, 268435520, 268439552, 4160, 266240, 262208, 268697664, 268701696, 4160, 0, 0, 268697664, 268435520, 268439552, 266304, 262144, 266304, 262144, 268701696, 4096, 64, 268697664, 4096, 266304, 268439552, 64, 268435520, 268697600, 268697664, 268435456, 262144, 268439616, 0, 268701760, 262208, 268435520, 268697600, 268439552, 268439616, 0, 268701760, 266240, 266240, 4160, 4160, 262208, 268435456, 268701696],
    w = des_createKeys(n),
    s = 0,
    c,
    h,
    o,
    l,
    a,
    e,
    f,
    p,
    v,
    k,
    y,
    d,
    g,
    nt,
    ct = t.length,
    b = 0,
    tt = w.length == 32 ? 3 : 9;
    for (p = tt == 3 ? i ? [0, 32, 2] : [30, -2, -2] : i ? [0, 32, 2, 62, 30, -2, 64, 96, 2] : [94, 62, -2, 32, 64, 2, 30, -2, -2], t += '\0\0\0\0\0\0\0\0', result = "", tempresult = "", r == 1 && (v = u.charCodeAt(s++) << 24 | u.charCodeAt(s++) << 16 | u.charCodeAt(s++) << 8 | u.charCodeAt(s++), y = u.charCodeAt(s++) << 24 | u.charCodeAt(s++) << 16 | u.charCodeAt(s++) << 8 | u.charCodeAt(s++), s = 0); s < ct;) {
        for (i ? (e = t.charCodeAt(s++) << 16 | t.charCodeAt(s++), f = t.charCodeAt(s++) << 16 | t.charCodeAt(s++)) : (e = t.charCodeAt(s++) << 24 | t.charCodeAt(s++) << 16 | t.charCodeAt(s++) << 8 | t.charCodeAt(s++), f = t.charCodeAt(s++) << 24 | t.charCodeAt(s++) << 16 | t.charCodeAt(s++) << 8 | t.charCodeAt(s++)), r == 1 && (i ? (e ^= v, f ^= y) : (k = v, d = y, v = e, y = f)), o = (e >>> 4 ^ f) & 252645135, f ^= o, e ^= o << 4, o = (e >>> 16 ^ f) & 65535, f ^= o, e ^= o << 16, o = (f >>> 2 ^ e) & 858993459, e ^= o, f ^= o << 2, o = (f >>> 8 ^ e) & 16711935, e ^= o, f ^= o << 8, o = (e >>> 1 ^ f) & 1431655765, f ^= o, e ^= o << 1, e = e << 1 | e >>> 31, f = f << 1 | f >>> 31, h = 0; h < tt; h += 3) {
            for (g = p[h + 1], nt = p[h + 2], c = p[h]; c != g; c += nt) l = f ^ w[c],
            a = (f >>> 4 | f << 28) ^ w[c + 1],
            o = e,
            e = f,
            f = o ^ (rt[l >>> 24 & 63] | ft[l >>> 16 & 63] | ot[l >>> 8 & 63] | ht[l & 63] | it[a >>> 24 & 63] | ut[a >>> 16 & 63] | et[a >>> 8 & 63] | st[a & 63]);
            o = e;
            e = f;
            f = o
        }
        e = e >>> 1 | e << 31;
        f = f >>> 1 | f << 31;
        o = (e >>> 1 ^ f) & 1431655765;
        f ^= o;
        e ^= o << 1;
        o = (f >>> 8 ^ e) & 16711935;
        e ^= o;
        f ^= o << 8;
        o = (f >>> 2 ^ e) & 858993459;
        e ^= o;
        f ^= o << 2;
        o = (e >>> 16 ^ f) & 65535;
        f ^= o;
        e ^= o << 16;
        o = (e >>> 4 ^ f) & 252645135;
        f ^= o;
        e ^= o << 4;
        r == 1 && (i ? (v = e, y = f) : (e ^= k, f ^= d));
        tempresult += i ? String.fromCharCode(e >>> 24, e >>> 16 & 255, e >>> 8 & 255, e & 255, f >>> 24, f >>> 16 & 255, f >>> 8 & 255, f & 255) : String.fromCharCode(e >>> 16 & 65535, e & 65535, f >>> 16 & 65535, f & 65535);
        b += i ? 16 : 8;
        b == 512 && (result += tempresult, tempresult = "", b = 0)
    }
    return result + tempresult
}
function des_createKeys(n) {
    var o;
    pc2bytes0 = [0, 4, 536870912, 536870916, 65536, 65540, 536936448, 536936452, 512, 516, 536871424, 536871428, 66048, 66052, 536936960, 536936964];
    pc2bytes1 = [0, 1, 1048576, 1048577, 67108864, 67108865, 68157440, 68157441, 256, 257, 1048832, 1048833, 67109120, 67109121, 68157696, 68157697];
    pc2bytes2 = [0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272, 0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272];
    pc2bytes3 = [0, 2097152, 134217728, 136314880, 8192, 2105344, 134225920, 136323072, 131072, 2228224, 134348800, 136445952, 139264, 2236416, 134356992, 136454144];
    pc2bytes4 = [0, 262144, 16, 262160, 0, 262144, 16, 262160, 4096, 266240, 4112, 266256, 4096, 266240, 4112, 266256];
    pc2bytes5 = [0, 1024, 32, 1056, 0, 1024, 32, 1056, 33554432, 33555456, 33554464, 33555488, 33554432, 33555456, 33554464, 33555488];
    pc2bytes6 = [0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746, 0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746];
    pc2bytes7 = [0, 65536, 2048, 67584, 536870912, 536936448, 536872960, 536938496, 131072, 196608, 133120, 198656, 537001984, 537067520, 537004032, 537069568];
    pc2bytes8 = [0, 262144, 0, 262144, 2, 262146, 2, 262146, 33554432, 33816576, 33554432, 33816576, 33554434, 33816578, 33554434, 33816578];
    pc2bytes9 = [0, 268435456, 8, 268435464, 0, 268435456, 8, 268435464, 1024, 268436480, 1032, 268436488, 1024, 268436480, 1032, 268436488];
    pc2bytes10 = [0, 32, 0, 32, 1048576, 1048608, 1048576, 1048608, 8192, 8224, 8192, 8224, 1056768, 1056800, 1056768, 1056800];
    pc2bytes11 = [0, 16777216, 512, 16777728, 2097152, 18874368, 2097664, 18874880, 67108864, 83886080, 67109376, 83886592, 69206016, 85983232, 69206528, 85983744];
    pc2bytes12 = [0, 4096, 134217728, 134221824, 524288, 528384, 134742016, 134746112, 16, 4112, 134217744, 134221840, 524304, 528400, 134742032, 134746128];
    pc2bytes13 = [0, 4, 256, 260, 0, 4, 256, 260, 1, 5, 257, 261, 1, 5, 257, 261];
    var s = n.length >= 24 ? 3 : 1,
    u = new Array(32 * s),
    h = [0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0],
    f,
    e,
    r = 0,
    c = 0,
    t;
    for (o = 0; o < s; o++) for (left = n.charCodeAt(r++) << 24 | n.charCodeAt(r++) << 16 | n.charCodeAt(r++) << 8 | n.charCodeAt(r++), right = n.charCodeAt(r++) << 24 | n.charCodeAt(r++) << 16 | n.charCodeAt(r++) << 8 | n.charCodeAt(r++), t = (left >>> 4 ^ right) & 252645135, right ^= t, left ^= t << 4, t = (right >>> -16 ^ left) & 65535, left ^= t, right ^= t << -16, t = (left >>> 2 ^ right) & 858993459, right ^= t, left ^= t << 2, t = (right >>> -16 ^ left) & 65535, left ^= t, right ^= t << -16, t = (left >>> 1 ^ right) & 1431655765, right ^= t, left ^= t << 1, t = (right >>> 8 ^ left) & 16711935, left ^= t, right ^= t << 8, t = (left >>> 1 ^ right) & 1431655765, right ^= t, left ^= t << 1, t = left << 8 | right >>> 20 & 240, left = right << 24 | right << 8 & 16711680 | right >>> 8 & 65280 | right >>> 24 & 240, right = t, i = 0; i < h.length; i++) h[i] ? (left = left << 2 | left >>> 26, right = right << 2 | right >>> 26) : (left = left << 1 | left >>> 27, right = right << 1 | right >>> 27),
    left &= -15,
    right &= -15,
    f = pc2bytes0[left >>> 28] | pc2bytes1[left >>> 24 & 15] | pc2bytes2[left >>> 20 & 15] | pc2bytes3[left >>> 16 & 15] | pc2bytes4[left >>> 12 & 15] | pc2bytes5[left >>> 8 & 15] | pc2bytes6[left >>> 4 & 15],
    e = pc2bytes7[right >>> 28] | pc2bytes8[right >>> 24 & 15] | pc2bytes9[right >>> 20 & 15] | pc2bytes10[right >>> 16 & 15] | pc2bytes11[right >>> 12 & 15] | pc2bytes12[right >>> 8 & 15] | pc2bytes13[right >>> 4 & 15],
    t = (e >>> 16 ^ f) & 65535,
    u[c++] = f ^ t,
    u[c++] = e ^ t << 16;
    return u
}
function stringToHex(n) {
    for (var i = "",
    r = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"], t = 0; t < n.length; t++) i += r[n.charCodeAt(t) >> 4] + r[n.charCodeAt(t) & 15];
    return i
}
function HexTostring(n) {
    for (var i = "",
    r, t = 0; t < n.length; t += 2) r = parseInt(n.substring(t, t + 2), 16),
    i += String.fromCharCode(r);
    return i
}
function encMe(n, t) {
    return stringToHex(des(t, n, 1, 0))
}
function uncMe(n, t) {
    return des(t, HexTostring(n), 0, 0)
} (function(n) {
    var t = function(t, i, r) {
        this.opts = i;
        this.ele = n(t);
        this.settings = n.extend({},
        n.fn.tabModule.defaults, this.opts, this.getSettings());
        this.tabhead = this.ele.children("[data-tbh]");
        this.olis = this.tabhead.find("[data-th]");
        this.tabBds = this.ele.children("[data-tbd]").length > 0 ? this.ele.children("[data-tbd]") : this.ele.find("[data-tbd]");
        this.fn = r;
        this.oindex = 0;
        this.oBtnl = null;
        this.oBtnr = null;
        this.listbox = null;
        this.nBtnClickNum = 0;
        this.runNum = 0;
        this.lastNum = 0;
        try {
            this.init()
        } catch(u) {
            return this.ele
        }
        return this.ele
    };
    t.prototype = {
        getSettings: function() {
            var t = this.ele.attr && this.ele.attr("data-setting") || "";
            if (t && t != "") return n.parseJSON(t)
        },
        init: function() {
            var r = this.tabhead.find("[data-th]"),
            u = r.size() > 1 ? !0 : !1,
            t = this.settings,
            i = n.trim(t.setHeight) == "" ? !1 : parseFloat(t.setHeight),
            f = n.trim(t.thAvgType) == "auto" ? !1 : !0,
            e = t.effectType;
            i != !1 && n.isNumeric(i) && this.setTabWrapHeight(t.setHeight);
            switch (e) {
            case 1:
                this.effectType1();
                break;
            case 2:
                this.effectType2();
                break;
            case 3:
                this.effectType3();
                break;
            case 4:
                this.effectType4()
            }
            if (f == !0 && this.settings.effectType == 0 && this.setThWidthOrHeight(), u) this.bindEvent();
            else return
        },
        bindEvent: function() {
            var t = this;
            t.tabhead.on(t.settings.eventType, "[data-th]",
            function(i) {
                var u, r;
                i.target.nodeName == i.currentTarget.nodeName && typeof i.target == "object" && (i.stopPropagation(), i.preventDefault(), u = t.olis.index(this), (n(this).attr("class") == undefined || n(this).attr("class").indexOf(t.settings.curClass) == -1) && (r = t.tabBds.eq(u), r.size() > 0 && (r.css("display", "block").addClass(" " + t.settings.curClass + " ").siblings("[data-tbd]").removeClass(t.settings.curClass).css("display", "none"), t.olis.removeClass(t.settings.curClass), n(this).addClass(" " + t.settings.curClass + " "), t.oindex = u, t.settings.effectType === 1 && t.tabhead.find(".mod-atscroll-txt").eq(0).text(n(this).text()), t.loadData(r)), n.isFunction(t.fn) && t.fn.call(this)));
                return
            })
        },
        loadData: function(t) {
            var i;
            t.attr("data-module") != null && (i = t);
            t.find("[data-module]").size() == 1 && (i = t.find("[data-module]"));
            i != null && n.trim(i.html()) == "" && n.LoadModuleItem(i.attr("data-module"), i.attr("data-templet"), n("body").data("RequestPvuCode"), i);
            return
        },
        formatString: function(n, t) {
            return n.replace(/\{#(\w+)#\}/g,
            function(n, i) {
                return t[i] != undefined ? t[i] : ""
            })
        },
        changeTabHeadDom: function(n) {
            for (var i = this.settings,
            s = i.lisTpl,
            h = i.curClass,
            u = this.olis.eq(0).text(), f = "", r = {
                curClass: "",
                sname: u
            },
            e = {
                curTxt: u,
                lis: "",
                headClass: this.tabhead.attr("class")
            },
            c = this.olis.size(), o = "", t = 0; t < c; t++) t == this.oindex && (r.curClass = h) || (r.curClass = ""),
            r.sname = this.olis.eq(t).text(),
            f += this.formatString(s, r);
            e.lis = f;
            o = this.formatString(n, e);
            this.tabhead.after(o);
            this.tabhead.remove();
            this.tabhead = this.ele.children("[data-tbh]");
            this.olis = this.tabhead.find("[data-th]");
            this.oBtnl = this.ele.find(i.btnlClass);
            this.oBtnr = this.ele.find(i.btnrClass)
        },
        bindEvent4effct1: function() {
            var t = this,
            u = 0,
            f = this.olis.eq(0).outerWidth(!0),
            r = this.olis.size(),
            i = this.settings;
            if (i.changeNum = i.changeNum > i.showNum ? i.showNum: i.changeNum, this.listbox = this.ele.find(i.listboxClass).eq(0), r <= i.showNum) this.oBtnl.css("display", "none"),
            this.oBtnr.css("display", "none");
            else {
                this.olis.each(function() {
                    var t = n(this);
                    return u += parseFloat(t.outerWidth(!0)),
                    t.index() + 1 == i.showNum ? !1 : void 0
                });
                this.listbox.parent().width(u);
                this.oBtnl.on("click.pre",
                function() {
                    var o = t.oindex - i.changeNum,
                    u = t.oindex == 0 ? r - 1 : t.oindex - i.changeNum,
                    e = t.tabBds.eq(u).size(),
                    n;
                    if (e > 0) {
                        for (n = 0; n < i.changeNum; n++) t.listbox.find("li:last").prependTo(t.listbox);
                        t.listbox.css({
                            "margin-left": -f * i.changeNum
                        });
                        t.listbox.animate({
                            marginLeft: 0
                        },
                        "normal");
                        t.olis.eq(u).trigger(i.eventType)
                    }
                });
                this.oBtnr.on("click.next",
                function() {
                    var n = t.oindex != r - 1 ? t.oindex + i.changeNum > r - 1 ? r - 1 : t.oindex + i.changeNum: 0,
                    o = t.tabBds.eq(n).size(),
                    u = n - t.oindex,
                    e = n == r - 1 && u < i.changeNum ? u: i.changeNum; ! t.listbox.is(":animated") && o > 0 && (t.listbox.animate({
                        marginLeft: -f * e
                    },
                    "normal",
                    function() {
                        for (var n = 0; n < e; n++) t.listbox.find("li").eq(0).appendTo(t.listbox),
                        t.listbox.css({
                            "margin-left": 0
                        })
                    }), t.olis.eq(n).trigger(i.eventType))
                })
            }
        },
        bindEvent4effct2: function() {
            var t = this,
            f = t.settings,
            i = f.showNum,
            e = this.olis.size(),
            o = this.tabhead.find(".mod-atscroll-main").eq(0),
            s = o.width(),
            h = 0,
            c = 0,
            l = 0,
            a = null,
            v = function(n, t, i, r) {
                for (var e = r >= t ? t: r + 1, f = 0, u = i; u < e; u++) f += n.eq(u).outerWidth(!0);
                return f
            },
            r = f.unClickClass,
            u = o.find(".mod-atscroll-tab").eq(0);
            t.runNum = Math.ceil(e / i) - 1;
            s == 0 && this.tabhead.parent().show(0,
            function() {
                s = o.width()
            }).hide(0);
            this.olis.each(function() {
                var t = n(this);
                return h += t.outerWidth(!0) - t.width(),
                c += parseFloat(t.css("margin-right")) + parseFloat(t.css("margin-left")),
                t.index() + 1 == i ? !1 : void 0
            });
            l = Math.floor(parseFloat((s - h) / i) * 1e5) / 1e5;
            a = c / (i - 1);
            this.olis.each(function() {
                var t = n(this),
                i = t.index();
                t.css({
                    float: "left",
                    "text-overflow": "ellipsis",
                    overflow: "hidden",
                    "white-space": "nowrap",
                    "margin-left": 0,
                    "margin-right": a + "px",
                    width: l + "px"
                })
            });
            u.width(t.olis.eq(0).outerWidth(!0) * e);
            t.oBtnl.addClass(" " + r + " ");
            t.oBtnr.on("click.btnr",
            function() {
                var s = n(this),
                o = t.nBtnClickNum,
                h = i * o < 0 ? 0 : i * o,
                c = i * (o + 1) - 1,
                l = v(t.olis, e, h, c),
                a = parseFloat(u.css("marginLeft")),
                y = a - l,
                p = s.attr("class").indexOf(r) == -1 ? !0 : !1,
                w = f.speed; ! u.is(":animated") && p && u.animate({
                    marginLeft: y + "px"
                },
                w,
                function() {
                    var n = o + 1;
                    t.nBtnClickNum == 0 && t.oBtnl.removeClass(r);
                    n < t.runNum ? t.nBtnClickNum = n: s.addClass(" " + r + " ")
                })
            });
            t.oBtnl.on("click.btnl",
            function() {
                var s = n(this),
                o = t.nBtnClickNum,
                h = i * o < 0 ? 0 : i * o,
                c = i * (o + 1) - 1,
                l = v(t.olis, e, h, c),
                a = parseFloat(u.css("marginLeft")),
                y = a + l,
                p = s.attr("class").indexOf(r) == -1 ? !0 : !1,
                w = f.speed; ! u.is(":animated") && p && u.animate({
                    marginLeft: y + "px"
                },
                w,
                function() {
                    var n = o - 1;
                    t.nBtnClickNum <= t.runNum - 1 && t.oBtnr.removeClass(r);
                    n >= 0 ? t.nBtnClickNum = n: s.addClass(" " + r + " ")
                })
            })
        },
        bindEvent4effct3: function(t, i, r, u) {
            var e = this,
            v = e.settings,
            f = this.tabhead,
            o = f.find("li").eq(0).outerHeight(!0),
            c = f.find("li").eq(0).innerHeight(),
            l = f.find(".mod-atscroll-main").eq(0),
            s = f.find("ul").eq(0),
            a = 0,
            h;
            for (f.data("curSetp", 0), h = 0; h < t; h++) a += h + 1 == t ? c: o;
            l.height(a);
            s.css({
                overflow: "hidden",
                height: "auto"
            }).find("li").css("float", "none");
            f.width(i);
            l.width(i);
            v.thAvgType == "vertical" && (e.settings.rowOrcol = 1, e.settings.thAvgType = "vertical1", e.setThWidthOrHeight(), o = f.find("li").eq(0).outerHeight(!0), c = f.find("li").eq(0).innerHeight());
            e.oBtnl.on("click.top",
            function() {
                var i = f.data("curSetp"),
                h = o * t,
                c = n(this),
                a = c.attr("class").indexOf("mod-atscroll-btnt-disabled") == -1 ? !0 : !1,
                l = h * (i + 1);
                a && !s.is(":animated") && (u != 0 && i + 1 == r && (l = i * h + u * o), s.animate({
                    marginTop: -l + "px"
                },
                500,
                function() {
                    f.data("curSetp", i + 1);
                    i + 1 == r && c.addClass(" mod-atscroll-btnt-disabled ");
                    i + 1 > 0 && e.oBtnr.removeClass("mod-atscroll-btnb-disabled")
                }))
            });
            e.oBtnr.on("click.bottom",
            function() {
                var i = f.data("curSetp"),
                u = o * t,
                h = u * (i - 1),
                r = n(this),
                c = r.attr("class").indexOf("mod-atscroll-btnb-disabled") == -1 ? !0 : !1;
                c && !s.is(":animated") && s.animate({
                    marginTop: -h + "px"
                },
                500,
                function() {
                    f.data("curSetp", i - 1);
                    i - 1 == 0 && r.addClass(" mod-atscroll-btnb-disabled ");
                    i - 1 >= 0 && e.oBtnl.removeClass("mod-atscroll-btnt-disabled")
                })
            })
        },
        bindEvent4effct4: function(t, i, r) {
            var u = this,
            e = 0,
            f;
            for (u.oBtnl = this.ele.find(r.btnlClass), u.oBtnr = this.ele.find(r.btnrClass), f = 0; f < t; f++) e += u.tabhead.find("li").eq(f).outerWidth(!0);
            u.oBtnr.on("click.next",
            function() {
                var i = n(this);
                if (u.tabhead.is(":animated")) return ! 1;
                u.tabhead.animate({
                    marginLeft: -e + "px"
                },
                "normal",
                function() {
                    for (var n = 0; n < t; n++) u.tabhead.find("li").eq(0).appendTo(u.tabhead),
                    u.tabhead.css("marginLeft", 0)
                })
            });
            u.oBtnl.on("click.prev",
            function() {
                var r = n(this),
                i;
                if (u.tabhead.is(":animated")) return ! 1;
                for (i = 0; i < t; i++) u.tabhead.find("li:last").prependTo(u.tabhead);
                u.tabhead.css("marginLeft", -e + "px");
                u.tabhead.animate({
                    marginLeft: 0
                },
                "normal")
            })
        },
        setThWidthOrHeight: function() {
            var i = this,
            r = i.settings,
            u = r.thAvgType,
            t = r.rowOrcol,
            f = {
                horizontal: function(i) {
                    var u = i.tabhead,
                    f = u.width(),
                    e = i.olis.size(),
                    o = 0,
                    h = 0,
                    s = 0,
                    c = 0,
                    l = 0,
                    r = Math.ceil(e / t);
                    t = Math.ceil(e / r);
                    f == 0 && u.parent().show(0,
                    function() {
                        f = u.width()
                    }).hide(0);
                    i.olis.each(function() {
                        var t = n(this);
                        return o += t.outerWidth(!0) - t.width(),
                        s += parseFloat(t.css("margin-right")) + parseFloat(t.css("margin-left")),
                        t.index() + 1 == r ? !1 : void 0
                    });
                    c = r == 1 ? 0 : s / (r - 1);
                    h = r == 1 ? s + f - o: f - o;
                    l = Math.floor(parseFloat(h / r) * 1e5) / 1e5;
                    u.addClass(" clearfix ");
                    i.olis.each(function() {
                        var t = n(this),
                        i = t.index();
                        t.css({
                            float: "left",
                            "text-overflow": "ellipsis",
                            overflow: "hidden",
                            "white-space": "nowrap",
                            "margin-left": 0,
                            "margin-right": c + "px",
                            width: l + "px"
                        }); (i + 1) % r == 0 && t.css("margin-right", 0);
                        e - 1 == i && t.addClass(" mod-tabhead-last ")
                    })
                },
                vertical: function(i) {
                    var u = i.tabhead,
                    f = 0,
                    s = i.olis.size(),
                    e = 0,
                    a = 0,
                    v = 0,
                    y = 0,
                    h = 0,
                    p = 0,
                    o = 0,
                    r = Math.ceil(s / t),
                    c = 0,
                    l = 0;
                    t = Math.ceil(s / r);
                    t == 1 && u.parent().addClass(" mod-mixtab-onecol ") || u.parent().addClass(" mod-mixtab-morecol ");
                    f = u.height();
                    l = u.width();
                    l == 0 && u.parent().show(0,
                    function() {
                        f = u.width()
                    }).hide(0);
                    f == 0 && u.parent().show(0,
                    function() {
                        f = u.height()
                    }).hide(0);
                    i.olis.each(function() {
                        var i = n(this);
                        i.index() + 1 <= r && (e += i.outerHeight(!0) - i.height(), h += parseFloat(i.css("margin-bottom")) + parseFloat(i.css("margin-top")));
                        i.index() + 1 <= t && (a += i.outerWidth(!0) - i.width());
                        i.index() + 1 == r && (e -= parseFloat(i.css("border-bottom-width")))
                    });
                    v = r == 1 ? f - e + h: f - e;
                    p = r == 1 ? 0 : h / (r - 1);
                    y = l - a;
                    c = Math.floor(parseFloat(y / t) * 1e5) / 1e5;
                    o = parseFloat(v / r);
                    o >= 12 && (u.addClass(" clearfix "), i.olis.each(function() {
                        var i = n(this),
                        u = i.index();
                        i.css({
                            "text-overflow": "ellipsis",
                            overflow: "hidden",
                            "white-space": "nowrap",
                            "margin-top": 0,
                            "margin-bottom": p + "px",
                            height: o + "px",
                            "line-height": o + "px"
                        });
                        t > 1 ? (i.css({
                            float: "left",
                            width: c + "px",
                            "-webkit-writing-mode": "vertical-lr",
                            "writing-mode": "tb-lr",
                            "writing-mode": "vertical-lr",
                            "text-align": "center",
                            "line-height": c + "px"
                        }), i.index() % t == 0 && i.css("margin-left", 0), u >= t * (r - 1) && i.css({
                            "margin-bottom": 0,
                            "border-bottom-width": 0
                        })) : (u + 1) % r == 0 && i.css({
                            "margin-bottom": 0,
                            "border-bottom-width": 0
                        });
                        s - 1 == u && r > 1 && i.addClass(" mod-tabhead-last ").css({
                            "margin-bottom": 0
                        })
                    }))
                },
                vertical1: function(i) {
                    var r = i.tabhead,
                    f = 0,
                    o = i.olis.size(),
                    s = 0,
                    c = 0,
                    l = 0,
                    a = 0,
                    v = 0,
                    e = 0,
                    u = o <= i.settings.showNum ? o: i.settings.showNum,
                    y = 0,
                    h = 0;
                    t = 1;
                    r.parent().addClass(" mod-mixtab-onecol ");
                    f = r.height();
                    h = r.width();
                    h == 0 && r.parent().show(0,
                    function() {
                        f = r.width()
                    }).hide(0);
                    f == 0 && r.parent().show(0,
                    function() {
                        f = r.height()
                    }).hide(0);
                    i.olis.each(function() {
                        var t = n(this);
                        t.index() + 1 <= u && (s += t.outerHeight(!0) - t.height(), a += parseFloat(t.css("margin-bottom")) + parseFloat(t.css("margin-top")));
                        t.index() + 1 == u && (s -= parseFloat(t.css("border-bottom-width")))
                    });
                    c = f - s;
                    v = u == 1 ? 0 : a / (u - 1);
                    l = +h;
                    y = Math.floor(parseFloat(l / t) * 1e5) / 1e5;
                    e = parseFloat(c / u);
                    e >= 12 && (r.addClass(" clearfix "), i.olis.each(function() {
                        var t = n(this),
                        i = t.index();
                        t.css({
                            "text-overflow": "ellipsis",
                            overflow: "hidden",
                            "white-space": "nowrap",
                            "margin-top": 0,
                            "margin-bottom": v + "px",
                            height: e + "px",
                            "line-height": e + "px"
                        });
                        o - 1 == i && u > 1 && t.addClass(" mod-tabhead-last ").css({
                            "margin-bottom": 0,
                            "border-bottom-width": 0
                        })
                    }))
                },
                vertical2: function(i) {
                    var r = i.tabhead,
                    u = 0,
                    s = i.olis.size(),
                    h = 0,
                    c = 0,
                    l = 0,
                    a = 0,
                    f = 0,
                    v = 1,
                    e = 0,
                    o = 0,
                    y = s > t ? !1 : !0;
                    r.parent().addClass(" mod-mixtab-morecol ");
                    t == 1 && (t = 2);
                    u = i.tabhead.height();
                    o = i.tabhead.width();
                    o == 0 && i.tabhead.parent().show(0,
                    function() {
                        u = r.width()
                    }).hide(0);
                    u == 0 && r.parent().show(0,
                    function() {
                        u = r.height()
                    }).hide(0);
                    i.olis.css("border-bottom", "none");
                    i.olis.each(function() {
                        var i = n(this);
                        i.index() + 1 <= v && (h += i.innerHeight() - i.height());
                        i.index() + 1 <= t && (c += i.outerWidth(!0) - i.width())
                    });
                    l = u - h;
                    a = o - c;
                    e = Math.floor(parseFloat(a / t) * 1e5) / 1e5;
                    f = parseFloat(l / v);
                    f >= 12 && e > 19 && (r.addClass(" clearfix "), i.olis.each(function() {
                        var i = n(this),
                        r = i.index();
                        i.css({
                            "text-overflow": "ellipsis",
                            overflow: "hidden",
                            "white-space": "nowrap",
                            "margin-top": 0,
                            height: f + "px",
                            "line-height": f + "px"
                        });
                        i.css({
                            float: "left",
                            width: e + "px",
                            "-webkit-writing-mode": "vertical-lr",
                            "writing-mode": "tb-lr",
                            "writing-mode": "vertical-lr",
                            "text-align": "center",
                            "line-height": e + "px"
                        });
                        y && r >= t * (Math.ceil(s / t) - 1) && i.css({
                            "margin-bottom": 0,
                            "border-bottom-width": 0
                        })
                    }))
                }
            };
            i.settings.thAvgType != "auto" && f[u] && f[u](i)
        },
        setTabWrapHeight: function(n) {
            var t = this.ele.find(".mod-tab-wrap");
            t.size() == 0 && this.ele.attr("class").indexOf("mod-tab-wrap") > -1 && (t = this.ele);
            t.length && t.height(n)
        },
        effectType1: function() {
            var n = this.settings;
            this.changeTabHeadDom(n.effectType1Tpl);
            this.bindEvent4effct1()
        },
        effectType2: function() {
            var i = this.settings,
            t = parseInt(i.showNum),
            r = this.olis.size();
            n.isNumeric(t) && t > 0 && (r <= t ? this.settings.effectType = 0 : (this.changeTabHeadDom(i.effectType2Tpl), this.bindEvent4effct2()))
        },
        effectType3: function() {
            var i = this.settings,
            t = parseInt(i.showNum),
            r = this.olis.size(),
            f = r % t,
            u = 0,
            e = this.tabhead.outerWidth();
            n.isNumeric(t) && t > 0 && (r <= t ? (this.settings.effectType = 0, i.thAvgType == "vertical" && (this.settings.rowOrcol = 1, this.settings.thAvgType = "vertical1", this.setThWidthOrHeight())) : (u = Math.ceil(r / t) - 1, this.settings = n.extend(i, {
                btnlClass: ".mod-atscroll-btnt",
                btnrClass: ".mod-atscroll-btnb"
            }), this.changeTabHeadDom(i.effectType3Tpl), this.bindEvent4effct3(t, e, u, f)))
        },
        effectType4: function() {
            var o = this.settings,
            s = this.olis.size(),
            i = 0,
            f = 0,
            t = 0,
            r = 0,
            u = 0,
            e = 0;
            this.tabhead.parent().addClass(" mod-mixtab-morecol ");
            this.settings.thAvgType = "vertical2";
            this.setThWidthOrHeight();
            i = this.tabhead.width();
            f = this.olis.eq(0).outerWidth(!0);
            t = Math.floor(i / f) - 1;
            n.isNumeric(t) && t > 0 && (s <= t + 1 ? this.settings.effectType = 0 : (this.tabhead.css("overflow", "hidden").wrap('<div class="mod-atscroll-wrap" ><\/div>'), this.ele.append('<div class="mod-atscroll-btnbox1"><span class="mod-atscroll-btnl ">左按钮<\/span><span class="mod-atscroll-btnr ">右按钮<\/span> <\/div>'), this.settings = n.extend(o, {
                btnrClass: ".mod-atscroll-btnr",
                btnlClass: ".mod-atscroll-btnl"
            }), this.bindEvent4effct4(t, i, this.settings)));
            r = this.tabhead.height();
            u = this.tabhead.find("li").eq(0).outerHeight();
            r < u && (e = r - (u - this.tabhead.find("li").eq(0).height()), this.tabhead.find("li").height(e))
        }
    };
    n.fn.tabModule = function(i, r) {
        return this.each(function() {
            var u = n(this),
            f;
            if (u.data("tabModule")) return u.data("tabModule");
            f = new t(this, i, r);
            u.data("tabModule", f)
        })
    };
    n.fn.tabModule.defaults = {
        curClass: "cur",
        eventType: "click",
        effectType: 0,
        setTabscroll: !1,
        effectType1Tpl: '<dl  class="mod-atscroll-head clearfix" data-tbh="" > <dt class="mod-atscroll-txt">{#curTxt#}<\/dt> <dd class="mod-atscroll-btnr ">右侧按钮<\/dd> <dd class="mod-atscroll-main"> <ul class="mod-atscroll-tab clearfix "> {#lis#} <\/ul> <\/dd> <dd class="mod-atscroll-btnl">左侧按钮<\/dd> <\/dl>',
        effectType2Tpl: '<div  class="mod-atscroll-head2 clearfix" data-tbh="" > <div class="mod-atscroll-main"> <ul class="mod-atscroll-tab clearfix "> {#lis#} <\/ul> <\/div> <div class="mod-atscroll-btnbox"> <span class="mod-atscroll-btnr ">右侧按钮<\/span> <span class="mod-atscroll-btnl">左侧按钮<\/span> <\/div> <\/div>',
        effectType3Tpl: '<div  class="mod-atscroll-head3 clearfix" data-tbh="" > <div class="mod-atscroll-main" style="position:relative;"> <ul class="mod-atscroll-tab  {#headClass#}  clearfix "> {#lis#} <\/ul> <\/div> <div class="mod-atscroll-btnbox"> <span class="mod-atscroll-btnt ">上按钮<\/span> <span class="mod-atscroll-btnb mod-atscroll-btnb-disabled">下按钮<\/span> <\/div> <\/div>',
        lisTpl: '<li class="{#curClass#}"  title="{#sname#}" data-th="">{#sname#}<\/li>',
        btnlClass: ".mod-atscroll-btnl",
        btnrClass: ".mod-atscroll-btnr",
        listboxClass: ".mod-atscroll-tab",
        unClickClass: "mod-atscroll-btn-unClick",
        speed: 500,
        showNum: 5,
        changeNum: 1,
        thAvgType: "auto",
        rowOrcol: 1,
        setHeight: ""
    }
})(jQuery),
function(n) {
    function t(n) {
        n.controlbox.size() > 0 && n.controlbox.find("li").eq(n.curNum).addClass(" active ").siblings("li").removeClass("active")
    }
    var r = {
        scrollNext: function(n, i, u, f) {
            var e = {
                leftFn: function(n, i, r, u) {
                    var l, e, s, a, y, h, p;
                    if (n.itembox.is(":animated")) n.timer && clearInterval(n.timer);
                    else {
                        var o = n.items.size() % (n.fsNum * n.row),
                        c = n.direct == "left" ? n.fsNum: n.row,
                        f = n.direct == "left" ? "left": "marginTop",
                        v = parseFloat(n.itembox.css(f));
                        n.direct == "left" && r + 1 == u && o != 0 && (c = o);
                        isNaN(v) || (l = (n.itembox.css(f) == "auto" ? 0 : v) - c * n.itemWH, e = {},
                        e[f] = l + "px", n.direct == "top" && (s = n.itemWH * n.row, a = Math.ceil(o / n.fsNum), n.nw = o == 0 ? -u * s: -((u - 1) * s + a * n.itemWH), n.itembox.css("width", "100%"), e[f] = r == u - 1 && o != 0 ? -((r - 1) * s + a * n.itemWH) : -r * s, e[f] = n.curNum == u - 1 ? n.nw: e[f]), y = (n.curNum + 1) * n.fsNum * n.row, h = (r + 1) * n.fsNum * n.row, h = h > n.items.size() ? n.items.size() : h, r > n.curNum && n.loadImgs(y, h), p = Math.ceil(c * n.speed / n.fsNum), n.itembox.stop().animate(e, p, "linear",
                        function() {
                            l == n.nw && n.itembox.css(f, 0)
                        }), n.curNum = r, t(n), n.showCurTit())
                    }
                },
                topFn: function(n, t, i, u) {
                    r.copyNext(n, t, i, u)
                }
            };
            e.leftFn(n, i, u, f)
        },
        scrollPre: function(n, i, u, f) {
            var e = n.direct == "left" ? "left": "marginTop",
            o = {
                leftFn: function(n, i, r, u) {
                    var o, c, s, a, v, h, y;
                    if (parseFloat(n.itembox.css("left")) == 0 && n.direct == "left" && n.itembox.css("left", n.nw), n.itembox.is(":animated")) n.timer && clearInterval(n.timer);
                    else {
                        var f = n.items.size() % (n.fsNum * n.row),
                        l = n.fsNum,
                        p = parseFloat(n.itembox.css(e));
                        n.direct == "left" && n.curNum == u - 1 && f != 0 && (l = f);
                        o = p + l * n.itemWH;
                        c = {};
                        n.direct == "top" && (s = n.itemWH * n.row, a = Math.ceil(f / n.fsNum), n.nw = f == 0 ? -u * s: -((u - 1) * s + a * n.itemWH), n.itembox.css("width", "100%"), o = -r * s, f != 0 && r == u - 1 && (o = -((r - 1) * s + a * n.itemWH)), c.top = "auto", r == u - 1 && n.itembox.css("marginTop", n.nw));
                        c[e] = o + "px";
                        v = (n.curNum + 1) * n.fsNum * n.row;
                        h = (r + 1) * n.fsNum * n.row;
                        h = h > n.items.size() ? n.items.size() : h;
                        r > n.curNum && n.loadImgs(v, h);
                        y = Math.ceil(l * n.speed / n.fsNum);
                        n.itembox.stop().animate(c, y, "linear",
                        function() {
                            o == n.nw && n.itembox.css(e, 0)
                        });
                        n.curNum = r;
                        t(n);
                        n.showCurTit()
                    }
                },
                topFn: function(n, t, i, u) {
                    r.copyPre(n, t, i, u)
                }
            };
            o.leftFn(n, i, u, f)
        },
        someOne: function(n, i, r, u) {
            var f = function(n) {
                n.showCurTit();
                t(n)
            },
            e = {
                sliderSingle: function(n, t, i, r) {
                    var s = n.items.size() % (n.fsNum * n.row),
                    o = n.direct == "left" ? "left": "marginTop",
                    c = i == r - 1 && s != 0 ? !0 : !1,
                    h = n.fsNum * n.row,
                    l = parseFloat(n.itembox.css(o)),
                    u,
                    a,
                    e,
                    v,
                    y;
                    obj = {};
                    u = c == !1 ? -i * n.itemWH * h: -i * n.itemWH * h + (n.fsNum * n.row - s) * n.itemWH;
                    obj[o] = u + "px";
                    n.direct == "top" && (n.itembox.css("width", "100%"), u = -i * n.itemWH * n.row, c == !0 && i == r - 1 && (u = l - (i - n.curNum - 1) * n.itemWH * n.row - Math.ceil(s / n.fsNum) * n.itemWH), u = i == 0 ? 0 : u, obj.top = "auto", obj[o] = u + "px");
                    n.itembox.is(":animated") ? n.timer && clearInterval(n.timer) : (a = (n.curNum + 1) * n.fsNum * n.row, e = (i + 1) * n.fsNum * n.row, e = e > n.items.size() ? n.items.size() : e, i > n.curNum && n.loadImgs(a, e), v = n.speed / (n.itemWH * h) * Math.abs(Math.ceil(u - l)), y = n.direct == "left" ? v: n.speed, n.itembox.stop().animate(obj, y, "linear",
                    function() {
                        u == n.nw && n.itembox.css(o, 0)
                    }), n.curNum = i, f(n))
                },
                sliderMoreL: function(n, t, i, r) {
                    var s = n.items.size() % (n.fsNum * n.row),
                    e = {},
                    u = i == r - 1 && s > 0 ? n.fsNum * n.row * (i - n.curNum) - s: n.fsNum * n.row * (i - n.curNum),
                    h,
                    o;
                    e[n.direct] = "auto";
                    n.curNum < i && (e.marginLeft = -n.itemWH * u + "px", h = i * n.fsNum * n.row, o = h + u, o = o > n.items.size() ? n.items.size() : o, n.loadImgs(h, o), n.itembox.is(":animated") ? n.timer && clearInterval(n.timer) : (n.itembox.animate(e, "normal", "linear",
                    function() {
                        n.itembox.find("li").slice(0, u).appendTo(n.itembox);
                        n.itembox.css({
                            marginLeft: 0
                        })
                    }), n.curNum = i, f(n)));
                    n.curNum > i && (u = n.curNum != r - 1 ? n.fsNum * n.row * (n.curNum - i) : n.fsNum * n.row * (n.curNum - i) - s, n.loadImgs( - u), e.marginLeft = -n.itemWH * u + "px", n.itembox.find("li").slice( - u).prependTo(n.itembox), n.itembox.css(e), n.itembox.is(":animated") ? n.timer && clearInterval(n.timer) : (n.itembox.animate({
                        marginLeft: 0
                    },
                    "normal"), n.curNum = i, f(n)))
                },
                sliderMoreT: function() {}
            };
            e[i] && e[i](n, i, r, u)
        },
        fadeNext: function(n, i, r, u) {
            var e = n.itembox.find("[data-clsn]"),
            s = r * n.fsNum * n.row,
            o = n.itembox.data("isAnimEnd"),
            f = s + n.fsNum * n.row;
            f = f > n.items.size() ? n.items.size() : f;
            r < 0 && (r = u);
            r >= u && (r = i == "pre" ? u - 1 : 0);
            n.curNum = r;
            n.loadImgs(s, f);
            o == undefined && n.itembox.data("isAnimEnd", !0); (e.is(":animated") || o != undefined) && o != !0 ? n.timer && clearInterval(n.timer) : (n.itembox.data("isAnimEnd", !1), e.stop().css({
                visiblity: "hidden"
            }).fadeOut(30), t(n), n.showCurTit(), e.slice(n.fsNum * n.row * r, n.fsNum * n.row * (r + 1)).stop().css({
                visiblity: "visible"
            }).fadeIn(50,
            function() {
                n.itembox.data("isAnimEnd", !0)
            }))
        },
        copyNext: function(n, i, r, u) {
            var h;
            if (n.itembox.is(":animated")) n.timer && clearInterval(n.timer);
            else {
                var f = {},
                c = "margin" + n.direct.replace(/(^|\s+)\w/g,
                function(n) {
                    return n.toUpperCase()
                }),
                l = n.curNum,
                s = n.curNum != 1 ? (n.curNum - 1) * n.fsNum * n.row: n.fsNum * n.row,
                e = s + n.fsNum * n.row,
                o = n.items.size(),
                a = o % n.fsNum;
                e = e > o ? o: e;
                h = {
                    leftFn: function(n, i, r, u) {
                        var h = n.items.size() % (n.fsNum * n.row),
                        e = r == u - 1 && h > 0 ? n.fsNum * n.row * (r - n.curNum) - h: n.fsNum * n.row * (r - n.curNum),
                        s,
                        o;
                        e = r == 0 && n.curNum == u - 1 ? n.fsNum * n.row: e;
                        f.marginLeft = -n.itemWH * e + "px";
                        s = r * n.fsNum * n.row;
                        o = s + e;
                        o = o > n.items.size() ? n.items.size() : o;
                        n.loadImgs(s, o);
                        n.curNum = r;
                        n.itembox.animate(f, "normal",
                        function() {
                            n.itembox.find("li").slice(0, e).appendTo(n.itembox);
                            n.itembox.css({
                                marginLeft: 0
                            });
                            t(n);
                            n.showCurTit()
                        })
                    },
                    topFn: function(n, i) {
                        n.direct == "top" && n.itembox.css("width", "100%");
                        var u = n.itembox.find("li").eq(0).outerHeight(!0),
                        e = n.itembox.height() - u * n.row,
                        h = Math.floor(i / (n.row * n.fsNum)),
                        o = Math.ceil(i % (n.row * n.fsNum) / n.fsNum),
                        s = parseFloat(n.itembox.css("marginTop")),
                        r = s - u * n.row,
                        l = n.speed;
                        n.curNum == h && o != 0 && (r = s - u * o); - r >= e && (r = -e);
                        f[c] = r + "px";
                        n.itembox.animate(f, l, "linear",
                        function() {
                            e == -r && n.itembox.css("marginTop", "0px");
                            t(n);
                            n.showCurTit()
                        })
                    }
                };
                n.loadImgs(s, e);
                h[n.direct + "Fn"](n, i, r, u, o)
            }
        },
        copyPre: function(n, i, r, u) {
            if (n.itembox.is(":animated")) n.timer && clearInterval(n.timer);
            else {
                var f = {},
                s = n.curNum,
                h = n.curNum != 1 ? (n.curNum - 1) * n.fsNum * n.row: n.fsNum * n.row,
                e = h + n.fsNum,
                o = n.items.size(),
                c = o % (n.fsNum * n.row);
                e = e > o ? o: e;
                n.loadImgs(h, e);
                omethod = {
                    leftFn: function(n, i, r, u) {
                        var e = n.curNum != u - 1 ? n.fsNum * n.row * (n.curNum - r) : n.fsNum * n.row * (n.curNum - r) - c;
                        e = n.curNum == 0 && r == u - 1 ? n.fsNum * n.row: e;
                        f.marginLeft = -n.itemWH * e + "px";
                        n.loadImgs( - e);
                        n.itembox.find("li").slice( - e).prependTo(n.itembox);
                        n.itembox.css(f);
                        n.curNum = r;
                        n.itembox.animate({
                            marginLeft: 0
                        },
                        "normal");
                        t(n);
                        n.showCurTit()
                    },
                    topFn: function(n, i, r, u, e) {
                        var h = n.itembox.find("li").eq(0).outerHeight(!0),
                        c = n.itembox.height() - h * n.row,
                        y = Math.ceil(e / (n.row * n.fsNum)),
                        v = n.speed,
                        a = Math.ceil(e % (n.row * n.fsNum) / n.fsNum),
                        l,
                        o;
                        s == 0 && n.itembox.css("marginTop", -c);
                        l = parseFloat(n.itembox.css("marginTop"));
                        o = l + h * n.row;
                        s == 0 && n.itembox.css("marginTop", -c);
                        n.curNum == 0 && a != 0 && (o = l + h * a);
                        f.marginTop = o + "px";
                        n.itembox.animate(f, v, "linear",
                        function() {
                            c == -o && n.itembox.css("marginTop", "0px");
                            t(n);
                            n.showCurTit()
                        })
                    }
                };
                omethod[n.direct + "Fn"](n, i, r, u, o)
            }
        }
    },
    u = {
        slideleft: function(n, t, i, u) {
            var f = t == "next" ? "scrollNext": t == "pre" ? "scrollPre": "someOne";
            r[f](n, "sliderSingle", i, u)
        },
        fade: function(n, t, i, u) {
            var f = t == "next" ? n.curNum + 1 : t == "pre" ? n.curNum - 1 : i;
            r["fadeNext"](n, "fadeAll", f, u)
        },
        moreRowSlider: function(n, t, i, u) {
            var f = null,
            e = "sliderMoreL";
            n.direct == "left" ? f = t == "next" ? "copyNext": t == "pre" ? "copyPre": "someOne": (f = t == "next" ? "scrollNext": t == "pre" ? "scrollPre": "someOne", e = "sliderSingle");
            r[f](n, e, i, u)
        },
        moreRowFade: function(n, t, i, u) {
            var f = t == "next" ? n.curNum + 1 : t == "pre" ? n.curNum - 1 : i,
            u = Math.ceil(n.items.size() / (n.fsNum * n.row));
            r["fadeNext"](n, t, f, u)
        }
    },
    f = function(t, i, r) {
        this.ele = n(t);
        this.opts = i;
        this.settings = n.extend({},
        n.fn.osliderCom.defaults, this.opts, this.getSetting());
        try {
            if (this.btnPre = this.ele.find(this.settings.btnPreClass), this.btnNext = this.ele.find(this.settings.btnNextClass), this.stepNum = this.settings.stepNum, this.row = this.settings.row, this.fsNum = this.settings.firScreenNum, this.itembox = this.ele.find(this.settings.listClass), this.items = this.itembox.find("[data-clsn]"), this.direct = n.trim(this.settings.direct), this.effect = this.settings.effect, this.imgs = null, this.imgW = this.settings.imgWidth != "0" && this.settings.imgWidth || !1, this.imgH = this.settings.imgHeight != "0" && this.settings.imgHeight || !1, this.itemW = this.settings.itemWidth, this.itemWrap = this.ele.find(this.settings.wrapClass), this.itemWH = parseFloat(this.direct == "left" ? this.itemW != "" ? this.itemW: this.imgW: this.imgH), this.controlbox = this.ele.find("." + this.settings.controlClass), this.titbox = this.ele.find(this.settings.titboxClass), this.speed = this.settings.speed, this.pauseTime = this.settings.pauseTime, this.isAuto = this.settings.isAuto, this.timer = null, this.itemboxWH = null, this.curNum = 0, this.nw = 0, this.moreW = 100, this.sqpd = this.settings.singleQuotePlaceholder, this.ptPrep = this.settings.picTxtPrep, this.picTxtArr = n.trim(this.settings.picTxtArr) != "" ? n.trim(this.settings.picTxtArr).split(this.ptPrep) : [], this.ptArrLen = this.picTxtArr.length, this.items && this.items.size() > 0) this.init(),
            n.isFunction(r) && r.call(this.ele);
            else return
        } catch(u) {
            alert("OsliderCom插件调用异常：" + u.message)
        }
    };
    f.prototype = {
        getSetting: function() {
            var t = this.ele && this.ele.attr && this.ele.attr("data-setting") || "";
            return t && t != "" ? n.parseJSON(t) : this.opts && this.opts || {}
        },
        init: function() {
            var t = this,
            r = t.items.size(),
            u = t.settings.controlBar,
            f = t.settings.navBar,
            e = this.settings.titbox,
            i;
            if (nwrapWidth = r < t.fsNum ? parseFloat(t.itemW) * r: parseFloat(t.itemW) * t.fsNum, oEleImgs = t.items.find(".imglist-img"), t.itemW != "" && (t.items.css({
                width: t.itemW
            }), oEleImgs.css({
                height: t.imgH
            })), t.itemW != "" && this.itemWrap.size() == 1 && this.itemWrap.css("width", nwrapWidth), t.ptArrLen > 0) {
                for (i = 0; i < t.ptArrLen; i++) n.trim(t.picTxtArr[i]) != "" && t.items.size() > i && t.items.eq(i).append('<div class="mod-imglist-picTxt">' + t.picTxtArr[i].replace(new RegExp(t.sqpd, "gmi"), "'") + "<\/div>");
                t.items = t.ele.find("[data-clsn]")
            }
            t.itembox && t.imgW != 0 && t.itembox.find("img").css({
                width: t.imgW,
                height: t.imgH
            });
            t.items.size() > t.fsNum * t.row ? (t.setItemboxWorH(), t.row == 1 && t.singleRowSlider() || t.moreRowSlider(), f == !0 ? t.btnBindEvent() : (t.btnPre && t.btnPre.css("display", "none"), t.btnNext && t.btnNext.css("display", "none")), t.isAuto && (t.autoScroll(), t.bindHoverEvent()), u == !0 && t.controlBindEvent() || t.ele.find("." + t.settings.controlClass).css("display", "none"), e === !1 && t.hideTitbox()) : (t.itembox.css("width", "100%"), t.imgs = t.itembox.find("img[ssrc]"), t.loadImgs(0), t.btnPre && t.btnPre.css("display", "none"), t.btnNext && t.btnNext.css("display", "none"))
        },
        hideTitbox: function() {
            var n = this,
            t = n.ele.find(n.settings.markClass),
            i = n.ele.find(n.settings.imglistTitClass);
            n.titbox && n.titbox.css("display", "none");
            t && t.css("display", "none");
            i && i.css("display", "none")
        },
        loadImgs: function(t, i) {
            var r = this.imgs.size(),
            u = this,
            o = r > i ? i: r,
            f = t,
            e;
            r > 0 && (e = i == undefined ? u.imgs.slice(f) : u.imgs.slice(f, o), e.each(function() {
                var t = n(this);
                t.attr("src") != t.attr("ssrc") && t.attr("src",
                function() {
                    return t.attr("ssrc")
                });
                u.imgs.splice(f + t.index(), 1, t)
            }))
        },
        setItemboxWorH: function() {
            var n = this;
            n.itemWH != "0px" && (n.itemboxWH = n.itemWH * (n.items.size() + n.fsNum) / n.row, n.nw = -parseFloat(n.itemboxWH) + n.fsNum * n.itemWH, n.direct == "left" && n.itembox.width(n.itemboxWH + n.moreW), n.direct == "top" && (n.itemWH = n.items.eq(0).outerHeight(!0), n.itemWrap.height(n.itemWH), n.nw = Math.ceil((n.items.size() + n.fsNum) / n.fsNum * n.row) * n.itemWH))
        },
        btnBindEvent: function() {
            var n = this,
            t = n.effect == "fade" || n.row > 1 ? Math.ceil(n.items.size() / (n.fsNum * n.row)) : Math.ceil(n.items.size() / n.fsNum),
            i = this.settings.eventType,
            r;
            fEvent = function(r) {
                r.btnNext.on(i,
                function() {
                    var i = n.curNum + 1 == t ? 0 : n.curNum + 1;
                    u[r.effect](n, "next", i, t)
                });
                r.btnPre.on(i,
                function() {
                    var i = n.curNum == 0 ? t - 1 : n.curNum - 1;
                    u[r.effect](n, "pre", i, t)
                })
            };
            n.btnNext.size() == 0 || n.btnPre.size() == 0 ? (r = '<div class="btnbox"> <span class=" btn btn-l">左侧按钮<\/span> <span class=" btn btn-r">右侧按钮<\/span> <\/div>', n.itembox.before(r), n.btnNext = n.ele.find(n.settings.btnNextClass), n.btnPre = n.ele.find(n.settings.btnPreClass), fEvent(n)) : fEvent(n)
        },
        autoScroll: function() {
            var n = this;
            clearInterval(n.timer);
            n.timer = null;
            setAutoScroll = function(n) {
                var t = n.effect == "fade" || n.row > 1 ? Math.ceil(n.items.size() / (n.fsNum * n.row)) : Math.ceil(n.items.size() / n.fsNum),
                i = n.curNum + 1 == t ? 0 : n.curNum + 1;
                n.btnNext.size() > 0 ? n.btnNext.trigger(n.settings.eventType) : u[n.effect](n, "next", i, t)
            };
            n.timer = setInterval(function() {
                setAutoScroll(n)
            },
            n.pauseTime)
        },
        bindHoverEvent: function() {
            var n = this;
            this.ele.hover(function() {
                clearInterval(n.timer);
                n.timer = null
            },
            function() {
                n.autoScroll()
            })
        },
        controlBindEvent: function() {
            var t = this,
            e = -parseFloat(t.itemboxWH) + parseFloat(t.fsNum * t.itemWH),
            r = t.effect == "fade" || t.row > 1 ? Math.ceil(t.items.size() / (t.fsNum * t.row)) : Math.ceil(t.items.size() / t.fsNum),
            f;
            createLis = function(t) {
                var u = '<li class="active " >1<\/li>';
                for (i = 2; i <= r; i++) u += "<li>" + i + "<\/li>";
                return n(u).appendTo(t.controlbox)
            };
            oControlLis = null;
            t.controlbox.size() == 0 && (f = '<ol class="' + t.settings.controlClass + '"><\/ol>', t.itembox.before(f), t.controlbox = t.ele.find("." + t.settings.controlClass));
            oControlLis = t.controlbox.find("li").size() > 0 ? t.controlbox.find("li") : createLis(t);
            this.controlbox.on(this.settings.controlEventType, "li",
            function() {
                var i = n(this),
                f = i.index(),
                e = i.attr("class") == undefined ? !0 : i.attr("class").indexOf("active") == -1 ? !0 : !1;
                if (!t.itembox.is(":animated") && e) u[t.effect](t, "someOne", f, r),
                i.addClass(" active ").siblings("li").removeClass("active");
                else return
            });
            return ! 0
        },
        showCurTit: function() {
            if (this.titbox.size() > 0) {
                var i = this.curNum + 1 + "",
                t = this.titbox.find('[data-order="' + i + '"]');
                t.size() && t.addClass(" cur ").siblings("[data-order]").removeClass("cur") || this.titbox.find("[data-order]").each(function() {
                    n(this).removeClass("cur")
                })
            }
        },
        singleRowSlider: function() {
            var n = this,
            t;
            return n.imgs = n.itembox.find("img[ssrc]"),
            n.loadImgs(0, n.fsNum),
            t = n.fsNum == n.stepNum ? -1 : -1 * n.fsNum,
            n.loadImgs(t),
            n.items.slice(0, n.fsNum).clone().appendTo(n.itembox),
            !0
        },
        moreRowSlider: function() {
            var n = this,
            t = n.items.eq(0),
            r = t.find(".imglist-img").eq(0).outerHeight(!0),
            u = t.find(".imglist-tit").eq(0).outerHeight(!0),
            f = r + u,
            i;
            n.effect = n.effect == "slideleft" ? "moreRowSlider": "moreRowFade";
            n.imgs = n.itembox.find("img[ssrc]");
            this.itemWrap.css({
                height: f * n.row + "px"
            });
            n.itembox.css({
                width: n.effect == "fade" ? "100%": "auto"
            });
            n.loadImgs(0, n.fsNum * n.row);
            n.direct == "top" && (n.items.slice(0, n.fsNum * n.row).clone().appendTo(n.itembox), n.imgs = n.itembox.find("img[ssrc]"), i = -1 * n.fsNum * n.row, n.loadImgs(i))
        }
    };
    n.fn.osliderCom = function(t, i) {
        return n.each(this,
        function(r, u) {
            var e = n(u),
            o;
            if (e.data("osliderCom")) return e.data("osliderCom");
            o = new f(this, t, i);
            e.data("osliderCom", o)
        })
    };
    n.fn.osliderCom.defaults = {
        listClass: ".mod-imglist-inner",
        controlClass: "controlBox",
        titboxClass: ".mod-imglist-titbox",
        markClass: ".mod-imglist-titbox-mark",
        wrapClass: ".mod-imglist-wrap",
        imglistTitClass: ".imglist-tit",
        firScreenNum: 6,
        row: 1,
        actionUrl: "",
        pauseTime: 5e3,
        speed: 1e3,
        isAuto: !0,
        effect: "slideleft",
        btnPreClass: ".btn-l",
        btnNextClass: ".btn-r",
        direct: "left",
        eventType: "click",
        controlEventType: "click",
        imgWidth: "132px",
        imgHeight: "160px",
        itemWidth: "183px",
        navBar: !0,
        controlBar: !1,
        titbox: !1,
        picTxtArr: "",
        picTxtPrep: "{#%}",
        singleQuotePlaceholder: "{##}"
    }
} (jQuery),
function(n) {
    var t = function(t, i, r) {
        try {
            this.opts = i;
            this.ele = n(t);
            this.settings = n.extend({},
            n.fn.treeModule.defaults, this.opts, this.getSettings());
            this.fn = r;
            this.getDataUrl = this.settings.baseUrl + this.settings.getDataUrl;
            this.detailBaseUrl = this.settings.baseUrl + this.settings.detailBaseUrl;
            this.defaultDb = n.trim(this.settings.dbCode).split(",");
            this.clsDb = n.trim(this.settings.clsDb).split(",");
            this.loadData = this.settings.loadData;
            this.listEles = this.ele.find("." + this.settings.listBoxClass);
            this.oNotShow = this.getNotShow();
            this.isSetHeight = n.trim(this.settings.treeModHeight) == "" ? !1 : !0;
            this.isOverflow = !1;
            this.init();
            this.fn && this.fn.call(this.ele)
        } catch(u) {
            return alert(u.message),
            this.ele
        }
        return this.ele
    };
    t.prototype = {
        getSettings: function() {
            var t = this.ele.attr && this.ele.attr("data-setting") || "";
            if (t && t != "") return n.parseJSON(t)
        },
        init: function() {
            this.setmodHeight();
            this.loadData && this.setData();
            this.bindClsEvent();
            this.settings.showMenu && this.bindShowMenuEvent()
        },
        getNotShow: function() {
            var i = this.settings.notShowNodes.replace("，", ",").split(","),
            t = {};
            return n.each(i,
            function(n, i) {
                t[i] = i
            }),
            t
        },
        getData: function(t, i, r, u, f, e) {
            var o = this;
            n.MyAjax(this.getDataUrl, {
                clsDB: i,
                clsName: r,
                clsCode: u,
                mode: "tree",
                ProductCode: f
            },
            "get", "jsonp", !0,
            function(i) {
                var s = "",
                b = "",
                c = 0,
                k = t.find(".nav-sub").size() == 0 ? !0 : !1,
                h,
                f,
                u,
                l;
                if (i.Modules.length > 0 && i.Modules[0].Children.length > 0 && k) {
                    switch (o.settings.urlType) {
                    case "3":
                        for (; c < i.Modules[0].Children.length; c++) {
                            var r = i.Modules[0].Children[c],
                            a = o.oNotShow[r.ClassName] == undefined ? !0 : !1,
                            v = o.oNotShow[r.ClassCode + "all"] != undefined ? !1 : !0,
                            y = n.trim(r.NodeCfg) != "" ? r.NodeCfg.replace(/\"/g, "\\'") : "",
                            d = n.trim(y) != "" ? "javascript:$.PostMore('" + e + "','" + r.ClassName + "','" + y + "',null,'发表时间|desc');": "javascript:;",
                            p = n.trim(y) != "" ? "": 'style="cursor:default;"';
                            if (a && (s += "<a " + p + ' class="nav-sub-01" title="' + r.ClassName + '" href="' + d + '">' + r.ClassName + "<\/a>"), r.IsChild && v) {
                                for (h = "", f = 0; f < r.Children.length; f++) {
                                    var u = r.Children[f],
                                    l = o.oNotShow[u.ClassCode] == undefined ? !0 : !1,
                                    w = n.trim(u.NodeCfg) != "" ? u.NodeCfg.replace(/\"/g, "\\'") : "",
                                    g = n.trim(w) != "" ? "javascript:$.PostMore('" + e + "','" + u.ClassName + "','" + w + "',null,'发表时间|desc');": "javascript:;";
                                    p = n.trim(w) != "" ? "": 'style="cursor:default;"';
                                    l && (h += "<li><a " + p + ' href="' + g + '">' + u.ClassName + "<\/a><\/li>")
                                }
                                s = s + '<ul class="nav-sub-02">' + h + "<\/ul>"
                            }
                        }
                        break;
                    default:
                        for (; c < i.Modules[0].Children.length; c++) {
                            var r = i.Modules[0].Children[c],
                            a = o.oNotShow[r.ClassName] == undefined ? !0 : !1,
                            v = o.oNotShow[r.ClassCode + "all"] != undefined ? !1 : !0;
                            if (a && (s += '<a class="nav-sub-01" title="' + r.ClassName + '" href="javascript:;" data-code="' + r.ClassCode + '" data-syscode="' + r.ParentCode + '">' + r.ClassName + "<\/a>"), r.IsChild && v) {
                                for (h = "", f = 0; f < r.Children.length; f++) u = r.Children[f],
                                l = o.oNotShow[u.ClassCode] == undefined ? !0 : !1,
                                l && (h += '<li><a href="javascript:;" data-code="' + u.ClassCode + '" data-syscode="' + u.ParentCode + '">' + u.ClassName + "<\/a><\/li>");
                                s = s + '<ul class="nav-sub-02">' + h + "<\/ul>"
                            }
                        }
                    }
                    b = '<div class="nav-sub">' + s + "<\/div>";
                    t.html(t.html() + b)
                } else i.ErrorMsg != null && alert("导航节点数据获取异常：" + i.ErrorMsg)
            })
        },
        setData: function() {
            var t = this,
            i = t.clsDb,
            r = n.trim(t.settings.ProductCode);
            n.each(t.listEles,
            function(u, f) {
                var e = n(f),
                o = t.defaultDb[u] != undefined ? t.defaultDb[u] : t.defaultDb[0],
                s = i[u] != undefined ? i[u] : i[0];
                rootEles = e.find("." + t.settings.rootClass);
                rootEles && rootEles.each(function() {
                    var i = n(this),
                    u = i.parent(".item"),
                    f = n.trim(i.text()),
                    e = i.attr("data-code");
                    t.getData(u, s, f, e, r, o)
                })
            })
        },
        bindShowMenuEvent: function() {
            var t = this,
            f = t.settings.hoverClass,
            s = t.settings.subNavClass,
            r = t.ele.find("." + t.settings.arrowClass),
            i = r.size() > 0 ? parseFloat(r.eq(0).css("right")) : 0,
            a = r.size() > 0 ? parseFloat(r.eq(0).css("top")) : 0,
            h = t.ele.find(".treelist-menu"),
            e = t.ele.find(".treelist-menu .item"),
            c = t.settings.setSubMenuTop,
            u = t.settings.sbuMenuPos,
            o = null,
            l = function(n, r, f, e, o) {
                var tt = n.index(),
                s = n.find("." + t.settings.arrowClass),
                c = n.parent(),
                p = c.scrollTop(),
                l = n.get(0).offsetTop - p,
                it = c.parent(".mod-treelist-wrap"),
                rt = n.innerWidth() - n.width(),
                w = o,
                b = n.get(0).offsetLeft,
                k = parseFloat(n.css("borderTopWidth")) || 0,
                h = n.find("." + r),
                g = n.find("." + t.settings.rootClass).eq(0),
                nt = h.outerHeight(!0),
                a = a = h.data("top") == undefined ? h.position().top: h.data("top"),
                d = n.get(0).offsetTop + n.outerHeight(!0) - p,
                v = 0,
                y;
                h.data("top", a);
                v = nt + a;
                v < d && e && h.css({
                    top: d - v
                });
                t.isOverflow == !0 ? (y = l + s.height() - n.parent().height(), w = u == "left" && -h.outerWidth() || o, h.css({
                    left: w + b,
                    right: "auto"
                }), s.length && y > 0 && s.height(s.height() - y), i = u == "def" ? c.outerWidth() - n.outerWidth() : c.outerWidth() - s.width() / 2, s && i != "auto" && s.css({
                    right: i - b,
                    left: "auto",
                    top: l + k
                })) : (c.css("position", "relative"), u == "left" && (h.css({
                    left: -h.outerWidth(),
                    right: "auto"
                }), s && i != "auto" && s.css({
                    left: i,
                    right: "auto"
                })), n.css("position") != "relative" && g.css("position") != "relative" && s && i != "auto" && s.css({
                    top: l + k
                }));
                n.addClass(" " + f + " ")
            };
            e && e.hover(function() {
                timer = null;
                var t = n(this),
                i = t.find(".nav-sub"),
                r = t.find(".nav-sub") && n.trim(i.eq(0).html()) != "" ? !0 : null;
                r != null && (o = n(this).width() - 1, timer = setTimeout(function() {
                    l(t, s, f, c, o)
                },
                300))
            },
            function() {
                timer && clearTimeout(timer);
                n(this).removeClass(f)
            });
            t.isOverflow == !0 && h.scroll(function() {
                var i = n(this).find("." + t.settings.hoverClass).eq(0);
                i.trigger("mouseleave")
            })
        },
        bindClsEvent: function() {
            var t = this,
            r = t.settings.clickSeletors,
            u = t.settings.systemno,
            i = t.settings.urlType;
            if (i == "3") {
                n.each(t.listEles,
                function() {
                    $eles = n(this).find('[href="javascript:void(0);"],[href="javascript:;"]');
                    $eles && $eles.length && $eles.attr("style", "cursor:default;")
                });
                return
            }
            n.each(t.listEles,
            function(f) {
                var e = t.clsDb[f] != undefined ? t.clsDb[f] : t.clsDb[0],
                o = t.defaultDb[f] != undefined ? t.defaultDb[f] : t.defaultDb[0],
                s = n(this);
                s.on("click", r,
                function(r) {
                    var c;
                    r.stopPropagation();
                    var f = n(this),
                    h = n.trim(f.attr("data-db")) == undefined || n.trim(f.attr("data-db")) == "" ? n.trim(o) : n.trim(f.attr("data-db")),
                    s = n.trim(f.attr("data-code")),
                    l = t.settings.kw && n.trim(f.text()) || "",
                    a = u == !0 ? n.trim(f.attr("data-syscode")) : s;
                    f.attr("href").indexOf("javascript:$.PostMore") == -1 && (r.preventDefault(), h == undefined || h == "" ? alert("导航节点:[" + f.text() + "]所需dbCode不存在！") : s == undefined || s == "" ? alert("导航节点:[" + f.text() + "]的data-code值不存在！") : e && e != "" ? (c = i == 1 ? t.detailBaseUrl + h + "&kw=" + encodeURIComponent(l) + "&korder=" + t.settings.korder + "&dscode=" + s + "&systemno=" + a + "&NaviField=" + encodeURIComponent(t.settings.NaviField) + "&NaviDatabaseName=" + e: i == 2 ? "/kns/RedirectPage.aspx?action=singlepage&code=" + h + "&kw=" + encodeURIComponent(l) + "&korder=" + t.settings.korder + "&dscode=" + s + "&systemno=" + a + "&NaviField=" + encodeURIComponent(t.settings.NaviField) + "&NaviDatabaseName=" + e + "&Clscode=" + e: "", c != "" ? window.open(c) : alert("跳转链接类型配置有误，请检查参数urlType的值")) : alert("导航节点:[" + f.text() + "]所在的结构中参数clsDb不存在或异常！"));
                    return
                })
            })
        },
        setmodHeight: function() {
            var i = this,
            e = this.settings,
            t = i.listEles,
            r = parseFloat(e.treeModHeight),
            u = 0,
            f;
            i.isSetHeight == !0 && t.length && (t.wrapAll('<div class="mod-treelist-wrap"><\/div>'), f = t.eq(0).parent(".mod-treelist-wrap"), t.each(function() {
                var t = n(this);
                u = t.outerHeight() > u ? t.outerHeight() : u
            }), i.isOverflow = u > r ? !0 : !1, t.css("height", r), i.isOverflow && (t.css({
                position: "static",
                "overflow-x": "hidden",
                "overflow-y": "auto",
                height: r
            }), f.css({
                position: "relative",
                height: r
            })), i.ele.find(".item").css("position", "static"))
        }
    };
    n.fn.treeModule = function(i, r) {
        return this.each(function() {
            var u = n(this),
            f;
            if (u.data("treeModule")) return u.data("treeModule");
            f = new t(this, i, r);
            u.data("treeModule", f)
        })
    };
    n.fn.treeModule.defaults = {
        baseUrl: "",
        getDataUrl: "/HYHPSContent/Module/GetTree",
        detailBaseUrl: "/kns/RedirectPage.aspx?action=singlepage&code=",
        NaviField: "行业分类代码",
        systemno: !1,
        kw: !1,
        loadData: !0,
        showMenu: !0,
        setSubMenuTop: !0,
        sbuMenuPos: "def",
        urlType: "1",
        korder: 2,
        listBoxClass: "treelist-menu",
        hoverClass: "item-hover",
        subNavClass: "nav-sub",
        arrowClass: "icon-arrowl",
        clickSeletors: ".nav-main,.nav-sub-01,.nav-sub-02 a",
        rootClass: "nav-main",
        clsDb: "",
        dbCode: "",
        clsCode: "",
        notShowNodes: "",
        treeModHeight: "",
        ProductCode: ""
    }
} (jQuery); !
function() {
    function a(n) {
        return n.replace(d, "").replace(g, ",").replace(nt, "").replace(tt, "").replace(it, "").split(rt)
    }
    function h(n) {
        return "'" + n.replace(/('|\\)/g, "\\$1").replace(/\r/g, "\\r").replace(/\n/g, "\\n") + "'"
    }
    function v(n, t) {
        function v(n) {
            return e += n.split(/\n/).length - 1,
            it && (n = n.replace(/\s+/g, " ").replace(/<!--[\w\W]*?-->/g, "")),
            n && (n = r[1] + h(n) + r[2] + "\n"),
            n
        }
        function g(n) {
            var h = e,
            s, u;
            return (y ? n = y(n, t) : f && (n = n.replace(/\n/g,
            function() {
                return e++,
                "$line=" + e + ";"
            })), 0 === n.indexOf("=")) && (s = rt && !/^=[=#]/.test(n), (n = n.replace(/^=[=#]?|[\s;]*$/g, ""), s) ? (u = n.replace(/\s*\([^\)]+\)/, ""), i[u] || /^(include|print)$/.test(u) || (n = "$escape(" + n + ")")) : n = "$string(" + n + ")", n = r[1] + n + r[2]),
            f && (n = "$line=" + h + ";" + n),
            c(a(n),
            function(n) {
                if (n && !p[n]) {
                    var t;
                    t = "print" === n ? ut: "include" === n ? ft: i[n] ? "$utils." + n: o[n] ? "$helpers." + n: "$data." + n;
                    k += n + "=" + t + ",";
                    p[n] = !0
                }
            }),
            n + "\n"
        }
        var f = t.debug,
        nt = t.openTag,
        tt = t.closeTag,
        y = t.parser,
        it = t.compress,
        rt = t.escape,
        e = 1,
        p = {
            $data: 1,
            $filename: 1,
            $utils: 1,
            $helpers: 1,
            $out: 1,
            $line: 1
        },
        w = "".trim,
        r = w ? ["$out='';", "$out+=", ";", "$out"] : ["$out=[];", "$out.push(", ");", "$out.join('')"],
        b = w ? "$out+=text;return $out;": "$out.push(text);",
        ut = "function(){var text=''.concat.apply('',arguments);" + b + "}",
        ft = "function(filename,data){data=data||$data;var text=$utils.$include(filename,data,$filename);" + b + "}",
        k = "'use strict';var $utils=this,$helpers=$utils.$helpers," + (f ? "$line=0,": ""),
        s = r[0],
        et = "return new String(" + r[3] + ");",
        u,
        l;
        c(n.split(nt),
        function(n) {
            n = n.split(tt);
            var t = n[0],
            i = n[1];
            1 === n.length ? s += v(t) : (s += g(t), i && (s += v(i)))
        });
        u = k + s + et;
        f && (u = "try{" + u + "}catch(e){throw {filename:$filename,name:'Render Error',message:e.message,line:$line,source:" + h(n) + ".split(/\\n/)[$line-1].replace(/^\\s+/,'')};}");
        try {
            return l = new Function("$data", "$filename", u),
            l.prototype = i,
            l
        } catch(d) {
            throw d.temp = "function anonymous($data,$filename) {" + u + "}",
            d;
        }
    }
    var n = function(n, t) {
        return "string" == typeof t ? u(t, {
            filename: n
        }) : f(n, t)
    },
    t,
    r,
    f,
    o,
    l;
    n.version = "3.0.0";
    n.config = function(n, i) {
        t[n] = i
    };
    t = n.defaults = {
        openTag: "<%",
        closeTag: "%>",
        escape: !0,
        cache: !0,
        compress: !1,
        parser: null
    };
    r = n.cache = {};
    n.render = function(n, t) {
        return u(n, t)
    };
    f = n.renderFile = function(t, i) {
        var r = n.get(t) || s({
            filename: t,
            name: "Render Error",
            message: "Template not found"
        });
        return i ? r(i) : r
    };
    n.get = function(n) {
        var i, t, f;
        return r[n] ? i = r[n] : "object" == typeof document && (t = document.getElementById(n), t && (f = (t.value || t.innerHTML).replace(/^\s*|\s*$/g, ""), i = u(f, {
            filename: n
        }))),
        i
    };
    var e = function(n, t) {
        return "string" != typeof n && (t = typeof n, "number" === t ? n += "": n = "function" === t ? e(n.call(n)) : ""),
        n
    },
    y = {
        "<": "&#60;",
        ">": "&#62;",
        '"': "&#34;",
        "'": "&#39;",
        "&": "&#38;"
    },
    p = function(n) {
        return y[n]
    },
    w = function(n) {
        return e(n).replace(/&(?![\w#]+;)|[<>"']/g, p)
    },
    b = Array.isArray ||
    function(n) {
        return "[object Array]" === {}.toString.call(n)
    },
    k = function(n, t) {
        var i, r;
        if (b(n)) for (i = 0, r = n.length; r > i; i++) t.call(n, n[i], i, n);
        else for (i in n) t.call(n, n[i], i)
    },
    i = n.utils = {
        $helpers: {},
        $include: f,
        $string: e,
        $escape: w,
        $each: k
    };
    n.helper = function(n, t) {
        o[n] = t
    };
    o = n.helpers = i.$helpers;
    n.onerror = function(n) {
        var i = "Template Error\n\n",
        t;
        for (t in n) i += "<" + t + ">\n" + n[t] + "\n\n";
        "object" == typeof console && console.error(i)
    };
    var s = function(t) {
        return n.onerror(t),
        function() {
            return "{Template Error}"
        }
    },
    u = n.compile = function(n, i) {
        function e(t) {
            try {
                return new h(t, f) + ""
            } catch(r) {
                return i.debug ? s(r)() : (i.debug = !0, u(n, i)(t))
            }
        }
        var o, f, h;
        i = i || {};
        for (o in t) void 0 === i[o] && (i[o] = t[o]);
        f = i.filename;
        try {
            h = v(n, i)
        } catch(c) {
            return c.filename = f || "anonymous",
            c.name = "Syntax Error",
            s(c)
        }
        return e.prototype = h.prototype,
        e.toString = function() {
            return h.toString()
        },
        f && i.cache && (r[f] = e),
        e
    },
    c = i.$each,
    d = /\/\*[\w\W]*?\*\/|\/\/[^\n]*\n|\/\/[^\n]*$|"(?:[^"\\]|\\[\w\W])*"|'(?:[^'\\]|\\[\w\W])*'|\s*\.\s*[$\w\.]+/g,
    g = /[^\w$]+/g,
    nt = new RegExp(["\\b" + "break,case,catch,continue,debugger,default,delete,do,else,false,finally,for,function,if,in,instanceof,new,null,return,switch,this,throw,true,try,typeof,var,void,while,with,abstract,boolean,byte,char,class,const,double,enum,export,extends,final,float,goto,implements,import,int,interface,long,native,package,private,protected,public,short,static,super,synchronized,throws,transient,volatile,arguments,let,yield,undefined".replace(/,/g, "\\b|\\b") + "\\b"].join("|"), "g"),
    tt = /^\d[^,]*|,\d[^,]*/g,
    it = /^,+|,+$/g,
    rt = /^$|,+/;
    t.openTag = "{{";
    t.closeTag = "}}";
    l = function(n, t) {
        var r = t.split(":"),
        u = r.shift(),
        i = r.join(":") || "";
        return i && (i = ", " + i),
        "$helpers." + u + "(" + n + i + ")"
    };
    t.parser = function(t) {
        var e;
        t = t.replace(/^\s/, "");
        var i = t.split(" "),
        r = i.shift(),
        f = i.join(" ");
        switch (r) {
        case "if":
            t = "if(" + f + "){";
            break;
        case "else":
            i = "if" === i.shift() ? " if(" + i.join(" ") + ")": "";
            t = "}else" + i + "{";
            break;
        case "/if":
            t = "}";
            break;
        case "each":
            var h = i[0] || "$data",
            c = i[1] || "as",
            a = i[2] || "$value",
            v = i[3] || "$index",
            y = a + "," + v;
            "as" !== c && (h = "[]");
            t = "$each(" + h + ",function(" + y + "){";
            break;
        case "/each":
            t = "});";
            break;
        case "echo":
            t = "print(" + f + ");";
            break;
        case "print":
        case "include":
            t = r + "(" + i.join(",") + ");";
            break;
        default:
            if (/^\s*\|\s*[\w\$]/.test(f)) {
                e = !0;
                0 === t.indexOf("#") && (t = t.substr(1), e = !1);
                for (var u = 0,
                o = t.split("|"), p = o.length, s = o[u++]; p > u; u++) s = l(s, o[u]);
                t = (e ? "=": "=#") + s
            } else t = n.helpers[r] ? "=#" + r + "(" + i.join(",") + ");": "=" + t
        }
        return t
    };
    "function" == typeof define ? define(function() {
        return n
    }) : "undefined" != typeof exports ? module.exports = n: this.template = n
} (),
function(n) {
    var i = null,
    r = null,
    t = function(n, t, i, r) {
        var u = document.createElement(n);
        return u.type = t,
        u.name = i,
        u.value = r,
        u
    };
    n.extend({
        RemoveFlag: function(t, i) {
            var u = n(t),
            r = n(t) && n.trim(i) != "" ? n(t).find("#" + i) : undefined;
            r != undefined && r && r.size() == 1 && r.removeClass("disable").removeData("login")
        },
        GetLoginStatus: function(t, i) {
            var r = config.Domain + "/klogin/request/login.ashx?act=json&jsonp=loingstatus";
            n.ajax({
                url: r,
                type: "GET",
                dataType: "jsonp",
                jsonpCallback: "loingstatus",
                success: function(r) {
                    var f = r[0].username,
                    o = r[0].uid,
                    s = r[0].libtype,
                    u = n(t),
                    e;
                    f != null && f != "" && u != null && n.trim(t) != "" && n(t) && (u.text(""), n("<span>", {
                        text: "欢迎您 " + f + " ！",
                        id: "showuser"
                    }).appendTo(u), n.trim(n("#hidLibInfo").val()) != "1" && (s == 1 ? (e = "http://elib.cnki.net/logindigital.aspx?action=single&uid=" + o + "&p=" + f, n("<a>", {
                        href: e,
                        text: "我的个人馆",
                        target: "_blank",
                        "class": "libbtn"
                    }).appendTo(u)) : s == 2 && (e = "http://elib.cnki.net/logindigital.aspx?action=single&uid=" + o + "&p=" + f, n("<a>", {
                        href: e,
                        text: "我的机构馆",
                        target: "_blank",
                        "class": "libbtn"
                    }).appendTo(u))), n("<a>", {
                        href: "javascript:;",
                        text: "退出",
                        "class": "lgoutbtn"
                    }).appendTo(u), n("<input>", {
                        type: "hidden",
                        id: "hdnUID",
                        value: o
                    }).appendTo(u), n(t).css("display", "block"), n.BindUID(), n.isFunction(i) && i());
                    n.RemoveFlag(t, "ipbutt");
                    n.RemoveFlag(t, "logingbutt")
                },
                error: function() {
                    n.RemoveFlag(t, "ipbutt");
                    n.RemoveFlag(t, "logingbutt")
                }
            })
        },
        Logout: function(t, i) {
            var u = config.Domain + "/kcms/Logout.aspx",
            r;
            n.post(u, null, null);
            u = config.Domain + "/klogin/request/login.ashx?act=logout&jsonp=logout";
            r = n(t);
            r && r.data("login") == undefined && (r.addClass(" disable ").data("login", !0), n.ajax({
                url: u,
                type: "GET",
                async: !1,
                dataType: "jsonp",
                jsonpCallback: "logout",
                success: function(data) {
                    if (data[0].IsLogout == "True") return typeof MyProduct != "undefined" && typeof eval(MyProduct.SetIsHintCookie) == "function" && MyProduct.SetIsHintCookie(1),
                    n.isFunction(i) ? i() : window.location.reload(),
                    !1
                },
                error: function() {
                    alert("请求数据失败！");
                    r.removeClass("disable").removeData("login")
                }
            }))
        },
        subStr: function(n, t) {
            for (var u = 0,
            e = "",
            o = /[^\x00-\xff]/g.test(n), f = n.length, r = 0; r < f; r++) if (e = n.charAt(r).toString(), u++, u > t) break;
            if (o) return i = !0,
            "e";
            if (t && f > t) return i = !0,
            "a";
            i = !0;
            var s = /[`~!#\$%\^\&\*\(\)\+<>\?:"\{\},\\\/;'\[\]]/im.test(n),
            h = /\s+/g.test(n);
            return s ? "b": h ? "d": "c"
        },
        Login: function(t, u) {
            var b = config.Domain + "/klogin/request/login.ashx?act=login&jsonp=login",
            f = n(t).find("input[name='username']"),
            e = n(t).find("input[name='password']"),
            o = n(t).find("input[name='checkcode']"),
            y;
            if (e.val().indexOf(" ") != -1) return alert("密码禁止输入空格！"),
            f.val("").focus(),
            e.val(""),
            n.RemoveFlag(t, "logingbutt"),
            !1;
            var h = n.trim(f.val()),
            c = n.trim(e.val()),
            v = n.trim(o.val()),
            p = 64,
            w = 1e3,
            l = !1,
            a = !1,
            k = n.subStr(h, p),
            d = n.subStr(c, w),
            s = "",
            g = function(t, i, s) {
                var h = {
                    username: encodeURI(i),
                    password: encodeURI(s),
                    checkcode: encodeURI(v)
                };
                o.length > 0 && (v == null || v == "") ? (alert("请输入验证码！"), o.val("").focus(), n.RemoveFlag(t, "logingbutt"), n.RemoveFlag(t, "ipbutt")) : (clearInterval(r), r = null, n.ajax({
                    url: b,
                    type: "POST",
                    data: h,
                    async: !1,
                    dataType: "jsonp",
                    jsonpCallback: "login",
                    success: function(i) {
                        if (i[0].IsLogin == !0) n.GetLoginStatus(t, u);
                        else {
                            i[0].Msg.indexOf("验证码不正确") > -1 ? (alert(i[0].Msg), o && o.val("").focus()) : (alert(i[0].Msg), f.val("").focus(), e.val(""), o && o.val(""));
                            var s = n("#chkcodeimg", t);
                            s.length && (s.trigger("click"), r = setInterval(function() {
                                s.trigger("click")
                            },
                            18e4));
                            n.RemoveFlag(t, "logingbutt");
                            n.RemoveFlag(t, "ipbutt")
                        }
                    },
                    error: function() {
                        n.RemoveFlag(t, "logingbutt");
                        n.RemoveFlag(t, "ipbutt");
                        alert("请求数据失败！");
                        f.val("").focus()
                    }
                }))
            }; (h == null || n.trim(h) == "") && (c == null || n.trim(c) == "") ? (alert("用户名密码不能为空！"), f.val("").focus(), e.val(""), n.RemoveFlag(t, "logingbutt")) : h == null || n.trim(h) == "" ? (alert("用户名不能为空！"), n.RemoveFlag(t, "logingbutt"), f.val("").focus()) : c == null || n.trim(c) == "" ? (alert("密码不能为空！"), e.val("").focus(), n.RemoveFlag(t, "logingbutt")) : i != null && i && (y = {
                a: function(n) {
                    var t = n == "u" ? "用户名": "密码",
                    r = n == "u" ? f: e,
                    i = n == "u" ? p: w;
                    return s += t + "长度大于" + i + "字节！\n",
                    0
                },
                b: function(n) {
                    var t = n == "u" ? "用户名": "密码",
                    i = n == "u" ? f: e;
                    return s += t + "中含有特殊字符\n",
                    0
                },
                c: function() {
                    return ! 0
                },
                d: function(n) {
                    var t = n == "u" ? "用户名": "密码",
                    i = n == "u" ? f: e;
                    return s += t + "中不能含有空白字符\n",
                    0
                },
                e: function() {
                    return s = "用户名或密码错误",
                    -1
                }
            },
            l = y[k]("u"), a = y[d]("p"), l === !0 && a === 0 ? (alert(s), n.RemoveFlag(t, "logingbutt"), e.val("").focus()) : l === 0 && a === !0 ? (alert(s), n.RemoveFlag(t, "logingbutt"), f.val("").focus()) : (l === 0 && a === 0 || l === -1 || a === -1) && (alert(s), n.RemoveFlag(t, "logingbutt"), e.val(""), f.val("").focus()));
            l === !0 && a === !0 && g(t, h, c)
        },
        IpLogin: function(t, i) {
            var u = config.Domain + "/klogin/request/login.ashx?act=iploginex&jsonp=iploginex",
            f = n(t).find("input[name='username']"),
            e = n(t).find("input[name='password']"),
            r = n(t).find("input[name='checkcode']"),
            o = n("#ipbutt");
            n.ajax({
                url: u,
                type: "POST",
                data: {
                    username: "",
                    password: ""
                },
                async: !1,
                dataType: "jsonp",
                jsonpCallback: "iploginex",
                success: function(u) {
                    u.IsLogin == !0 ? n.GetLoginStatus(t, i) : (alert(u.Msg), f.val(""), e.val(""), r && r.val(""), n.RemoveFlag(t, "ipbutt"), n.RemoveFlag(t, "logingbutt"))
                },
                error: function() {
                    n.RemoveFlag(t, "ipbutt");
                    alert("请求数据失败！")
                }
            })
        },
        BindEvent: function(t, i, u, f, e) {
            var o = n(t),
            s = o && n.trim(i) != "" ? o.find(i) : undefined,
            c = o && n.trim(u) != "" ? o.find(u) : undefined,
            h = o && n.trim("#chkcodeimg") != "" ? o.find("#chkcodeimg") : undefined;
            if (s != undefined) s.on("click",
            function() {
                var i = n(this);
                if (i.data("login") == undefined) i.data("login", "ture").addClass(" disable "),
                n.Login(t, f);
                else return ! 1
            });
            if (c != undefined) c.on("click",
            function() {
                var i = n(this);
                if (i.data("login") == undefined) i.data("login", "ture").addClass(" disable "),
                n.IpLogin(t, f);
                else return ! 1
            });
            if (h != undefined && (h.on("click",
            function() {
                var t = config.Domain + "/klogin/checkcode.aspx?t=" + (new Date).getTime();
                n(this).attr("src", t)
            }).css("cursor", "pointer"), h.click(), r = setInterval(function() {
                h.trigger("click")
            },
            18e4)), o && s != undefined) o.on("keydown", "input",
            function(i) {
                i.keyCode == 13 && s.data("login") == undefined && (s.data("login", !0).addClass(" disable "), n.Login(t, f))
            });
            o.off("click.lgout");
            o.on("click.lgout", ".lgoutbtn",
            function() {
                n.Logout(this, e)
            })
        },
        Reg: function() {
            var n = window.location.href,
            t = "http://my.cnki.net/elibregister/commonRegister.aspx?returnurl=" + encodeURI(n);
            window.open(t)
        },
        BindUID: function() {
            var uid = n("#hdnUID");
            if (uid.length > 0) {
                n("body").on("click", 'a:not([data-uid="no"])',
                function() {
                    var t = n.trim(n(this).attr("href")),
                    i;
                    t != null && t != "" && t.indexOf("#") != 0 && t.indexOf("beian.gov.cn") == -1 && t.indexOf("uid=") == -1 && (i = t.toLowerCase().substring(0, 4), i != "java" && t.indexOf("uid=") < 0 && (t.indexOf("?") > 0 ? n(this).attr("href", t + "&uid=" + uid.val()) : n(this).attr("href", t + "?uid=" + uid.val())))
                });
                typeof MyProduct != "undefined" && typeof eval(MyProduct.TanCeng) == "function" && MyProduct.TanCeng("Ecp_LoginStuts")
            }
        },
        BindSearch: function(t, i) {
            var r = t,
            e = "current",
            o = ".secbtn",
            s = r.find(".secnav"),
            f = ".secnav > a",
            u = n(".secnav-morebox", r),
            h = i && n.trim(i) != "" ? i: "click";
            n(f).attr("href", "javascript:;");
            r.on(h, f,
            function() {
                n(f, r).removeClass(e);
                n(this).addClass(e)
            });
            r.on("click.btnClick", o,
            function() {
                var t = r.find("input[type='text']").val(),
                w = f + "." + e,
                i = r.find(w),
                k = i.text(),
                o = n.trim(i.attr("data-code")),
                h = null,
                c = null,
                l = null,
                a = null,
                v = null,
                y = null,
                s = null,
                b = n.trim(r.find("#korder").val()) == "" ? "1": n.trim(r.find("#korder").val()),
                p,
                u;
                return n.IsSQLInject(t) ? (alert("请输入有效检索词！"), r.find("input[type='text']").focus(), !1) : (p = t, t = t.replace(/[\；|\————|\!|\@|\#|\$|\%|\^|\&|\(|\)|\_|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\?]|\b(and)\b|\b(or)\b/g, ""), p != "" && t == "") ? (alert("请输入有效检索词！"), r.find("input[type='text']").focus(), !1) : t == null || t == "" || n.trim(t) == "" || t == "请填入您所要搜索的关键词..." ? (alert("请输入检索词！"), r.find("input[type='text']").focus(), !1) : !!o && o != "" ? (u = config.Domain + "/kns/brief/singleresult.aspx?dbPrefix=" + o + "&action=indexpage&code=" + o + "&kw=" + encodeURIComponent(t) + "&korder=" + b + "&sel=1", o == "CIDX" && (u = "http://epub.cnki.net/kns/brief/default_result.aspx?action=scdbsearch&dbPrefix=SCDB&db_opt=CIDX&singleDB=CIDX&singleDBName=%E6%8C%87%E6%95%B0&txt_1_special1=%25&txt_1_sel=sval&txt_1_value1=" + encodeURIComponent(t)), window.open(u), !0) : r.attr("data-effect") === "search-g" ? (h = r.attr("data-db"), a = r.attr("data-NaviField"), v = i.attr("data-NaviField") || a, l = i.attr("data-db") || h, c = i.attr("data-clsDb"), s = n.trim(i.attr("data-cd")), y = s.split(",") ? s.split(",")[0] : "", u = config.Domain + "/kns/brief/result.aspx?dbprefix=" + l + "&kw=" + encodeURIComponent(t) + "&NaviDatabaseName=" + c + "&NaviField=" + encodeURIComponent(v) + "&systemno=" + y + "&DSCode=" + s + "&other=&korder=0&sel=1", window.open(u), !0) : void 0
            });
            r.find("input[type='text']").on("keypress",
            function(n) {
                var t = n.which ? n.which: n.keyCode;
                t == 13 && r.find(o).click()
            });
            if (!n.isEmptyObject(u)) {
                u.on("click", ".secnav-morebox-list  a",
                function() {
                    var i = n(this),
                    o = n.trim(i.text()),
                    r = n.trim(i.attr("data-code")),
                    c = s.children('a[data-code*="' + r + '"]'),
                    t = s.children("a:last"),
                    f,
                    e;
                    t.size() > 0 && (f = t.text(), e = t.attr("data-code"), t.attr("data-code", r), t.text(o), t.trigger(h), i.attr("data-code", e), i.text(f), u.trigger("mouseleave"))
                });
                n.showOrHideSubmenu(u, u, ".secnav-morebox-list", "secnav-morebox-click")
            }
        },
        showOrHideSubmenu: function(t, i, r, u) {
            var o = t.find(r),
            u = n.trim(u),
            f = null,
            e = i;
            t.on("mouseenter",
            function() {
                var t = n(this);
                f = null;
                f = setTimeout(function() {
                    t.addClass(" " + u + " ")
                },
                300)
            });
            e.on("mouseleave",
            function() {
                clearTimeout(f);
                t.removeClass(u)
            })
        },
        SearchScholar: function(i) {
            var r = n(i);
            if (r && r.size() > 0) {
                r.on("click", "input[type='button']",
                function() {
                    var i = n("#schkey"),
                    u,
                    r;
                    i.val() == null || n.trim(i.val()) == "" || i.val().length <= 1 || n.IsSQLInject(i.val()) ? (alert("请输入两字以上有效检索词！"), i.focus()) : (u = "http://scholar.cnki.net/result.aspx", r = [], r.push(t("input", "hidden", "type", "quick")), r.push(t("input", "hidden", "q2", i.val())), r.push(t("input", "hidden", "q", encodeURIComponent(i.val()))), r.push(t("input", "hidden", "rt", "all")), n.PostSumit("partform", u, r, "partform"))
                });
                r.find("input[type='text']").keypress(function(n) {
                    var t = n.which ? n.which: n.keyCode;
                    t == 13 && r.find("input[type='button']").click()
                })
            }
        },
        PubSearch: function(i, r, u) {
            var e = n.trim(r) != "" ? n.trim(r) : ".pub-txt",
            f = n.trim(u) != "" ? n.trim(u) : ".pub-btn";
            n(i) && n(i).each(function() {
                var r = n(this),
                u = n.trim(r.attr("data-db")),
                s = r.find(".pholder").eq(0);
                dbName = n.trim(r.attr("data-name"));
                btn = r.find(f);
                odblist = r.find(".dblist").eq(0);
                olinks = {
                    cross: "/KNavi/cross/navi/",
                    journal: "/KNavi/journal/navi/",
                    refbook: "/KNavi/refbook/navi/",
                    conference: "/KNavi/conference/navi/",
                    yearbook: "/KNavi/yearbook/navi/",
                    newspaper: "/KNavi/newspaper/navi/"
                };
                ofiled = {
                    cross: "*",
                    journal: "CYKM|ISSN|CN|TitleCN",
                    refbook: "Title|TitleVice|TitlePart|Publisher|ISBNSearch",
                    conference: "Title|ConferenceOrg",
                    yearbook: "TitleCN|CN|Publisher|KeyWord|ISBN",
                    newspaper: "TitleCN|CN"
                };
                oPlaceholder = {
                    cross: "请输入检索内容...",
                    journal: "请输入期刊名称、ISSN、CN",
                    refbook: "请输入书名、出版社、ISBN",
                    conference: "请输入论文集名称、主办单位",
                    yearbook: "请输入年鉴名、出版者、关键词、ISBN",
                    newspaper: "请输入报纸名称、刊号"
                };
                var o = olinks[dbName],
                h = s[dbName],
                l = function(t, i, r) {
                    var l = "dblist-hover",
                    f = r ? r: !1,
                    c = null;
                    f && f.hover(function() {
                        var t = n(this);
                        c = null;
                        c = setTimeout(function() {
                            t.addClass(" " + l + " ")
                        },
                        300)
                    },
                    function() {
                        var t = n(this);
                        c && clearTimeout(c);
                        t.removeClass(l)
                    });
                    f && f.on("click", "li",
                    function() {
                        var i = n(this),
                        r = i.text() + "<i><\/i>",
                        c = f.find(".active");
                        u = i.attr("data-code");
                        dbName = i.attr("data-name");
                        o = olinks[dbName];
                        h = oPlaceholder[dbName];
                        t.attr({
                            "data-db": u,
                            "data-name": dbName
                        });
                        c.html(r);
                        i.addClass(" current ").siblings("li").removeClass("current");
                        f.trigger("mouseout");
                        t.find(e).val("");
                        s.text(h).css("display", "block")
                    })
                },
                c = n(".pholder", r);
                c.size() > 0 && n.SetPlaceHolder(c);
                odblist.size() > 0 && l(r, btn, odblist);
                r.on("click", f,
                function() {
                    var r = n(this).siblings(".pub-txt"),
                    e,
                    f;
                    r.val() == null || n.trim(r.val()) == "" || r.val().length <= 0 ? (alert("请输入检索词！"), r.focus()) : n.IsSQLInject(r.val()) ? (alert("请输入检索词！"), r.focus()) : u != undefined && o != undefined ? (e = config.Domain + o + u + "?&field=" + encodeURI(ofiled[dbName]) + "&value=" + encodeURI(r.val()), f = [], f.push(t("input", "hidden", "value", encodeURIComponent(r.val()))), f.push(t("input", "hidden", "field", ofiled[dbName])), n.PostSumit("partform", e, f, "partform")) : alert("请检查：" + i + "元素上是否正确填写属性【data-db】【data-name】及值！")
                })
            });
            n(i).on("keypress", ".pub-txt",
            function(t) {
                var i = t.which ? t.which: t.keyCode,
                r = n(this).siblings(f);
                i == 13 && r.trigger("click")
            })
        },
        SearchAll: function(t, i, r, u) {
            var f;
            if (t instanceof jQuery) f = t;
            else if (n(t) instanceof jQuery) f = n(t);
            else return;
            var c = f.find(u),
            s = f.find(r),
            h = "1",
            e = f.find(".keylist"),
            l = e.find(".list >.current"),
            a = "keylist-hover",
            o = null;
            e.length && l.length && (h = l.attr("data-rel"), e && e.hover(function() {
                var t = n(this);
                o = null;
                o = setTimeout(function() {
                    t.addClass(" " + a + " ")
                },
                300)
            },
            function() {
                var t = n(this);
                o && clearTimeout(o);
                t.removeClass(a)
            }), e && e.on("click", "li",
            function() {
                var t = n(this),
                i = t.text() + "<i><\/i>",
                r = e.find(".active");
                h = t.attr("data-rel");
                r.html(i);
                t.addClass(" current ").siblings("li").removeClass("current");
                e.trigger("mouseout")
            }));
            f && s && s.on("click",
            function() {
                var t = f.find(u).val(),
                r;
                return n.trim(t) == "" || t == "请填入您所要搜索的关键词..." ? (alert("请输入检索词！"), f.find(u).focus(), !1) : n.IsSQLInject(t) ? (alert("请输入有效检索词！"), f.find(u).focus(), !1) : (r = config.Domain + "/kns/brief/singleresult.aspx?dbPrefix=" + i + "&action=indexpage&code=" + i + "&kw=" + encodeURIComponent(t) + "&korder=" + h + "&sel=1", i == "CIDX" && (r = "http://epub.cnki.net/kns/brief/default_result.aspx?action=scdbsearch&dbPrefix=SCDB&db_opt=CIDX&singleDB=CIDX&singleDBName=%E6%8C%87%E6%95%B0&txt_1_special1=%25&txt_1_sel=sval&txt_1_value1=" + encodeURIComponent(t)), window.open(r), !0)
            });
            c && c.keypress(function(n) {
                var t = n.which ? n.which: n.keyCode;
                t == 13 && s.click()
            })
        },
        SetPlaceHolder: function(t) {
            t && t.size() > 0 && t.each(function() {
                var t = n(this),
                r = n.trim(t.attr("for")),
                i = t.siblings("#" + r).eq(0);
                i.val("");
                i.blur();
                r != "" && i.size() == 1 ? (t.click(function() {
                    n(this).hide();
                    i.focus()
                }), i.focusout(function() {
                    n(this).val() || t.show()
                }).focusin(function() {
                    t.hide()
                })) : alert("请检查检索框内，label元素属性for的值与对应的input元素的id值是否存在且一致&#40;id值收尾不能有空格&#41;！页面内id值是否重复！")
            })
        },
        IsSQLInject: function(n) {
            var t = !1;
            return /select|update|delete|truncate|join|union|exec|insert|drop|count|’|"|;|>|<|%/i.test(n) && (t = !0),
            t
        },
        AdvancedSearch: function(t, i, r, u, f) {
            if (t instanceof jQuery != !1 && i instanceof jQuery != !1) {
                var e = i,
                s = t,
                r = n.trim(r) != "" ? n.trim(r) : "current",
                h = f && n.type(f) == "boolean" ? f: !0,
                o = null;
                s.off("click.gj");
                s.on("click.gj",
                function() {
                    var t = e && e.size() > 0 ? n.trim(e.find(r).attr("data-code")) : !1,
                    f = n.trim(u) != "" ? n.trim(u) : "/KNS/brief/result.aspx?dbprefix=",
                    i = null;
                    if (t && s) if (t == "CIDX") {
                        if (h) return o = e.find("input[type='text']").val(),
                        n.trim(o) != "" && o.indexOf("请填入您所要搜索的关键词") == -1 && o.indexOf("请填入") == -1 && o.indexOf("请写入") == -1 ? (url = "http://epub.cnki.net/kns/brief/default_result.aspx?action=scdbsearch&dbPrefix=SCDB&db_opt=CIDX&singleDB=CIDX&singleDBName=%E6%8C%87%E6%95%B0&txt_1_special1=%25&txt_1_sel=sval&txt_1_value1=" + encodeURIComponent(o), window.open(url), !1) : (alert("高级检索-指数：请输入关键词！"), e.find("input[type='text']").focus(), !1);
                        t = e.find("[data-code]").eq(0);
                        i = f + t
                    } else i = config.Domain + f + n.trim(t),
                    t != undefined && n.trim(t) != "" && n(this).attr("href", i)
                })
            }
        },
        ScrollImgLeft: function() {
            function f() {
                i.offsetWidth - n.scrollLeft <= 0 ? n.scrollLeft -= t.offsetWidth: n.scrollLeft++
            }
            var u = 20,
            t = document.getElementById("scroll_begin"),
            i = document.getElementById("scroll_end"),
            n = document.getElementById("scroll_div"),
            r;
            i.innerHTML = t.innerHTML;
            r = setInterval(f, u);
            t && i && n && (n.onmouseover = function() {
                clearInterval(r)
            },
            n.onmouseout = function() {
                r = setInterval(f, u)
            })
        },
        MyAjax: function(t, i, r, u, f, e, o) {
            n.ajax({
                url: t,
                data: i,
                type: r,
                cache: f,
                dataType: u,
                success: e,
                error: o
            })
        },
        PostSumit: function(t, i, r, u) {
            var f, e;
            if (n("#" + t).remove(), f = n("<form>"), f[0].action = i, f[0].method = "post", f[0].target = u, f[0].style.display = "none", f[0].id = t, r) for (e = 0; e < r.length; e++) f[0].appendChild(r[e]);
            return document.body.appendChild(f[0]),
            f[0].submit(),
            f
        },
        PostMore: function(i, r, u, f, e) {
            $('#meta').attr('content',"no-referrer")
            var s = config.Domain + "/kns/brief/more.aspx",
            c = !0,
            o;
            if (f != undefined && n.trim(f) != "") {
                c = !1;
                s = f.indexOf("//") == -1 ? f.indexOf("/") != 0 ? f: config.Domain + f: f;
                window.open(s);
                return
            }
            if (r == undefined && f == undefined && u == undefined) var h = n("#" + i),
            i = h.attr("data-code"),
            u = h.val(),
            r = h.attr("data-tip");
            c ? (o = [], o.push(t("input", "hidden", "dbPrefix", i)), o.push(t("input", "hidden", "sel", encodeURIComponent(u))), o.push(t("input", "hidden", "tip", r)), e != null && e.indexOf("|") > -1 && o.push(t("input", "hidden", "orderby", n.trim(e))), n.PostSumit("partform", s, o, "partform")) : n.PostSumit("partform", s, null, "partform")
            setTimeout(()=>{$('#meta').attr('content',"default" )},300)
        },
        DBCnt: function(t, i, r) {
            n.ajax({
                url: config.Domain + "/SmartContent/GetCount.ashx",
                data: {
                    db: i,
                    sel: r
                },
                timeout: 3e4,
                type: "get",
                cache: !0,
                dataType: "jsonp",
                jsonp: "callback",
                success: function(i) {
                    n(t).text(i.DBCount)
                },
                error: function() {
                    alert("数据加载失败,查看是否参数错误！")
                }
            })
        }
    })
} (jQuery),
function(n) {
    n.fn.mScrollSuper = function(t) {
        return n.fn.mScrollSuper.defaults = {
            type: "slide",
            effect: "fade",
            slideType: "Img",
            bgcArr: [],
            autoPlay: !1,
            delayTime: 500,
            interTime: 2500,
            triggerTime: 150,
            defaultIndex: 0,
            titCell: ".hd li",
            mainCell: ".bd",
            targetCell: null,
            trigger: "mouseover",
            scroll: 1,
            vis: 1,
            titOnClassName: "on",
            autoPage: !1,
            prevCell: ".prev",
            nextCell: ".next",
            pageStateCell: ".pageState",
            opp: !1,
            pnLoop: !0,
            showNavBar: !0,
            easing: "swing",
            startFun: null,
            endFun: null,
            switchLoad: null,
            playStateCell: ".playState",
            mouseOverStop: !0,
            defaultPlay: !0,
            returnDefault: !1,
            imgWidth: 0,
            imgHeight: 0
        },
        this.each(function() {
            var i = n.extend({},
            n.fn.mScrollSuper.defaults, t),
            f = n(this),
            k = i.effect,
            nt = n(i.prevCell, f),
            tt = n(i.nextCell, f),
            tr = n(i.pageStateCell, f),
            lt = n(i.playStateCell, f),
            a = n(i.titCell, f),
            o = a.size(),
            r = n(i.mainCell, f),
            e = r.children().size(),
            v = i.switchLoad,
            st = n(i.targetCell, f),
            u = parseInt(i.defaultIndex),
            c = parseInt(i.delayTime),
            ii = parseInt(i.interTime),
            ur = parseInt(i.triggerTime),
            s = parseInt(i.scroll),
            w = parseInt(i.vis),
            ir = i.autoPlay == "false" || i.autoPlay == !1 ? !1 : !0,
            ri = i.opp == "false" || i.opp == !1 ? !1 : !0,
            rr = i.autoPage == "false" || i.autoPage == !1 ? !1 : !0,
            yt = i.pnLoop == "false" || i.pnLoop == !1 ? !1 : !0,
            ui = i.mouseOverStop == "false" || i.mouseOverStop == !1 ? !1 : !0,
            at = i.defaultPlay == "false" || i.defaultPlay == !1 ? !1 : !0,
            pi = i.returnDefault == "false" || i.returnDefault == !1 ? !1 : !0,
            y = 0,
            p = 0,
            ct = 0,
            vt = 0,
            b = i.easing,
            ht = null,
            pt = null,
            wt = null,
            it = i.titOnClassName,
            wi = a.index(f.find("." + it)),
            fi = u = wi == -1 ? u: wi,
            bi = u,
            ft = u,
            h = e >= w ? e % s != 0 ? e % s: s: 0,
            g,
            rt = k == "leftMarquee" || k == "topMarquee" ? !0 : !1,
            ki = function() {
                n.isFunction(i.startFun) && i.startFun(u, o, f, n(i.titCell, f), r, st, nt, tt)
            },
            d = function() {
                n.isFunction(i.endFun) && i.endFun(u, o, f, n(i.titCell, f), r, st, nt, tt)
            },
            ei = function() {
                a.removeClass(it);
                at && a.eq(bi).addClass(it)
            },
            di = function(n, t) {
                n && i.imgWidth != 0 && i.imgHeight != 0 && (n.css({
                    width: i.imgWidth,
                    height: i.imgHeight
                }), t.css("width", i.imgWidth))
            },
            bt,
            kt,
            ut,
            oi,
            dt,
            et,
            l,
            nr,
            vi,
            yi;
            if (i.type == "menu") {
                at && a.removeClass(it).eq(u).addClass(it);
                a.hover(function() {
                    g = n(this).find(i.targetCell);
                    var t = a.index(n(this));
                    pt = setTimeout(function() {
                        u = t;
                        a.removeClass(it).eq(u).addClass(it);
                        ki();
                        switch (k) {
                        case "fade":
                            g.stop(!0, !0).animate({
                                opacity: "show"
                            },
                            c, b, d);
                            break;
                        case "slideDown":
                            g.stop(!0, !0).animate({
                                height: "show"
                            },
                            c, b, d)
                        }
                    },
                    i.triggerTime)
                },
                function() {
                    clearTimeout(pt);
                    switch (k) {
                    case "fade":
                        g.animate({
                            opacity:
                            "hide"
                        },
                        c, b);
                        break;
                    case "slideDown":
                        g.animate({
                            height:
                            "hide"
                        },
                        c, b)
                    }
                });
                pi && f.hover(function() {
                    clearTimeout(wt)
                },
                function() {
                    wt = setTimeout(ei, c)
                });
                return
            }
            if (o == 0 && (o = e), rt && (o = 2), rr) {
                if (e >= w ? k == "leftLoop" || k == "topLoop" ? o = e % s != 0 ? (e / s ^ 0) + 1 : e / s: (bt = e - w, o = 1 + parseInt(bt % s != 0 ? bt / s + 1 : bt / s), o <= 0 && (o = 1)) : o = 1, a.html(""), kt = "", i.autoPage == !0 || i.autoPage == "true") for (ut = 0; ut < o; ut++) kt += "<li>" + (ut + 1) + "<\/li>";
                else for (ut = 0; ut < o; ut++) kt += i.autoPage.replace("$", ut + 1);
                a.html(kt);
                a = a.children()
            }
            if (e >= w) {
                r.children().each(function() {
                    var i = n(this).find(".pic img").length && n(this).find(".pic img").eq(0) || !1,
                    r = n(this).find(".title"),
                    t = n(this);
                    di(i, r);
                    n(this).width() > ct && (ct = n(this).width(), p = n(this).outerWidth(!0));
                    n(this).height() > vt && (vt = n(this).height(), y = n(this).outerHeight(!0));
                    y == 0 && f.parent().show(0,
                    function() {
                        ct = t.width();
                        p = t.outerWidth(!0);
                        vt = t.height();
                        y = t.outerHeight(!0)
                    }).hide(0)
                });
                oi = r.children();
                dt = function() {
                    for (var n = 0; n < w; n++) oi.eq(n).clone().addClass("clone").appendTo(r);
                    for (n = 0; n < h; n++) oi.eq(e - n - 1).clone().addClass("clone").prependTo(r)
                };
                switch (k) {
                case "fold":
                    r.css({
                        position:
                        "relative",
                        width: p,
                        height: y
                    }).children().css({
                        position: "absolute",
                        width: ct,
                        left: 0,
                        top: 0,
                        display: "none"
                    });
                    break;
                case "top":
                    r.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; height:' + w * y + 'px"><\/div>').css({
                        top: -(u * s) * y,
                        position: "relative",
                        padding: "0",
                        margin: "0"
                    }).children().css({
                        height: vt
                    });
                    break;
                case "left":
                    r.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; width:' + w * p + 'px"><\/div>').css({
                        width: e * p,
                        left: -(u * s) * p,
                        position: "relative",
                        overflow: "hidden",
                        padding: "0",
                        margin: "0"
                    }).children().css({
                        float: "left",
                        width: ct
                    });
                    break;
                case "leftLoop":
                case "leftMarquee":
                    dt();
                    r.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; width:' + w * p + 'px"><\/div>').css({
                        width: (e + w + h) * p,
                        position: "relative",
                        overflow: "hidden",
                        padding: "0",
                        margin: "0",
                        left: -(h + u * s) * p
                    }).children().css({
                        float: "left",
                        width: ct
                    });
                    break;
                case "topLoop":
                case "topMarquee":
                    dt();
                    r.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; height:' + w * y + 'px"><\/div>').css({
                        height: (e + w + h) * y,
                        position: "relative",
                        padding: "0",
                        margin: "0",
                        top: -(h + u * s) * y
                    }).children().css({
                        height: vt
                    });
                    break;
                case "fullfold":
                    if (f.attr("data-effect") == "fullslider") switch (i.slideType) {
                    case "Img":
                        f.css("height", "auto");
                        break;
                    case "ImgBg":
                        f.attr("class", "mod-fullslider-bg");
                        et = i.bgcArr;
                        l = n.trim(i.itemHeight);
                        r.children().each(function(t) {
                            var i = n(this),
                            r = i.find(".link").eq(0);
                            i.addClass(" bgItem" + (t + 1) + " ");
                            t == 0 && i.css({
                                backgroundImage: "url(" + r.attr("ssrc") + ")",
                                backgroundPosition: "center 0",
                                backgroundRepeat: "no-repeat",
                                backgroundColor: et[t] || "#fff"
                            });
                            l && n.isNumeric(l) && l > 1 && (f.css("height", l), i.css("height", l))
                        })
                    }
                    r.css({
                        position: "relative",
                        width: "100%",
                        height: y
                    }).children().css({
                        position: "absolute",
                        width: "100%",
                        left: 0,
                        top: 0,
                        display: "none"
                    });
                    break;
                case "fullLeft":
                    if (f.attr("data-effect") == "fullslider") switch (i.slideType) {
                    case "Img":
                        f.css("height", "auto");
                        break;
                    case "ImgBg":
                        f.attr("class", "mod-fullslider-bg");
                        et = i.bgcArr;
                        l = n.trim(i.itemHeight);
                        r.children().each(function(t) {
                            var i = n(this),
                            r = i.find(".link").eq(0);
                            i.addClass(" bgItem" + (t + 1) + " ");
                            t == 0 && i.css({
                                backgroundImage: "url(" + r.attr("ssrc") + ")",
                                backgroundPosition: "center 0",
                                backgroundRepeat: "no-repeat",
                                backgroundColor: et[t] || "#fff"
                            });
                            l && n.isNumeric(l) && l > 1 && (f.css("height", l), i.css("height", l))
                        })
                    }
                    r.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; width:100%"><\/div>').css({
                        width: e * 100 + "%",
                        left: -u * 100 + "%",
                        position: "relative",
                        overflow: "hidden",
                        padding: "0",
                        margin: "0"
                    }).children().css({
                        float: "left",
                        width: parseFloat(100 / e) + "%"
                    });
                    break;
                case "fullLeftLoop":
                    if (f.attr("data-effect") == "fullslider") switch (i.slideType) {
                    case "Img":
                        f.css("height", "auto");
                        break;
                    case "ImgBg":
                        f.attr("class", "mod-fullslider-bg");
                        et = i.bgcArr;
                        l = n.trim(i.itemHeight);
                        r.children().each(function(t) {
                            var i = n(this),
                            r = i.find(".link").eq(0);
                            i.addClass(" bgItem" + (t + 1) + " ");
                            t == 0 && i.css({
                                backgroundImage: "url(" + r.attr("ssrc") + ")",
                                backgroundPosition: "center 0",
                                backgroundRepeat: "no-repeat",
                                backgroundColor: et[t] || "#fff"
                            });
                            l && n.isNumeric(l) && l > 1 && (f.css("height", l), i.css("height", l))
                        })
                    }
                    dt();
                    r.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; width:100%"><\/div>').css({
                        width: (e + w + h) * 100 + "%",
                        position: "relative",
                        overflow: "hidden",
                        padding: "0",
                        margin: "0",
                        left: -(h + u * s) * 100 + "%"
                    }).children().css({
                        float: "left",
                        width: parseFloat(100 / (e + 2)) + "%"
                    })
                }
            } else r.children().each(function() {
                var t = n(this).find(".pic img").length && n(this).find(".pic img").eq(0) || !1,
                i = n(this).find(".title");
                di(t, i)
            });
            var gt = function(n) {
                var t = n * s;
                return n == o ? t = e: n == -1 && e % s != 0 && (t = -e % s),
                t
            },
            gi = function(t) {
                var e = function(u) {
                    for (var s, o, e = u; e < w + u; e++) o = t.eq(e).find("img[" + v + "]").size() == 0 ? !1 : !0,
                    o && t.eq(e).find("img[" + v + "]").each(function() {
                        var i = n(this),
                        u,
                        t;
                        if (i.attr("src", i.attr(v)).removeAttr(v), r.find(".clone")[0]) for (u = r.children(), t = 0; t < u.size(); t++) u.eq(t).find("img[" + v + "]").each(function() {
                            n(this).attr(v) == i.attr("src") && n(this).attr("src", n(this).attr(v)).removeAttr(v)
                        })
                    });
                    if (f.attr("data-effect") == "fullslider" && i.slideType == "ImgBg") for (s = i.bgcArr, e = u; e < w + u; e++) o = t.eq(e).find(".link[" + v + "]").size() == 0 ? !1 : !0,
                    o && t.eq(e).find(".link[" + v + "]").each(function() {
                        var t = n(this),
                        u = t.parent(),
                        e = u.attr("class").indexOf("bgItem") + 6,
                        o = parseInt(u.attr("class").substr(e)) - 1,
                        f,
                        i;
                        if (u.css({
                            backgroundImage: "url(" + t.attr(v) + ")",
                            backgroundPosition: "center 0",
                            backgroundRepeat: "no-repeat",
                            backgroundColor: s[o] || "#fff"
                        }), t.removeAttr(v), r.find(".clone")[0]) for (f = r.children(), i = 0; i < f.size(); i++) f.eq(i).find(".link[" + v + "]").each(function() {
                            var i = n(this).parent(),
                            r = i.attr("class").indexOf("bgItem") + 6,
                            u = parseInt(i.attr("class").substr(r)) - 1;
                            n(this).attr(v) == i.css("backgroundImage") && (i.css({
                                backgroundImage: "url(" + t.attr(v) + ")",
                                backgroundPosition: "center 0",
                                backgroundRepeat: "no-repeat",
                                backgroundColor: s[u] || "#fff"
                            }), n(this).removeAttr(v))
                        })
                    })
                },
                o;
                switch (k) {
                case "fade":
                case "fold":
                case "fullfold":
                case "top":
                case "left":
                case "slideDown":
                    e(u * s);
                    break;
                case "leftLoop":
                case "fullLeftLoop":
                case "fullLeft":
                case "topLoop":
                    e(h + gt(ft));
                    break;
                case "leftMarquee":
                case "topMarquee":
                    var c = k == "leftMarquee" ? r.css("left").replace("px", "") : r.css("top").replace("px", ""),
                    l = k == "leftMarquee" ? p: y,
                    a = h;
                    c % l != 0 && (o = Math.abs(c / l ^ 0), a = u == 1 ? h + o: h + o - 1);
                    e(a)
                }
            },
            ot = function(n) {
                var i, f, t;
                if (!at || fi != u || n || rt) {
                    if (rt ? u >= 1 ? u = 1 : u <= 0 && (u = 0) : (ft = u, u >= o ? u = 0 : u < 0 && (u = o - 1)), ki(), v != null && gi(r.children()), st[0] && (g = st.eq(u), v != null && gi(st), k == "slideDown" ? (st.not(g).stop(!0, !0).slideUp(c), g.slideDown(c, b,
                    function() {
                        r[0] || d()
                    })) : (st.not(g).stop(!0, !0).hide(), g.animate({
                        opacity: "show"
                    },
                    c,
                    function() {
                        r[0] || d()
                    }))), e >= w) switch (k) {
                    case "fade":
                        r.children().stop(!0, !0).eq(u).animate({
                            opacity: "show"
                        },
                        c, b,
                        function() {
                            d()
                        }).siblings().hide();
                        break;
                    case "fold":
                        r.children().stop(!0, !0).eq(u).animate({
                            opacity: "show"
                        },
                        c, b,
                        function() {
                            d()
                        }).siblings().animate({
                            opacity: "hide"
                        },
                        c, b);
                        break;
                    case "top":
                        r.stop(!0, !1).animate({
                            top: -u * s * y
                        },
                        c, b,
                        function() {
                            d()
                        });
                        break;
                    case "left":
                        r.stop(!0, !1).animate({
                            left: -u * s * p
                        },
                        c, b,
                        function() {
                            d()
                        });
                        break;
                    case "leftLoop":
                        t = ft;
                        r.stop(!0, !0).animate({
                            left: -(gt(ft) + h) * p
                        },
                        c, b,
                        function() {
                            t <= -1 ? r.css("left", -(h + (o - 1) * s) * p) : t >= o && r.css("left", -h * p);
                            d()
                        });
                        break;
                    case "topLoop":
                        t = ft;
                        r.stop(!0, !0).animate({
                            top: -(gt(ft) + h) * y
                        },
                        c, b,
                        function() {
                            t <= -1 ? r.css("top", -(h + (o - 1) * s) * y) : t >= o && r.css("top", -h * y);
                            d()
                        });
                        break;
                    case "leftMarquee":
                        i = r.css("left").replace("px", "");
                        u == 0 ? r.animate({
                            left: ++i
                        },
                        0,
                        function() {
                            r.css("left").replace("px", "") >= 0 && r.css("left", -e * p)
                        }) : r.animate({
                            left: --i
                        },
                        0,
                        function() {
                            r.css("left").replace("px", "") <= -(e + h) * p && r.css("left", -h * p)
                        });
                        break;
                    case "topMarquee":
                        f = r.css("top").replace("px", "");
                        u == 0 ? r.animate({
                            top: ++f
                        },
                        0,
                        function() {
                            r.css("top").replace("px", "") >= 0 && r.css("top", -e * y)
                        }) : r.animate({
                            top: --f
                        },
                        0,
                        function() {
                            r.css("top").replace("px", "") <= -(e + h) * y && r.css("top", -h * y)
                        });
                        break;
                    case "fullfold":
                        r.children().stop(!0, !0).eq(u).animate({
                            opacity: "show"
                        },
                        c, b,
                        function() {
                            d()
                        }).siblings().animate({
                            opacity: "hide"
                        },
                        c, b);
                        break;
                    case "fullLeft":
                        r.stop(!0, !1).animate({
                            left: -u * s * 100 + "%"
                        },
                        c, b,
                        function() {
                            d()
                        });
                        break;
                    case "fullLeftLoop":
                        t = ft;
                        r.stop(!0, !0).animate({
                            left: -(gt(ft) + h) * 100 + "%"
                        },
                        c, b,
                        function() {
                            t <= -1 ? r.css("left", -(h + (o - 1) * s) * 100 + "%") : t >= o && r.css("left", -h * 100 + "%");
                            d()
                        })
                    }
                    a.removeClass(it).eq(u).addClass(it);
                    fi = u;
                    yt || (tt.removeClass("nextStop"), nt.removeClass("prevStop"), u == 0 && nt.addClass("prevStop"), u == o - 1 && tt.addClass("nextStop"));
                    tr.html("<span>" + (u + 1) + "<\/span>/" + o)
                }
            };
            at && ot(!0);
            e <= w && (nt.remove(), tt.remove(), n(i.titCell, f).parent().remove());
            i.showNavBar === !1 && (nt.remove(), tt.remove());
            pi && f.hover(function() {
                clearTimeout(wt)
            },
            function() {
                wt = setTimeout(function() {
                    u = bi;
                    at ? ot() : k == "slideDown" ? g.slideUp(c, ei) : g.animate({
                        opacity: "hide"
                    },
                    c, ei);
                    fi = u
                },
                300)
            });
            var si = function(n) {
                ht = setInterval(function() {
                    ri ? u--:u++;
                    ot()
                },
                !n ? ii: n)
            },
            ni = function(n) {
                ht = setInterval(ot, !n ? ii: n)
            },
            ti = function() {
                ui || (clearInterval(ht), si())
            },
            hi = function() { (yt || u != o - 1) && (u++, ot(), rt || ti())
            },
            ci = function() { (yt || u != 0) && (u--, ot(), rt || ti())
            },
            li = function() {
                clearInterval(ht);
                rt ? ni() : si();
                lt.removeClass("pauseState")
            },
            ai = function() {
                clearInterval(ht);
                lt.addClass("pauseState")
            };
            ir ? rt ? (ri ? u--:u++, ni(), ui && r.hover(ai, li)) : (si(), ui && f.hover(ai, li)) : (rt && (ri ? u--:u++), lt.addClass("pauseState"));
            lt.click(function() {
                lt.hasClass("pauseState") ? li() : ai()
            });
            i.trigger == "mouseover" ? a.hover(function() {
                var n = a.index(this);
                pt = setTimeout(function() {
                    u = n;
                    ot();
                    ti()
                },
                i.triggerTime)
            },
            function() {
                clearTimeout(pt)
            }) : a.click(function() {
                u = a.index(this);
                ot();
                ti()
            });
            rt ? (tt.mousedown(hi), nt.mousedown(ci), yt && (vi = function() {
                nr = setTimeout(function() {
                    clearInterval(ht);
                    ni(ii / 10 ^ 0)
                },
                150)
            },
            yi = function() {
                clearTimeout(nr);
                clearInterval(ht);
                ni()
            },
            tt.mousedown(vi), tt.mouseup(yi), nt.mousedown(vi), nt.mouseup(yi)), i.trigger == "mouseover" && (tt.hover(hi,
            function() {}), nt.hover(ci,
            function() {}))) : (tt.click(hi), nt.click(ci))
        })
    }
} (jQuery);
jQuery.easing.jswing = jQuery.easing.swing;
jQuery.extend(jQuery.easing, {
    def: "easeOutQuad",
    swing: function(n, t, i, r, u) {
        return jQuery.easing[jQuery.easing.def](n, t, i, r, u)
    },
    easeInQuad: function(n, t, i, r, u) {
        return r * (t /= u) * t + i
    },
    easeOutQuad: function(n, t, i, r, u) {
        return - r * (t /= u) * (t - 2) + i
    },
    easeInOutQuad: function(n, t, i, r, u) {
        return (t /= u / 2) < 1 ? r / 2 * t * t + i: -r / 2 * (--t * (t - 2) - 1) + i
    },
    easeInCubic: function(n, t, i, r, u) {
        return r * (t /= u) * t * t + i
    },
    easeOutCubic: function(n, t, i, r, u) {
        return r * ((t = t / u - 1) * t * t + 1) + i
    },
    easeInOutCubic: function(n, t, i, r, u) {
        return (t /= u / 2) < 1 ? r / 2 * t * t * t + i: r / 2 * ((t -= 2) * t * t + 2) + i
    },
    easeInQuart: function(n, t, i, r, u) {
        return r * (t /= u) * t * t * t + i
    },
    easeOutQuart: function(n, t, i, r, u) {
        return - r * ((t = t / u - 1) * t * t * t - 1) + i
    },
    easeInOutQuart: function(n, t, i, r, u) {
        return (t /= u / 2) < 1 ? r / 2 * t * t * t * t + i: -r / 2 * ((t -= 2) * t * t * t - 2) + i
    },
    easeInQuint: function(n, t, i, r, u) {
        return r * (t /= u) * t * t * t * t + i
    },
    easeOutQuint: function(n, t, i, r, u) {
        return r * ((t = t / u - 1) * t * t * t * t + 1) + i
    },
    easeInOutQuint: function(n, t, i, r, u) {
        return (t /= u / 2) < 1 ? r / 2 * t * t * t * t * t + i: r / 2 * ((t -= 2) * t * t * t * t + 2) + i
    },
    easeInSine: function(n, t, i, r, u) {
        return - r * Math.cos(t / u * (Math.PI / 2)) + r + i
    },
    easeOutSine: function(n, t, i, r, u) {
        return r * Math.sin(t / u * (Math.PI / 2)) + i
    },
    easeInOutSine: function(n, t, i, r, u) {
        return - r / 2 * (Math.cos(Math.PI * t / u) - 1) + i
    },
    easeInExpo: function(n, t, i, r, u) {
        return t == 0 ? i: r * Math.pow(2, 10 * (t / u - 1)) + i
    },
    easeOutExpo: function(n, t, i, r, u) {
        return t == u ? i + r: r * ( - Math.pow(2, -10 * t / u) + 1) + i
    },
    easeInOutExpo: function(n, t, i, r, u) {
        return t == 0 ? i: t == u ? i + r: (t /= u / 2) < 1 ? r / 2 * Math.pow(2, 10 * (t - 1)) + i: r / 2 * ( - Math.pow(2, -10 * --t) + 2) + i
    },
    easeInCirc: function(n, t, i, r, u) {
        return - r * (Math.sqrt(1 - (t /= u) * t) - 1) + i
    },
    easeOutCirc: function(n, t, i, r, u) {
        return r * Math.sqrt(1 - (t = t / u - 1) * t) + i
    },
    easeInOutCirc: function(n, t, i, r, u) {
        return (t /= u / 2) < 1 ? -r / 2 * (Math.sqrt(1 - t * t) - 1) + i: r / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + i
    },
    easeInElastic: function(n, t, i, r, u) {
        var f = 1.70158,
        e = 0,
        o = r;
        return t == 0 ? i: (t /= u) == 1 ? i + r: (e || (e = u * .3), o < Math.abs(r) ? (o = r, f = e / 4) : f = e / (2 * Math.PI) * Math.asin(r / o), -(o * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * u - f) * 2 * Math.PI / e)) + i)
    },
    easeOutElastic: function(n, t, i, r, u) {
        var f = 1.70158,
        e = 0,
        o = r;
        return t == 0 ? i: (t /= u) == 1 ? i + r: (e || (e = u * .3), o < Math.abs(r) ? (o = r, f = e / 4) : f = e / (2 * Math.PI) * Math.asin(r / o), o * Math.pow(2, -10 * t) * Math.sin((t * u - f) * 2 * Math.PI / e) + r + i)
    },
    easeInOutElastic: function(n, t, i, r, u) {
        var f = 1.70158,
        e = 0,
        o = r;
        return t == 0 ? i: (t /= u / 2) == 2 ? i + r: (e || (e = u * .3 * 1.5), o < Math.abs(r) ? (o = r, f = e / 4) : f = e / (2 * Math.PI) * Math.asin(r / o), t < 1) ? -.5 * o * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * u - f) * 2 * Math.PI / e) + i: o * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * u - f) * 2 * Math.PI / e) * .5 + r + i
    },
    easeInBack: function(n, t, i, r, u, f) {
        return f == undefined && (f = 1.70158),
        r * (t /= u) * t * ((f + 1) * t - f) + i
    },
    easeOutBack: function(n, t, i, r, u, f) {
        return f == undefined && (f = 1.70158),
        r * ((t = t / u - 1) * t * ((f + 1) * t + f) + 1) + i
    },
    easeInOutBack: function(n, t, i, r, u, f) {
        return (f == undefined && (f = 1.70158), (t /= u / 2) < 1) ? r / 2 * t * t * (((f *= 1.525) + 1) * t - f) + i: r / 2 * ((t -= 2) * t * (((f *= 1.525) + 1) * t + f) + 2) + i
    },
    easeInBounce: function(n, t, i, r, u) {
        return r - jQuery.easing.easeOutBounce(n, u - t, 0, r, u) + i
    },
    easeOutBounce: function(n, t, i, r, u) {
        return (t /= u) < 1 / 2.75 ? r * 7.5625 * t * t + i: t < 2 / 2.75 ? r * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + i: t < 2.5 / 2.75 ? r * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + i: r * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + i
    },
    easeInOutBounce: function(n, t, i, r, u) {
        return t < u / 2 ? jQuery.easing.easeInBounce(n, t * 2, 0, r, u) * .5 + i: jQuery.easing.easeOutBounce(n, t * 2 - u, 0, r, u) * .5 + r * .5 + i
    }
}),
function(n, t, i, r) {
    template.config("escape", !1);
    template.helper("loadHTML",
    function(n) {
        var i = {},
        t, r, u;
        return i = n.SubModules != null ? {
            Request: null,
            OutModule: n,
            ErrorMsg: null,
            Modules: n.SubModules,
            Target: "_blank"
        }: {
            Request: null,
            OutModule: null,
            ErrorMsg: null,
            Modules: [n],
            Target: "_blank"
        },
        n.ModuleTemplate != null && (t = n.ModuleTemplate.Html, t != null) ? (r = template.compile(t), u = r(i), u) : void 0
    });
    template.helper("filterSnapShort",
    function(n) {
        return n.replace(/<正>/, "")
    });
    template.helper("loadTab",
    function(parameset, i, level, pi) {
        var tabNames, outArr, arr;
        if (parameset != null && parameset != "") {
            if (parameset = eval("(" + parameset + ")"), level == 1) return tabNames = parameset.oneLevelTabName,
            tabNames = tabNames.replace(/，/g, ","),
            arr = tabNames.split(","),
            arr[i] != r && arr[i] != "" ? arr[i] : "选项卡" + (i + 1);
            if (console.log(pi), tabNames = parameset.TwoLevelTabName, tabNames = tabNames.replace(/，/g, ","), tabNames = tabNames.replace(/；/g, ";"), outArr = tabNames.split(";"), outArr[pi] != r && n.trim(outArr[pi]) != "") {
                if (arr = outArr[pi].split(","), arr[i] != r && arr[i] != "") return arr[i]
            } else return "选项卡" + (i + 1)
        } else return "选项卡" + (i + 1)
    });
    template.helper("GetNaviParams",
    function(modules) {
        var newParams, params;
        if (modules != null) {
            if (modules.length == 1) return modules[0].Params;
            var clsDb = "",
            dbCode = "",
            notShowNodes = "";
            return n.each(modules,
            function(i, m) {
                var p = eval("(" + m.ParamSet + ")");
                clsDb == "" ? clsDb = p.clsDb: clsDb += "," + p.clsDb;
                i == 0 && (dbCode = p.dbcode);
                n.trim(p.notShowNodes) != "" && (notShowNodes == "" ? notShowNodes = p.notShowNodes: notShowNodes += "," + p.notShowNodes)
            }),
            newParams = {
                clsDb: clsDb,
                dbCode: dbCode,
                notShowNodes: notShowNodes
            },
            params = n.extend({},
            eval("(" + modules[0].ParamSet + ")"), newParams),
            JSON.stringify(params)
        }
    });
    n.extend({
        Domain: "http://10.89.145.250",
        IsDES: !0,
        DESKey: "CNKI@CNKI.NET",
        MyExpando: function() {
            return "My" + ("" + Math.random()).replace(/\D/g, "")
        },
        MyAjax: function(t, i, r, u, f, e, o) {
            n.ajax({
                url: t,
                data: i,
                type: r,
                cache: f,
                dataType: u,
                success: e,
                error: o
            })
        },
        Preview: function(t, f) {
            var e = arguments; (function(e, o) {
                n("script:contains('" + t + "',)").length == 1 ? n("script:contains('" + t + "',)").eq(0).after("<div id=" + e + " style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示：数据加载中，请稍后...<\/div>") : i.write("<div id=" + e + " style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示：数据加载中，请稍后...<\/div>");
                n.MyAjax(config.Domain + "/HYHPSContent/Module/GetData", {
                    ModuleID: t,
                    PvuCode: n.trim(f),
                    PreView: !0
                },
                "get", "jsonp", !0,
                function(n) {
                    u.DataIn(n, e, o, r, !0)
                })
            })(n.MyExpando(), e)
        },
        SmartContent: function(t, r, f) {
            var e = arguments; (function(e, o) {
                f ? f.after("<div id=" + e + " style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示：数据加载中，请稍后...<\/div>") : n("script[data-fn][id='" + t + "']").length == 1 ? n("script[data-fn][id='" + t + "']").eq(0).after("<div id=" + e + " style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示：数据加载中，请稍后...<\/div>") : i.write("<div id=" + e + " style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示：数据加载中，请稍后...<\/div>");
                n.MyAjax(config.Domain + "/HYHPSContent/Module/GetData", {
                    ModuleID: t,
                    PvuCode: n.trim(r)
                },
                "get", "jsonp", !0,
                function(n) {
                    for(var i of n.Modules){
                        if(i.Items){
                            for(var j of i.Items){
                                j.Link=config.Domain+j.Link;
                            }
                        }
                        
                    }
                    u.DataIn(n, e, o)       
   
                },
                function() {
                    n("#" + e).html('<span class="loadedMsg" data-rid="' + t + '" style="display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;">温馨提示：数据加载失败！<\/span>')
               
                })
            })(n.MyExpando(), e)
        },
        LoadModuleItem: function(t, i, r, u) {
            u && (u.html('<span class="loadedMsg" data-rid="' + t + '" style="display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;">温馨提示：数据加载中，请稍后...<\/span>'), n.MyAjax(config.Domain + "/HYHPSContent/Module/GetDataItem", {
                ModuleID: t,
                TempletCode: i,
                PvuCode: n.trim(r),
                PreView: n.trim(n.QueryString("PreView")) == "true" ? !0 : !1
            },
            "get", "jsonp", !0,
            function(t) {
                if (t && t.ErrorMsg == null && t.Module && n.trim(t.ItemTemplateHtml) != "") {
                        for(let h of t.Module.Items){
                          h.Link=config.Domain+h.Link;
                        }
                            
                    var i = t.ItemTemplateHtml,
                    r = template.compile(i),
                    f = r(t);
                    u.html(f)
                }
            },
            function() {
                u.html('<span class="loadedMsg" data-rid="' + t + '" style="display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;">温馨提示：数据加载失败！<\/span>')
            }))
        },
        PostSumit: function(t, r, u, f) {
            var e, o;
            if (n("#" + t).remove(), e = n("<form>"), e[0].action = r, e[0].method = "post", e[0].target = f, e[0].style.display = "none", e[0].id = t, u) for (o = 0; o < u.length; o++) e[0].appendChild(u[o]);
            return i.body.appendChild(e[0]),
            e[0].submit(),
            e
        },
        CreateElement: function(n, t, r, u) {
            var f = i.createElement(n);
            return f.type = t,
            f.name = r,
            f.value = u,
            f
        },
        LoadHeaderFooter: function(t, i, r, u) {
            r = r || "#DivPageHeader";
            u = u || "#DivPageFooter";
            var f = r + "," + u;
            n(f).html('<span class="loadedMsg" style="display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;">温馨提示：头尾内容加载中，请稍候...<\/span>');
            n.MyAjax(config.Domain + "/HYHPSContent/Resource/GetUnDetailPageHfContent", {
                pvuCode: t,
                fileName: i
            },
            "get", "jsonp", !0,
            function(t) {
                if (t != null) {
                    var i = n(r),
                    f = n(u);
                    n(t.HeaderContent).insertBefore(i);
                    n(t.FooterContent).insertBefore(f);
                    i.empty();
                    f.empty()
                }
            },
            function() {
                n(f).html('<span class="loadedMsg" style="display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;">温馨提示：头尾内容加载失败！<\/span>')
            })
        },
        QueryString: function(n) {
            var r = new RegExp("(^|&)" + n + "=([^&]*)(&|$)", "i"),
            i = t.location.search.substr(1).match(r);
            return i != null ? unescape(decodeURI(i[2])) : null
        },
        GetDBCount: function(t, i, r, u) {
            r = n.IsDES == !0 ? encMe(r, n.DESKey) : encodeURIComponent(r);
            var f = {
                ProductCode: t,
                DB: i,
                Where: r
            };
            n.ajax({
                url: config.Domain + "/HYHPSContent/Module/GetCount",
                data: f,
                timeout: 3e4,
                type: "get",
                cache: !0,
                dataType: "jsonp",
                success: u,
                error: function() {
                    alert("数据加载失败,查看是否参数错误！")
                }
            })
        }
    });
    var u = function(t, i, u, f, e) {
        this.ID = t;
        u.obj && (this.Render = u.obj);
        i.Target = u.moduleDisplayParams && u.moduleDisplayParams.target ? u.moduleDisplayParams.target: "_blank";
        this.CurData = i;
        this.ModuleTemplate = u && u.customModuleType ? u.customModuleType: i.OutModule ? i.OutModule.ModuleTemplate: i.Modules[0].ModuleTemplate;
        this.OnError = u.moduleDisplayParams && u.moduleDisplayParams.onerror ? u.moduleDisplayParams.onerror: "";
        this.TipImg = u.moduleDisplayParams && u.moduleDisplayParams.tipimg ? u.moduleDisplayParams.tipimg: "";
        this.IsPreview = e;
        this.BindEvent = r;
        this.Html = "";
        this.Params = u;
        this.Init();
        this.OutPut();
        f && n.isFunction(f) && n.isEmptyObject(f) && f()
    };
    u.DataIn = function(t, i, r, f, e) {

        if (t && (t.Modules && t.Modules.length > 0 || t.OutModule) && t.ErrorMsg == null) try {
            new u(i, t, r, f, e);
            n("#" + i).remove();
            r.obj && r.obj.siblings("[id^='My']").remove()
        } catch(o) {
            n("#" + i).parent("div").append("<div class='list_module serviceMsg' data-rid='" + r.moduleID + "' style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示:模块类型的模板或绑定的事件有错！<\/div>");
            n("#" + i).remove()
        } else t && t.Modules && t.Modules.length == 0 && t.ErrorMsg != null ? (n("#" + i).parent("div").append("<span class='list_module serviceMsg' data-rid='" + r.moduleID + "' style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示:" + t.ErrorMsg + "  请联系系统管理员！<\/span>"), n("#" + i).remove()) : (n("#" + i).parent("div").append("<div class='list_module serviceMsg' data-rid='" + r.moduleID + "' style='display: block;padding-top: 20px;width: 100%;text-align: center;color: #333;'>温馨提示:无此模块，请检查产品模块关系id值！<\/div>"), n("#" + i).remove())
    };
    u.prototype = {
        Init: function() {
            var source, render, html;
            this.CurData && (source = this.ModuleTemplate.Html, this.ModuleTemplate.BindEvent != null && this.ModuleTemplate.BindEvent != "" && (this.BindEvent = eval("0,(" + this.ModuleTemplate.BindEvent + ")")), render = template.compile(source), html = render(this.CurData), html = html.replace(/templatescript/g, "script"), this.IsPreview && this.ModuleTemplate.PreviewStyle != null && n.trim(this.ModuleTemplate.PreviewStyle) != "" && (html = '<div  class="main wrap" style="' + this.ModuleTemplate.PreviewStyle + '">' + html + "<\/div>"), this.Html = html)
        },
        OutPut: function() {
            this.Html && (this.Render ? this.Render.after(this.Html) : n("#" + this.ID).before(this.Html));
            this.BindEvent != null && this.BindEvent != "" && this.BindEvent(n("#" + this.ID).parent())
        }
    };
    n.fn.loadTree = function() {
        n(this).treeModule({
            baseUrl: config.Domain
        },
        function() {
            this.tabModule({
                curClass: "cur",
                enentType: "click"
            })
        })
    };
    n.fn.loadSearchEvent = function() {
        var t = n(this),
        u = t.find(".secnav").size() == 0 ? !1 : !0,
        i = n.trim(t.attr("data-code")),
        f = n(".pholder", t),
        e,
        o;
        f.size() > 0 && n.SetPlaceHolder(f);
        u ? (e = t.attr("data-event") == "" || t.attr("data-event") == r ? "click": t.attr("data-event"), n.BindSearch(t, e), o = n(".search-g-link", t).size() > 0 ? !0 : !1, o && n.AdvancedSearch(n(".search-g-link", t), t, ".current", "/KNS/brief/result.aspx?dbprefix=")) : u == !1 && i != r && i != "" && n.SearchAll(t, i, ".secbtn", ".inputtext")
    };
    n.fn.xbAccordion = function(t) {
        var r = n(this),
        i = n.extend({
            eventType: "click",
            btnClass: ".icon-opt",
            activeClass: "mod-open",
            itemSelector: "[data-target='accorItem']"
        },
        t),
        u = r.find(i.itemSelector);
        if (u.length) r.on(i.eventType, i.btnClass,
        function() {
            var t = n(this),
            r = t.parent(),
            u = t.attr("class").indexOf(n.trim(i.activeClass)) == -1 ? !0 : !1;
            u && r.addClass(" " + n.trim(i.activeClass) + " ").siblings().removeClass(n.trim(i.activeClass))
        })
    }
} (jQuery, window, document),
function(n) {
    n.fn.floating = function(t) {
        var i = {},
        r = {
            init: function() {
                r.destroy.apply(this);
                r.update.apply(this, i)
            },
            destroy: function() {
                n(this).find(".parts-floating").remove()
            },
            update: function() {
                var t = n("<div/>"),
                u = n("<ul/>");
                r.destroy.apply(this); (i.state || i.moveState) && (t.addClass("parts-floating"), t.attr("data-position", i.position), t.attr("data-theme", i.theme), t.attr("data-size", i.size), t.attr("data-state", i.state), t.attr("data-moveState", i.moveState), n.each(i.account,
                function(t, i) {
                    var r = n("<li/>"),
                    f,
                    e;
                    i.url ? r.append('<a href="' + i.url + '" target="_blank"><span class="iconfont"><\/span><\/a>') : r.append('<span class="iconfont"><\/span>');
                    i.test && r.attr("title", i.test);
                    i.tip && (f = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/, e = new RegExp(f), f = e.test(i.tip) ? '<img src="' + i.tip + '">': i.tip, r.append('<div class="tooltip parts-li-tip"><div class="tooltip-inner">' + f + "<\/div><\/div>"));
                    r.attr("data-type", i.type);
                    i.type == "Top" && r.click(function() {
                        n("body,html,nav.top_nav ~ .section").animate({
                            scrollTop: 0
                        },
                        500)
                    });
                    r.appendTo(u)
                }), n.each(i.tip,
                function(n, t) {
                    u.find(".parts-li-tip .tooltip-inner").css(n, t)
                }), u.appendTo(t), n(this).append(t))
            }
        };
        return r[t] ? r[t].apply(this, Array.prototype.slice.call(arguments, 1)) : typeof t == "object" || !t ? (i = n.extend(i, t), r.init.apply(this, i)) : void 0
    }
} (jQuery),
function() {
    function c(n) {
        var i = new RegExp("(^|&)" + n + "=([^&]*)(&|$)"),
        t = window.location.search.substr(1).match(i);
        return t != null ? t[2] : ""
    }
    var h = window.location,
    i = h.protocol + "//" + h.host,
    r, u, f, e, o, s, n, t, l;
    i = /(file:\/\/)|127.0.0.1|localhost/ig.exec(i) ? "": "";
    $.extend({
        //i
        Domain: "http://10.89.145.250",
        CustomerHandle: function(n, t, i) {
            var r = {
                ModuleID: n,
                DataParams: t
            };
            $.ajax({
                url: $.Domain + "/HYHPSContent/Module/GetData",
                data: r,
                timeout: 3e4,
                type: "get",
                cache: !0,
                dataType: "jsonp",
                success: i,
                error: function() {
                    alert("数据加载失败,查看是否参数错误！")
                }
            })
        }
    });
    r = $("#lgarea").size() > 0 ? !0 : !1;
    r && $("#lgarea").attr("data-lg") != "outFn" && $.GetLoginStatus("#lgarea");
    r && $("#lgarea").attr("data-lg") != "outFn" && $.BindEvent("#lgarea", "#logingbutt", "#ipbutt");
    u = $('.mod-search:has([name="searchtexthid"])');
    u && u.each(function() {
        var n = $(this),
        i = n.find(".secnav").size() == 0 ? !1 : !0,
        t = $.trim(n.attr("data-code")),
        r = $(".pholder", n),
        u,
        f;
        r.size() > 0 && $.SetPlaceHolder(r);
        i ? (u = n.attr("data-event") == "" || n.attr("data-event") == undefined ? "click": n.attr("data-event"), $.BindSearch(n, u), f = $(".search-g-link", n).size() > 0 ? !0 : !1, f && $.AdvancedSearch($(".search-g-link", n), n, ".current", "/KNS/brief/result.aspx?dbprefix=")) : i == !1 && t != undefined && t != "" && $.SearchAll(n, t, ".secbtn", ".inputtext")
    });
    $.PubSearch && $.PubSearch(".J_pubsearch", ".pub-txt", ".pub-btn");
    $.SearchScholar && $.SearchScholar("#J_searchScholar");
    $("img[ssrc]").each(function() {
        var n = $(this);
        n.attr("src", n.attr("ssrc"))
    });
    $('[data-effect="mod-tab"]').tabModule({
        enentType: "click"
    });
    $('[data-effect="mod-tab-slider"]').tabModule({
        curClass: "cur",
        enentType: "click"
    },
    function() {
        var n = $(this),
        t = n.index();
        $.each(n.parent('[data-effect="mod-tab-slider"]').find(".mod-tab-bd"),
        function(n, i) {
            var r = $(i).data("osliderCom");
            clearInterval(r.timer);
            r.timer = null;
            t == n && (r.autoScroll(), $(i).data("osliderCom", r))
        })
    });
    $('[data-effect="mod-slider"]').osliderCom();
    $('[data-effect="treeMod"]').treeModule({
        baseUrl: $.Domain
    },
    function() {
        this.tabModule({
            curClass: "cur",
            enentType: "click"
        })
    });
    f = $('[data-effect="mImgSlide"]');
    f && f.each(function() {
        var r = $(this),
        t = r.attr("data-opts").split(","),
        e = t.length,
        u = null,
        n = null,
        f = {},
        i;
        if (t && e > 0) {
            for (i = 0; i < t.length; i++) u = t[i].split(":")[0],
            n = t[i].split(":")[1],
            n = /^[-\+]?\d+$/.test(n) == !0 ? parseInt(n) : n,
            f[u] = n == "false" ? !1 : n == "true" ? !0 : n;
            r.mScrollSuper(f)
        }
    });
    e = $('[data-effect="mTxtSlide"]');
    e && e.each(function() {
        var r = $(this),
        t = r.attr("data-opts").split(","),
        e = t.length,
        u = null,
        n = null,
        f = {},
        i;
        if (t && e > 0) {
            for (i = 0; i < t.length; i++) u = t[i].split(":")[0],
            n = t[i].split(":")[1],
            n = /^[-\+]?\d+$/.test(n) == !0 ? parseInt(n) : n,
            f[u] = n == "false" ? !1 : n == "true" ? !0 : n;
            r.mScrollSuper(f)
        }
    });
    o = $('[data-effect="fullslider"]');
    o.length && o.each(function() {
        var r = $(this),
        t = r.attr("data-opts").split(","),
        e = t.length,
        u = null,
        n = null,
        f = {},
        o = /^;(\W*\w*\{\})+\w*\W*\S*;$/ig,
        i;
        if (t && e > 0) {
            for (i = 0; i < t.length; i++) u = t[i].split(":")[0],
            n = t[i].split(":")[1],
            n = o.test(n) == !0 ? n.replace(/;/g, "").replace(/{}/g, ",").split(",") : n,
            n = /^[-\+]?\d+$/.test(n) == !0 ? parseInt(n) : n,
            f[u] = n == "false" ? !1 : n == "true" ? !0 : n;
            r.mScrollSuper(f)
        }
    });
    s = $('[data-effect="mScroll"]');
    s.length && s.each(function() {
        var u = $(this),
        i = u.attr("data-opts").split(","),
        e = i.length,
        f = null,
        n = null,
        t = {},
        r;
        if (i && e > 0) {
            for (r = 0; r < i.length; r++) f = i[r].split(":")[0],
            n = i[r].split(":")[1],
            n = /^[-\+]?\d+$/.test(n) == !0 ? parseInt(n) : n,
            t[f] = n == "false" ? !1 : n == "true" ? !0 : n;
            u.find(t.prevCell + "," + t.nextCell).fadeOut();
            u.hover(function() {
                $(this).find(t.prevCell + "," + t.nextCell).stop(!0, !0).fadeTo("show", .8)
            },
            function() {
                $(this).find(t.prevCell + "," + t.nextCell).fadeOut()
            });
            u.mScrollSuper(t)
        }
    });
    $('[data-effect="mod-accordion"]').xbAccordion();
    $(document).on("loadData", "[data-fn]",
    function(e) {
        var _this = $(this),
        fFnStr = _this.attr("data-fn"),
        oscriptId = _this.attr("id"),
        reg = /^[a-z0-9]{1}([a-z0-9]|[-]){35}$/g,
        flag = reg.test(fFnStr),
        str1 = null,
        strOptionalParams = _this.attr("data-params");
        _this.next().is("[data-mod]") || flag && (str1 = fFnStr == undefined || fFnStr == "" ? "": ("obj" + Math.random() * 100).replace(".", ""), oscriptId == undefined && _this.attr("id", str1), strOptionalParams != null && strOptionalParams != "" ? $.SmartContent(fFnStr, n, _this, null, eval("(" + strOptionalParams + ")")) : $.SmartContent(fFnStr, n, _this))
    });
    $(document).on("click", ".btn-reload",
    function(n) {
        n.preventDefault();
        var t = $(this),
        i = t.attr("data-relation");
        $("#" + i).trigger("loadData");
        t.parent().remove()
    });
    n = "";
    $.ajax({
        url: "config.json",
        type: "GET",
        dataType: "json",
        async: !1,
        success: function(t) {
            n = t.PvuCode
        },
        error: function() {
            n = c("pvucode")
        }
    });
    $("body").data("RequestPvuCode", $.trim(n));
    t = "";
    $.trim(location.pathname) != "" && (t = location.pathname.substr(location.pathname.lastIndexOf("/") + 1), $.trim(t) == "" && (t = "index.html"));
    l = c("ispageedit") == "1" ? !0 : !1;
    l || $.trim(n) == "" || $.trim(t) == "" || $.LoadHeaderFooter(n, t);
    $('[data-fn]:not([data-delay="yes"])').trigger("loadData")
} (jQuery),
function(n) {
    var t = function() {};
    t.ProductAccessStatistics = function() {
        n.ajax({
            url: config.Domain + "/HYHPSContent/Product/ProductStatistics?t=" + (new Date).getTime(),
            data: {
                hostname: location.hostname
            },
            timeout: 3e4,
            type: "get",
            cache: !0,
            dataType: "jsonp",
            error: function() {}
        })
    };
    window.AccessStatistics = t
} (jQuery);
$(function() {
    AccessStatistics.ProductAccessStatistics()
})