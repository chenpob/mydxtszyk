(function (moduleName4Html, global, factory) {
    if (typeof module !== 'undefined' && typeof exports === 'object') {//CommonJS(NodeJS)
        module.exports = factory(global);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {//AMD   CMD
        define( function () { factory(global); } );
    } else {//ES(Browser)
        global[moduleName4Html] = factory(global);
    }
}("start", this, function (global) {
    'use strict';
    var start = {
        init: function (url, index) {
            var xhr = null;
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();//IE7, Firefox, Opera, etc.
            }
            else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");//IE6, IE5
            }

            if (xhr != null) {
                xhr.open("GET", url, false);
                xhr.send(null);

                document.getElementsByTagName('script')[index].outerHTML = "";
                document.write(xhr.responseText);
            }
            else {
                alert("Your browser does not support XMLHTTP.");
            }
        }
    }

    start.init("/shared/shared.html", 0);

    return null;
}));
