(function (moduleName4Html, global, factory) {
    if (typeof module !== 'undefined' && typeof exports === 'object') {//CommonJS(NodeJS)
        module.exports = factory(global);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {//AMD   CMD
        define( function () { factory(global); } );
    } else {//ES(Browser)
        global[moduleName4Html] = factory(global);
    }
}("base", this, function (global) {
    'use strict';
    var htmlEncodeMap = { '<': '&lt;', '>': '&gt;', '&': '&amp;', '"': '&quot;' };
    var htmlDecodeMap = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
    var base = {
        patrn : /[`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘'，。、]/im,
        htmlEncode: function (htmlStr) {
            return htmlStr.replace(
                /[<>&"]/g,
                function (c) {
                    return htmlEncodeMap[c];
                }
            );
        },

        htmlDecode: function (str) {
            return str.replace(
                /&(lt|gt|nbsp|amp|quot);/ig,
                function (all, t) {
                    return htmlDecodeMap[t];
                }
            );
        }, 
        getQueryParam: function (paramName) {
            var regExp = new RegExp("(^|&)" + paramName + "=([^&]*)(&|$)", "i");
            var regExpMatchArray = window.location.search.substr(1).match(regExp);
            return !!regExpMatchArray ? decodeURI(regExpMatchArray[2]) : null;
        },

        getQueryParams: function () {
            var res = {};
            if (window.location.search) {
                var arr = window.location.search.substr(1).split('&');
                //for (var item of arr) { for of 不兼容IE8
                for (var i = 0; i < arr.length; i++) {
                    //var [key, value] = arr[i].split("=");
                    var temp = arr[i].split("=");
                    res[temp[0]] = temp[1];
                }
            }
            return res;
        },
        replaceWord:function(word){
          return  word.replace(/\\/g, "\\\\").replace(/"/g, '\\"').replace(/'/g, "\\'");
        },
        strlen:function(str) {
            var len = 0;
            for (var i = 0; i < str.length; i++) {
                var c = str.charCodeAt(i);
                //单字节加1 
                if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
                    len++;
                }
                else {
                    len += 2;
                }
            }
            return len;
        },
        cutStrByByte:function(str, l ){
            var result = '',
                strLen = str.length, // 字符串长度
                chrLen = str.replace(/[^\x00-\xff]/g, '**').length // 字节长度
            if (chrLen <= l) {
                return str
            }
            var regu = "^[ ]+$"
            var re = new RegExp(regu)
            for (var i = 0, j = 0; i < strLen; i++) {
                var chr = str.charAt(i)
                if (/[\x00-\xff]/.test(chr)) {
                        j++ // ascii码为0-255，一个字符就是一个字节的长度
                } else {
                    j += 2 // ascii码为0-255以外，一个字符就是两个字节的长度
                }
                if (j <= l) { // 当加上当前字符以后，如果总字节长度小于等于l，则将当前字符真实的+在result后
                    result += chr
                } else { // 反之则说明result已经是不拆分字符的情况下最接近l的值了，直接返回
                    return result
                }
            }
        },
        overflow:function(str, id, height){
            var ele = document.getElementById(id);
            if (ele && str) {
                var middle;
                var left = 1;
                var right = str.length;
                while(left !== right) {
                    middle = Math.ceil((left + right) / 2);
                    ele.innerHTML = str.substr(0, middle);
                    if (ele.offsetHeight <= height) {
                        left = middle;
                    } else {
                        right = middle - 1;
                    }
                }
                if ( left < str.length){
                    return str.substr(0, left <= 3 ? left : left - 2) + "...";
                }
            }

                   return str;
           }
       }
    return base;
}));
