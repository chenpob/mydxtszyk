package web.utils;

import web.entity.CodeTreeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/1 14:45
 * @Version 1.0
 */
public class DatabaseTreeNode {


    /**
     * 关系型数据库,组建形数据结构
     * @param parentId
     * @param treesList
     * @return
     */
    public   static List<CodeTreeEntity> getTreeNode(String parentId, List<CodeTreeEntity> treesList) {
        List<CodeTreeEntity> newTrees = new ArrayList<>();
        for (CodeTreeEntity tree : treesList) {
            if (parentId.equals(tree.getPid())) {
                // 递归获取子节点下的子节点，
                tree.setChildTreeList(getTreeNode(tree.getSysCode(), treesList));
                newTrees.add(tree);
            }
        }
        return newTrees;
    }

    /**
     * 非关系型数据库(没有pid),组建树形数据结构
     * @param parentId
     * @param treesList
     * @return
     */
    public  static List<CodeTreeEntity> getTreeNode_NoRelation(String parentId,List<CodeTreeEntity> treesList) {
        List<CodeTreeEntity> newTreesList = DatabaseTreeNode.setPid(treesList);
        List<CodeTreeEntity> overTreeList = DatabaseTreeNode.getTreeNode(parentId, newTreesList);
        return overTreeList;
    }

    /**
     * 非关系型数据库构造父id
     * @param beanList
     * @return
     */
    public static List<CodeTreeEntity> setPid(List<CodeTreeEntity> beanList) {
        for (CodeTreeEntity bean : beanList) {
            String myGrade = bean.getGrade();
            if (myGrade.equals("1")) {
                bean.setPid("0");
            }
            if (myGrade.equals("2")) {
                bean.setPid(bean.getSysCode().substring(0, 4));
            }
            if (myGrade.equals("3")) {
                bean.setPid(bean.getSysCode().substring(0, 8));
            }
        }
        return beanList;
    }
}
