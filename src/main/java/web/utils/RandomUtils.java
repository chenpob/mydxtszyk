package web.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class RandomUtils {
    public static String getRandomString(String suffix){
        String uuid = UUID.randomUUID().toString();
        String time  = "yyyy-MM-dd HH:mm:ss";
        Date now = new Date();
        SimpleDateFormat df = new SimpleDateFormat(time);
        String nowStr = df.format(now);
        String result = uuid+nowStr +"."+suffix;
        String finalStr = result.replaceAll("-","a").replaceAll(" ","b").replaceAll(":","c");
        return finalStr;
    }

    public static String getDirByName(String sFileName){
        //参数判空
        if (sFileName == "" || sFileName == null){
            return null;
        }

        long randomDirName = 0;

        for (int i = 0; i < sFileName.length(); i++) {
            int item = sFileName.charAt(i);
            String unicode = Integer.toHexString(item);
            long tempLong = Long.parseLong(unicode,16);
            randomDirName += tempLong;
        }
        //res = num%1000
        randomDirName =  randomDirName % 1000;
        return Long.toString(randomDirName);
    }

    /**
     * 返回当前时间的字符串
     * @return 当前时间的指定格式的字符串
     */
    public static String nowStr (String pat){
        if (pat==""){
            pat = "yyyy-MM-dd HH:mm:ss";
        }
        Date now = new Date();
        SimpleDateFormat df = new SimpleDateFormat(pat);
        String nowStr = df.format(now);
        return nowStr;
    }
}
