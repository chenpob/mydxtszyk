package web.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReUtils {
    public static Boolean checkSth(String s,String reExpression){
        Pattern pattern = Pattern.compile(reExpression);
        Matcher matcher = pattern.matcher(s);
        boolean matches = matcher.matches();
        return matches;
    }


    /**
     * 入字母、数字、特殊符号，账号字数限制7个字符以内
     * 特殊符号仅限 @ $ ^ ! ~ , . *
     * @param account
     * @return
     */
    public static Boolean checkAccount(String account){
        String valicateAccount="^[\\w@\\$\\^!~,.\\*]{4,10}+$";
        Pattern pattern = Pattern.compile(valicateAccount);
        Matcher matcher = pattern.matcher(account);
        boolean matches = matcher.matches();
        return matches;
    }


}
