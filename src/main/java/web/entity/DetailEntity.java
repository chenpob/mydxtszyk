package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class DetailEntity {

    @Column("SYS_FLD_SYSID")
    private String sysId;

    @Column("题名")
    private String title;

    @Column("机构")
    private String org;

    @Column("来源")
    private String source;

    @Column("作者")
    private String author;

    @Column("TABLENAME")
    private String tableName;

    @Column("来源数据库")
    private String DataSource;

    @Column("发表时间")
    private String publishDate;

    @Column("关键词")
    private String keyWord;

    @Column("DOI")
    private String doi;

    @Column("全文")
    private String text;

    @Column("导师")
    private String doctor;

    @Column("会议名称")
    private String meetName;

    @Column("会议时间")
    private String meetTime;

    @Column("文件名")
    private String fileName;

    @Column("摘要")
    private String subtext;

    @Column("编号")
    private String senumber;

    @Column("年")
    private String year;
    @Column("期")
    private String stage;

    @Column("主办单位")
    private String mainorg;

    @Column("THNAME")
    private String thname;

    @Column("光盘号")
    private String discNumber;

    @Column("专辑代码")
    private String albumCode;



}
