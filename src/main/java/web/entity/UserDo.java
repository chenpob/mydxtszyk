package web.entity;

import lombok.Data;
import org.springframework.core.annotation.Order;
import web.config.ReqArgValidationGroups.AddUserValidate;
import web.validation.account.AccountValidator;
import web.validation.cellphone.CellphoneValidator;
import web.validation.password.PwdValidator;
import web.validationAPI.User.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UserDo {

    private Integer id;


    @NotNull(groups = {
            Login.class,
            SelfAddUser.class,
            AdminAddUser.class,
            AdminEditUser.class,
            DeleteUser.class
            },
            message = "缺省account")

    @NotBlank(groups = {
            Login.class,
            SelfAddUser.class,
            AdminAddUser.class,
            AdminEditUser.class,
            DeleteUser.class
    },
            message = "account为空")

    @AccountValidator(groups = {
            SelfAddUser.class,
//            SelfEditUser.class,
            AdminAddUser.class,
    })
    private String account;

    @NotNull(groups = {
            SelfAddUser.class,
            SelfEditUser.class,
            AdminAddUser.class,
            AdminEditUser.class
    },
            message = "缺省user_name"
    )
    @NotBlank(groups = {
            SelfAddUser.class,
            SelfEditUser.class,
            AdminAddUser.class,
            AdminEditUser.class
    },
            message = "user_name不能为空"
    )
    private String user_name;

    @NotNull(groups = {
            Login.class,
            SelfAddUser.class
    },
            message = "缺省password")
    @PwdValidator(
            groups = {
                    SelfAddUser.class
            }
    )
    private String password;

    @NotNull(groups = {
            UserChangePwd.class
    },
            message = "缺省new_password"
    )
    @NotBlank(groups = {
            UserChangePwd.class
    },
            message = "new_password为空"
    )
    private String new_password;

    private String status;

//    @NotNull(groups = {
//            SelfAddUser.class,
//            SelfEditUser.class,
//            AdminAddUser.class,
//            AdminEditUser.class,
//    },
//            message = "缺省department"
//    )
//    @NotBlank(groups = {
//            SelfAddUser.class,
//            SelfEditUser.class,
//            AdminAddUser.class,
//            AdminEditUser.class,
//    },
//            message = "department不能为空"
//    )
    private String department;

    private String email;

    @NotNull(groups = {
            SelfAddUser.class,
            SelfEditUser.class,
            AdminAddUser.class,
            AdminEditUser.class,
    },
            message = "缺省phonenumber"
    )
    @NotBlank(groups = {
            SelfAddUser.class,
            SelfEditUser.class,
            AdminAddUser.class,
            AdminEditUser.class,
    },
            message = "phonenumber为空"
    )
    @CellphoneValidator(groups = {
            SelfAddUser.class,
            SelfEditUser.class,
            AdminAddUser.class,
            AdminEditUser.class,
    })
    private String phonenumber;

    private String sex;

    private String avatar;

    private String user_type;

    private String create_by;

    private String create_time;

    private String update_by;

    private String update_time;

    private Integer del_flag;

    private String del_time;

    private String delete_by;

    private  int  dep_id;
   private  String departmentName;
    private List<User2RoleDo> roles;
}