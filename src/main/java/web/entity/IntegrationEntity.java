package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class IntegrationEntity {
    @Column("时间")
    private String time;
    @Column("题名")
    private String title;
    @Column("发表时间")
    private String publishDate;
    @Column("关键词")
    private String keyWord;
    @Column("作者")
    private String author;
    @Column("文件名")
    private String fileName;
    @Column("来源数据库")
    private String SourceDatabase;
    @Column("下载频次")
    private String downloadTimes;
    @Column("被引频次")
    private String CitedFrequency;
    @Column("图片地址")
    private String pictureAddress;
    @Column("TABLENAME")
    private String tableName;
    @Column("SYS_FLD_SYSID")
    private String sysID;
    @Column("机构")
    private String organ;
    @Column("行业分类代码")
    private String code;

    @Column("编号")
    private String compile;
    @Column("书名")
    private String bookName;
    @Column("主办单位")
    private String sponsor;
    @Column("主编")
    private String chiefEditor;
    @Column("年")
    private String year;
    @Column("期")
    private String stage;
    @Column("地区")
    private String area;
    @Column("分类")
    private String classify;
    @Column("总期")
    private String totalPeriod;
    @Column("物理页码")
    private String physicalPage;
    @Column("中文摘要")
    private String abstractCN;

}
