package web.entity;

import lombok.Data;

@Data
public class User2RoleDo {
    private Integer userId;
    private Integer roleId;
}
