package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/16 15:21
 * @Version 1.0
 */
@Data
public class CodeTree {
    @Column("SYS_FLD_CLASS_CODE")
    private String code;
    @Column("SYS_FLD_CLASS_NAME")
    private String name;
    @Column("SYS_FLD_CLASS_GRADE")
    private String grade;
    @Column("SYS_FLD_PARENT_CODE")
    private String parent;
    @Column("SYS_FLD_SYSID")
    private String sysId;

    private List<CodeTree> childTrees;

}
