package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class PeriodicalYearEntity {
    @Column("年")
    private String year;
}
