package web.entity;

import lombok.Data;

@Data
public class BrowseDownLoadStaticsEntity {
    private String resource_title;
    private String resource_name;
    private String resource_id;
    private String resource_code;
    private String url;
    private String num;
}
