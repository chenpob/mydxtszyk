package web.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 统计PV UV数据(Website)实体类 statistics
 */
@Data
public class WebsiteEntity implements Serializable {
    private static final Long serialVersionUID = 654590859980231033L;

    private Integer id;

    private Integer pvNum;

    private Integer uvNum;

    private Integer ipNum;

    private String recordTime;

    private String pageName;

    private String remoteAddr;

    private String path;

    private String type;

    private String account;

    private String userName;

}
