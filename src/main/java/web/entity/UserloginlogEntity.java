package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class UserloginlogEntity {

    private String id;

    private String userId;

    private String userName;

    private String ip;

    private String createTime;

    private String countnumber;

}
