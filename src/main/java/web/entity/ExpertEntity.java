package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class ExpertEntity {
    @Column("机构")
    private  String organ;

    @Column("姓名")
    public String name;

    @Column("机构")
    public String org;

    @Column("地区")
    public String region;

    @Column("全文")
    public String text;

    @Column("TABLENAME")
    public String tableName;

    @Column("SYS_FLD_DIGITFILENAME")
    public String picture;

    @Column("SYS_FLD_SYSID")
    public String id;

    private byte[] picResource;


}
