package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/11/15 14:33
 * @Version 1.0
 */
@Data
public class HotWord {
    @Column("热词")
    private String name;
}
