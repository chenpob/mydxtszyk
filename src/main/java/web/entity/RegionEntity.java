package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RegionEntity {
    @Column("CLSID")
    private Integer id;

    @Column("NAME")
    private String name;

    @Column("GRADE")
    private Integer grade;

    private List<RegionEntity> child =new ArrayList<>();

}
