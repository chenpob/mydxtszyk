package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

/**
 * @author cb
 * @Description TODO
 * @date 2023/12/9
 */

@Data
public class AdminDepartmentPo {

    private String departmentCode;//
    private String departmentName;//
    private Integer id;
    private String create_by;

    private String create_time;

    private String update_by;

    private String update_time;

    private Integer del_flag;

    private String del_time;

    private String delete_by;



}
