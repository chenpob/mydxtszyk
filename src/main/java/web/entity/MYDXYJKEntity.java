package web.entity;


import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class MYDXYJKEntity {


    @Column("题名")
    private String titile;

    @Column("关键词")
    private String keyWord;

    @Column("图片地址")
    private String picAddress;

    @Column("类型")
    private String type;

    @Column("文件地址")
    private String fileAddress;

    @Column("全文")
    private String fullText;

    @Column("行业分类代码")
    private String classCode;

    @Column("TABLENAME")
    private String tableName;

    @Column("来源数据库")
    private String datasource;

    @Column("作者")
    private String author;

    @Column("SYS_FLD_DIGITFILENAME")
    private String  sysFileName;

}
