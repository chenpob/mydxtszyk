package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

/**
 * Description:期刊库-内刊数据库
 *
 * @author
 * @date: 2022/6/28 15:56
 * @Version 1.0
 */
@Data
public class CJFXLAST {
    @Column("篇名")
    private String title;

    @Column("作者")
    private String author;
//
//    @Column("第一责任人")
//    private String firstDuty;
//
//    @Column("机构")
//    private String agency;
//
//    @Column("文献作者")
//    private String literatureAuthor;
//
//    @Column("中文关键词")
//    private String chineseKeywords;
//
    @Column("中文摘要")
    private String chineseSummary;
//
//    @Column("英文篇名")
//    private String englishTitle;
//
//    @Column("英文作者")
//    private String englishAuthor;
//
//    @Column("英文摘要")
//    private String englishSummary;
//
//    @Column("英文关键字")
//    private String englishKeywords;
//
//    @Column("引文")
//    private String citation;
//
//    @Column("基金")
//    private String fund;
//
//    @Column("光盘号")
//    private String CdNum;
//
//    @Column("文献号")
//    private String literatureNum;
//
//    @Column("拼音刊名")
//    private String pinyinName;
//
//    @Column("拼音刊名")
//    private String pinyinPeriodicalTitle;
//
    @Column("中文刊名")
    private String chinesePeriodicalTitle;
//
    @Column("英文刊名")
    private String englishPeriodicalTitle;
//
//    @Column("出版单位")
//    private String publisher;
//
//    @Column("来源代码")
//    private String sourceCode;
//
    @Column("年")
    private String year;

    @Column("期")
    private String phase;

    @Column("年期")
    private String year_phase;
//
//    @Column("语种")
//    private String language;
//
//    @Column("影响因子")
//    private String impactFactor;
//
    @Column("文件名")
    private String fileName;

    @Column("tablename")
    private String tableName;
//
//    @Column("CN")
//    private String CN;
//
//    @Column("ISSN")
//    private String ISSN;
//
//    @Column("DOI")
//    private String DOI;
//
//    @Column("注册DOI")
//    private String registerDOI;
//
//    @Column("卷期号")
//    private String VolumeNum;
//
//    @Column("EISSN")
//    private String EISSN;
//
//    @Column("页")
//    private String page;
//
//    @Column("页数")
//    private String totalPage;
//
//    @Column("文件大小")
//    private String fileSize;
//
//    @Column("分类号")
//    private String classification;
//
//    @Column("专辑代码")
//    private String yy;
//
//    @Column("专题代码")
//    private String albumCode;
//
//    @Column("专题子栏目代码")
//    private String TopicSub_columnCode;
//
//    @Column("子栏目代码")
//    private String Sub_columnCode;
//
//    @Column("全文")
//    private String fullText;
//
//    @Column("正文快照")
//    private String BodySnapshot;
//
//    @Column("SCI收录刊")
//    private String SCI;
//
//    @Column("EI收录刊")
//    private String EI;
//
//    @Column("核心期刊")
//    private String coreJournals;
//
//    @Column("中英文篇名")
//    private String chi_engTitle;
//
//    @Column("中英文作者")
//    private String chi_engAuthor;
//
//    @Column("中英文刊名")
//    private String chi_engPeriodicalTitle;
//
//    @Column("中英文摘要")
//    private String chi_engSummary;
//
//    @Column("复合关键词")
//    private String compoundKeywords;
//
//    @Column("主题")
//    private String topic;
//
    @Column("发表时间")
    private String publishDate;
//
//    @Column("更新日期")
//    private String updateDate;
//
//    @Column("机标关键词")
//    private String MachineLogoKeywords;
//
//    @Column("SYS_VSM")
//    private String SYS_VSM;
//
//    @Column("是否高下载")
//    private String isHighlyDownload;
//
//    @Column("是否高被引")
//    private String isHighlyCited;
//
//    @Column("是否高他引")
//    private String isHighCitedByOther;
//
    @Column("下载频次")
    private String downloadTimes;

    @Column("被引频次")
    private String CitedFrequency;
//
//    @Column("他引频次")
//    private String CitedByOtherFrequency;
//
//    @Column("作者代码")
//    private String authorCode;
//
//    @Column("机构代码")
//    private String agencyCode;
//
//    @Column("机构作者代码")
//    private String agencyAuthorCode;
//
//    @Column("基金代码")
//    private String fundCode;
//
//    @Column("是否基金文献")
//    private String isfundLiterature;
//
//    @Column("文献标识码")
//    private String literatureCode;
//
//    @Column("文献类型标识")
//    private String literatureType;
//
//    @Column("期刊标识码")
//    private String journalCode;
//
//    @Column("来源标识码")
//    private String sourceIdentificationCode;
//
//    @Column("主题词")
//    private String topicWords;
//
    @Column("来源数据库")
    private String sourceDatabase;
//
//    @Column("TABLENAME")
//    private String TABLENAME;
//
//    @Column("作者简介")
//    private String aboutTheAuthor;
//
//    @Column("文章属性")
//    private String articleProperties;
//
//    @Column("允许全文上网")
//    private String allowOnline;
//
//    @Column("允许检索")
//    private String allowRetrieval;
//
//    @Column("印刷页码")
//    private String printedPage;
//
//    @Column("原文格式")
//    private String originalformat;
//
//    @Column("专题整刊代码")
//    private String TopicJournalCodeCode;
//
//    @Column("复合专题代码")
//    private String CompoundThematicCode;
//
//    @Column("网络总库专辑代码")
//    private String networkLibraryAlbumCode;
//
//    @Column("期刊栏目层次")
//    private String periodicalColumnLevel;
//
    @Column("FX内容代码")
    private String FXcode;
//
//    @Column("FX文献类型")
//    private String FXtype;
//
//    @Column("地域导航")
//    private String geographical;
//
//    @Column("发布机关名称")
//    private String publishAgency;
//
//    @Column("发布机关代码")
//    private String publishAgencyCode;
//
//    @Column("时效性")
//    private String timeliness;
//
//    @Column("发文日期")
//    private String writeDate;
//
//    @Column("实施日期")
//    private String implementationDate;
//
//    @Column("发文字号")
//    private String textNumber;
//
//    @Column("推荐文献")
//    private String RecommendedLiterature;
//
//    @Column("封面文献")
//    private String coverLiterature;
//
//    @Column("栏目")
//    private String column;
//
//    @Column("CLASS1")
//    private String CLASS1;
//
//    @Column("CLASS2")
//    private String CLASS2;
//
//    @Column("CLASS3")
//    private String CLASS3;
//
//    @Column("CLASS4")
//    private String CLASS4;
//
//    @Column("CLASS5")
//    private String CLASS5;
//
//    @Column("THNAME")
//    private String THNAME;

}
