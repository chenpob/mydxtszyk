package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class PeriodicalStageEntity {
    @Column("期")
    private String stage;
}
