package web.entity.admin.user;

import lombok.Data;

import java.io.Serializable;

/**
 * myupload
 * @author CNKI
 */
@Data
public class MyuploadEntity implements Serializable {
    /**
     * 自增id
     */
    private Long id;

    /**
     * 题名
     */
    private String name;

    /**
     * 关键词（; 隔开）
     */
    private String keyword;

    /**
     * 备注
     */
    private String notes;

    /**
     * 作者
     */
    private String author;

    /**
     * 文件类型（文档，图片，音视频，其他）
     */
    private int type;

    /**
     * 源文件名
     */
    private String sourceName;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 生成的文件名
     */
    private String fileName;

    /**
     * 上传时间
     */
    private String createTime;

    /**
     * 上传用户账户account
     */
    private String createUser;

    /**
     * 修改时间
     */
    private String editTime;

    /**
     * 修改用户账号account
     */
    private String editUser;

    /**
     * 部门id
     */
    private int depId;

    private static final long serialVersionUID = 1L;
}