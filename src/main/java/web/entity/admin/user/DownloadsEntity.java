package web.entity.admin.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DownloadsEntity {
    private String id;

    private String user_id;

    private String user_name;

    private String resource_title;

    private String ip;

    private String create_time;

    private String resource_name;

    private String resource_code;
}
