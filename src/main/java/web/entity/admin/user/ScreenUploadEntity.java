package web.entity.admin.user;

import lombok.Data;

import java.io.Serializable;

/**
 * myupload
 * @author CNKI
 */
@Data
public class ScreenUploadEntity implements Serializable {
    /**
     * 自增id
     */
    private Long id;

    /**
     * 题名
     */
    private String name;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 生成的文件名
     */
    private String fileName;

    /**
     * 上传时间
     */
    private String createTime;

    /**
     * 上传用户账户account
     */
    private String createUser;

    /**
     * 修改时间
     */
    private String editTime;

    /**
     * 修改用户账号account
     */
    private String editUser;


    /**
     * 是否显示
     */
    private  int display;

    /**
     * 链接
     */
    private String link;

    private static final long serialVersionUID = 1L;
}