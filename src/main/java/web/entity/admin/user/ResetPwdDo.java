package web.entity.admin.user;

import lombok.Data;
import web.validation.password.PwdValidator;
import web.validationAPI.User.AdminRestPwd;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ResetPwdDo {
    @NotNull(groups = {
            AdminRestPwd.class
    },
            message = "缺省account")
    @NotBlank(groups = {
            AdminRestPwd.class
    },
            message = "account为空")
    private String account;

    @NotNull(groups = {
            AdminRestPwd.class
    },
            message = "缺省newPwd")
    @NotBlank(groups = {
            AdminRestPwd.class
    },
            message = "newPwd为空")
    @PwdValidator(groups = {
            AdminRestPwd.class
    })
    private String newPwd;

    private String update_by;

    private String update_time;
}
