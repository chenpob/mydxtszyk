package web.entity.admin.log;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class LoginDo {

    private String id;

    private String user_id;

    private String user_name;

    private String ip;

    private String create_time;

    private String accessed;

    private int depid;
}
