package web.entity.admin.log;

import lombok.Data;

@Data
public class SearchDo {

    private String id;

    private String user_id;

    private String user_name;

    private String ip;

    private String str_query;

    private String create_time;
}
