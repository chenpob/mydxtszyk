package web.entity.admin.log;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;
import web.config.ReqArgValidationGroups.MirrorResourceViewDownloadArgValidate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class ViewDownloadDo {

    private String id;

    private String user_id;

    private String user_name;

    private String ip;

    @NotNull(groups = {MirrorResourceViewDownloadArgValidate.class},message = "resource_id缺省")
    private String resource_id;

    @NotNull(groups = {MirrorResourceViewDownloadArgValidate.class},message = "resource_code缺省")
    private String resource_code;

    @NotNull(groups = {MirrorResourceViewDownloadArgValidate.class},message = "resource_name缺省")
    private String resource_name;

    @NotNull(groups = {MirrorResourceViewDownloadArgValidate.class},message = "resource_title缺省")
    private String resource_title;

    @NotNull(groups = {MirrorResourceViewDownloadArgValidate.class},message = "url缺省")
    private String url;

    private String create_time;
    private Integer hyfl_code;
}
