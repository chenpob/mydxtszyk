package web.entity.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description:后台-浏览下载日志
 *
 * @author BaiGe
 * @date: 2022/7/12 11:00
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrowseDownLoadLogEntity {
    /**
     * 浏览日志id
     */
    private Integer browseLogId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户名
     */
    private String UserName;
    /**
     * ip地址
     */
    private String ip;
    /**
     * 资源id
     */
    private String resourcesID;
    /**
     * 资源code
     */
    private String resourcesCode;
    /**
     * 资源名称
     */
    private String resourcesName;
    /**
     * 浏览日志状态
     */
    private String browseLogStatus;
    /**
     * 运转时间
     */
    private String operateTime;
    /**
     * 运转时间
     */
    private String createTime;
    /**
     * 来源
     */
    private String source;

    /**
     * 分组之后的总条数
     */
    private Integer count;
}
