package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class TotalSearchEntity {

    @Column("行业分类代码")
    private String code;

    @Column("期刊标识码")
    private String periodicalCode ;

    @Column("SYS_FLD_SYSID")
    private String sysID;

    @Column("英文题名")
    private String EnglishTitile;

    @Column("作者")
    private String Author;

    @Column("题名")
    private String title;

    @Column("发表时间")
    private String publishDate;

    @Column("关键词")
    private String keyWord;

    @Column("文件名")
    private String fileName;

    @Column("来源数据库")
    private String SourceDatabase;

    @Column("下载频次")
    private String downloadTimes;

    @Column("被引频次")
    private String CitedFrequency;

    @Column("图片地址")
    private String pictureAddress;

    @Column("TABLENAME")
    private String tableName;

}
