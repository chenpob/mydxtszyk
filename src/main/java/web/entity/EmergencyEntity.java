package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

/**
 * Description:应急库
 *
 * @author BaiGe
 * @date: 2022/6/20 15:38
 * @Version 1.0
 */
@Data
public class EmergencyEntity {

    @Column("SYS_FLD_SYSID")
    private String sysId;

    @Column("题名")
    private String title;

    @Column("关键词")
    private String keyWord;

    @Column("图片地址")
    private String picturePath;

    /**
     * 1:文献  2:图片  3:视频
     *
     */
    @Column("类型")
    private String type;

    @Column("文件地址")
    private String filePath;

    @Column("SYS_FLD_DIGITFILENAME")
    private String fileName;

    @Column("全文")
    private String text;

    @Column("行业分类代码")
    private String sortCode;

    @Column("TABLENAME")
    private String tableCode;

    @Column("SYS_FLD_RECORDADDDATE")
    private String releaseDate;
}
