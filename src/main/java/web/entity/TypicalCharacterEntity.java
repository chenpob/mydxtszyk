package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

/**
 * Description:典型人物
 *
 * @author BaiGe
 * @date: 2022/11/11 15:23
 * @Version 1.0
 */
@Data
public class TypicalCharacterEntity {

    @Column("姓名")
    private String name;

    @Column("简介")
    private String introduce;

    @Column("顺序号")
    private String serialNumber;

    @Column("全文")
    private String text;

    @Column("类名")
    private String category;

    //照片
    @Column("SYS_FLD_DIGITFILENAME")
    private String photo;

    //全局ID
    @Column("SYS_FLD_SYSID")
    private String sysId;

    @Column("TABLENAME")
    private String tableName;

}
