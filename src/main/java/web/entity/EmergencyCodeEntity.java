package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class EmergencyCodeEntity {
    @Column("SYS_FLD_CLASS_CODE")
    private String SysFidClassCode;

    @Column("SYS_FLD_CLASS_NAME")
    private String SysFidClassName;

    @Column("SYS_FLD_CLASS_GRADE")
    private String SysFidClassGrade;

    @Column("SYS_FLD_CHILD_FLAG")
    private String SysFidClassFlag;

    @Column("SYS_FLD_SYS_CODE")
    private String SysFldSysCode;

    @Column("SYS_FLD_PARENT_CODE")
    private String SysFldParentCode;

    @Column("SYS_FLD_SYSID")
    private String Sysid;

    @Column("SYS_FLD_ISSUB")
    private String SysFldIssubS;


}
