package web.entity.Periodical.fullPeriodicalDisplay;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class CoverEntity {
    @Column("PYKM")
    private String pykm;

    @Column("C_NAME")
    private String C_NAME;

    @Column("主办单位1")
    private String organizer;

    @Column("刊期1")
    private String type;

    @Column("CBD_403")
    private String location;

    @Column("ISSN")
    private String ISSN;

    @Column("CN")
    private String CN;
}
