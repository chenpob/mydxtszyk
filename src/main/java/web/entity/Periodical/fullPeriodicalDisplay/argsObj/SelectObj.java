package web.entity.Periodical.fullPeriodicalDisplay.argsObj;

import lombok.Data;

@Data
public class SelectObj {
    private String pykm;
    private String year;
    private String qi;
    private String page;
    private String pageSize;
    private Integer pageStart;

    private String sno;
}
