package web.entity.Periodical.fullPeriodicalDisplay;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

import java.util.List;

@Data
public class Cover_YearEntity extends CoverEntity {

    private List<String> years;

}
