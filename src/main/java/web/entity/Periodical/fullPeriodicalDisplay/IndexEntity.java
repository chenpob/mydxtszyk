package web.entity.Periodical.fullPeriodicalDisplay;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class IndexEntity {
    @Column("篇名")
    private String name;

    @Column("作者")
    private String author;

    @Column("印刷页码")
    private String page;

    @Column("期刊栏目层次")
    private String classify;

    @Column("文件名")
    private String fileName;

    @Column("TABLENAME")
    private String TABLENAME;
    @Column("编号")
    private String  sno;

    @Column("SYS_FLD_SYSID")
    private String sysId;

    @Column("来源数据库")
    private String  dataSource;


}
