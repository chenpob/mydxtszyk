package web.entity.Periodical.index;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class PopularArticles {
    @Column("篇名")
    private String title;

    @Column("作者")
    private String author;

    @Column("中文摘要")
    private String chineseSummary;

    @Column("出版日期")
    private String publishDate;

    @Column("更新日期")
    private String updateDate;

    @Column("中英文刊名")
    private String articleName;

    @Column("来源数据库")
    private String sourceDatabase;

    @Column("拼音刊名")
    private String pingyinName;

    @Column("tableName")
    private String tableName;

    @Column("文件名")
    private String fileName;

}
