package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

/**
 * Description:整刊展示
 *
 * @author BaiGe
 * @date: 2022/6/28 14:38
 * @Version 1.0
 */
@Data
public class CJFXBASEINFO {
    /**
     * 主键id
     */
    @Column("idno")
    private String sysId;

    /**
     * 题名
     */
    @Column("C_NAME")
    private String title;
    /**
     * 期刊标识码
     * 期刊标识码='X02?'
     */
    @Column("期刊标识码")
    private String code;

    /**
     * 主办单位1，
     * 党校发行 -> 主办单位1 like '*党校' ,相关党建 -> 主办单位1= * not  主办单位1 like '*党校'
     */
    @Column("主办单位1")
    private String SponsorOne;

    @Column("影响因子")
    private String effectParam;

    @Column("被引频次")
    private String quoteTimes;

    @Column("下载频次")
    private String downLoadTimes;

    @Column("载文量")
    private String articleNumber;

    @Column("PYKM")
    private String pinyinName;
}
