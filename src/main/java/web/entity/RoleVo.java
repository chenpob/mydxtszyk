package web.entity;

import lombok.Data;

@Data
public class RoleVo extends RoleEntity {
    private String create_time;


    private Integer page;

    private Integer pageSize;

    private Integer pageStart;

    public void setStart(Integer page, Integer pageSize) {
        this.pageStart = (page - 1) * pageSize;
    }
}
