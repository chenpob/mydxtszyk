package web.entity;

import lombok.Data;
import web.entity.admin.user.ScreenUploadEntity;

/**
 * MyUploadSearchVo
 */

@Data
public class ScreenUploadSearchBo extends ScreenUploadEntity  {

    private String account;

    private String searchType;

    private Integer page;

    private Integer pageSize;

    private Integer pageStart;

    public void setStart(Integer page, Integer pageSize) {
        this.pageStart = (page - 1) * pageSize;
    }
}
