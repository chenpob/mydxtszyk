package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class PictureResources {
    @Column("题名")
    private String title;

    @Column("TABLENAME")
    private String tableName;

    @Column("SYS_FLD_DIGITFILENAME")
    private String sysName;

    @Column("图片地址")
    private String picAddress;
}
