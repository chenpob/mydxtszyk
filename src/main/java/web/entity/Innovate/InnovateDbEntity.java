package web.entity.Innovate;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Cleanup;
import lombok.Data;

/**
 * Description:协同创新库entity
 *
 * @author BaiGe
 * @date: 2022/7/12 18:01
 * @Version 1.0
 */
@Data
public class InnovateDbEntity {
    @Column("SYS_FLD_SYSID")
    private String sysId;

    @Column("题名")
    private String title;

    @Column("作者")
    private String author;

    @Column("机标关键词")
    private String keyWord;

    @Column("发表时间")
    private String pushDate;

    //ca1:协同创新 ca2:应急管理
    @Column("行业分类代码")
    private String sortCode;

    @Column("TABLENAME")
    private String tableName;

    @Column("来源数据库")
    private String dataSource;

    @Column("文件名")
    private String fileName;

    @Column("类型")
    private String type;

    @Column("全文")
    private String text;

    @Column("图片地址")
    private String coverPicture;

    //MYDXSPIDER_METADATA专用
    @Column("封面图片")
    private String coverPicMydxSpider;
    //下载频次
    @Column("SYS_FLD_DOWNLOADRATE")
    private String downLoadRate;
    //浏览频次
    @Column("SYS_FLD_BROWSERATE")
    private String browseRate;

    @Column("摘要")
    private String digest;

    @Column("机构")
    private String mechanism;

    @Column("编号")
    private String num;

}
