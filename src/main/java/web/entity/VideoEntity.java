package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class VideoEntity {

    @Column("题名")
    private String title;

    @Column("行业分类代码")
    private String classifyCode;

    @Column("类型")
    private String type;

    @Column("文件名")
    private String fileName;

    @Column("顺序号")
    private String soutNumber;

    @Column("时间")
    private String date;

    @Column("机构")
    private String org;

    @Column("来源数据库")
    private String DataSource;

    @Column("图片地址")
    private String PicAddress;

    @Column("TABLENAME")
    private String tableName;

    @Column("SYS_FLD_SYSID")
    private String sysId;


}
