package web.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author cc
 * @create 2022/9/30 18:50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Security_UserDetail implements UserDetails {

    private UserDo userDo;

    private List<User2RoleDo> roles;

    public void setUserDo(UserDo userDo){
        this.userDo = userDo;
    }

    public Security_UserDetail(UserDo userDo, List<User2RoleDo> roles) {
        this.userDo = userDo;
        this.roles = roles;
    }

    public UserDo getUserDo(){
        return this.userDo;
    }

    @JSONField(serialize = false)
    private String authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        for (User2RoleDo o :roles) {
            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(o.getRoleId().toString()));
        }

        return simpleGrantedAuthorities;
    }

    @Override
    public String getPassword() {
        return userDo.getPassword();
//        return null;
    }

    @Override
    public String getUsername() {
        return userDo.getAccount();
//        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
//        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
        //        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
        //        return false;
    }

    @Override
    public boolean isEnabled() {
        return true;
        //        return false;
    }
}
