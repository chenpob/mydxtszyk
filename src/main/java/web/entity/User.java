package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class User {
    @Column("code")
    private String sys_fld_class_code;

    @Column("name")
    private String sys_fld_class_name;

    @Column("grade")
    private String sys_fld_class_grade;
}
