package web.entity;

import lombok.Data;

@Data
public class SSOBo {
    public String Key;

    public String UserName;

    public String UserIp;
}
