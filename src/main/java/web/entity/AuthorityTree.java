package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/16 15:21
 * @Version 1.0
 */
@Data
public class AuthorityTree {
    public String title;

    public Integer id;

    public String filed;

    public Boolean checked;

    public Boolean spread;

    public String value;

    public List<AuthorityTree> children;
}
