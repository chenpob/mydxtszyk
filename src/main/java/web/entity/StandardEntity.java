package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class StandardEntity {
    @Column("行业分类代码")
    private String code;

    @Column("SYS_FLD_SYSID")
    private String sysID;

    @Column("英文题名")
    private String EnglishTitile;

    @Column("作者")
    private String Author;

    @Column("作者单位")
    private String AuthorDepartment;

    @Column("作者简介")
    private String AuthorIntroduce;

    @Column("导师")
    private String Doctor;

    @Column("论文级别")
    private String PaperLevel;

    @Column("学科专业名称")
    private String MajorName;

    @Column("分类号")
    private String ClassifyCode;

    @Column("学科代码")
    private String MajorCode;

    @Column("论文提交日期")
    private String PaperSubmitTime;

    @Column("论文答辩日期")
    private String PaperDefenseTime;

    @Column("答辩委员会主席")
    private String CommitteeChairman;

    @Column("中文关键词")
    private String ChineseKeyWord;

    @Column("中文摘要")
    private String ChineseAbstract;

   @Column("英文摘要")
    private String EnglishAbstract;

    @Column("中文详细摘要")
    private String AbstractDetail;

    @Column("基金")
    private String Fund;

    @Column("引文")
    private String Quotation;

    @Column("学位年度")
    private String DegreeLevel;

    @Column("英文关键词")
    private String EnglishKeyWords;

    @Column("专题代码")
    private String TopicCode;

    @Column("文件名")
    private String FileName;

    @Column("来源数据库")
    private String DataSource;

    @Column("更新日期")
    private String UpdateTime;

    @Column("被引频次")
    private String quoteTime;

    @Column("下载频次")
    private String downTime;

    @Column("中文刊名")
    private String ChinaTitle;

    @Column("发表时间")
    private String PublishTime;

    @Column("TABLENAME")
    private String tableName;

    @Column("题名")
    private String title;

    @Column("封面图片")
    private String coverPhoto;

    @Column("类型")
    private String type;

    @Column("SYS_FLD_SYSID")
    private String sysId;

    @Column("文件名")
    private String fileName;
}
