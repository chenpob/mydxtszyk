package web.entity;


import org.springframework.util.StringUtils;



/**
 * Description:数据库表文件对应的存放文件夹名
 *
 * @author BaiGe
 * @date: 2022/7/14 16:58
 * @Version 1.0
 */
public enum DbEnum {
    db_a("XTXD0220", "CDMD"),
    db_b("XTXJ7922", "CJFD"),
    db_c("XTXM0021", "CDMD"),
    db_d("XTXN0022", "CCND"),
    db_e("XTXP8922", "CPFD"),
    db_f("YJTD0521", "CDMD"),
    db_g("YJTJ7922", "CJFD"),
    db_h("YJTM0121", "CDMD"),
    db_i("YJTN0022", "CCND"),
    db_j("YJTP7921", "CPFD"),
    db_l("YJTDLAST2022", "CDMD"),
    db_m("YJTMLAST2022", "CDMD"),
    db_n("YJTNLAST2022", "CCND"),
    db_o("YJTPLAST2022", "CPFD"),
    db_k("YJTJLAST2022", "CJFD");


    //表名
    private String tableName;
    //文件夹名
    private String folderName;

    public static String getFolderNameByTableName(String tableName) {
        if (!StringUtils.hasLength(tableName)) {
            return "";
        }
        for (DbEnum dbEnum : DbEnum.values()) {
            if (tableName.equals(dbEnum.getTableName())) {
                return dbEnum.folderName;
            }
        }
        return "";
    }

    DbEnum(String tableName,String folderName) {
        this.tableName = tableName;
        this.folderName = folderName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
