package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class MenuEntity {
    private Integer id;
    private String menuName;
    private String icon;
    private String parent;
    private String path;

}
