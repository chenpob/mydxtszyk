package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class PeriodicalEntity {
    @Column("分类")
    private  String classify;

    @Column("编号")
    private String number;

    @Column("书名")
    private String title;
    
    @Column("主办单位")
    private String organizer;

    @Column("主编")
    private String chiefEditor;

    @Column("年")
    private String year;

    @Column("期")
    private String stage;

    @Column("总期")
    private String totalPeriod;

    @Column("来源数据库")
    private String source;

    @Column("TABLENAME")
    private String tableName;

    @Column("地区")
    private String region;


}
