package web.entity;

import lombok.Data;

@Data
public class UserVo extends UserDo {
    private String create_time;

    private String resource_title;

    private String account;

    private String org_name;

    private Integer page;

    private Integer pageSize;

    private Integer pageStart;

    public void setStart(Integer page, Integer pageSize) {
        this.pageStart = (page - 1) * pageSize;
    }
}
