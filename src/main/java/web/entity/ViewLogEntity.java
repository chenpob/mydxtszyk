package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

import java.util.Date;

@Data
public class ViewLogEntity {

    @Column("id")
    private String id;

    @Column("user_id")
    private String user_id;

    @Column("user_name")
    private String user_name;

    @Column("ip")
    private String ip;

    @Column("resource_id")
    private String resource_id;

    @Column("resource_code")
    private String resource_code;

    @Column("resource_name")
    private String resource_name;

    @Column("resource_title")
    private String resource_title;

    @Column("url")
    private String url;

    @Column("create_time")
    private String  create_time;

}
