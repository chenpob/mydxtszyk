package web.entity;


import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class EveEntity {

    @Column("题名")
    private String title;

    @Column("分类")
    private String classfy;

    @Column("TABLENAME")
    private String tableName;

    @Column("SYS_FLD_SYSID")
    private String eveid;

    @Column("SYS_FLD_DIGITFILENAME")
    private String covrPicture;

    @Column("描述")
    private String eveDetail;

}
