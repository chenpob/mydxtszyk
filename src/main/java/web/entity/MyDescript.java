package web.entity;


import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class MyDescript {

    @Column("DATABASECODE")
    private String dataBaseCode;

    @Column("DATABASENAME")
    private String databaseName;

    @Column("DATABASETYPE")
    private String dataBaseType;

    @Column("DATABASEPATH")
    private String dataBasePath;
}
