package web.entity;

import lombok.Data;

@Data
public class RoleEntity {
    private Integer id;
    private String name;
    private String role_key;
    private String status;
    private String del_flag;
    private String create_by;
    private String create_time;
    private String update_by;
    private String update_time;
    private String remark;

}
