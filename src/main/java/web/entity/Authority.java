package web.entity;

import lombok.Data;

@Data
public class Authority {
    private String id;

    private String name;

    private String status;

    private String del_flag;

    private String remark;

    private Integer page;

    private Integer pageSize;

    private Integer pageStart;

    public void setStart(Integer page, Integer pageSize) {
        this.pageStart = (page - 1) * pageSize;
    }
}

