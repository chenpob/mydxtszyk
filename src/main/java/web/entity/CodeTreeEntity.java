package web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cnki.tpi.kbatis.annotation.Column;


import java.util.List;

/**
 * Description: 数据库树形导航结构-非关系型
 *
 * @author BaiGe
 * @date: 2022/7/1 14:47
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CodeTreeEntity {
    @Column("NAME")
    private String name;
    @Column("code")
    private String code;
    @Column("grade")
    private String grade;
    @Column("sysCode")
    private String sysCode ;


    private String pid;
    private List<CodeTreeEntity> childTreeList;
}
