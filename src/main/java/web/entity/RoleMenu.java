package web.entity;

import lombok.Data;

@Data
public class RoleMenu {
    private String role_id;

    private String menu_id;

}
