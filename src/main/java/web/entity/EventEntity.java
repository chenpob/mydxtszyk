package web.entity;


import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class EventEntity {

    @Column("题名")
    private String title;

    @Column("分类")
    private String classfy;

    @Column("TABLENAME")
    private String tableCode;

    @Column("封面图片")
    private String coverPic;

    @Column("年")
    private String year;

    @Column("月")
    private String month;

    @Column("ID")
    private String id;

    @Column("大事记日期")
    private String EventDate;

    @Column("内容")
    private String detail;

    @Column("摘要")
    private String abstractTitle;

    @Column("SYS_FLD_SYSID")
    private String eveDetailId;
}
