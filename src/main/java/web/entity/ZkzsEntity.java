package web.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

@Data
public class ZkzsEntity {
    @Column("分类")
    private  String classify;

    @Column("编号")
    private String number;

    @Column("书名")
    private String title;

    @Column("主办单位")
    private String organizer;

    @Column("主编")
    private String chiefEditor;

    @Column("年")
    private String year;

    @Column("期")
    private String stage;

    @Column("总期")
    private String totalPeriod;

    @Column("来源数据库")
    private String source;

    @Column("TABLENAME")
    private String tableName;

    @Column("地区")
    private String region;

    /**
     * 主键id
     */
    @Column("idno")
    private String sysId;

    /**
     * 题名
     */
    @Column("C_NAME")
    private String titleName;
    /**
     * 期刊标识码
     * 期刊标识码='X02?'
     */
    @Column("期刊标识码")
    private String code;

    /**
     * 主办单位1，
     * 党校发行 -> 主办单位1 like '*党校' ,相关党建 -> 主办单位1= * not  主办单位1 like '*党校'
     */
    @Column("主办单位1")
    private String SponsorOne;

    @Column("影响因子")
    private String effectParam;

    @Column("被引频次")
    private String quoteTimes;

    @Column("下载频次")
    private String downLoadTimes;

    @Column("载文量")
    private String articleNumber;

    @Column("PYKM")
    private String pinyinName;

}
