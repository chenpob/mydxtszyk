package web.entity;


import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentEntity {

    @Column("篇名")
    private String title;

    @Column("年")
    private String year;

    @Column("期")
    private String stage;

    @Column("文件名")
    private String fileName;

    @Column("TABLENAME")
    private String TABLENAME;

    @Column("机构")
    private String organ;

    @Column("中英文刊名")
    private String journalName;
}
