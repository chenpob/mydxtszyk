package web.config;

import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import web.common.Result;

/**
 * 配合 controller的@Validated
 * 实体类的@NotNull等的使用
 * 处理参数验证失败时候的异常,返回统一的result结果
 */
@RestControllerAdvice
public class ReqArgsExceptionHandlerConfig {
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public Result MethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        // 从异常对象中拿到ObjectError对象
//        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        //可能有很多错误,这里只反馈第一个
        int size = e.getBindingResult().getAllErrors().size();
        ObjectError firstError = e.getBindingResult().getAllErrors().get(size-1);
        return Result.fail(firstError.getDefaultMessage());
    }
}

