package web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class staticConfig  implements WebMvcConfigurer {


//    private String filePath="E:\\MCPC\\";

    //映射浏览器可以直接访问的本地路径
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        if (!filePath.endsWith(File.separator)) {
//            filePath = filePath + File.separator;
//        }
        registry.addResourceHandler("/pdf/**")
                .addResourceLocations("file:D:\\cnki\\");

    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST").allowedHeaders("*").maxAge(3600 * 24);
        WebMvcConfigurer.super.addCorsMappings(registry);
    }
}
