package web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import web.filter.Security_TokenFilter;
import web.filter.VerificationCodeFilter;

/**
 * @author cc
 * @create 2022/9/30 18:50
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Scurity_Config extends WebSecurityConfigurerAdapter {

    @Autowired
    Security_TokenFilter securityTokenFilter;

    @Autowired
    VerificationCodeFilter verificationCodeFilter;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /*白名单开始*/
    @Autowired
    private SecurityWhitelistHandler whitelistHandler;

    @Bean
    public static Resource securityWhitelistResource() {
        return new ClassPathResource("/security_whitelist.properties");
    }
    /*白名单结束*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        whitelistHandler.handle(http)

                //关闭csrf
                .csrf().disable().cors().disable()
                //关闭Session获取SecurityContext
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()

//                .antMatchers("/**").anonymous(); // 对所有接口不鉴权(测试业务用,如果controller上写了权限这个配置将失效)
                .anyRequest().authenticated();
//                .anyRequest().permitAll();//取消全部的认证要求
        //在 Security 中添加token过滤器
        http.addFilterBefore(securityTokenFilter, UsernamePasswordAuthenticationFilter.class);

        http.addFilterBefore(verificationCodeFilter, UsernamePasswordAuthenticationFilter.class);

        //配置异常处理器
        http.exceptionHandling()
                //配置认证失败处理器
                .authenticationEntryPoint(authenticationEntryPoint)
                .accessDeniedHandler(accessDeniedHandler);
        http.cors();
    }


}
