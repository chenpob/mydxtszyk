package web.config.ReqArgValidationGroups;

import javax.validation.groups.Default;

/**
 * 镜像资源浏览下载时的参数认证
 */
public interface MirrorResourceViewDownloadArgValidate extends Default {
}
