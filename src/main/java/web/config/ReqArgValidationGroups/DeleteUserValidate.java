package web.config.ReqArgValidationGroups;

import javax.validation.groups.Default;

/**
 * 删除用户时的参数检测
 */
public interface DeleteUserValidate extends Default {
}
