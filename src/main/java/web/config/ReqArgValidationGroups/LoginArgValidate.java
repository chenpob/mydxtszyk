package web.config.ReqArgValidationGroups;

import javax.validation.groups.Default;

/**
 * 用户登录接口的参数验证
 */
public interface LoginArgValidate extends Default {
}
