package web.validation.account;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;


/*
要求:
1. 长度为6-18
2. 只能包含大写字母,小写字母和数字
 */
public class AccountValidatorImpl implements ConstraintValidator<AccountValidator,String> {

    @Override
    public void initialize(AccountValidator constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        /*校验正则*/
        Pattern pattern = compile("[0-9|a-z|A-Z]{6,18}");
        try {
            Matcher matcher = pattern.matcher(s);
            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
}