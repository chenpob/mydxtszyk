package web.validation.cellphone;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({ElementType.FIELD})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = CellphoneValidatorImpl.class)
public @interface CellphoneValidator {
    String message() default "电话格式错误";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}