package web.validation.cellphone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;


/*
要求:
1. 首数字是1
2. 共11位数字
 */
public class CellphoneValidatorImpl implements ConstraintValidator<CellphoneValidator,String> {


    @Override
    public void initialize(CellphoneValidator constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        /*校验正则*/

        Pattern pattern = compile("^1[0-9]{10}");
        try {
            Matcher matcher = pattern.matcher(s);
            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
}