package web.validation.password;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({ElementType.FIELD})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = PwdValidatorImpl.class)
public @interface PwdValidator {
    String message() default "密码格式错误:长度[6,20],非中文";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}