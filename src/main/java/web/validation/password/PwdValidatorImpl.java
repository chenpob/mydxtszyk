package web.validation.password;

import web.validation.cellphone.CellphoneValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;


/*
要求:
1. 长度为[6,20]
2. 非中文
 */
public class PwdValidatorImpl implements ConstraintValidator<PwdValidator,String> {


    @Override
    public void initialize(PwdValidator constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        /*校验正则*/
//        Pattern pattern = compile("^[^\\u4e00-\\u9fa5]*$");
        Pattern pattern = compile("[^\\u4e00-\\u9fa5\\s]{6,20}");
        try {
            Matcher matcher = pattern.matcher(s);
            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
}