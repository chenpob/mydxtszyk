package web;

import com.cnki.dataSource.DynamicDataSourceAnnotationAdvisor;
import com.cnki.dataSource.DynamicDataSourceAnnotationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import web.config.dataSource.DynamicDataSourceRegister;



@Import(DynamicDataSourceRegister.class)
@MapperScan("web.mapper")
@SpringBootApplication
@EnableTransactionManagement
public class CnkiApplication {
    @Bean
    public DynamicDataSourceAnnotationAdvisor dynamicDatasourceAnnotationAdvisor() {
        return new DynamicDataSourceAnnotationAdvisor(new DynamicDataSourceAnnotationInterceptor());
    }
    public static void main(String[] args) {
        SpringApplication.run(CnkiApplication.class, args);
    }
}
