package web.common;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result implements Serializable {
    private int code;
    private String msg;
    private Object data;

    public static Result succ(Object data) {
        Result m = new Result();
        m.setCode(200);
        m.setData(data);
        m.setMsg("操作成功");
        return m;
    }

    public static Result succ(String mess, Object data) {
        Result m = new Result();
        m.setCode(200);
        m.setData(data);
        m.setMsg(mess);
        return m;
    }

    public static Result fail(String mess) {
        Result m = new Result();
        m.setCode(500);
        m.setData(null);
        m.setMsg(mess);
        return m;
    }

    public static Result fail(String mess, Object data) {
        Result m = new Result();
        m.setCode(500);
        m.setData(data);
        m.setMsg(mess);
        return m;
    }

    /**
     * 由于前段只处理了 200 和 非200 的状态码,局限.因此增加这个
     * @param mess
     * @return
     */
    public static Result succ732(String mess) {
        Result m = new Result();
        m.setCode(732);
        m.setData("1");
        m.setMsg(mess);
        return m;
    }

    public static Result tokenFail() {
        Result m = new Result();
        m.setCode(1001);
        m.setMsg("token过期或失效,请重新登录");
        return m;
    }

    /**
     * 自定义code
     * @param code 1200:changeMyPwd:修改成功 1201:sql执行失败 1202:密码验证不通过 1203:用户不存在
     * @param msg 消息
     * @return 响应对象
     */
    public static Result DiyCode(int code,String msg) {
        Result m = new Result();
        m.setCode(code);
        m.setMsg(msg);
        return m;
    }
}
