package web.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Description:分页工具类
 * @author cy
 * @date: 2020-10-10 15:21
 * @Version 1.0
 */
@Data
public  class CutPageBean<T> implements Serializable {
	/** 当前页需要显示的数据 */
	private List<T> list;
	/** 总条数 */
	private int count;
	/** 总页数 */
	private int totalPage;
	/** 每页显示的记录数 */
	private  int pageSize;

	public void setTotalPage(int count,int pageSize){
		if (pageSize==0) {
		  return;
		}
		if (count % pageSize == 0) {
			this.totalPage= count/pageSize;
		}else{
			this.totalPage=count/pageSize+1;
		}
	}

	/**
	 * 分页实体参数完善
	 * @param count 总条数
	 * @param pageSize 每页显示条数
	 * @param list 分页实体
	 */
	public void   initCutPage(int count,int pageSize,List<T> list){
		//设置总条数
		this.setCount(count);
		//设置每页显示条数
		this.setPageSize(pageSize);
		//设置分页实体
		this.setList(list);
		//设置总页数
		this.setTotalPage(count,pageSize);
	}
}
