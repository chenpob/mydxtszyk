package web.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.bo.BrowseDownLoadLogBo;
import web.common.Result;
import web.entity.admin.BrowseDownLoadLogEntity;
import web.service.logService.BrowseDownloadLogService;

import java.util.List;

/**
 * Description:后台-日志浏览下载
 *
 * @author BaiGe
 * @date: 2022/7/12 11:20
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/browseDownloadLog")
public class BrowseDownloadLogController {
    @Autowired
    private BrowseDownloadLogService browseDownloadLogService;

    @PostMapping("/cutPage")
    public Result cutPage(@RequestBody BrowseDownLoadLogBo browseLogBo) {
        PageHelper.startPage(1, 5);
        List<BrowseDownLoadLogEntity> list = browseDownloadLogService.searchBrowseDownloadLog(browseLogBo);
        PageInfo<BrowseDownLoadLogEntity> pageInfo = new PageInfo<BrowseDownLoadLogEntity>(list);
        return Result.succ(pageInfo);
    }
}
