package web.controller.admin;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import web.bo.UserloginlogEntityBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.UserloginlogEntity;
import web.service.UserLoginLogService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/adminLog")
//@PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
public class UserloginlogController {

    @Autowired
    UserLoginLogService userLoginLogService;

    @GetMapping("/LoginLogList")
    public Result LoginLogList(    @PathParam("groupType") String groupType,
                                   @PathParam("searchValue") String searchValue,
                                   @PathParam("startTime") String startTime,
                                   @PathParam("endTime") String endTime,
                                   @PathParam("page") Integer page,
                                   @PathParam("pageSize") Integer pageSize){
        UserloginlogEntityBo userloginlogEntityBo = new UserloginlogEntityBo();
        userloginlogEntityBo.setSearchValue(searchValue);
        userloginlogEntityBo.setStartTime(startTime);
        userloginlogEntityBo.setEndTime(endTime);
        userloginlogEntityBo.setPage(page);
        userloginlogEntityBo.setPageSize(pageSize);
        userloginlogEntityBo.setEndTime(endTime);
        userloginlogEntityBo.setPageStart((page - 1) * pageSize);
        userloginlogEntityBo.setType(groupType);
        //分页

        if(groupType.equals("user_id") || groupType.equals("ip")){
            CutPageBean<UserloginlogEntity> userLog = userLoginLogService.getUserOtherLog(userloginlogEntityBo);
            return Result.succ(userLog);
        }
        else{
            PageHelper.startPage(page,pageSize);
            List<UserloginlogEntityBo> list = userLoginLogService.LoginLogList(userloginlogEntityBo);
            PageInfo<UserloginlogEntityBo> info=new PageInfo<>(list);
            CutPageBean cutPageBean=new CutPageBean();
            cutPageBean.initCutPage((int)info.getTotal(),page,list);
            return Result.succ(cutPageBean);
        }

    }
}
