package web.controller.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import web.Annotation.LogAnnotation;
import web.common.Result;
import web.config.ReqArgValidationGroups.MirrorResourceViewDownloadArgValidate;
import web.entity.admin.log.ViewDownloadDo;
import web.service.IndexService;
import web.service.LogService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/admin/log")
public class LogController {


    @Autowired
    LogService logService;


    @PostMapping("/view")
    public Result view(@Validated({MirrorResourceViewDownloadArgValidate.class}) @RequestBody ViewDownloadDo viewDownloadDo,
                       HttpServletRequest request) {
        return logService.viewLog(viewDownloadDo, request);
    }

    @PostMapping("/download")
    public Result download(@Validated({MirrorResourceViewDownloadArgValidate.class}) @RequestBody ViewDownloadDo viewDownloadDo,
                           HttpServletRequest request) {
        return logService.downLoadLog(viewDownloadDo, request);
    }

    @PostMapping("/anonymous-view")
    public Result anonymousView(@Validated({MirrorResourceViewDownloadArgValidate.class}) @RequestBody ViewDownloadDo viewDownloadDo,
                                HttpServletRequest request) {
        return logService.anonymousView(viewDownloadDo, request);
    }

    @LogAnnotation("LL")
    @GetMapping("/viewFileTest")
//    public Result viewFileTest(@RequestAttribute ViewDownloadDo viewDownloadDo, HttpServletRequest request) {
    public Result viewFileTest() {
        return Result.succ("YOU ARE VIEW FILE");
    }

    @LogAnnotation("XZ")
    @GetMapping("/DownloadFileTest")
//    public Result DownloadFileTest(@RequestBody ViewDownloadDo viewDownloadDo, HttpServletRequest request) {
    public Result DownloadFileTest() {
        return Result.succ("YOU ARE DOWNLOAD FILE");
    }



    @LogAnnotation("CX")
    @GetMapping("/SearchTest")
//    public Result DownloadFileTest(@RequestBody ViewDownloadDo viewDownloadDo, HttpServletRequest request) {
    public Result SearchTest() {
        return Result.succ("YOU ARE SEARCHING SOMETHING");
    }


    @GetMapping("/authTest")
//    @PreAuthorize("@cnkiAuthType.inGroupAnd('4','5','6')")
//    @PreAuthorize("@cnkiAuthType.inGroupOr('5','6')")
    public Result authTest() {
        return Result.succ("YOU HAVE PASSED THE AUTH");
    }
}
