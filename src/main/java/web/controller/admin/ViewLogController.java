package web.controller.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import web.bo.ViewLogBo;
import web.common.CutPageBean;
import web.common.Result;
import web.service.ViewLogService;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/ViewLog")
//@PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
public class ViewLogController {

    @Autowired
    ViewLogService viewLogService;


    @GetMapping("/getViewLog")
    public Result getViewLog(@PathParam("groupType") String groupType,
                             @PathParam("groupValue") String groupValue,
                             @PathParam("startTime") String startTime,
                             @PathParam("orderType") String orderType,
                             @PathParam("orderValue") String orderValue,
                             @PathParam("page") Integer page,
                             @PathParam("pageSize") Integer pageSize,
                             @PathParam("endTime") String endTime){
        ViewLogBo viewLogBo = new ViewLogBo();
        viewLogBo.setPage(page);
        viewLogBo.setPageSize(pageSize);
        viewLogBo.setStart((page - 1) * pageSize);
        viewLogBo.setType(groupType);
        viewLogBo.setStartTime(startTime);
        viewLogBo.setEndTime(endTime);
        viewLogBo.setSearchValue(groupValue);
        CutPageBean<ViewLogBo> viewLog = null;
        if (groupType.equals("user_name") || groupType.equals("resource_name")|| groupType.equals("resource_title")||groupType.equals("hyfl_code")||groupType.equals("dep_id")){
            viewLog =viewLogService.getOtherViewLog(viewLogBo);
        }else {
            viewLog =viewLogService.getViewLog(viewLogBo);
        }
        return Result.succ(viewLog);
    }


    @GetMapping("/getDownLog")
    public Result getDownLog(@PathParam("groupType") String groupType,
                             @PathParam("groupValue") String groupValue,
                             @PathParam("startTime") String startTime,
                             @PathParam("orderType") String orderType,
                             @PathParam("orderValue") String orderValue,
                             @PathParam("page") Integer page,
                             @PathParam("pageSize") Integer pageSize,
                             @PathParam("endTime") String endTime){
        ViewLogBo viewLogBo = new ViewLogBo();
        viewLogBo.setPage(page);
        viewLogBo.setPageSize(pageSize);
        viewLogBo.setStart((page - 1) * pageSize);
        viewLogBo.setType(groupType);
        viewLogBo.setStartTime(startTime);
        viewLogBo.setEndTime(endTime);
        viewLogBo.setSearchValue(groupValue);
        CutPageBean<ViewLogBo> viewLog = null;
        if (groupType.equals("user_name") || groupType.equals("resource_name")|| groupType.equals("resource_title")||groupType.equals("hyfl_code")||groupType.equals("dep_id")){
            viewLog =viewLogService.getOtherDownLog(viewLogBo);
        }else {
            viewLog =viewLogService.getDownLog(viewLogBo);
        }
        return Result.succ(viewLog);
    }
}
