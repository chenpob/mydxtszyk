package web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import web.common.Result;
import web.config.ReqArgValidationGroups.LoginArgValidate;
import web.entity.UserDo;
import web.service.Security_LoginService;
import web.validationAPI.User.Login;


@RestController
@RequestMapping("/admin")
public class Security_LoginController {

    @Autowired
    Security_LoginService securityLoginService;

//    @LogAnnotation("DL")
    @PostMapping("/login")
    public Result login(@Validated({Login.class}) @RequestBody UserDo ddd) {
        return securityLoginService.login(ddd);
    }
}
