package web.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.admin.log.SearchDo;
import web.service.KeyWordLogService;


import java.util.List;
import java.util.Map;

/**
 * Description: 后台-关键词
 *
 * @author BaiGe
 * @date: 2022/8/30 10:39
 * @Version 1.0
 */
@RestController
@RequestMapping("/keyWord")
//@PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
public class KeyWordController {

    @Autowired
    private KeyWordLogService logService;

    /**
     * 关键词分页
     * @param map
     * operater : 操作人
     * keyWord :搜索的关键词
     * startTime : 开始日期
     * endTime : 结束日期
     * orderType :排序
     * orderValue : 排序值
     * page : 页码
     * pageSize : 每页条数
     * @return
     */
    @GetMapping("/cutPageKeyWord")
    public Result cutPageKey(@RequestParam Map<String,String> map){
        int page = Integer.valueOf( map.get("page"));
        int pageSize = Integer.valueOf( map.get("pageSize"));
        PageHelper.startPage(page,pageSize);
        List<SearchDo> list = logService.cutPageKeyWord(map);
        PageInfo<SearchDo> info=new PageInfo<>(list);

        CutPageBean<SearchDo> pageBean=new CutPageBean<>();
        pageBean.initCutPage((int)info.getTotal(),pageSize,list);
        return  Result.succ(pageBean);
    }
}
