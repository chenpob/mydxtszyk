package web.controller.admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.AuthorityTree;
import web.entity.RoleVo;
import web.entity.UserVo;
import web.service.SystemManageService;
import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/sys-mgr")
public class SystemManageController {

    @Autowired
    SystemManageService systemManageService;

    /**
     * 是否开启登录时的验证码功能
     * @param params 前端参数
     * @return 修改状态
     */
    @PostMapping("/change-login-vc-status")
    public Result changeLoginVerificationCodeStatus(@RequestBody HashMap<String, Object> params) {
        String status = (String) params.get("status");
        boolean res = systemManageService.changeLoginVerificationCodeStatus(status);
        if(res){
            return Result.succ("请求成功");
        }
        return Result.fail("请求失败");
    }

    @GetMapping("/grouplist")
    public Result grouplist(
            @PathParam("name") String name,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize
    ) {
        RoleVo vo = new RoleVo();
        vo.setName(name);
        vo.setPage(page);
        vo.setPageSize(pageSize);
        vo.setPageStart((page - 1) * pageSize);
        CutPageBean<HashMap<String, Object>> cutPageBean = systemManageService.grouplist(vo);
        return Result.succ(cutPageBean);
//        return null;
    }

    /**
     * 更改权限组启用/禁用状态
     * @param objectHashMap 状态，组id
     * @return String类型
     */
    @PostMapping("/changeStatus")
    public Result changeStatus(
            @RequestBody HashMap<String,Object> objectHashMap){
        Object status = objectHashMap.get("status");
        Object id = objectHashMap.get("id");
        return systemManageService.changeStatus(id.toString(), status.toString());
    }

    /**
     * 新建或编辑权限信息
     * @param objectHashMap 权限相关信息
     * @return String类型
     */
    @PostMapping("/addOrEditAuthority")
    public Result addOrEditAuthority(
            @RequestBody HashMap<String,Object> objectHashMap){
        Object dataObj = objectHashMap.get("dataObj");
        return systemManageService.addOrEditAuthority(dataObj);
    }

    /**
     * 获取权限树
     * @return List
     */
    @GetMapping("/getMenuTree")
    public List<AuthorityTree> getMenuTree(
            String role_id){
        /*Object role_id = objectHashMap.get("role_id");*/
        return systemManageService.getMenuTree(role_id);
    }

    /**
     * 删除权限组
     * @param objectHashMap 权限组信息
     * @return String
     */
    @PostMapping("/deleteAuthority")
    public Result deleteAuthority(
            @RequestBody HashMap<String,Object> objectHashMap
    ){
        Object role_id= objectHashMap.get("role_id");
        return systemManageService.deleteAuthority(role_id.toString());
    }

    //region 列表样式编辑成员
    /**
     * 编辑成员时获取成员
     * @param account 账号
     * @param user_name 姓名
     * @param page 页码
     * @param pageSize 页面大小
     * @param role 权限组id
     * @param type tab类型
     * @return json
     */
    @GetMapping("/userList")
    public Result userList(
            @PathParam("account") String account,
            @PathParam("user_name") String user_name,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize,
            @PathParam("role") String role,
            @PathParam("type") String type
    ) {
        UserVo vo = new UserVo();
        vo.setAccount(account);
        vo.setUser_name(user_name);
        vo.setPage(page);
        vo.setPageSize(pageSize);
        vo.setPageStart((page - 1) * pageSize);
        CutPageBean<HashMap<String, Object>> cutPageBean = systemManageService.userList(vo, role, type);
        return Result.succ(cutPageBean);
    }

    /**
     * 添加成员
     * @param objectHashMap 成员信息
     * @return String
     */
    @PostMapping("/addMember")
    public Result addMember(
            @RequestBody HashMap<String,Object> objectHashMap
    ){
        Object user_id = objectHashMap.get("user_id");
        Object role_id = objectHashMap.get("role_id");
        Object type = objectHashMap.get("type");
        return systemManageService.addMember(user_id.toString(), role_id.toString(),type.toString());
    }

    /**
     * 移除已选成员
     * @param objectHashMap 用户id，组id
     * @return String
     */
    @PostMapping("/moveMember")
    public Result moveMember(
            @RequestBody HashMap<String,Object> objectHashMap){
        Object user_id = objectHashMap.get("user_id");
        Object role_id = objectHashMap.get("role_id");
        return systemManageService.moveMember(user_id.toString(), role_id.toString());
    }
    //endregion

    //region 上下框样式的选择成员
    /**
     * 编辑成员时获取成员(另一种样式)
     * @param account 账号
     * @param user_name 姓名
     * @param page 页码
     * @param pageSize 页面大小
     * @param role 权限组id
     * @param type tab类型
     * @return List
     */
    @GetMapping("/userListNew")
    public Result userListNew(
            @PathParam("account") String account,
            @PathParam("user_name") String user_name,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize,
            @PathParam("role") String role,
            @PathParam("type") String type
    ) {
        UserVo vo = new UserVo();
        vo.setAccount(account);
        vo.setUser_name(user_name);
        vo.setPage(page);
        vo.setPageSize(pageSize);
        vo.setPageStart((page - 1) * pageSize);
        List<CutPageBean<HashMap<String, Object>>> cutPageBean = systemManageService.userListNew(vo, role, type);
        return Result.succ(cutPageBean);
    }

    //endregion

    //region 穿梭框样式的选择成员

    /**
     * 获取成员
     * @param account 账号
     * @param user_name 姓名
     * @param page 页码
     * @param pageSize 页面大小
     * @param role 权限组id
     * @param type 类型
     * @return List
     */
    @GetMapping("/userListTwo")
    public List<HashMap<String,String>> userListTwo(
            @PathParam("account") String account,
            @PathParam("user_name") String user_name,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize,
            @PathParam("role") String role,
            @PathParam("type") String type
    ){
        UserVo vo = new UserVo();
        vo.setAccount(account);
        vo.setUser_name(user_name);
        vo.setPage(page);
        vo.setPageSize(pageSize);
        vo.setPageStart((page - 1) * pageSize);
        return systemManageService.userListTwo(vo, role, type);
    }

    /**
     * 获取成员
     * @param role 权限组id
     * @return ArrayList
     */
    @GetMapping("/userListTwoChoose")
    public ArrayList<String> userListTwoChoose(
            @PathParam("role") String role
    ){
        return systemManageService.userListTwoChoose(role);
    }

    /**
     * 修改权限组的成员信息
     * @param objectHashMap 成员信息
     * @return String
     */
    @PostMapping("/saveCheckedDatasTwo")
    public Result saveCheckedDatasTwo(
            @RequestBody HashMap<String,Object> objectHashMap
    ){
        Object checkedDatas = objectHashMap.get("checkedData");
        Object role = objectHashMap.get("role");
        return systemManageService.saveCheckedDatasTwo(checkedDatas, role.toString());
    }
    //endregion
}
