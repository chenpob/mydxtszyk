package web.controller.admin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.common.Result;

@RestController
@RequestMapping("/adminResponse")
public class AdminResponseController {

    @GetMapping("/getLoginFail")
    public Result GetLogin() {
        return Result.fail("请登录！");
    }

    @GetMapping("/postLoginFail")
    public Result PostLogin() {
        return Result.fail("请登录！");
    }

    @GetMapping("/getLoginNoRight")
    public Result getLoginNoRight() {
        return Result.fail("此账号无管理员权限请联系管理员");
    }

    @GetMapping("/postLoginNoRight")
    public Result postLoginNoRight() {
        return Result.fail("此账号无管理员权限请联系管理员");
    }
}