package web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import web.bo.ScreenUploadRecordBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.admin.user.ScreenUploadEntity;
import web.service.ScreenUploadService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/admin/ScreenUpload")
public class ScreenUploadController {

    @Autowired
    private ScreenUploadService scService;

    /**
     * 列表页
     * @param page
     * @param pageSize
     * @param searchValue
     * @return
     */
    @GetMapping("/getList")
    public Result getList(@PathParam("page") Integer page,
                          @PathParam("pageSize") Integer pageSize,
                          @PathParam("searchValue") String searchValue){
        CutPageBean<ScreenUploadEntity> res = scService.getList(page,pageSize,searchValue);
        return Result.succ(res);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @GetMapping("/delete")
    public Result delete(@PathParam("id") Integer id) {
        return scService.delete(id);
    }

    /**
     * 保存
     * @param recordBo
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestParam(value = "filePicture",required = false) MultipartFile filePicture, ScreenUploadRecordBo recordBo) {
        return scService.save(filePicture,recordBo);
    }

    /**
     * 编辑
     * @param recordBo
     * @return
     */
    @PostMapping("/edit")
    public Result edit(@RequestParam(value = "filePicture",required = false) MultipartFile filePicture, ScreenUploadRecordBo recordBo) {
        return scService.edit(filePicture,recordBo);
    }

    /**
     * 所有可显示的文件
     * @return
     */
    @GetMapping("/getListALL")
    public Result getListALL() {
        List<ScreenUploadEntity> res = scService.getListALL();
        return Result.succ(res);
    }

}
