package web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.bo.admin.AdminDepartmentBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.AdminDepartmentPo;
import web.entity.UserDo;
import web.entity.UserVo;
import web.service.AdminDepartmentService;
import web.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.HashMap;

/**
 * @author cb
 * @Description TODO
 * @date 2023/12/9
 */

@RestController
@CrossOrigin
@RequestMapping("/adminDepartmentInfo")
public class AdminDepartmentInfoController {

    @Autowired
    AdminDepartmentService adminDepartmentService;

    @GetMapping("/departmentlist")
    public Result list(
            @PathParam("departmentname") String departmentname,
            @PathParam("orderType") String orderType,
            @PathParam("orderValue") String orderValue,
            @PathParam("page") int page,
            @PathParam("pageSize") int pageSize
    ) {
        AdminDepartmentBo adminDepartmentBo = new AdminDepartmentBo();

        adminDepartmentBo.setDepartmentName(departmentname);
        adminDepartmentBo.setOrderType(orderType);
        adminDepartmentBo.setOrderValue(orderValue);
        adminDepartmentBo.setPage(page);
        adminDepartmentBo.setPageSize(pageSize);
        adminDepartmentBo.setStartPage((page-1)*pageSize);
        CutPageBean<HashMap<String, Object>> cutPageBean = adminDepartmentService.cutPage(adminDepartmentBo);
        return Result.succ(cutPageBean);

    }
    @PostMapping("deleteDepartment")
    @ResponseBody
    public Result deleteDepartment(@RequestBody AdminDepartmentPo adminDepartmentPo){

           return  adminDepartmentService.DeleteOne(adminDepartmentPo);
    }

    @PostMapping("addDepartment")
    @ResponseBody
    public  Result addOne(@RequestBody AdminDepartmentPo adminDepartmentPo){
        return adminDepartmentService.AddOne(adminDepartmentPo);

    }






}
