package web.controller.admin;

import com.sun.org.apache.xpath.internal.functions.FuncSubstring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.common.Result;
import web.entity.UserDo;
import web.service.Security_LoginService;
import web.service.UserService;
import web.validationAPI.User.Login;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/admin/validate")
public class PhoneLoginController {

    @Autowired
    Security_LoginService securityLoginService;
    @Autowired
    UserService userService;

    @PostMapping("/phonelogin")
    public Result yzm(@RequestBody Map<String,String> ssologinparams) {
        String code=ssologinparams.get("code");
        String phone=ssologinparams.get("phone");
        if(!StringUtils.isEmpty(code)){
           return securityLoginService.Phonelogin(phone);
        }else{
            return null;
        }
    }


    @PostMapping("/getPhoneUser")
    public Result getPhoneUser(@RequestBody Map<String,String> ssologinparams) {
        String code=ssologinparams.get("code");//1为已登录教务系统，0为未登录教务系统
        String phone=ssologinparams.get("phone");
        if(!StringUtils.isEmpty(code)&&code.equals("1")){
           boolean isExist= userService.isPhoneExist(phone);
            return Result.succ(isExist);
        }else{
            return Result.fail("非法操作！");
        }
    }

}
