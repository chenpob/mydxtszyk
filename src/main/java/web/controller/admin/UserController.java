package web.controller.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.UserDo;
import web.entity.UserVo;
import web.entity.admin.user.DownloadsEntity;
import web.entity.admin.user.ResetPwdDo;
import web.service.UserService;
import web.validationAPI.User.*;

import javax.websocket.server.PathParam;
import java.util.HashMap;

@RestController
@RequestMapping("/admin/user")

public class UserController {

    @Autowired
    UserService userService;

//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @GetMapping("/list")
    public Result list(
            @PathParam("account") String account,
            @PathParam("user_name") String user_name,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize
    ) {
        UserVo vo = new UserVo();
        vo.setAccount(account);
        vo.setUser_name(user_name);
        vo.setPage(page);
        vo.setPageSize(pageSize);
        vo.setPageStart((page - 1) * pageSize);
        CutPageBean<HashMap<String, Object>> cutPageBean = userService.list(vo);
        return Result.succ(cutPageBean);
    }

//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @GetMapping("/list_org_like")
    public Result list_org_like(
            @PathParam("account") String account,
            @PathParam("user_name") String user_name,
            @PathParam("org_name") String org_name,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize
    ) {
        UserVo vo = new UserVo();
        vo.setAccount(account);
        vo.setUser_name(user_name);
        vo.setOrg_name(org_name);
        vo.setPage(page);
        vo.setPageSize(pageSize);
        vo.setPageStart((page - 1) * pageSize);
        CutPageBean<HashMap<String, Object>> cutPageBean = userService.list_org_like(vo);
        return Result.succ(cutPageBean);
    }

    /**
     * 注册
     * @param userDo
     * @return
     */
    @PostMapping("/self-add")
    public Result selfAdd(
            @Validated({SelfAddUser.class}) @RequestBody UserDo userDo
    ) {
        Result res = userService.selfAdd(userDo);
        return res;
    }

    @PostMapping("/self-edit")
    public Result selfEdit(
            @Validated(SelfEditUser.class) @RequestBody UserDo userDo
    ) {
        Result res = userService.selfEdit(userDo);
        return res;
    }

    /**
     * 管理员添加用户
     * @param userDo
     * @return
     */
//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @PostMapping("/admin-add")
    public Result adminAdd(@Validated({AdminAddUser.class}) @RequestBody UserDo userDo) {
        Result res = userService.adminAdd(userDo);
        return res;
    }

//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @PostMapping("/admin-edit")
    public Result adminEdit(@Validated(AdminEditUser.class)@RequestBody UserDo userDo) {
        Result res = userService.adminEdit(userDo);
        return res;
    }

//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @PostMapping("/admin-batch-add")
    public Result adminBatchAdd(

    ) {

        return null;
    }

    /**
     * 管理员删除用户
     * @param userDo
     * @return
     */
//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @PostMapping("/deleteUser")
    public Result deleteUser(
            @Validated(DeleteUser.class) @RequestBody UserDo userDo
    ) {
        Result res = userService.deleteUser(userDo);
        return res;
    }


    /**
     * 管理员重置用户密码
     * 新密码由管理员指定
     * @return
     */
//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @PostMapping("/resetPwd")
    public Result resetPwd(
//            @RequestBody HashMap<String, String> map
            @Validated(AdminRestPwd.class) @RequestBody ResetPwdDo resetPwdDo
    ) {
        Result res = userService.resetPwd(resetPwdDo);
        return res;
    }


    /**
     * 个人中心修改密码
     * @param userDo 参数
     * @return
     */
    @PostMapping("/change-pwd")
    public Result changePwd(
            @Validated(UserChangePwd.class) @RequestBody UserDo userDo
    ) {
        Result res = userService.changePwd(userDo);
        return res;
    }

    @GetMapping("/basicInfo")
    public Result basicInfo() {
        Result res = userService.basicInfo();
//        Result res = Result.succ("xxx");
        return res;
    }

    //@PathParam("resource_title") Integer resource_title,@PathParam("create_time ") Integer create_time
    @GetMapping("/downloads")
    public Result downloads(  @PathParam("page") Integer page,
                              @PathParam("pageSize") Integer pageSize,
                              @PathParam("resource_title") String resource_title,
                              @PathParam("create_time ") String create_time){
        CutPageBean<DownloadsEntity>  res = userService.downloads(page,pageSize,resource_title,create_time);
        return Result.succ(res);
    }

    @GetMapping("/preview")
    public Result preview(  @PathParam("page") Integer page,
                              @PathParam("pageSize") Integer pageSize,
                            @PathParam("resource_title") String resource_title,
                            @PathParam("create_time ") String create_time){
        CutPageBean<DownloadsEntity>  res = userService.preview(page,pageSize,resource_title,create_time);
        return Result.succ(res);
    }
}
