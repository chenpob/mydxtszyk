package web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import web.bo.MyUploadRecordBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.admin.user.MyuploadEntity;
import web.service.MyUploadService;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

/**
 * MyUploadController
 * 个人中心--我的上传  相关接口
 * @author CNKI
 */

@RestController
@RequestMapping("/admin/myUpload")
public class MyUploadController {


    @Autowired
    MyUploadService myUploadService;

    /**
     *  条件搜索，个人资料列表
     * @param page 页码
     * @param pageSize 页容量
     * @param keyWord 搜索词
     * @param type 搜索条件
     * @return Result
     */
    @GetMapping("/getList")
    public Result getList(@PathParam("page") Integer page,
                            @PathParam("pageSize") Integer pageSize,
                            @PathParam("keyWord") String keyWord,
                            @PathParam("type ") String type){
        CutPageBean<MyuploadEntity> res = myUploadService.getList(page,pageSize,keyWord,type);
        return Result.succ(res);
    }

    /**
     * 根据id，获取信息
     * @param id 记录id
     * @return Result
     */
    @GetMapping("/detail")
    public Result detail(@PathParam("id") Integer id) {
        return myUploadService.detail(id);
    }


    /**
     * 上传 源文件
     * @param file 源文件
     * @return Result 返回源文件的信息保存到mysql表
     */
    @PostMapping("/uploadFile")
    public Result uploadFile(@RequestParam("file") MultipartFile file) {
        return myUploadService.uploadFile(file);
    }


    /**
     * 下载 源文件
     * @param id 删除的id
     * @return Result
     */
    @GetMapping("/downloadFile")
    public void downloadFile(@PathParam("id") Integer id, HttpServletResponse response) {
        if (id == null) {
            return;
        }
        try {
            myUploadService.downloadFile(id,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 新增保存上传的文件记录信息
     * @param recordBo 上传的文件记录信息
     * @return Result
     */
    @PostMapping("/save")
    public Result save(@RequestBody MyUploadRecordBo recordBo) {
        return myUploadService.save(recordBo);
    }

    /**
     * 修改上传的文件记录信息
     * @param recordBo 上传的文件记录信息
     * @return Result
     */
    @PostMapping("/edit")
    public Result edit(@RequestBody MyUploadRecordBo recordBo) {
        return myUploadService.edit(recordBo);
    }


    /**
     * 删除上传的 个人资料和MySQL记录
     * @param id 删除的记录id
     * @return Result
     */
    @GetMapping("/delete")
    public Result delete(@PathParam("id") Integer id) {
        return myUploadService.delete(id);
    }

    /**
     * 删除上传的文件
     * @param pathFile 删除的文件路径
     * @return Result
     */
    @GetMapping("/deleteFile")
    public Result deleteFile(@PathParam("pathFile") String pathFile) {
        return myUploadService.deleteFile(pathFile);
    }


    /**
     * 判断 源文件   是否存在
     * @param id 删除的id
     * @return Result
     */
    @GetMapping("/existFile")
    public Result existFile(@PathParam("id") Integer id) {
        return  myUploadService.existFile(id);
    }
}
