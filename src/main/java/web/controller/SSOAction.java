package web.controller;

import com.alibaba.fastjson.JSON;
import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.bo.VIdeoResourBo;
import web.common.Result;
import web.entity.SSOBo;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/ssoLogin")
public class SSOAction {
    //配置引用yml文件地址
    @Value("${filePath.tokenPath}")
    private String tokenPath;


    @PostMapping("/index")
    public Result homeItem(){
        String token=obtainToken();
//        String loginUrl="https://my.cnki.net/IntegrateLogin/Redirect.ashx?token="+token+"&ReturnUrl=https://www.cnki.net&AppendUID=0";
        String loginUrl="https://my.cnki.net/IntegrateLogin/Redirect.ashx?token="+token+"&ReturnUrl=";
        return Result.succ(loginUrl);
    }

    public String obtainToken(){
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(tokenPath);
        SSOBo ssoBo=new SSOBo();
        ssoBo.setKey("5568D35A-8FE2-4A66-893F-A45314CD1418");
        ssoBo.setUserName("xn0778");
        ssoBo.setUserIp("218.89.178.103");

        String jsonString = JSON.toJSONString(ssoBo);

        StringEntity entity = new StringEntity(jsonString, "UTF-8");

        // post请求是将参数放在请求体里面传过去的;这里将entity放入post请求体中
        httpPost.setEntity(entity);

        httpPost.setHeader("Content-Type", "application/json;charset=utf8");

        // 响应模型
        CloseableHttpResponse response = null;
        JSONObject jsonObject=null;
        String token=null;
        try {

            // 由客户端执行(发送)Post请求
            response = httpClient.execute(httpPost);
            // 从响应模型中获取响应实体
            HttpEntity responseEntity = response.getEntity();
            jsonObject = JSONObject.fromObject(EntityUtils.toString(responseEntity));
            token=(String) jsonObject.get("Token");
        }catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return token;
    }
}
