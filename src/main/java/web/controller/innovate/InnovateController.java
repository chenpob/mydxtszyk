package web.controller.innovate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import web.Annotation.LogAnnotation;
import web.bo.Innovate.InnovateDbBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.Innovate.InnovateDbEntity;
import web.service.innovate.InnovateService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Description:协同创新库
 *
 * @author BaiGe
 * @date: 2022/7/12 17:02
 * @Version 1.0
 */
@RestController
@RequestMapping("/innovate")
public class InnovateController {
    @Autowired
    private InnovateService innovateService;


    @GetMapping("/upload")
    public Object upload(HttpServletRequest request,@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        System.out.println(originalFilename);
        return originalFilename;
    }

    /**
     * 协同创新库最新几条的接口
     *
     * @return
     */
    //@LogAnnotation("CX")
    @PostMapping("/commonSearchTopInnovate")
    public Result commonSearchTopInnovate(@RequestBody InnovateDbBo innovateDbBo) {
        List<InnovateDbEntity> list = innovateService.commonSearchTopInnovate(innovateDbBo);
        return Result.succ(list);
    }


    /**
     * 协同创新库的共用检索接口
     *
     * @return
     */
    @LogAnnotation("CX")
    @PostMapping("/commonSearchInnovate")
    public Result commonSearchInnovate(@RequestBody InnovateDbBo innovateDbBo) {
        CutPageBean<InnovateDbEntity> cutPageList = innovateService.commonSearchInnovate(innovateDbBo);
        return Result.succ(cutPageList);
    }


    /**
     * 协同创新库的高级检索接口
     *
     * @return
     */
    //@LogAnnotation("CX")
    @PostMapping("/advSearchInnovate")
    public Result advSearchInnovate(@RequestBody InnovateDbBo innovateDbBo) {
        CutPageBean<InnovateDbEntity> cutPageList = innovateService.advSearchInnovate(innovateDbBo);
        return Result.succ(cutPageList);
    }

    @PostMapping("/commonadvSearch")
    public Result commonadvSearch(@RequestBody InnovateDbBo innovateDbBo) {
        CutPageBean<InnovateDbEntity> cutPageList = innovateService.commonadvSearch(innovateDbBo);
        return Result.succ(cutPageList);
    }

    /**
     * 协同创新库的通用文件接口--post方式
     *
     * @return
     */
//    @LogAnnotation("XZ")
    @PostMapping("/commonFileInterface")
    public void commonFileInterface(@RequestBody InnovateDbBo innovateDbBo,
                                    HttpServletResponse response) throws IOException, InterruptedException {
        Object o = innovateService.commonFileInterface(innovateDbBo, response);
    }

    /**
     * 协同创新库的通用文件接口--get方式
     *
     * @return
     */
//    @LogAnnotation("XZ")
    @GetMapping("/commonFileInterface2")
    public void commonFileInterface2(@RequestParam("sysId") String sysId,
                                          @RequestParam("tableName") String tableName,
                                          @RequestParam("fileName") String fileName,
                                          @RequestParam("num") String num,
                                          HttpServletResponse response) throws IOException, InterruptedException {
        InnovateDbBo innovateDbBo=new InnovateDbBo();
        innovateDbBo.setSysId(sysId);
        innovateDbBo.setTableName(tableName);
        innovateDbBo.setFileName(fileName);
        innovateDbBo.setNum(num);
        Object o1 = innovateService.commonFileInterface(innovateDbBo, response);
    }

    /**
     * 理论研究分类专用接口
     *
     * @return
     */
    //@LogAnnotation("CX")
    @PostMapping("/onlyForTheoryResearch")
    public Result onlyForTheoryResearch(@RequestBody InnovateDbBo innovateDbBo) {
        List<InnovateDbEntity> list = innovateService.onlyForTheoryResearch(innovateDbBo);
        return Result.succ(list);
    }

    /**
     * 详情页
     *
     * @return
     */
    @GetMapping("/indexInnovate")
    public Result indexInnovate(@RequestParam(value = "fileName",required = true) String fileName,
                                @RequestParam(value = "tableName",required = true) String tableName,
                                @RequestParam(value = "sysId",required = true) String  sysId) {
        InnovateDbEntity innovate = innovateService.indexInnovate(fileName,tableName,sysId);
        return Result.succ(innovate);
    }

    /**
     * 获取抓取图片
     *
     * @return
     */
    @GetMapping("/getCaptureCover/{sortCode}/{topNum}")
    public Result getCaptureCover(@PathVariable("sortCode") String sortCode,
                                  @PathVariable("topNum") Integer topNum) {
        List<InnovateDbEntity> captureCover = innovateService.getCaptureCover(sortCode, topNum);
        return Result.succ(captureCover);
    }

}
