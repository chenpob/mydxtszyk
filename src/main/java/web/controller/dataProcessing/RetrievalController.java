package web.controller.dataProcessing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import web.bo.IntegrationEntityBO;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.IntegrationEntity;
import web.service.RetrievalService;

/**
 * 数据管理
 */
@RestController
@RequestMapping("/dataProcessing")
public class RetrievalController {
    @Autowired
    private RetrievalService retrievalService;

    /**
     * 检索
     *
     * @param integrationEntityBO
     * @return
     */
    @PostMapping("/retrieval")
    public Result retrieval(@RequestBody IntegrationEntityBO integrationEntityBO) {
        CutPageBean<IntegrationEntity> gg = retrievalService.retrieval(integrationEntityBO);
        return Result.succ(gg);
    }

    /**
     * 检索党校期刊
     *
     * @param
     * @return
     */
    @PostMapping("/retrievalQK")
    public Result retrievalQK(@RequestBody IntegrationEntityBO integrationEntityBO) {
        CutPageBean<IntegrationEntity> cutPageBean = retrievalService.retrievalQK(integrationEntityBO);
        return Result.succ(cutPageBean);
    }

    /**
     * 应急库,协同创新,期刊库删除
     *
     * @param integrationEntityBO
     * @return
     */
    @PostMapping("/delete")
    public Result delete(@RequestBody IntegrationEntityBO integrationEntityBO) {
        String dataBase=integrationEntityBO.getDatabase();
        String result="";
        if ("MYDXBDQKXQ".equalsIgnoreCase(dataBase)) { //期刊子库
            result = retrievalService.deleteQkZk(integrationEntityBO);

        } else if ("MYDXBDQK".equalsIgnoreCase(dataBase)) { //期刊
             result = retrievalService.deleteQk(integrationEntityBO);
        }  else if ("LDYXBD".equalsIgnoreCase(dataBase)||
                  "LDYXBD_WX".equalsIgnoreCase(dataBase)) { //两弹一星
             result = retrievalService.deleteLdyx(integrationEntityBO);
        }else {
             result = retrievalService.delete(integrationEntityBO);
        }

        if (result.equals("删除失败")) {
            return Result.fail(result);
        } else {
            return Result.succ(result);
        }
    }

    /**
     * 应急库,协同创新,期刊库更新
     *
     * @param integrationEntityBO
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestParam(value = "file", required = false) MultipartFile file,
                         @RequestParam(value = "filePicture", required = false) MultipartFile filePicture,
                         IntegrationEntityBO integrationEntityBO) {
        String result = "";
        if ("MYDXBDQK".equalsIgnoreCase(integrationEntityBO.getDatabase())) {
            result = retrievalService.updateQk(file, filePicture, integrationEntityBO);
        } else if ("MYDXBDQKXQ".equalsIgnoreCase(integrationEntityBO.getDatabase())) {
            result = retrievalService.updateQkZk(file, integrationEntityBO);
        }else if ("3".equals(integrationEntityBO.getType()) ) {//两弹一星
            result = retrievalService.updateLdyx(file,filePicture, integrationEntityBO);
        } else {
            result = retrievalService.update(file, filePicture, integrationEntityBO);

        }

        if (result.equals("更新失败")) {
            return Result.fail(result);
        } else {
            return Result.succ(result);
        }
    }


    /**
     * 应急库,协同创新,期刊库插入
     *
     * @param integrationEntityBO
     * @return
     */
    //required 默认为ture必传
    @PostMapping("/insert")
    public Result insert(@RequestParam(value = "file", required = false) MultipartFile file, @RequestParam(value = "filePicture", required = false) MultipartFile filePicture, IntegrationEntityBO integrationEntityBO) {
        String result="";
        if ("2".equals(integrationEntityBO.getType())) { //党校期刊
             result = retrievalService.insertQk(file, filePicture, integrationEntityBO);
        }else if ("3".equalsIgnoreCase(integrationEntityBO.getType())) { //两弹一星
             result = retrievalService.insertLdyxData(file, filePicture,integrationEntityBO);
        } else if ("MYDXBDQKXQ".equalsIgnoreCase(integrationEntityBO.getDatabase())) { //党校期刊子库
             result = retrievalService.insertQkZk(file, integrationEntityBO);
        } else {
             result = retrievalService.insert(file, filePicture, integrationEntityBO);
        }

        if (result.equals("存在相同文件")) {
            return Result.fail(result);
        } else if (result.equals("新增失败")) {
            return Result.fail(result);
        } else {
            return Result.succ(result);
        }
    }



}
