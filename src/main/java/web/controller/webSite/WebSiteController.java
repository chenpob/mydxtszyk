package web.controller.webSite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.bo.WebsiteEntityBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.WebsiteEntity;
import web.service.impl.WebsiteService;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/8/15 11:15
 * @Version 1.0
 */
@RestController
@RequestMapping("/webSite")
public class WebSiteController {
    @Autowired
    private WebsiteService websiteService;

    @PostMapping("/webSiteLog")
    public Result webSiteLog(@RequestBody WebsiteEntity websiteEntity, HttpServletRequest request) {
        Map map = websiteService.webSiteLog(request, websiteEntity);
        return Result.succ(map);
    }

    /**
     * 数量统计
     * @param
     * @param
     * @param
     * @return
     */
    @GetMapping("/numStatistics")
    public Result numStatistics() {
        List<Map> map = websiteService.numStatistics();
        return Result.succ(map);
    }

    /**
     * 查找今日,昨日浏览量总数
     * @param
     * @param
     * @param
     * @return
     */
    @GetMapping("/findWebSiteByDate")
    public Result getWebSiteNum(@RequestParam String date) {
        List<Map> maps = websiteService.getWebSiteNumByDateAndType(date);
        return Result.succ(maps);
    }

    /**
     * 趋势图报表
     * @param date
     * @return
     */
    @GetMapping("/reportForm")
    public Result reportForm(@RequestParam String date) {
//        Map maps = websiteService.reportForm(fromDate,toDate);
        Map maps =   websiteService.getReportFormByDate(date);
        return Result.succ(maps);
    }

    @GetMapping("/top10SearchWord")
    public Result top10SearchWord(){
        CutPageBean pageBean=new CutPageBean();
        List<Map> maps = websiteService.top10SearchWord();
        pageBean.initCutPage(10, 10, maps);

        return Result.succ(pageBean);
    }


    @GetMapping("/urlAccessLogList")
    public Result urlAccessLogList(
            @PathParam("groupType") String groupType,
            @PathParam("groupValue") String groupValue,
            @PathParam("startTime") String startTime,
            @PathParam("orderType") String orderType,
            @PathParam("orderValue") String orderValue,
            @PathParam("page") Integer page,
            @PathParam("pageSize") Integer pageSize,
            @PathParam("endTime") String endTime
    ){
        WebsiteEntityBo websiteEntityBo = new WebsiteEntityBo();
        websiteEntityBo.setGroupType(groupType);
        websiteEntityBo.setSearchValue(groupValue);
        websiteEntityBo.setStartTime(startTime);
        websiteEntityBo.setEndTime(endTime);
        websiteEntityBo.setOrderType(orderType);
        websiteEntityBo.setOrderValue(orderValue);
        websiteEntityBo.setPageStart((page - 1) * pageSize);
        websiteEntityBo.setPageSize(pageSize);


        if ("nochoice".equals(groupType)){
            CutPageBean<WebsiteEntity> pageBean = websiteService.urlAccessLogList_notChoice(websiteEntityBo);
            return Result.succ(pageBean);
        }
        else if ("user_name".equals(groupType)){
            CutPageBean<Map> mapCutPageBean = websiteService.urlAccessLogList_choiceUsername(websiteEntityBo);
            return Result.succ(mapCutPageBean);
        }else {
            CutPageBean pageBean=new CutPageBean();
            return Result.succ(pageBean);
        }
    }


}
