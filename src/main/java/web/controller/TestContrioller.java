package web.controller;


import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.dao.UtilDao;
import web.entity.CodeTree;
import web.entity.User2;
import web.service.UserService;
import web.utils.RandomUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/test")
public class TestContrioller {
    @Autowired
    private UserService userService;

    @Autowired
    private UtilDao utilDao;


    @PostMapping("/kbase")
    public Object kbase() {
        User2 kbase = userService.kbase(1l);
        return kbase;
    }


    @GetMapping("/aa")
    public void aa(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie cookie = new Cookie("x1", "ddd");
        response.addCookie(cookie);
//        return cookie;

    }

    @GetMapping("/bb")
    public String bb(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getCookies();
        String cookies1 = request.getHeader("Cookie_1");
//      response.sendRedirect("https://weibo.com/l/wblive/p/show/1022:2321324774929827758668");
        return "111222";

    }

    @GetMapping("/getTree")
    public List<CodeTree> getTree() {
        List<CodeTree> tree = userService.getTree();

        List<CodeTree> childrenNode = getChildrenNode("0", tree);
        //CodeTree[] codeTrees = tree.toArray(new CodeTree[tree.size()]);

        return childrenNode;
    }

    /**
     * 递归获取子节点下的子节点
     *
     * @param parentId  父节点的ID
     * @param treesList 所有菜单树集合
     * @return
     */
    private List<CodeTree> getChildrenNode(String parentId, List<CodeTree> treesList) {
        List<CodeTree> newTrees = new ArrayList<>();
        for (CodeTree tree : treesList) {
            if (StrUtil.isBlank(tree.getParent())) {
                continue;
            }
            if (parentId.equals(tree.getParent())) {
                //递归获取子节点下的子节点，即设置树控件中的children
                tree.setChildTrees(getChildrenNode(tree.getCode(), treesList));
                newTrees.add(tree);
            }
        }
        return newTrees;
    }


    public static List<Map<String, String>> abcd(CodeTree[] codeTrees, int length) {
        if (codeTrees.length == length) {
            return null;
        }

        abcd(codeTrees, length + 1);
        return null;
    }

        @GetMapping("/path")
    public void calculationImgPath() {
        String path = calculateAbstractPath("4.png", "MYDXYJK");
        System.out.println(path);
    }


    public String calculateAbstractPath(String fileName, String dbBaseCode) {
        String resultDes = dbBaseCode + "_DATABASE_DESCRIPT";
        String preUrl  = utilDao.getDes(resultDes);
        String dbBasePath = preUrl + "\\\\" + dbBaseCode + "\\\\Pages";
        // String dbBasePath =   "C:\\MYDX\\MYDXYJK\\Pages";
        System.out.println(dbBasePath);
        String dirName = RandomUtils.getDirByName(fileName);
        String tmpPath = new File(dbBasePath, dirName).toString();
        String dbAbsPath = new File(tmpPath, fileName).toString();
        return dbAbsPath;
    }


    @GetMapping("/log")
    @CrossOrigin("/*")
    public String tokens(HttpServletResponse response) {
        return "成功了?????";
    }
}
