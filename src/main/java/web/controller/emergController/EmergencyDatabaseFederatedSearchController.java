package web.controller.emergController;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.Annotation.LogAnnotation;
import web.bo.EmergencyEntityBo;
import web.bo.TypicalCharacterBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.*;
import web.entity.Innovate.InnovateDbEntity;
import web.service.EmergencyDatabaseFederatedSearchService;
import web.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Description: 应急库
 *
 * @author BaiGe
 * @date: 2022/6/20 15:14
 * @Version 1.0
 */
@RestController
@RequestMapping("/emergency")
public class EmergencyDatabaseFederatedSearchController {

    @Autowired
    private EmergencyDatabaseFederatedSearchService emergencyService;

    @Autowired
    private UserService userService;



    /**
     * 整合检索-应急库
     * @param emergencyEntity
     * @return
     */
    //@LogAnnotation("CX")
    @PostMapping("/complexSearchEmergency")
    public Result  complexSearchEmergencyDatabase(@RequestBody EmergencyEntityBo emergencyEntity){
        CutPageBean<EmergencyEntity> cutPageBean = emergencyService.complexSearchEmergency(emergencyEntity);
        return Result.succ(cutPageBean);
    }


    /**
     * 获取图片
     * @param tableCode
     * @param fileName
     * @param response
     */
    @GetMapping("/getPicture")
    public void   getPicture(@RequestParam("tableCode") String tableCode,
                             @RequestParam("fileName") String fileName,
                             HttpServletResponse response){
        emergencyService.getPicture(tableCode,fileName,response);
    }

    @GetMapping("/getIndex")
    public Result getIndex(@RequestParam String sysId){
        EmergencyEntity emergencyEntity = emergencyService.getEmergencyEntityIndex(sysId);
        return Result.succ(emergencyEntity);
    }


    /**
     * 获得典型人物
     * @param entity 典型人物Bo
     * @return 分页集合
     */
    @PostMapping("/getTypicalCharacter")
    public Result getTypicalCharacter(@RequestBody TypicalCharacterBo entity){
        CutPageBean<TypicalCharacterEntity> pageBean = emergencyService.getTypicalCharacter(entity);
        return Result.succ(pageBean);
    }

    /**
     * 获得两弹一星人物
     * @param entity 典型人物Bo
     * @return 分页集合
     */
    @PostMapping("/getLDYX")
    public Result getLDYXList(@RequestBody TypicalCharacterBo entity){
        CutPageBean<TypicalCharacterEntity> pageBean = emergencyService.getLDYXList(entity);
        return Result.succ(pageBean);
    }


    /**
     * 获得典型人物-首页几条
     * @param entity 典型人物Bo
     * @return 分页集合
     */
    @PostMapping("/getTypicalCharacterTopNum")
    public Result getTypicalCharacterTopNum(@RequestBody TypicalCharacterBo entity){
        List<TypicalCharacterEntity> pageBean = emergencyService.getTypicalCharacterTopNum(entity);
        return Result.succ(pageBean);
    }


    /**
     * 获取热词
     * @param limitNum
     * @return
     */
    @GetMapping("/getHotWord")
    public Result getHotWord(@RequestParam("limitNum") Integer limitNum){
        List<HotWord> hotWord = emergencyService.getHotWord(limitNum);
        return Result.succ(hotWord);
    }

    /**
     * 获取抓取图片-应急库
     *
     * @return
     */
    @GetMapping("/getCaptureCover/{sortCode}/{topNum}")
    public Result getCaptureCover(@PathVariable("sortCode") String sortCode,
                                  @PathVariable("topNum") Integer topNum) {
        List<IndexPictureEntity> captureCover = emergencyService.getCaptureCover(sortCode, topNum);
        return Result.succ(captureCover);
    }



    @GetMapping("/getTree")
    public List<CodeTree> getTree() {
        List<CodeTree> tree = userService.getTree();

        List<CodeTree> childrenNode = getChildrenNode("0", tree);
        //CodeTree[] codeTrees = tree.toArray(new CodeTree[tree.size()]);

        return childrenNode;
    }

    /**
     * 递归获取子节点下的子节点
     *
     * @param parentId  父节点的ID
     * @param treesList 所有菜单树集合
     * @return
     */
    private List<CodeTree> getChildrenNode(String parentId, List<CodeTree> treesList) {
        List<CodeTree> newTrees = new ArrayList<>();
        for (CodeTree tree : treesList) {
            if (StrUtil.isBlank(tree.getParent())) {
                continue;
            }
            if (parentId.equals(tree.getParent())) {
                // 递归获取子节点下的子节点，即设置树控件中的children
                tree.setChildTrees(getChildrenNode(tree.getCode(), treesList));
                newTrees.add(tree);
            }
        }
        return newTrees;
    }
}
