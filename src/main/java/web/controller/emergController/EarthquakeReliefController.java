package web.controller.emergController;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import web.bo.*;
import web.common.Result;
import web.entity.DetailEntity;
import web.service.EarthquakeReliefService;
import web.utils.FileuploadUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/earthquakeRelief")
/**
 * -抗震救灾
 */
public class EarthquakeReliefController {


    @Value("${filePath.VideoPath}")
    private String VideoPath;


    @Autowired
    private EarthquakeReliefService earthquakeReliefService;

    /**
     * -获取文献
     *
     * @param standardEntityBo
     * @return
     */
    @PostMapping("/getDocument")
    public Result ReviewEmergency(@RequestBody StandardEntityBo standardEntityBo) {
        List<StandardEntityBo> standardEntityBos = earthquakeReliefService.ReviewEmergency(standardEntityBo);
        return Result.succ(standardEntityBos);
    }

    /**
     * -获取图片
     *
     * @param pictureEntityBo
     * @return
     */
    @PostMapping("/getPic")
    public Result leaderIdea(@RequestBody PictureEntityBo pictureEntityBo) {
        List<PictureEntityBo> pictureEntityBos = earthquakeReliefService.leaderIdea(pictureEntityBo);
        return Result.succ(pictureEntityBos);
    }

    /**
     * -获取视频
     *
     * @param vIdeoResourBo
     * @return
     */
    @PostMapping("/getVideo")
    public Result getVideo(@RequestBody VIdeoResourBo vIdeoResourBo) {
        List<VIdeoResourBo> pictureEntityBos = earthquakeReliefService.getVideo(vIdeoResourBo);
        return Result.succ(pictureEntityBos);
    }

    /**
     * -获取视频detail
     *
     * @param
     * @return
     */
    @GetMapping("/getVideoDetail")
    public void getVideo(String fileName, HttpServletResponse response) {
        String path = VideoPath + fileName;
        FileuploadUtils.downloadJSYY(path, response);
    }


    /**
     * -get文献详情
     *
     * @param
     * @return
     */
    @PostMapping("/getDocumentDetail")
    public Result getDocumentDetail(@RequestBody DetailEntity entity) {
        DetailEntity detailEntity = earthquakeReliefService.getDocumentDetail(entity);
        return Result.succ(detailEntity);
    }

    @GetMapping("/changeEnd")
    public void changeEnd(@RequestBody DetailEntity entity) {
       Boolean res =  earthquakeReliefService.changeEnd(entity);
    }

}
