package web.controller.emergController;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.bo.EveBo;
import web.bo.EventDetailBo;
import web.common.Result;
import web.dao.UtilDao;
import web.service.EventService;
import web.utils.FileuploadUtils;
import web.utils.RandomUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;


/**
 * -大事记
 */
@RestController
@RequestMapping("/event")
public class EventController {

    @Autowired
    EventService eventService;

    @Autowired
    private UtilDao utilDao;

    @PostMapping("/eventDate")
    public Result getEventDate(@RequestBody EventDetailBo eventDetailBo) {
        List<String> eventDate = eventService.getEventDate(eventDetailBo);
        return Result.succ(eventDate);
    }

    @PostMapping("/getEventByYear")
    public Result getEventByYear(@RequestBody EventDetailBo eventDetailBo) {
        List<EventDetailBo> eventDate = eventService.getEventByYear(eventDetailBo);
        return Result.succ(eventDate);
    }

    @PostMapping("/getEventById")
    public Result getEventById(@RequestBody EventDetailBo eventDetailBo) {
        List<EventDetailBo> eventDate = eventService.getEventById(eventDetailBo);
        return Result.succ(eventDate);
    }

    @PostMapping("/eventList")
    public Result getEventDate(@RequestBody EveBo eveBo) {
        List<EveBo> eventDate = eventService.getEventList(eveBo);
        return Result.succ(eventDate);
    }


    @PostMapping("/getBigEventById")
    public Result getBigEventById(@RequestBody EveBo eveBo) {
        EveBo eventDate = eventService.getBigEventById(eveBo);
        return Result.succ(eventDate);
    }


    @RequestMapping(value = "GetEventPicture",method = RequestMethod.GET)
    public void  GetIndexPicture(String tableName , String picture, HttpServletResponse response){
        if (tableName.split("_").length>1){
            tableName = tableName.split("_")[0];
        }
        String resultDes = tableName + "_DATABASE_DESCRIPT";
        String preUrl  = utilDao.getDes(resultDes);
        String dbBasePath = preUrl + "\\" + tableName + "\\"+"Pages";
        // String dbBasePath =   "C:\\MYDX\\MYDXYJK\\Pages";
        String dirName = RandomUtils.getDirByName(picture);
        String tmpPath = new File(dbBasePath, dirName).toString();
        String dbAbsPath = new File(tmpPath, picture).toString();
        FileuploadUtils.download(dbAbsPath,response);
    }

}
