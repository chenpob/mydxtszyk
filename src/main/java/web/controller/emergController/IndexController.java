package web.controller.emergController;

import com.zaxxer.hikari.util.SuspendResumeLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import web.Annotation.LogAnnotation;
import web.bo.IntegrationEntityBO;
import web.bo.TotalSearchEntityBo;
import web.bo.VIdeoResourBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.FileEntity;
import web.entity.IntegrationEntity;
import web.entity.StandardEntity;
import web.service.IndexService;
import web.utils.FileuploadUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping("/Index")
public class IndexController {
    @Autowired
    private IndexService indexService;

    @Value("${filePath.VideoPath}")
    private String VideoPath;

    @Value("${filePath.ZztpPath}")
    private String ZztpPath;


    //String Code, String type    Code,type
    @RequestMapping(value = "/GetIndexData",method =RequestMethod.GET)
    public Result GetIndexData(@RequestParam("code") String code,@RequestParam("top") String top ){
        List<StandardEntity> gg =indexService.GetIndexData(code, top);
        return Result.succ(gg);
    }

    @RequestMapping(value = "/GetPicture",method =RequestMethod.GET)
    public Result GetPicture(@RequestParam("code") String code,@RequestParam("top") String top ){
        List<StandardEntity> gg =indexService.GetPicture(code, top);
        return Result.succ(gg);
    }

    @PostMapping("/GetVideo")
    public Result getVideo(@RequestBody VIdeoResourBo vIdeoResourBo){
        List<VIdeoResourBo> pictureEntityBos=  indexService.GetVideo(vIdeoResourBo);
        return Result.succ(pictureEntityBos);
    }

    @GetMapping("/GetVideoDetail")
    public void getVideo(String fileName, HttpServletResponse response){
        String path =VideoPath+fileName;
        FileuploadUtils.downloadJSYY(path,response);
    }

   // @LogAnnotation("CX")
    @RequestMapping(value = "/Integration",method =RequestMethod.POST)
    public Result Integration(@RequestBody IntegrationEntityBO integrationEntityBO){
        CutPageBean<IntegrationEntity> gg =indexService.Integration(integrationEntityBO);
        return Result.succ(gg);
    }



    @RequestMapping(value = "/totalSearch",method =RequestMethod.POST)
    public Result totalSearch(@RequestBody TotalSearchEntityBo totalSearchEntityBo){
        CutPageBean<TotalSearchEntityBo> gg =indexService.totalSearch(totalSearchEntityBo);
        return Result.succ(gg);
    }

//    @PreAuthorize("@cnkiAuthType.ingroupAnd('3')")
    @RequestMapping(value = "/apitotalSearch",method =RequestMethod.POST)
    public Result totalSearch1(@RequestBody TotalSearchEntityBo totalSearchEntityBo){
        CutPageBean<TotalSearchEntityBo> gg =indexService.totalSearch(totalSearchEntityBo);
        return Result.succ(gg);
    }

    @RequestMapping(value = "/getFileNumber",method =RequestMethod.POST)
    public Result getFileNumber(@RequestBody FileEntity fileEntity){
        String path = ZztpPath+fileEntity.getName().toLowerCase();
//        String path = "D:\\cnki\\myts110001";
        File file = new File(path);
        if (!file.exists()){
            return  Result.fail("文件不存在");
        }
        ArrayList<String> arr = new ArrayList<>();
        Integer length = file.listFiles().length;
        for (File listFile : file.listFiles()) {
            arr.add(listFile.getName());
        }
//        arr.sort(new SortLenth());
        FileEntity fileEnti = new FileEntity();
        fileEnti.setLen(length);
        fileEnti.setArrayList(arr);
        return Result.succ(fileEnti);
    }


//     class SortLenth implements Comparator<String> {
//        @Override
//        public int compare(String o1, String o2) {
//             String nameOne = o1;
//             String nametwo = o2;
//             String nameOneStr=  nameOne.split("_")[1].split("\\.")[0];
//             String nameTwoStr=  nametwo.split("_")[1].split("\\.")[0];
//             Integer x = new Integer(nameOneStr);
//             Integer y= new Integer(nameOneStr);
//             return x-y;    //o2-o1  降序排列
//        }
//    }


}
