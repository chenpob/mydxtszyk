package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.common.Result;
import web.entity.Periodical.fullPeriodicalDisplay.argsObj.SelectObj;
import web.service.FullPeriodicalDisplayService;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/fullPeriodicalDisplay")
public class FullPeriodicalDisplayController {
    @Autowired
    FullPeriodicalDisplayService fullPeriodicalDisplayService;


    /**
     * 进入页面后发起请求
     *
     * @return Result
     */
    @GetMapping("/defaultsearch")
    public Result defaultsearchdefault(
            @PathParam("pykm") String pykm
    ) {
          return fullPeriodicalDisplayService.defaultsearch(pykm);
    }

    /**
     * 选择年份后,返回所选年份的所有期
     *
     * @param pykm 期刊代码
     * @param year 年份
     * @return 所选年份的所有期
     */
    @GetMapping("/selectyear")
    public Result selectyear(
            @PathParam("pykm") String pykm,
            @PathParam("year") String year
//            @RequestBody SelectObj selectObj
    ) {
        SelectObj selectObj = new SelectObj();
        selectObj.setPykm(pykm);
        selectObj.setYear(year);
        return fullPeriodicalDisplayService.selectyear(selectObj);
    }

    /**
     * 选择某期
     *
     * @param pykm     期刊id
     * @param year     年份
     * @param qi       期
     * @param page     页数
     * @param pageSize 页容量
     * @return res
     */
    @GetMapping("/selectqi")
    public Result selectyear(
            @PathParam("pykm") String pykm,
            @PathParam("year") String year,
            @PathParam("qi") String qi,
            @PathParam("page") String page,
            @PathParam("pageSize") String pageSize
    ) {
        SelectObj selectObj = new SelectObj();
        selectObj.setPykm(pykm);
        selectObj.setYear(year);
        selectObj.setQi(qi);
        selectObj.setPage(page);
        selectObj.setPageSize(pageSize);
        selectObj.setPageStart((Integer.parseInt(page) - 1) * Integer.parseInt(pageSize));
        return fullPeriodicalDisplayService.selectqi(selectObj);
    }

    @GetMapping("/selectbdqk")
    public Result selectbdqk(
            @PathParam("sno") String sno

    ) {
        SelectObj selectObj = new SelectObj();
        selectObj.setSno(sno);
        return fullPeriodicalDisplayService.selectbdqk(selectObj);
    }
}
