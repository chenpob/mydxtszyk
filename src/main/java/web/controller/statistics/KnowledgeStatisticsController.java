package web.controller.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.bo.KnowledgeStatisticYearBo;
import web.bo.KnowledgeStatisticsBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.BrowseDownLoadStaticsEntity;
import web.entity.CJFXBASEINFO;
import web.service.KnowledgeStatisticsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/knowledgeStatistics")
public class KnowledgeStatisticsController {
    @Autowired
    private KnowledgeStatisticsService knowledgeStatisticsService;

    /**
       根据行业分类获取本地文献数量  协同创新、应急管理、党校期刊
     */
    @PostMapping("/bdKnowledgeCount")
    public Result GetBDKnowledgeCount(@RequestBody KnowledgeStatisticsBo knowledgeStatisticsBo){
        Map<String,Long> map=knowledgeStatisticsService.bdKnowledgeCountByHyflCode(knowledgeStatisticsBo);
        return Result.succ(map);
    }
    /**
     根据行业分类获取公共知识数量  协同创新、应急管理、党校期刊
     */
    @PostMapping("/commonKnowledgeCount")
    public Result GetCommonKnowledgeCount(@RequestBody KnowledgeStatisticsBo knowledgeStatisticsBo){
        Map<String,Long> map=knowledgeStatisticsService.commonKnowledgeCountByHyflCode(knowledgeStatisticsBo);
        return Result.succ(map);
    }
    /**
     知识操作统计Top 浏览量、下载量
     */
    @PostMapping("/knowledgeBrowseDownloadTop")
    public Result GetKnowledgeBrowseDownloadTop(@RequestBody KnowledgeStatisticsBo knowledgeStatisticsBo){
        List<BrowseDownLoadStaticsEntity> knowledgeBrowseDownloadTops=knowledgeStatisticsService.knowledgeBrowseDownloadTop(knowledgeStatisticsBo);
        return Result.succ(knowledgeBrowseDownloadTops);
    }
    /**
     资源可视化分析-协同创新，应急管理，党校期刊各库数据量统计
     */
    @PostMapping("/dataBaseKnowledgeCount")
    public Result GetDataBaseKnowledgeCount(@RequestBody KnowledgeStatisticsBo knowledgeStatisticsBo){
        Map<String,Long> map=knowledgeStatisticsService.dataBaseKnowledgeCount(knowledgeStatisticsBo);
        return Result.succ(map);
    }
    /**
     资源可视化分析-知识类型分布统计，机构提交知识分布
     */
    @PostMapping("/knowledgeTypeYearCount")
    public Result GetKnowledgeTypeYearCount(@RequestBody KnowledgeStatisticsBo knowledgeStatisticsBo){
        Map<String,List<KnowledgeStatisticYearBo>> map= knowledgeStatisticsService.knowledgeTypeYearList(knowledgeStatisticsBo);
        return Result.succ(map);
    }
    /**
     资源可视化分析-顶部数据总量
     */
    @PostMapping("/knowledgeDataUploadDeptCount")
    public Result GetknowledgeDataUploadDeptCount(){
        Map<String,Long> map= knowledgeStatisticsService.knowledgeDataUploadDeptCount();
        return Result.succ(map);
    }

    /**
     资源可视化分析-知识操作情况分析
     */
    @PostMapping("/knowledgeOperationAnalysisByDate")
    public Result GetKnowledgeOperationAnalysisByDate(@RequestBody KnowledgeStatisticsBo knowledgeStatisticsBo){
        Map<String,List<KnowledgeStatisticYearBo>> map= knowledgeStatisticsService.knowledgeOperationAnalysisByDate(knowledgeStatisticsBo);
        return Result.succ(map);
    }

}
