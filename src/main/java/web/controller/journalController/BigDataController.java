package web.controller.journalController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.PropertiesBo;
import web.common.Result;
import web.service.BigDataService;
import web.utils.CommonConvertUtils;
import web.utils.FileUtil;

import java.util.List;


@RestController
@RequestMapping("/bigData")
public class BigDataController {
    @Autowired
    BigDataService bigDataService;

    /**
     * 热门文章推荐
     * @return Result
     */
    @PostMapping("/rankList")
    public Result rankList(@RequestBody CJFXBASEINFOBo cjfxbaseinfoBo){
        List<CJFXBASEINFOBo> cjfxbaseinfoBos = bigDataService.rankList(cjfxbaseinfoBo);
        return  Result.succ(cjfxbaseinfoBos);
    }



    /**
     * 文献数据分析
     * @return Result
     */
    @PostMapping("/dataAnalysis")
    public Result dataAnalysis(@RequestBody CJFXLASTBo cjfxlastBo){
        List<CJFXLASTBo> cjfxbaseinfoBos = bigDataService.dataAnalysis(cjfxlastBo);
        return  Result.succ(cjfxbaseinfoBos);
    }



    /**
     * 热门文章推荐
     * @return Result
     */
    @PostMapping("/circlePic")
    public Result circlePic(){
        List<PropertiesBo> propertiesBos = bigDataService.circlePic();
        return  Result.succ(propertiesBos);
    }
}
