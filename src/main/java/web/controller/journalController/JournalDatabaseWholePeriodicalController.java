package web.controller.journalController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.Annotation.LogAnnotation;
import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.ZkzsEntityBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.CJFXBASEINFO;
import web.entity.CJFXLAST;
import web.entity.ZkzsEntity;
import web.service.JournalDatabaseFederatedSearchService;
import web.service.JournalDatabaseWholePeriodicalService;

/**
 * Description:期刊库-整刊展示
 *
 * @author BaiGe
 * @date: 2022/6/28 15:53
 * @Version 1.0
 */
@RestController
@RequestMapping("/journalWholePeriodical")
public class JournalDatabaseWholePeriodicalController {

    @Autowired
    private JournalDatabaseWholePeriodicalService journalService;

    @LogAnnotation("CX")
    @PostMapping("/search")
    public Result  search(@RequestBody CJFXBASEINFOBo cjfxbaseinfoBo){
        CutPageBean<CJFXBASEINFO> pageBean = journalService.searchWholePeriodical(cjfxbaseinfoBo);
        return Result.succ(pageBean);
    }

    @PostMapping("/ZkJssearch")
    public Result  ZkJssearch(@RequestBody ZkzsEntityBo zkzsEntityBo){
        CutPageBean<ZkzsEntityBo> pageBean = journalService.ZkJssearch(zkzsEntityBo);
        return Result.succ(pageBean);
    }

}
