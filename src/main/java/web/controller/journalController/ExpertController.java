package web.controller.journalController;

//import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.json.JSONParserTokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.bo.ExpertEntityBO;
import web.common.CutPageBean;
import web.common.Result;
import web.dao.UtilDao;
import web.entity.CodeTreeEntity;
import web.entity.DocumentEntity;
import web.entity.ExpertEntity;
import web.service.ExpertService;
import web.utils.FileUtil;
import web.utils.FileuploadUtils;
import web.utils.RandomUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/Expert")
public class ExpertController {
    @Autowired
    private ExpertService expertService;

    @Autowired
    private UtilDao utilDao;

    @PostMapping(value = "/ExpertResource")
    public Result GetExpertData(@RequestBody ExpertEntityBO expertEntityBO) {
        CutPageBean<ExpertEntity> cutPageBean = expertService.GetExpertData(expertEntityBO);
        return Result.succ(cutPageBean);
    }

    @PostMapping(value = "/ExpertResource1")
    public Result GetExpertData1(@RequestBody ExpertEntityBO expertEntityBO) {
        CutPageBean<ExpertEntity> cutPageBean = expertService.GetExpertData1(expertEntityBO);
        return Result.succ(cutPageBean);
    }

    @PostMapping(value = "/ExpertResourceFive")
    public Result ExpertResourceFive() {
        List<ExpertEntity> expertEntities = expertService.ExpertResourceFive();
        for (ExpertEntity expertEntity : expertEntities) {
            System.out.println(expertEntity);
            String picture = expertEntity.getPicture();
            String tableName="MYDXQKZJK";
            String resultDes = tableName + "_DATABASE_DESCRIPT";
            String preUrl  = utilDao.getDes(resultDes);
            String dbBasePath = preUrl + "\\" + tableName + "\\"+"Pages";
            String dirName = RandomUtils.getDirByName(picture);
            String tmpPath = new File(dbBasePath, dirName).toString();
            String dbAbsPath = new File(tmpPath, picture).toString();
            byte[] res = FileuploadUtils.getImageBinary(dbAbsPath);
            expertEntity.setPicResource(res);
        }
        return Result.succ(expertEntities);
    }


    @RequestMapping(value = "/GetExpert",method = RequestMethod.GET)
    public Result GetExpert(@RequestParam("id") String id) {
        ExpertEntity expertEntity = expertService.GetExpert(id);
        return Result.succ(expertEntity);
    }

    @RequestMapping(value = "GetExpertPicture",method =RequestMethod.GET)
    public void  GetIndexPicture(String tableName , String picture, HttpServletResponse response){
            if (tableName.split("_").length>1){
                tableName = tableName.split("_")[0];
            }
        String resultDes = tableName + "_DATABASE_DESCRIPT";
        String preUrl  = utilDao.getDes(resultDes);
        //String dbBasePath = preUrl + "\\" + tableName + "\\"+"Pages";
        // String dbBasePath =   "C:\\MYDX\\MYDXYJK\\Pages";
        String dirName = RandomUtils.getDirByName(picture);
        //String tmpPath = new File(dbBasePath, dirName).getPath();
       // String dbAbsPath = new File(tmpPath, picture).getPath();
        String dbAbsPath = Paths.get(preUrl,tableName,"Pages",dirName, picture).toString();
        System.out.println("文件路径:"+dbAbsPath);
        FileuploadUtils.download(dbAbsPath,response);
    }

    @RequestMapping(value = "/GetRegion",method = RequestMethod.GET)
    public Result GetExpertData(){
        List<CodeTreeEntity> codeTree=expertService.selectRegion();
        return Result.succ(codeTree);
    }

    @RequestMapping(value = "/GetDocument",method = RequestMethod.GET)
    public Result GetDocument(@RequestParam("author") String author,@RequestParam("page") int page,@RequestParam("pageSize") int pageSize){
        CutPageBean<DocumentEntity> cutPageBean = expertService.selectDocument(author,page,pageSize);
        return Result.succ(cutPageBean);
    }

}
