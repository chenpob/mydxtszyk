package web.controller.journalController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.ExpertEntityBO;
import web.bo.PopularArticlesBo;
import web.common.Result;
import web.service.PeriodicalIndexService;

import java.util.List;


@RestController
@RequestMapping("/periodical")
public class PeriodicalIndexController {
    @Autowired
    PeriodicalIndexService periodicalIndexService;

    /**
     * 热门文章推荐
     * @return Result
     */
    @PostMapping("/popularArticles")
    public Result popularArticles(@RequestBody PopularArticlesBo popularArticlesBo){
        List<PopularArticlesBo> popularArticlesBoList = periodicalIndexService.popularArticles(popularArticlesBo);
        return  Result.succ(popularArticlesBoList);
    }

    /**
     * 整刊展示
     * @return Result
     */
    @PostMapping("/wholeAtiicles")
    public Result wholeAtiicles(@RequestBody CJFXBASEINFOBo cjfxbaseinfoBo){
        List<CJFXBASEINFOBo> cjfxbaseinfoBos = periodicalIndexService.wholeAtiicles(cjfxbaseinfoBo);
        return  Result.succ(cjfxbaseinfoBos);
    }


    /**
     * 党刊资源
     * @return Result
     */
    @PostMapping("/partyResource")
    public Result partyResource(@RequestBody CJFXLASTBo cjfxlastBo){
        List<CJFXLASTBo> cjfxbaseinfoBos = periodicalIndexService.partyResource(cjfxlastBo);
        return  Result.succ(cjfxbaseinfoBos);
    }


    /**
     * 专家学者
     * @return Result
     */
    @PostMapping("/getExpert")
    public Result getExpert(@RequestBody ExpertEntityBO expertBO){
        List<ExpertEntityBO> expertbos = periodicalIndexService.getExpert(expertBO);
        return  Result.succ(expertbos);
    }



    /**
     * 专家学者
     * @return Result
     */
    @PostMapping("/getExpertOrg")
    public Result getExpertOrg(){
        List<ExpertEntityBO> expertbos = periodicalIndexService.getExpertOrg();
        return  Result.succ(expertbos);
    }
}
