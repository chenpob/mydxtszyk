package web.controller.journalController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import web.Annotation.LogAnnotation;
import web.bo.CJFXLASTBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.CJFXLAST;
import web.service.JournalDatabaseFederatedSearchService;

/**
 * Description:期刊库-整合检索
 *
 * @author BaiGe
 * @date: 2022/6/28 15:53
 * @Version 1.0
 */
@RestController
@RequestMapping("/journalFederatedSearch")
public class JournalDatabaseFederatedSearchController {

    @Autowired
    private JournalDatabaseFederatedSearchService journalService;

    @LogAnnotation("CX")
    @PostMapping("/complexSearchJournal")
    public Result complexSearchJournalDatabase(@RequestBody CJFXLASTBo cjfxlastBo){
        CutPageBean<CJFXLAST> pageBean = journalService.complexSearchJournal(cjfxlastBo);
        return Result.succ(pageBean);
    }

}
