package web.controller.journalController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import web.bo.PeriodicalBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.PeriodicalEntity;
import web.entity.PeriodicalStageEntity;
import web.entity.PeriodicalYearEntity;
import web.service.PeriodicalService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/Periodical")
public class PeriodicalController {
    @Autowired
    private PeriodicalService periodicalService;


    @PostMapping(value = "/PeriodicalResource")
    public Result GetPeriodicalData(@RequestBody PeriodicalBo periodicalBo) {
        CutPageBean<PeriodicalEntity> cutPageBean = periodicalService.GetPeriodicalData(periodicalBo);
        return Result.succ(cutPageBean);
    }

    @PostMapping(value = "/Get512WWK")
    public Result Get512WWK(@RequestBody PeriodicalBo periodicalBo) {
        CutPageBean<PeriodicalEntity> cutPageBean = periodicalService.Get512WWK(periodicalBo);
        return Result.succ(cutPageBean);
    }

    @PostMapping(value = "/Periodical")
    public Result GetPeriodical(@RequestBody PeriodicalBo periodicalBo) {
        List<PeriodicalEntity> list = periodicalService.GetPeriodical(periodicalBo);
        return Result.succ(list);
    }

    @PostMapping(value = "/PeriodicalYear")
    public Result PeriodicalYear(@RequestBody PeriodicalBo periodicalBo) {
        List<PeriodicalYearEntity> list = periodicalService.PeriodicalYear(periodicalBo);
        String[] strArr = null;
        if (list != null && list.size() > 0) {
            strArr = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                strArr[i] = list.get(i).getYear();
            }
        }
        return Result.succ(strArr);
    }

    @PostMapping(value = "/PeriodicalStage")
    public Result PeriodicalStage(@RequestBody PeriodicalBo periodicalBo) {
        List<PeriodicalStageEntity> list = periodicalService.PeriodicalStage(periodicalBo);
        String[] strArr = null;
        if (list != null && list.size() > 0) {
            strArr = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                strArr[i] = list.get(i).getStage();
            }
        }
        return Result.succ(strArr);
    }

    /**
     * 以流的方式下载文件
     *
     * @param title 文件存放绝对路径
     * @param response
     * @return
     */
    //D:\绵阳党校期刊库本地资源\pdf
    /*
    @RequestMapping(value = "JournalPreview",method =RequestMethod.GET)
    public void JournalPreview(String title, HttpServletResponse response) {
        String book=title+".pdf";
        String path = JournalPath+book;
        FileuploadUtils.download(path, response);
    }
    */

}
