package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import web.bo.KnowledgeStatisticDeptBo;
import web.bo.KnowledgeStatisticsBo;
import web.bo.KnowledgeStatisticsOperationBo;
import web.entity.BrowseDownLoadStaticsEntity;

import java.util.List;

@Mapper
public interface KnowledgeStatisticsMapper {
    @DataSource("cnki-mysql")
    Long knowledgeDownloadCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo);
    @DataSource("cnki-mysql")
    Long knowledgeBrowseCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo);
    @DataSource("cnki-mysql")
    List<BrowseDownLoadStaticsEntity> knowledgeBrowseDownloadTop(KnowledgeStatisticsBo knowledgeStatisticsBo);
    @DataSource("cnki-mysql")
    List<KnowledgeStatisticDeptBo> knowledgeDeptUploadYearList(KnowledgeStatisticsBo knowledgeStatisticsBo);
    @DataSource("cnki-mysql")
    Long knowledgeUploadCount();
    @DataSource("cnki-mysql")
    Long knowledgeDeptCount();

    @DataSource("cnki-mysql")
    KnowledgeStatisticsOperationBo knowledgeOperationAnalysisByDate(KnowledgeStatisticsBo knowledgeStatisticsBo);
}
