package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import web.bo.BrowseDownLoadLogBo;
import web.entity.admin.BrowseDownLoadLogEntity;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/12 13:57
 * @Version 1.0
 */
@Mapper
public interface BrowseDownloadLogMapper {
    /**
     * 查询浏览下载集合
     * @param browseLogBo
     * @return
     */
    @DataSource("cnki_mysql")
    List<BrowseDownLoadLogEntity> searchBrowseDownloadList(BrowseDownLoadLogBo browseLogBo);

    /**
     * 查询浏览下载总条数
     * @param browseLogBo
     * @return
     */
    @DataSource("cnki_mysql")
    Integer searchBrowseDownloadCount(BrowseDownLoadLogBo browseLogBo);
}
