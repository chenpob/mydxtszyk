package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import web.bo.WebsiteEntityBo;
import web.entity.WebsiteEntity;

import java.util.List;
import java.util.Map;


@Mapper
public interface WebsiteMapper {
    /**
     * 查询今天之内 表中是否含有该远程地址
     *
     * @param remoteAddr IP地址
     * @param preDate    前一天
     * @param nextDate    后一天
     * @return 影响行数
     */
    @DataSource("cnki_mysql")
    List<WebsiteEntity> queryHasRecord(@Param("remoteAddr") String remoteAddr, @Param("preDate")String preDate, @Param("nextDate")String nextDate);

    /**
     * 插入Pv记录
     *
     * @param websiteEntity 参数为pageName和path
     * @return
     */
    @DataSource("cnki_mysql")
    int insertPv(WebsiteEntity websiteEntity);

    /**
     * 插入Uv记录
     *
     * @param websiteEntity 参数为pageName和path
     * @return
     */
    @DataSource("cnki_mysql")
    int insertUv(WebsiteEntity websiteEntity);

    /**
     * 插入Ip记录
     *
     * @param websiteEntity 参数为pageName和path
     * @return
     */
    @DataSource("cnki_mysql")
    int insertIp(WebsiteEntity websiteEntity);

    /**
     * 获得各个类型的记录总数
     * @return
     */
    @DataSource("cnki_mysql")
    List<Map> getNumGroupByType();

    /**
     * 根据时间和类型  获取记录数
     * @return
     */
    @DataSource("cnki_mysql")
   List<Map> getWebSiteNumByDateAndType(@Param("date") String date);

    /**
     * 报表统计
     * 根据时间和type分组,限定起始时间
     * @param fromDate
     * @param toDate
     * @return
     */
    @DataSource("cnki_mysql")
   List<Map> reportForm(@Param("fromDate") String fromDate,@Param("toDate") String toDate);

    /**
     * 获取相应日期的报表统计
     * @param date
     * @return
     */
    @DataSource("cnki_mysql")
    List<Map>  getReportFormByDate(@Param("date") String date);


    @DataSource("cnki_mysql")
    List<Map> top10SearchWord();

    @DataSource("cnki_mysql")
    @MapKey("id")
    List<WebsiteEntity> urlAccessLogList_notChoice(WebsiteEntityBo websiteEntityBo);

    @DataSource("cnki_mysql")
    Integer urlAccessLogList_notChoiceCounter(WebsiteEntityBo websiteEntityBo);

    @DataSource("cnki_mysql")
    List<Map> urlAccessLogList_choiceUsername(WebsiteEntityBo websiteEntityBo);

    @DataSource("cnki_mysql")
    Integer urlAccessLogList_choiceUsernameCounter(WebsiteEntityBo websiteEntityBo);
}
