package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Param;
import web.entity.CdPath;
import web.entity.MenuEntity;

import java.util.List;

public interface CommonMapper {
    @DataSource("cnki_mysql")
    List<MenuEntity> getInitJson(@Param("id") Integer id);

    @DataSource("cnki_mysql")
    String captchaRequire();

    @DataSource("cnki_mysql")
    String getCdPath(@Param("Cdrom") String  Cdrom);
}
