package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import web.entity.MyUploadSearchBo;
import web.entity.admin.user.MyuploadEntity;

import java.util.List;

/**
 * MyUploadMysqlMapper
 * @author CNKI
 */
@Mapper
public interface MyUploadMysqlMapper {

    /**
     *  条件搜索   个人中心-我的上传记录
     * @param vo 条件
     * @return List<MyuploadEntity>
     */
    @DataSource("cnki_mysql")
    List<MyuploadEntity> getList(MyUploadSearchBo vo);

    /**
     *  搜索结果，计数，翻页
     * @param vo 条件
     * @return Integer
     */
    @DataSource("cnki_mysql")
    Integer count(MyUploadSearchBo vo);

    /**
     * 根据id，查询上传文件记录信息
     * @param id id
     * @return 上传文件记录信息
     */
    MyuploadEntity selectById(Integer id);

    /**
     * 保存上传的文件记录信息
     * @param entity 上传的文件记录信息
     * @return int
     */
    int insert(MyuploadEntity entity);

    /**
     * 根据id，删除个人资料
     * @param id 个人资料id
     * @return int
     */
    int deleteById(Integer id);

    /**
     * 修改上传的文件记录信息
     * @param entity 上传的文件记录信息
     * @return int
     */
    int updateById(MyuploadEntity entity);
}
