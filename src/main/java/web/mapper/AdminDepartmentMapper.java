package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import web.bo.admin.AdminDepartmentBo;
import web.entity.AdminDepartmentPo;

import java.util.HashMap;
import java.util.List;

/**
 * @author cb
 * @Description TODO
 * @date 2023/12/9
 */

@Mapper
public interface AdminDepartmentMapper {
    @DataSource("cnki_mysql")
    List<HashMap<String, Object>> list (AdminDepartmentBo adminDepartmentBo);
    @DataSource("cnki_mysql")
    Integer totalCount (AdminDepartmentBo adminDepartmentBo);
    @DataSource("cnki_mysql")
   Integer  deleteDept(AdminDepartmentPo adminDepartmentPo);
    @DataSource("cnki_mysql")
    Integer  isDeptExist(String name);
    @DataSource("cnki_mysql")
    Integer addOne(AdminDepartmentPo adminDepartmentPo);
    @DataSource("cnki_mysql")
    Integer  editDept(AdminDepartmentPo adminDepartmentPo);
    @DataSource("cnki_mysql")
    AdminDepartmentPo findoneByid(AdminDepartmentPo adminDepartmentPo);
}
