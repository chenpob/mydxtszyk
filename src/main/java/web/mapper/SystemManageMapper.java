package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Param;
import web.entity.*;
import java.util.HashMap;
import java.util.List;

//@Mapper
public interface SystemManageMapper {

    @DataSource("cnki_mysql")
    boolean changeLoginVerificationCodeStatus(@Param("status") String status);

    List<HashMap<String, Object>> grouplist(RoleVo vo);

    Integer totalCount(RoleVo vo);

    /*修改组禁用状态*/
    Integer statusChange(String id, String status);

    /*获取所有成员列表*/
    List<HashMap<String, Object>> userList(UserVo vo);

    /*获取所有成员列表 另一种样式*/
    List<HashMap<String, Object>> userListNew(UserVo vo);

    /*获取所有成员数量*/
    Integer userCount(UserVo vo);

    /*获取已选成员列表*/
    List<HashMap<String, Object>> userListByRole(@Param("vo") UserVo vo, @Param("ro") String role);

    /*获取已选成员列表 另一种样式*/
    List<HashMap<String, Object>> userListByRoleNew(@Param("vo") UserVo vo, @Param("ro") String role);

    /*获取已选成员列表 第三种样式*/
    List<UserVo> userListTwo(@Param("vo") UserVo vo, @Param("ro") String role);
    List<UserVo> userListTwoChoose(String role);

    /*获取已选成员数量*/
    Integer userCountByRole(@Param("vo") UserVo vo, @Param("ro") String role);

    /*弹出框移除已选成员*/
    Integer moveMember(String user_id, String role_id);

    /*弹出框添加成员*/
    Integer addMember(String user_id, String role_id);

    /*新建权限信息*/
    Integer addAuthority(String name, String remark, String create_time, String create_by);

    /*编辑权限信息*/
    Integer editAuthority(String name, String remark, String roleId, String update_time, String update_by);

   /*删除该权限旧的所有权限*/
    Integer deleteOldPermission(String role_id);

    /*将新的所有权限插入表中*/
    Integer insertNewPermission(String role_id, String menu_id);

    /*获取权限树*/
    List<MenuEntity> getMenuTree();

    /*获取权限组的权限*/
    List<RoleMenu> getMenuByRole(String role_id);

    /*获取最新的权限组数据*/
    List<RoleVo> getNewestOne();

    /*删除权限组*/
    Integer deleteAuthority(String role_id);

    Integer delMemByRole(String role);

    Integer saveCheckedDatasTwo(String user_id,String role_id);

    Integer isAccountExist(String name);
}
