package web.mapper;


import com.cnki.dataSource.DataSource;
import web.common.CutPageBean;
import web.entity.UserDo;
import web.entity.UserVo;
import web.entity.admin.log.LoginDo;
import web.entity.admin.user.DownloadsEntity;
import web.entity.admin.user.ResetPwdDo;

import java.util.HashMap;
import java.util.List;


//@DataSource("cnki_mysql")
public interface UserMysqlMapper {
    @DataSource("cnki_mysql")
    List<HashMap<String, Object>> list(UserVo vo);

    @DataSource("cnki_mysql")
    List<HashMap<String, Object>> list_org_like(UserVo vo);

    @DataSource("cnki_mysql")
    Integer adminAdd(UserDo userDo);

    @DataSource("cnki_mysql")
    Integer adminEdit(UserDo userDo);

    @DataSource("cnki_mysql")
    Integer totalCount(UserVo vo);

    @DataSource("cnki_mysql")
    Integer totalCount_org_like(UserVo vo);

    @DataSource("cnki_mysql")
    Integer addUser(UserDo userDo);

    @DataSource("cnki_mysql")
    Integer editUser(UserDo userDo);

    @DataSource("cnki_mysql")
    Integer deleteUser(UserDo userDo);

    @DataSource("cnki_mysql")
    Integer isAccountExist(String account);

    @DataSource("cnki_mysql")
    Integer isPhoneExist(String phoneNumber);

    @DataSource("cnki_mysql")
    Integer isPhoneExistExceptOne(UserDo userDo);

//    @DataSource("cnki_mysql")
//    Integer resetPwd(HashMap<String, String> map);

    @DataSource("cnki_mysql")
    Integer resetPwd(ResetPwdDo resetPwdDo);


    @DataSource("cnki_mysql")
    UserDo getUserByAccount(String account);

    @DataSource("cnki_mysql")
    UserDo getUserByAccountAndPhone(String account);


    @DataSource("cnki_mysql")
    Integer selfAdd(UserDo userDo);


    @DataSource("cnki_mysql")
    Integer selfEdit(UserDo userDo);

    @DataSource("cnki_mysql")
    Integer changePwd(UserDo userDo);

    @DataSource("cnki_mysql")
    List<DownloadsEntity> downloadsList(UserVo vo);

    @DataSource("cnki_mysql")
    Integer count(UserVo vo);

    @DataSource("cnki_mysql")
    List<DownloadsEntity> previewList(UserVo vo);

    @DataSource("cnki_mysql")
    Integer count1(UserVo vo);

    @DataSource("cnki_mysql")
    List<HashMap<String, Object>> getMenuListById(String id);
}
