package web.mapper;

import com.cnki.dataSource.DataSource;
import web.entity.admin.log.SearchDo;

import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/8/30 11:25
 * @Version 1.0
 */
public interface KeyWordLogMapper {
    @DataSource("cnki_mysql")
    public List<SearchDo> cutPageKeyWord(Map map);
}
