package web.mapper;

import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import web.entity.ScreenUploadSearchBo;
import web.entity.admin.user.ScreenUploadEntity;

import java.util.List;

/**
 * ScreenUploadMysqlMapper
 * @author CNKI
 */
@Mapper
public interface ScreenUploadMysqlMapper {

    /**
     *  条件搜索
     * @param vo 条件
     * @return List<ScreenUploadEntity>
     */
    @DataSource("cnki_mysql")
    List<ScreenUploadEntity> getList(ScreenUploadSearchBo vo);

    /**
     * 所有显示的图片-显示屏用
     * @return
     */
    @DataSource("cnki_mysql")
    List<ScreenUploadEntity> getListALL();

    /**
     *  搜索结果，计数，翻页
     * @param vo 条件
     * @return Integer
     */
    @DataSource("cnki_mysql")
    Integer count(ScreenUploadSearchBo vo);

    /**
     * 根据id，查询
     * @param id id
     * @return
     */
    ScreenUploadEntity selectById(Integer id);

    /**
     * 保存
     * @param entity
     * @return int
     */
    int insert(ScreenUploadEntity entity);

    /**
     * 删除
     * @param id 主表id
     * @return int
     */
    int deleteById(Integer id);

    /**
     * 修改
     * @param entity
     * @return int
     */
    int updateById(ScreenUploadEntity entity);
}
