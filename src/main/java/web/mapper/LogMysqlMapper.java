package web.mapper;


import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import web.entity.admin.log.LoginDo;
import web.entity.admin.log.SearchDo;
import web.entity.admin.log.ViewDownloadDo;


//@DataSource("cnki_mysql")
@Mapper
public interface LogMysqlMapper {
    @DataSource("cnki_mysql")
    Integer insertViewLog(ViewDownloadDo viewDownloadDo);

    @DataSource("cnki_mysql")
    Integer insertDownLoadLog(ViewDownloadDo viewDownloadDo);

    @DataSource("cnki_mysql")
    Integer insertLoginLog(LoginDo loginDo);

    @DataSource("cnki_mysql")
    Integer insertSearchLog(SearchDo searchDo);


}
