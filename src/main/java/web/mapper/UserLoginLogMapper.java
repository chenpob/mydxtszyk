package web.mapper;


import com.cnki.dataSource.DataSource;
import web.bo.UserloginlogEntityBo;
import web.entity.UserloginlogEntity;

import java.util.List;


//@DataSource("cnki_mysql")
public interface UserLoginLogMapper {
    @DataSource("cnki_mysql")
    List<UserloginlogEntityBo> LoginLogList(UserloginlogEntityBo userloginlogEntityBo);

    @DataSource("cnki_mysql")
    List<UserloginlogEntity> getUserOtherLog(UserloginlogEntityBo userloginlogEntityBo);

    @DataSource("cnki_mysql")
    Long getUserOtherLogCount(UserloginlogEntityBo userloginlogEntityBo);
}
