package web.mapper;


import com.cnki.dataSource.DataSource;
import web.bo.ViewLogBo;

import java.util.List;


public interface ViewLoglMapper {

    @DataSource("cnki_mysql")
    List<ViewLogBo> getViewLog(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    Long getViewLogCount(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    List<ViewLogBo> getOtherViewLog(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    Long getOtherViewLogCount(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    List<ViewLogBo> getDownLLog(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    Long getDownLLogCount(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    List<ViewLogBo> getOtherDownLLog(ViewLogBo viewLogBo);

    @DataSource("cnki_mysql")
    Long getOtherDownLLogCount(ViewLogBo viewLogBo);
}
