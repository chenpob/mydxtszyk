package web.bo.Innovate;

import lombok.Data;
import web.entity.Innovate.InnovateDbEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/12 18:53
 * @Version 1.0
 */
@Data
public class InnovateDbBo extends InnovateDbEntity {
    /**
     * 当前页
     */
    private Integer page;
    /**
     * 每页显示条数
     */
    private Integer pageSize;
    /**
     * 起始条数
     */
    private Integer start;
    /**
     * 搜索词
     */
    private String searchType;

    /**
     * 搜索值
     */
    private String searchValue;

    /**
     * 分组字段
     */
    private String groupBy;
    /**
     * 排序字段
     */
    private String orderByType;
    /**
     * 排序值  升序：ase  降序：desc 相关性排序：RELEVANT
     */
    private String orderByValue;
    /**
     * 首展示最新几条
     */
    private Integer topNum;
    /**
     * 资源分类集合
     */
    private List<String> sortCodeList;

    /**
     * 资源分类Sql
     */
    private String sortCodeSql;
    /**
     * 初级检索条件sql
     */
    private String primarySearchSql;

    /**
     * 高级检索条件集合
     */
    private List<Map<String, String>> advSearchList;

    private String dbcode;

    /**
     * 高级检索条件sql
     */
    private String advSearchSql;
    /**
     * 检索方向 0:文献  1:图片  2:音视频
     */
    private Integer searchDirection;

    /**
     * 起始时间
     */
    private String beginDate;

    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 更新时间
     */
    private String updateDate;

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }

    public void setTheUpdateDate(String createDate) {
        long mill = System.currentTimeMillis();
        if ("最近一周".equals(createDate)) {
            mill -= 86400000l * 7;
        } else if ("最近一月".equals(createDate)) {
            mill -= 86400000l * 31;
        } else if ("最近半年".equals(createDate)) {
            mill -= 86400000l * 30.5 * 6;
        } else if ("最近一年".equals(createDate)) {
            mill -= 86400000l * 365;
        } else if ("今年迄今".equals(createDate)) {
            String year = new SimpleDateFormat("yyyy").format(new Date());
            String thisYear = year + "-01-01";
            setUpdateDate(thisYear);
            return;
        } else if ("上一年度".equals(createDate)) {
            String year = new SimpleDateFormat("yyyy").format(new Date());
            String lastYear = Integer.toString(Integer.parseInt(year) - 1);
            String overLastYear = lastYear + "-01-01";
            setUpdateDate(overLastYear);
            return;
        } else if ("不限".equals(createDate)) {
            setUpdateDate("");
            return;
        } else {
            setUpdateDate("");
            return;
        }
        setUpdateDate(new SimpleDateFormat("yyyy-MM-dd").format(mill));
    }
}
