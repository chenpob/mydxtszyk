package web.bo;

import lombok.Data;

import java.util.List;

@Data
public class KnowledgeStatisticsBo {
    /**
     * 数据库知识表 MYDXWX文献、MYDXTP图片、MYDXSP音视频、MYDXBDQK书籍
     */
    private String bdDataTable;
    /**
     * 行业分类代码 1 协同创新 2应急管理 3党校期刊  为空查询全部
     */
    private String hyflCode;
    /**
     * 查询数据类型  1数据量  2下载量  3浏览量
     */
    private Integer dataNumCode;
    /**
     * 本地知识表 MYDXWX文献、MYDXTP图片、MYDXSP音视频、MYDXBDQK书籍List<String>集合
     */
    private List<String> resourceCodes;
    /**
     * 知识操作统计类型  1下载 2浏览
     */
    private Integer actionType;
    /**
     * 知识操作统计类型Top数量 默认10
     */
    private Integer actionTypeLimitNun;
    /**
     * 知识统计年份 近几年 3|识操作情况分析 年
     */
    private Integer year;
    /**
     * 知识统计年份 机构提交开始年
     */
    private String starYear;
    /**
     * 知识统计年份 机构提交结束年
     */
    private String endYear;

    /**
     * 知识操作情况分析 月份
     */
    private Integer month;
    /**
     *统计查询条件
     */
    private String whereSql;

}
