package web.bo;

import lombok.Data;
import web.entity.TotalSearchEntity;

@Data
public class TotalSearchEntityBo extends TotalSearchEntity {

    private String type;

    private String dataSource;
    /**
     * 每页显示条数
     */
    public int pageSize;

    /**
     * 起始下标
     */
    private Integer start;

    /**
     * 页码
     */
    public Integer page;

    /**
     * 排序
     */
    private String orderBy;
    /**
     * 排序值
     */
    private String orderByValue;
    /**
     * 搜索选项
     */
    private String searchType;

    /**
     * 搜索词
     */
    private String searchValue;

    private String[] codeList;

    private String primarySearchSql;

    private String advSearchSql;

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }


}
