package web.bo;

import lombok.Data;
import web.entity.UserloginlogEntity;


@Data
public class UserloginlogEntityBo extends UserloginlogEntity {

    private String type;

    private String searchValue;

    private String startTime;

    private String endTime;

    private Integer page;

    private Integer pageSize;

    private Integer pageStart;
}
