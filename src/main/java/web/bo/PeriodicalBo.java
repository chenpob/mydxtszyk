package web.bo;

import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import web.entity.PeriodicalEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeriodicalBo extends PeriodicalEntity {
    /**
     * 党校分类
     */
    private  String classify;

    /**
     * 每页显示条数
     */
    public int pageSize;

    /**
     * 起始下标
     */
    private int start;

    /**
     * 页码
     */
    public int page;

    private String region;

    private String title;

    private String year;

    private String stage;

    private String searchType;

    private String searchValue;

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }

}
