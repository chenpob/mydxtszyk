package web.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bytedeco.javacpp.freenect;
import web.entity.EmergencyCodeEntity;
import web.entity.EmergencyEntity;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmergencyCodeEntityBo extends EmergencyCodeEntity {
    private String sysCode;
}
