package web.bo;

import lombok.Data;
import web.entity.Periodical.index.PopularArticles;

@Data
public class PopularArticlesBo extends PopularArticles {

    private String LimitNumber;

    private Integer randomNumber;
}
