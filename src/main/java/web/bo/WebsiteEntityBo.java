package web.bo;

import lombok.Data;
import web.entity.ViewLogEntity;
import web.entity.WebsiteEntity;

@Data
public class WebsiteEntityBo extends WebsiteEntity {

    private int pageSize;

    private int pageIndex;

    private  String endTime;

    private  String startTime;

    private String groupType;

    private String searchValue;

    private String orderType;

    private String orderValue;

    private int pageStart;

}
