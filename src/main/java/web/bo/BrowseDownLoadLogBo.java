package web.bo;

import lombok.Data;
import web.entity.admin.BrowseDownLoadLogEntity;

/**
 * Description:浏览日志bo
 *
 * @author BaiGe
 * @date: 2022/7/12 11:31
 * @Version 1.0
 */
@Data
public class BrowseDownLoadLogBo extends BrowseDownLoadLogEntity {
    /**
     * 分组值
     */
    private String  groupByValue;
    /**
     * 检索方式  1:浏览日志  2:下载日志
     */
    private Integer  type;
}
