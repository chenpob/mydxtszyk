package web.bo;

import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KnowledgeStatisticDeptBo {
    /**
     * 部门名称
     */
    @Column("departmentName")
    private String departmentName;
    /**
     * 数量
     */
    @Column("num")
    private Integer num;
}
