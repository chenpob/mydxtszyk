package web.bo;

import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KnowledgeStatisticYearBo {
    /**
     * 年份
     */
    @Column("year")
    private String year;
    /**
     * 数量
     */
    @Column("num")
    private Integer num;
}
