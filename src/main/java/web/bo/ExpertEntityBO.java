package web.bo;

import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import web.entity.ExpertEntity;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpertEntityBO  extends ExpertEntity {


    /**
     * 每页显示条数
     */
    public int pageSize;

    /**
     * 起始下标
     */
    private int start;

    /**
     * 页码
     */
    public int page;

    private String[] regionList;

    private String[] sysCodeList;

    private String region;
    private String LimitNumber;

    @Column("SYSCODE")
    private String sysCode;

    @Column("NAME")
    private String regionName;

    @Column("number")
    private String number;


    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }
}
