package web.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IndexBO {
    private String code;
    private String type;
    private  String  top;

    public IndexBO(String code, String type, String top) {
        this.code = code;
        this.type = type;
        this.top = top;
    }
}
