package web.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import web.entity.StandardEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StandardEntityBo extends StandardEntity {
    private String code;
    private String top;
    private String limitNumber;

    public StandardEntityBo(String code, String top) {
        this.code = code;
        this.top = top;
    }
}
