package web.bo;

import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KnowledgeStatisticsOperationBo {
    /**
     * 下载量
     */
    @Column("downloadNum")
    private Integer downloadNum;
    /**
     * 浏览量
     */
    @Column("browseNum")
    private Integer browseNum;
    /**
     * 检索量
     */
    @Column("searchNum")
    private Integer searchNum;
}
