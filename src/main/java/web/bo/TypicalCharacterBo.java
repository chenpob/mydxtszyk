package web.bo;

import lombok.Data;
import web.entity.TypicalCharacterEntity;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/11/11 15:34
 * @Version 1.0
 */
@Data
public class TypicalCharacterBo extends TypicalCharacterEntity  {
    /**
     * 当前页
     */
    private Integer page;
    /**
     * 每页显示条数
     */
    private Integer pageSize;
    /**
     * 起始条数
     */
    private Integer start;
    /**
     * 搜索词
     */
    private String searchType;

    /**
     * 搜索值
     */
    private String searchValue;

    /**
     * 首页显示几条
     */
    private Integer topNum;

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }
}
