package web.bo;

import cnki.tpi.kbatis.annotation.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddFrequencyBo {
   private String id;
   private String TABLENAME;
   private String tableName;
   private String fileName;

}
