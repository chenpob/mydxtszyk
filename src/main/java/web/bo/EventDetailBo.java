package web.bo;

import lombok.Data;
import web.entity.EventEntity;

@Data
public class EventDetailBo extends EventEntity {

    private String sort;

}
