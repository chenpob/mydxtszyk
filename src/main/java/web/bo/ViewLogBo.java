package web.bo;

import lombok.Data;
import web.entity.PictureResources;
import web.entity.ViewLogEntity;

@Data
public class ViewLogBo extends ViewLogEntity {
    /**
     * 每页显示条数
     */
    private int pageSize;

    /**
     * 起始下标
     */
    private int start;

    /**
     * 页码
     */
    private int page;

    private String time;

    private String searchValue;

    private String type;

    private String whereValue;

    private String countnumber;

    private  int hyfl_code;

    private  String departmentName;

    private  String endTime;

    private  String startTime;

}
