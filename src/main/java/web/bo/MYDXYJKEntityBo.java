package web.bo;


import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;
import web.entity.MYDXYJKEntity;

@Data
public class MYDXYJKEntityBo extends MYDXYJKEntity {

        private String limitNumber;

        private String  dbCode;

}
