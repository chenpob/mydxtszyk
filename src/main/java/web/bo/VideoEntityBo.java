package web.bo;

import lombok.Data;
import web.entity.PictureResources;

@Data
public class VideoEntityBo extends PictureResources {
    private String limitNumber;

    private String  code;
}
