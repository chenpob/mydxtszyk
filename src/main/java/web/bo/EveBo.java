package web.bo;

import lombok.Data;
import web.entity.EveEntity;
import web.entity.EventEntity;

@Data
public class EveBo extends EveEntity {

    private String sort;

    private String limitNumber;
}
