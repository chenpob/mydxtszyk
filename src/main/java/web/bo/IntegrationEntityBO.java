package web.bo;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import web.entity.IntegrationEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class IntegrationEntityBO extends IntegrationEntity {

    //原有的文件名
    private String oldfileName;
    //原有的图片名
    private String oldfilePictureName;
    //图片名称
    private String pictureName;
    //封面名称
    private String coverName;
    //最近更新时间
    private String currentTime;
    /**
     *  新增类型
     */
    private String insertType;

    private String transportType;

    /**
     * 新增值
     */
    private String insertValues;
    private String database;
    /**
     * 类型 0,1，2，3  ==> 分别对应应急管理,协同创新,党校期刊,两弹一星
     */
    private String type;

    /**
     * 每页显示条数
     */
    public int pageSize;

    /**
     * 起始下标
     */
    private int start;

    /**
     * 页码
     */
    public int page;

    /**
     * 排序
     */
    private String orderBy;

    /**
     * 排序值
     */
    private String orderByValue;

    /**
     * 起始时间
     */
    private String beginDate;
    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 更新时间
     */
    private String updateDate;


    /**
     * 搜索选项
     */
    private String searchType;

    /**
     * 搜索词
     */
    private String searchValue;

    /**
     * 初级检索条件SQL
     */
    private String primarySearchSql;

    /**
     * 高级检索条件集合
     */
    private List<Map<String, String>> advSearchList;

    /**
     * 高级检索条件SQL
     */
    private String advSearchSql;

    private String[] codeList;


    public void setTheUpdateDate(String createDate) {
        long mill = System.currentTimeMillis();
        if ("最近一周".equals(createDate)) {
            mill -= 86400000l * 7;
        } else if ("最近一月".equals(createDate)) {
            mill -= 86400000l * 31;
        } else if ("最近半年".equals(createDate)) {
            mill -= 86400000l * 30.5 * 6;
        } else if ("最近一年".equals(createDate)) {
            mill -= 86400000l * 365;
        } else if ("今年迄今".equals(createDate)) {
            String year = new SimpleDateFormat("yyyy").format(new Date());
            String thisYear = year + "-01-01 00:00:01";
            setUpdateDate(thisYear);
            return;
        } else if ("上一年度".equals(createDate)) {
            String year = new SimpleDateFormat("yyyy").format(new Date());
            String lastYear = Integer.toString(Integer.parseInt(year) - 1);
            String overLastYear = lastYear + "-01-01 00:00:01";
            setUpdateDate(overLastYear);
            return;
        } else if ("不限".equals(createDate)) {
            setUpdateDate("");
            return;
        } else {
            setUpdateDate("");
            return;
        }
        setUpdateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(mill));
    }

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }
}
