package web.bo;

import lombok.Data;
import web.entity.PictureEntity;
import web.entity.StandardEntity;

@Data
public class PictureEntityBo extends PictureEntity {

    private String code;

    private String limitNumber;
}
