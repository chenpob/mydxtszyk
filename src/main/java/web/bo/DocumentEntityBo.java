package web.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import web.entity.DocumentEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class DocumentEntityBo extends DocumentEntity {
    /**
     * 每页显示条数
     */
    private int pageSize;

    /**
     * 起始下标
     */
    private int start;

    /**
     * 页码
     */
    private int page;

    private String region;

    private String LimitNumber;

    private String author;

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }
}
