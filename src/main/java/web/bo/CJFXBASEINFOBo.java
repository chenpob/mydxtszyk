package web.bo;


import lombok.Data;
import web.entity.CJFXBASEINFO;

import java.util.List;

@Data
public class CJFXBASEINFOBo  extends CJFXBASEINFO {

    private String LimitNumber;

    /**
     * 0:全部 不做区分  1:党校发行   2:相关党建
     */
    private String  type;

    private String conditionSql;

    /**
     * 资源分类集合
     */
    private List<String> sortCodeList;

    /**
     * 资源分类SQL
     */
    private String sortCodeSql;

    /**
     * 搜索选项
     */
    private String searchType;

    /**
     * 搜索词
     */
    private String searchValue;

    /**
     * 页码
     */
    private int page;

    /**
     * 每页显示条数
     */
    private int pageSize;

    /**
     * 起始下标
     */
    private int start;


    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }
}
