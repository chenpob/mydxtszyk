package web.bo;


import lombok.Data;

import java.io.Serializable;

/**
 * MyUploadRecord 上传文件记录基础信息
 */
@Data
public class ScreenUploadRecordBo implements Serializable {

    /**
     * 题名
     */
    private int id;

    /**
     * 题名
     */
    private String name;

    /**
     * 关键词（; 隔开）
     */
    private String keyword;


    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 生成的文件名
     */
    private String fileName;

    /**
     * 上传时间
     */
    private String createTime;

    /**
     * 上传用户账户account
     */
    private String createUser;

    /**
     * 修改时间
     */
    private String editTime;

    /**
     * 修改用户账号account
     */
    private String editUser;

    /**
     * 是否显示
     */
    private int display;

    /**
     * 链接地址
     */
    private String link;

    private static final long serialVersionUID = 1L;
}
