package web.bo.admin;

import lombok.Data;
import web.entity.AdminDepartmentPo;

/**
 * @author cb
 * @Description TODO
 * @date 2023/12/9
 */
@Data
public class AdminDepartmentBo extends AdminDepartmentPo {

    private String orderType;
    //排序值
    private String orderValue;
    //页数
    private int page;
    //页容量
    private int pageSize;
    private int startPage;
}
