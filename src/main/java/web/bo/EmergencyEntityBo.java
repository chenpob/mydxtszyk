package web.bo;

import lombok.Data;
import web.entity.EmergencyEntity;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/20 15:47
 * @Version 1.0
 */
@Data
public class EmergencyEntityBo extends EmergencyEntity {
    /**
     * 排序
     */
    private String orderBy;

    /**
     * 排序值
     */
    private String orderByValue;

    /**
     * 搜索选项
     */
    private String searchType;

    /**
     * 搜索词
     */
    private String searchValue;

    /**
     * 分类代码集合
     */
    private List<String> sortCodeList;

    /**
     * 分类代码sql
     */
    private String sortCodeSql;

    /**
     * 页码
     */
    private int page;

    /**
     * 每页显示条数
     */
    private int pageSize;

    /**
     * 起始下标
     */
    private int start;


    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }
}
