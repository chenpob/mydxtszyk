package web.bo;

import lombok.Data;
import web.entity.CJFXLAST;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Description:期刊库-内刊数据库Bo
 *
 * @author BaiGe
 * @date: 2022/6/28 15:56
 * @Version 1.0
 */
@Data
public class CJFXLASTBo extends CJFXLAST {
    private String LimitNumber;

    private String type;

    private String code;

    private String conditionSql;

    /**
     * 排序
     */
    private String orderBy;

    /**
     * 排序值
     */
    private String orderByValue;

    /**
     * 搜索选项
     */
    private String searchType;

    /**
     * 搜索词
     */
    private String searchValue;

    /**
     * 资源分类集合
     */
    private List<String> sortCodeList;

    /**
     * 资源分类Sql
     */
    private String sortCodeSql;

    /**
     * 高级检索条件集合
     */
    private List<Map<String, String>> advSearchList;

    /**
     * 初级检索条件SQL
     */
    private String primarySearchSql;

    /**
     * 高级检索条件SQL
     */
    private String advSearchSql;

    /**
     * 页码
     */
    private int page;

    /**
     * 每页显示条数
     */
    private int pageSize;

    /**
     * 起始下标
     */
    private int start;

    /**
     * 起始时间
     */
    private String beginDate;

    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 更新时间
     */
    private String updateDate;

    public void setStart(Integer page, Integer pageSize) {
        this.start = (page - 1) * pageSize;
    }

    public void setTheUpdateDate(String createDate) {
        long mill = System.currentTimeMillis();
        if ("最近一周".equals(createDate)) {
            mill -= 86400000l * 7;
        } else if ("最近一月".equals(createDate)) {
            mill -= 86400000l * 31;
        } else if ("最近半年".equals(createDate)) {
            mill -= 86400000l * 30.5 * 6;
        } else if ("最近一年".equals(createDate)) {
            mill -= 86400000l * 365;
        } else if ("今年迄今".equals(createDate)) {
            String year = new SimpleDateFormat("yyyy").format(new Date());
            String thisYear = year + "-01-01 00:00:01";
            setUpdateDate(thisYear);
            return;
        } else if ("上一年度".equals(createDate)) {
            String year = new SimpleDateFormat("yyyy").format(new Date());
            String lastYear = Integer.toString(Integer.parseInt(year) - 1);
            String overLastYear = lastYear + "-01-01 00:00:01";
            setUpdateDate(overLastYear);
            return;
        } else if ("不限".equals(createDate)) {
            setUpdateDate("");
            return;
        } else {
            setUpdateDate("");
            return;
        }
        setUpdateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(mill));
    }
}
