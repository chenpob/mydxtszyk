package web.bo;

import lombok.Data;

@Data
public class PropertiesBo {

    private String name;

    private String value;

    private String conditionSql;

}
