package web.filter;


import cnki.tpi.util.StrUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import web.entity.UserDo;
import web.mapper.UserMysqlMapper;
import web.utils.JwtUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
/*

@Component
@WebFilter("/*")
public class RoleFilter implements Filter {

    @Autowired
    private UserMysqlMapper userMysqlMapper;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String method = request.getMethod();
        System.out.println( request.getRequestURI());
        System.out.println(method);
        if ("OPTIONS".equalsIgnoreCase(method)) {
            chain.doFilter(request, response);
            //如果是OPTIONS预处理不做 任何处理
        }
        // 得先判断是前段还是后端的接口。
//        System.out.println("--------------------------------------------------");
//        System.out.println("RequestURI:" + request.getRequestURI());
//        System.out.println("--------------------------------------------------");
        String url = request.getRequestURI();
        ArrayList<String> urlList = new ArrayList<>();
        urlList.add("/ViewLog/getDownLog");
        urlList.add("/ViewLog/getViewLog");
        urlList.add("/adminLog/LoginLogList");
        urlList.add("/keyWord/cutPageKeyWord");
        urlList.add("/webSite/top10SearchWord");
        urlList.add("/webSite/findWebSiteByDate");
        urlList.add("/webSite/findWebSiteByDate");
        urlList.add("/webSite/reportForm");
        urlList.add("/adminLog/LoginLogList");
        urlList.add("/admin/user/list");
        Boolean res = StrUtil.isContain(urlList, url);
        if (!res){
            chain.doFilter(request, response);
            return;
        }
        String mydx_token = request.getHeader("mydx_token");
        //解析token获取id
        String userid = "";
        try {
            Claims claims = JwtUtil.parseJWT(mydx_token);
            userid = claims.getSubject();
        } catch (Exception e) {
        }
        if (userid!=null && userid!=""){
            System.out.println(userid);
            UserDo user = userMysqlMapper.getUserByAccount(userid);
            String userType= user.getUser_type();
            System.out.println(user.getUser_type());
            if ("0".equals(userType)){
                chain.doFilter(request, response);
            }else {
                servletRequest.getRequestDispatcher("/adminResponse/getLoginNoRight").forward(servletRequest, response);
            }
        }else {
            servletRequest.getRequestDispatcher("/adminResponse/postLoginFail").forward(servletRequest, response);
        }

      //  chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }
}
*/
