package web.service;


import org.springframework.beans.factory.annotation.Autowired;
import web.bo.MYDXYJKEntityBo;
import web.dao.KbaseDao;
import web.dao.ResouceDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther: xiongping22369
 * @Date: 2018/8/17 08:55
 * @Description:
 */

public interface ResourceService {

    List<MYDXYJKEntityBo> getResouce(MYDXYJKEntityBo mydxyjkEntityBo);
}
