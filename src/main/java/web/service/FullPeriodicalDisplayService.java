package web.service;

import web.common.Result;
import web.entity.Periodical.fullPeriodicalDisplay.argsObj.SelectObj;

public interface FullPeriodicalDisplayService {
    Result defaultsearch(String pykm);

    Result selectyear(SelectObj selectObj);

    Result selectqi(SelectObj selectObj);

    Result selectbdqk(SelectObj selectObj);
}
