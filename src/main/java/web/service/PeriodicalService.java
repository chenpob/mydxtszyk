package web.service;


import web.bo.PeriodicalBo;
import web.common.CutPageBean;
import web.entity.CodeTreeEntity;
import web.entity.PeriodicalEntity;
import web.entity.PeriodicalStageEntity;
import web.entity.PeriodicalYearEntity;

import java.util.List;

public interface PeriodicalService {
    public CutPageBean<PeriodicalEntity> GetPeriodicalData(PeriodicalBo periodicalBo);

    public List<PeriodicalEntity> GetPeriodical(PeriodicalBo periodicalBo);

    public List<PeriodicalYearEntity> PeriodicalYear(PeriodicalBo periodicalBo);

    public List<PeriodicalStageEntity> PeriodicalStage(PeriodicalBo periodicalBo);

    public CutPageBean<PeriodicalEntity> Get512WWK(PeriodicalBo periodicalBo);

}
