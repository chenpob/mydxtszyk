package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import web.entity.Security_UserDetail;
import web.entity.UserDo;
import web.mapper.UserMysqlMapper;

import java.util.Objects;

@Service
public class Security_UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMysqlMapper userMysqlMapper;

    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        /*通过accout查询mysql，查询用户信息，并封装给UserDetails的实现类*/
//        UserDo userDo = userMysqlMapper.getUserByAccount(account);
        UserDo userDo = userMysqlMapper.getUserByAccountAndPhone(account);

        /*用户名或密码错误 为混淆视听的说法，其实应该是用户不存在*/
        if (Objects.isNull(userDo)) {
            throw new RuntimeException("用户名或密码错误");
        }

        Security_UserDetail security_userDetail = new Security_UserDetail(userDo,userDo.getRoles());
//        security_userDetail.setUserDo(userDo);

        //todo 查询权限信息，封装给UserDetails的实现类
        return security_userDetail;
    }
}
