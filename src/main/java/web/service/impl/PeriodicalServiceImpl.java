package web.service.impl;

import org.springframework.stereotype.Service;
import web.bo.PeriodicalBo;
import web.common.CutPageBean;
import web.dao.PeriodicalDao;
import web.entity.PeriodicalEntity;
import web.entity.PeriodicalStageEntity;
import web.entity.PeriodicalYearEntity;
import web.service.PeriodicalService;


import javax.annotation.Resource;
import java.util.List;

@Service
public class PeriodicalServiceImpl implements PeriodicalService {
    @Resource(name = "kbasePeriodical")
    private PeriodicalDao periodicalDao;

    @Override
    public CutPageBean<PeriodicalEntity> GetPeriodicalData(PeriodicalBo periodicalBo) {
        periodicalBo.setStart(periodicalBo.page,periodicalBo.pageSize);
        CutPageBean<PeriodicalEntity> cutPage = new CutPageBean<>();
        if ("source_cykm".equals(periodicalBo.getSearchType())){
          periodicalBo.setSearchType("书名");
        }
        if ("publisher".equals(periodicalBo.getSearchType())){
          periodicalBo.setSearchType("主办单位");
        }
        List<PeriodicalEntity> list = periodicalDao.GetPeriodicalData(periodicalBo);
        List<String> count=  periodicalDao.GetCount(periodicalBo);
        Integer counter =count.size();
        cutPage.initCutPage(counter, periodicalBo.pageSize,list);
        return cutPage;
    }

    @Override
    public CutPageBean<PeriodicalEntity> Get512WWK(PeriodicalBo periodicalBo) {
        periodicalBo.setStart(periodicalBo.page,periodicalBo.pageSize);
        CutPageBean<PeriodicalEntity> cutPage = new CutPageBean<>();
        if ("source_cykm".equals(periodicalBo.getSearchType())){
            periodicalBo.setSearchType("书名");
        }
        if ("publisher".equals(periodicalBo.getSearchType())){
            periodicalBo.setSearchType("主办单位");
        }
        List<PeriodicalEntity> list = periodicalDao.Get512WWK(periodicalBo);
        Long count=  periodicalDao.Get512Count(periodicalBo);
        Integer counter =Math.toIntExact(count);
        cutPage.initCutPage(counter, periodicalBo.pageSize,list);
        return cutPage;
    }

    @Override
    public List<PeriodicalEntity> GetPeriodical(PeriodicalBo periodicalBo) {
        List<PeriodicalEntity> list =periodicalDao.GetPeriodical(periodicalBo);
        return list;
    }

    @Override
    public List<PeriodicalYearEntity> PeriodicalYear(PeriodicalBo periodicalBo) {
        List<PeriodicalYearEntity> list =periodicalDao.PeriodicalYear(periodicalBo);
        return list;
    }

    @Override
    public List<PeriodicalStageEntity> PeriodicalStage(PeriodicalBo periodicalBo) {
        List<PeriodicalStageEntity> list =periodicalDao.PeriodicalStage(periodicalBo);
        return list;
    }




}
