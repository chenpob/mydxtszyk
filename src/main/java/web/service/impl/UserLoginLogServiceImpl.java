package web.service.impl;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.UserloginlogEntityBo;
import web.common.CutPageBean;
import web.entity.UserloginlogEntity;
import web.mapper.UserLoginLogMapper;
import web.service.UserLoginLogService;

import java.util.List;

@Service
public class UserLoginLogServiceImpl implements UserLoginLogService {

    @Autowired
    private UserLoginLogMapper userLoginLogMapper;

    @Override
    public List<UserloginlogEntityBo> LoginLogList(UserloginlogEntityBo userloginlogEntityBo) {
        List<UserloginlogEntityBo> userloginlogEntityBos = userLoginLogMapper.LoginLogList(userloginlogEntityBo);
        return userloginlogEntityBos;
    }

    public CutPageBean<UserloginlogEntity> getUserOtherLog(UserloginlogEntityBo userloginlogEntityBo) {
        PageHelper.startPage(userloginlogEntityBo.getPage(), userloginlogEntityBo.getPageSize());
        CutPageBean<UserloginlogEntity> cutPage = new CutPageBean<>();
        List<UserloginlogEntity> list = userLoginLogMapper.getUserOtherLog(userloginlogEntityBo);
        Long count = userLoginLogMapper.getUserOtherLogCount(userloginlogEntityBo);
        Integer counter = count.intValue();
        cutPage.initCutPage(counter, userloginlogEntityBo.getPageSize(), list);
        return cutPage;
    }

}
