package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import web.bo.CJFXBASEINFOBo;
import web.bo.ZkzsEntityBo;
import web.common.CutPageBean;
import web.dao.JournalDatabaseWholePeriodicalDao;
import web.entity.CJFXBASEINFO;
import web.service.JournalDatabaseWholePeriodicalService;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/4 10:24
 * @Version 1.0
 */
@Service
public class JournalDatabaseWholePeriodicalServiceImpl implements JournalDatabaseWholePeriodicalService {
    @Autowired
    private JournalDatabaseWholePeriodicalDao wholePeriodicalDao;

    @Override
    public CutPageBean<CJFXBASEINFO> searchWholePeriodical(CJFXBASEINFOBo cjfxbaseinfoBo) {
        cjfxbaseinfoBo.setStart(cjfxbaseinfoBo.getPage(), cjfxbaseinfoBo.getPageSize());
        String sortCode = this.getSortCode(cjfxbaseinfoBo.getSortCodeList());
        cjfxbaseinfoBo.setSortCodeSql(sortCode);
        //搜索词转换为数据库列名
        String searchType = cjfxbaseinfoBo.getSearchType();
        String searchValue = cjfxbaseinfoBo.getSearchValue();
        if (StringUtils.hasText(searchType) && StringUtils.hasText(searchValue)) {
            switch (searchType) {
                case "source_cykm":
                    cjfxbaseinfoBo.setSearchType("C_NAME");
                    break;
                case "publisher":
                    cjfxbaseinfoBo.setSearchType("主办单位1");
                    break;
                case "cn":
                    cjfxbaseinfoBo.setSearchType("CN");
                    break;
                case "issn":
                    cjfxbaseinfoBo.setSearchType("ISSN");
                    break;
            }
        }

        CutPageBean<CJFXBASEINFO> pageBean = new CutPageBean<>();
        List<CJFXBASEINFO> list = wholePeriodicalDao.searchWholePeriodicalList(cjfxbaseinfoBo);
        int count = (int) wholePeriodicalDao.searchWholePeriodicalCount(cjfxbaseinfoBo);
        //todo 显示36条
        if ("2".equals(cjfxbaseinfoBo.getType())) {
            if(count >=36){
                pageBean.initCutPage(36, cjfxbaseinfoBo.getPageSize(), list);
            }else{
                pageBean.initCutPage(count, cjfxbaseinfoBo.getPageSize(), list);
            }
        }else{
            pageBean.initCutPage(count, cjfxbaseinfoBo.getPageSize(), list);
        }
        return pageBean;
    }

    @Override
    public CutPageBean<ZkzsEntityBo> ZkJssearch(ZkzsEntityBo zkzsEntityBo) {
        zkzsEntityBo.setStart(zkzsEntityBo.getPage(), zkzsEntityBo.getPageSize());
        //搜索词转换为数据库列名
        String searchType = zkzsEntityBo.getSearchType();
        String searchValue = zkzsEntityBo.getSearchValue();
        String preSql= "";
        if (searchType!=null && searchValue!=null && searchType!="" && searchValue!=""){
            preSql = getPrefixSql(searchType,searchValue);
        }
        zkzsEntityBo.setConditionSql(preSql);
        CutPageBean<ZkzsEntityBo> pageBean = new CutPageBean<>();
        List<ZkzsEntityBo> list = wholePeriodicalDao.ZkJssearchList(zkzsEntityBo);
        for (ZkzsEntityBo entityBo : list) {
            if (entityBo.getPinyinName()==null || entityBo.getPinyinName()==""){
                entityBo.setPinyinName("1");
            }

            if (entityBo.getNumber()==null || entityBo.getNumber()==""){
                entityBo.setNumber("1");
            }
        }
        List<String> listss =  wholePeriodicalDao.ZkJssearchListCount(zkzsEntityBo);
        pageBean.initCutPage(listss.size(), zkzsEntityBo.getPageSize(), list);
        return pageBean;
    }

    private String getPrefixSql(String searchType, String searchValue) {
        if (searchType.equals("source_cykm")){
            //书名 ="阿坝*"  or  C_NAME % "百*"
            String  sqlOne = "书名 ="+"'"+searchValue+"*"+"'" + " or C_NAME % " +"'"+searchValue+"*"+"'";
            return  sqlOne;
        }
        if (searchType.equals("publisher")){
            //主办单位 ="阿坝*"  or  主办单位1  % "安徽*"
            String  sqlTwo = "主办单位= " + searchValue+"*"+" or 主办单位1 % " +searchValue+"*";
            return  sqlTwo;
        }
        return "";
    }


    /**
     * 获拼接左侧勾选信息
     *
     * @param sortCodeList
     * @return
     */
    private String getSortCode(List<String> sortCodeList) {
        if (CollectionUtils.isEmpty(sortCodeList))
            return null;
        StringBuffer bf = new StringBuffer("(");
        String column = "期刊标识码";
        for (String sort : sortCodeList) {
            if (!"".equals(sort) && sort != null) {
                if (sort.split(",").length > 1) {
                    String[] split = sort.split(",");
                    for (int i = 0; i < split.length; i++) {
                        bf.append(column).append(" = ").append(split[i]).append("?").append("  or ");
                    }
                    continue;
                }
                bf.append(column).append(" = ").append(sort).append("?").append("  or ");
            }
        }
        if (bf.length() == 0) {
            return null;
        }
        String returnStr = bf.substring(0, bf.length() - 3) + ")";
        return returnStr;
    }
}
