package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.EveBo;
import web.bo.EventDetailBo;
import web.bo.StandardEntityBo;
import web.common.Result;
import web.dao.EarthquakeReliefDao;
import web.dao.EventDao;
import web.dao.impl.FullPeriodicalDisplayDaoImpl;
import web.entity.Periodical.fullPeriodicalDisplay.Cover_YearEntity;
import web.entity.Periodical.fullPeriodicalDisplay.argsObj.SelectObj;
import web.service.EventService;
import web.service.FullPeriodicalDisplayService;

import java.util.HashMap;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventDao eventDao;


    @Override
    public List<String> getEventDate(EventDetailBo eventDetailBo) {
        return eventDao.getEventDate(eventDetailBo);
    }

    @Override
    public List<EventDetailBo> getEventByYear(EventDetailBo eventDetailBo) {
        return eventDao.getEventByYear(eventDetailBo);
    }

    @Override
    public List<EventDetailBo> getEventById(EventDetailBo eventDetailBo) {
        return eventDao.getEventById(eventDetailBo);
    }

    @Override
    public List<EveBo> getEventList(EveBo eveBo) {
        return eventDao.getEventList(eveBo);
    }

    @Override
    public EveBo getBigEventById(EveBo eveBo) {
        return eventDao.getBigEventById(eveBo);
    }
}
