package web.service.impl;

import org.springframework.stereotype.Service;
import web.bo.DocumentEntityBo;
import web.common.CutPageBean;
import web.dao.ExpertDao;
import web.bo.ExpertEntityBO;
import web.entity.CodeTreeEntity;
import web.entity.DocumentEntity;
import web.entity.ExpertEntity;
import web.service.ExpertService;
import web.utils.CommonConvertUtils;
import web.utils.DatabaseTreeNode;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExpertServiceImpl implements ExpertService {
    @Resource(name = "kbaseExpert")
    private ExpertDao expertDao;


    @Override
    public CutPageBean<ExpertEntity> GetExpertData(ExpertEntityBO  expertEntityBO) {
        expertEntityBO.setStart(expertEntityBO.page,expertEntityBO.pageSize);
        String region= CommonConvertUtils.sqllistor("地区",expertEntityBO.getRegionList());
        expertEntityBO.setRegion(region);
        CutPageBean<ExpertEntity> cutPage = new CutPageBean<>();
        List<ExpertEntity> list = expertDao.GetExpertData(expertEntityBO);
        Long count =  expertDao.GetCount(expertEntityBO);
        Integer counter =count.intValue();
        cutPage.initCutPage(counter, expertEntityBO.pageSize, list);
        return cutPage;
    }

    @Override
    public CutPageBean<ExpertEntity> GetExpertData1(ExpertEntityBO expertEntityBO) {
//        List<String> list= Arrays.asList(expertEntityBO.getSysCodeList());
        List<String> list1 = new ArrayList<>();
//        List<ExpertEntity> list1= new ArrayList<>();
        for (String s : expertEntityBO.getSysCodeList()) {
            expertEntityBO.setSysCode(s);
            ExpertEntityBO expert = expertDao.GetName(expertEntityBO);
            list1.add(expert.getRegionName());
        }
        String[] array2 = list1.toArray(new String[list1.size()]);
        expertEntityBO.setStart(expertEntityBO.page,expertEntityBO.pageSize);
//        String region= CommonConvertUtils.sqllistor("地区",expertEntityBO.getRegionList());
        String region= CommonConvertUtils.sqllistor("地区",  array2);
        expertEntityBO.setRegion(region);
        CutPageBean<ExpertEntity> cutPage = new CutPageBean<>();
        List<ExpertEntity> list = expertDao.GetExpertData(expertEntityBO);
        Long count =  expertDao.GetCount(expertEntityBO);
        Integer counter =count.intValue();
        cutPage.initCutPage(counter, expertEntityBO.pageSize, list);
        return cutPage;
    }


    @Override
    public ExpertEntity GetExpert(String id) {
        ExpertEntityBO expertEntityBO = new ExpertEntityBO();
        expertEntityBO.setId(id);
        return expertDao.GetExpert(expertEntityBO);
    }

    @Override
    public List<CodeTreeEntity> selectRegion() {
        List<CodeTreeEntity> codeTreeEntityList=expertDao.selectRegion();
        return  DatabaseTreeNode.getTreeNode_NoRelation("0",codeTreeEntityList);
    }

    @Override
    public CutPageBean<DocumentEntity> selectDocument(String author, int page, int pageSize) {
        DocumentEntityBo documentEntity=new DocumentEntityBo();
        documentEntity.setAuthor(author);
        documentEntity.setPage(page);
        documentEntity.setPageSize(pageSize);
        documentEntity.setStart(page,pageSize);
        CutPageBean<DocumentEntity> cutPage = new CutPageBean<>();
        List<DocumentEntity> list = expertDao.selectDocument(documentEntity);
        int count = (int) expertDao.GetCount1(documentEntity);
        cutPage.initCutPage(count, pageSize, list);
        return cutPage;
    }

    @Override
    public List<ExpertEntity> ExpertResourceFive() {
        List<ExpertEntity> list = expertDao.ExpertResourceFive();


        return list;
    }


}



