package web.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import web.bo.MYDXYJKEntityBo;
import web.dao.ResouceDao;
import web.dao.UtilDao;
import web.service.ResourceService;
import web.utils.PictureUtils;
import web.utils.RandomUtils;
import web.utils.VideoUtils;

import java.io.File;
import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/4/19 10:31
 * @Version 1.0
 */
@Service
public class ResouceServiceImpl implements ResourceService {

    @Autowired
    private ResouceDao resouceDao;

    @Autowired
    private UtilDao utilDao;

    @Value("${filePath.PicPath}")
    private String PicPath;


    @Override
    public List<MYDXYJKEntityBo> getResouce(MYDXYJKEntityBo mydxyjkEntityBo) {
        List<MYDXYJKEntityBo> mydxyjkEntityBos = resouceDao.getResouce(mydxyjkEntityBo);
        //在这里插入截图 并且进行反写
        for (MYDXYJKEntityBo entityBo : mydxyjkEntityBos) {
            //这里进行截图反写
            if (entityBo.getPicAddress() == "" || entityBo.getPicAddress() == null) {
                try{
                    // 1. 先获取视频的地址
                    String dbBaseCode = entityBo.getTableName();
                    if (dbBaseCode.split("_").length>1){
                        dbBaseCode = dbBaseCode.split("_")[0];
                    }
                    String fileName = entityBo.getSysFileName();
                    String resultDes = dbBaseCode + "_DATABASE_DESCRIPT";
                    String preUrl  = utilDao.getDes(resultDes);
                    String dbBasePath = preUrl + "\\" + dbBaseCode + "\\"+"Pages";
                    String dirName = RandomUtils.getDirByName(fileName);
                    String tmpPath = new File(dbBasePath, dirName).toString();
                    String dbAbsPath = new File(tmpPath, fileName).toString();
                    String path = dbAbsPath;
                    String s = VideoUtils.fetchFrame(path);
                    String suffix = "png";//图片文件的后缀名。可以是jpg、gif
                    String filePath =  entityBo.getTitile()+"."+suffix;
                    String newFileName = PicPath+"\\"+filePath;//在Eclipse里面，生成的文件也在工程文件夹下
                    PictureUtils.base64StringToImage(s, newFileName, suffix);
                    // 然后这里进行更新我们的哪条数据 进行反写
                    entityBo.setPicAddress(filePath);
                    Boolean b = resouceDao.UpdatemydxyjkEntityBo(entityBo);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return mydxyjkEntityBos;
    }
}