package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.common.Result;
import web.dao.impl.FullPeriodicalDisplayDaoImpl;
import web.entity.Periodical.fullPeriodicalDisplay.Cover_YearEntity;
import web.entity.Periodical.fullPeriodicalDisplay.IndexEntity;
import web.entity.Periodical.fullPeriodicalDisplay.argsObj.SelectObj;
import web.service.FullPeriodicalDisplayService;

import java.util.HashMap;
import java.util.List;

@Service
public class FullPeriodicalDisplayServiceImpl implements FullPeriodicalDisplayService {
    @Autowired
    FullPeriodicalDisplayDaoImpl fullPeriodicalDisplayDaoImpl;

    @Override
    public Result defaultsearch(String pykm) {
        Cover_YearEntity cover_yearEntity = fullPeriodicalDisplayDaoImpl.defaultsearch(pykm);
        if(cover_yearEntity == null){
            return null;
        }
        List<String> years = fullPeriodicalDisplayDaoImpl.defaultsearchYears(pykm);
        cover_yearEntity.setYears(years);
        return Result.succ("ok", cover_yearEntity);
    }

    @Override
    public Result selectyear(SelectObj selectObj) {
        List<String> qi = fullPeriodicalDisplayDaoImpl.selectyear(selectObj);
        Result result = Result.succ("ok", qi);
        return result;
    }

    @Override
    public Result selectqi(SelectObj selectObj) {
        HashMap<String,Object> qi = fullPeriodicalDisplayDaoImpl.selectqi(selectObj);
        Result result = Result.succ("ok", qi);
        return result;
    }

    @Override
    public Result selectbdqk(SelectObj selectObj) {
        List<IndexEntity> qi = fullPeriodicalDisplayDaoImpl.selectbdqk(selectObj);
        Result result = Result.succ("ok", qi);
        return result;
    }
}
