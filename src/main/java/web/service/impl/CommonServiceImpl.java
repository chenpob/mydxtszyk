package web.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.common.Result;
import web.entity.MenuEntity;
import web.entity.UserDo;
import web.mapper.CommonMapper;
import web.mapper.UserMysqlMapper;
import web.service.CommonService;
import web.utils.RandomUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    CommonMapper commonMapper;

    @Autowired
    UserMysqlMapper userMysqlMapper;

    @Override
    public HashMap<String, Object> getInitJson(String account) {
        UserDo userDo = userMysqlMapper.getUserByAccount(account);
        Integer id = userDo.getId();
        List<MenuEntity> entity_list = commonMapper.getInitJson(id);


//        最外层结构
        HashMap<String, Object> final_res = new HashMap<>();
//次外层对象 1
        HashMap<String, HashMap<String, String>> homeInfo = new HashMap<>();
        HashMap<String, String> inner_context_1 = new HashMap<>();
        inner_context_1.put("title","首页");
        inner_context_1.put("href","/admin/mypages/log/webSite.html");
//        homeInfo.put("homeInfo",inner_context_1);
//次外层对象 2
        HashMap<String, HashMap<String, String>> logoInfo = new HashMap<>();
        HashMap<String, String> inner_context_2 = new HashMap<>();
        inner_context_2.put("title","后台管理");
        inner_context_2.put("image","images/logo.png");
        inner_context_2.put("href","");
//        logoInfo.put("logoInfo",inner_context_2);


//次外层对象 3
        List<HashMap<String,Object>> menuInfoList = new ArrayList<>();

        HashSet<String> parentNames = new HashSet<>();
        for (MenuEntity menuEntity :entity_list) {
            parentNames.add(menuEntity.getParent());
        }

        for (String parentName :parentNames) {
            HashMap<String,Object> menuObj = new HashMap<>();
            menuObj.put("title",parentName);
            menuObj.put("icon","fa fa-address-book");
            menuObj.put("href","");
            menuObj.put("target","_self");

            List<HashMap<String,String>> children_list = new ArrayList<>();
//            menuObj.put("child",childs);
            for (MenuEntity menuEntity :entity_list) {
                if (parentName.equals(menuEntity.getParent())){
                    HashMap<String,String> childObj = new HashMap<>();
                    childObj.put("title",menuEntity.getMenuName());
                    childObj.put("href",menuEntity.getPath());
                    childObj.put("icon",menuEntity.getIcon());
                    childObj.put("target","_self");
                    children_list.add(childObj);
                }
            }
            menuObj.put("child",children_list);
            menuInfoList.add(menuObj);
        }

        final_res.put("homeInfo",inner_context_1);
        final_res.put("logoInfo",inner_context_2);
        final_res.put("menuInfo",menuInfoList);






//        String userJson = JSON.toJSONString(res);
//        JSONObject user = JSONObject.parseObject(userJson);
        return final_res;
    }

    @Override
    public Result captchaRequire() {
        String ds = commonMapper.captchaRequire();
        String randomString = "";
        if ("1".equals(ds)){
            randomString = RandomUtils.getRandomString("");
        }
        HashMap<String,String> res = new HashMap<>();
        res.put("require",ds);
        res.put("serial",randomString);
        return Result.succ(res);
    }
}
