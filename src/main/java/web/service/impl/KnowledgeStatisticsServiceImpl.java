package web.service.impl;

import cn.hutool.core.date.DateTime;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.*;
import web.bo.Innovate.InnovateDbBo;
import web.dao.IndexDao;
import web.dao.KnowledgeStatisticsDao;
import web.dao.innovate.InnovateDao;
import web.entity.BrowseDownLoadStaticsEntity;
import web.mapper.BrowseDownloadLogMapper;
import web.mapper.KnowledgeStatisticsMapper;
import web.service.KnowledgeStatisticsService;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class KnowledgeStatisticsServiceImpl implements KnowledgeStatisticsService {
    @Autowired
    private InnovateDao innovateDao;
    @Autowired
    private IndexDao indexDao;
    @Autowired
    private KnowledgeStatisticsDao knowledgeStatisticsDao;
    @Autowired
    private KnowledgeStatisticsMapper knowledgeStatisticsMapper;

    @Override
    public Map<String, Long> bdKnowledgeCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo) {
        if (knowledgeStatisticsBo.getDataNumCode() == null) {
            knowledgeStatisticsBo.setDataNumCode(1);
        }
        if (knowledgeStatisticsBo.getHyflCode() == null) {
            knowledgeStatisticsBo.setHyflCode("");
        }
        Map<String, Long> map = new HashMap<>();//创建HashMap对象
        //数据类型  1数量 2下载量 3浏览量
        Integer numType = knowledgeStatisticsBo.getDataNumCode();
        //1 协同创新 2应急管理 3党校期刊
        //本地知识表 MYDXWX文献、MYDXTP图片、MYDXSP音视频、MYDXBDQK书籍  512WCDZWXK 、WJMYTS 书籍    应急管理 LDYXBD 两弹一星本地 书籍  协同创新
        //两弹一星协同视图期刊
        String LDYJTOTAL = "LDYTLKCCJDLAST1M,LDYTLKCCJDLAST2M,LDYTLKCJFD2000M,LDYTLKCJFD2001M,LDYTLKCJFD2002M,LDYTLKCJFD2003M,LDYTLKCJFD2004M,LDYTLKCJFD2005M,LDYTLKCJFD2006M,LDYTLKCJFD2007M,LDYTLKCJFD2008M,LDYTLKCJFD2009M,LDYTLKCJFD2010M,LDYTLKCJFD2011M,LDYTLKCJFD2012M,LDYTLKCJFD2013M,LDYTLKCJFD2014M,LDYTLKCJFD7984M,LDYTLKCJFD8589M,LDYTLKCJFD9093M,LDYTLKCJFD9495M,LDYTLKCJFD9697M,LDYTLKCJFD9899M,LDYTLKCJFDHIS2M,LDYTLKCJFDLASN2014M,LDYTLKCJFDLASN2015M,LDYTLKCJFDLASN2016M,LDYTLKCJFDLASN2017M,LDYTLKCJFDLASN2018M,LDYTLKCJFDLASN2019M,LDYTLKCJFDLASN2020M,LDYTLKCJFDLASN2021M,LDYTLKCJFDLASN2022M,LDYTLKCJFDLASN2023M,LDYTLKCJFDLAST2015M,LDYTLKCJFDLAST2016M,LDYTLKCJFDLAST2017M,LDYTLKCJFDLAST2018M,LDYTLKCJFDLAST2019M,LDYTLKCJFDLAST2020M,LDYTLKCJFDLAST2021M,LDYTLKCJFDLAST2022M,LDYTLKCJFDLAST2023M,LDYTLKCJFDN0508M,LDYTLKCJFDN0911M,LDYTLKCJFDN1214M,LDYTLKCJFDN7904M";
        //两弹一星协同视图博士
        String LDYDTOTAL = "LDYTLKCDFD0911M,LDYTLKCDFD1214M,LDYTLKCDFD9908M,LDYTLKCDFDLAST2015M,LDYTLKCDFDLAST2016M,LDYTLKCDFDLAST2017M,LDYTLKCDFDLAST2018M,LDYTLKCDFDLAST2019M,LDYTLKCDFDLAST2020M,LDYTLKCDFDLAST2021M,LDYTLKCDFDLAST2022M,LDYTLKCDFDLAST2023M";
        //两弹一星协同视图硕士
        String LDYMTOTAL = "LDYTLKCMFD0506M,LDYTLKCMFD2007M,LDYTLKCMFD2008M,LDYTLKCMFD2009M,LDYTLKCMFD2010M,LDYTLKCMFD2011M,LDYTLKCMFD2012M,LDYTLKCMFD201301M,LDYTLKCMFD201302M,LDYTLKCMFD201401M,LDYTLKCMFD201402M,LDYTLKCMFD201501M,LDYTLKCMFD201502M,LDYTLKCMFD201601M,LDYTLKCMFD201602M,LDYTLKCMFD201701M,LDYTLKCMFD201702M,LDYTLKCMFD201801M,LDYTLKCMFD201802M,LDYTLKCMFD201901M,LDYTLKCMFD201902M,LDYTLKCMFD202001M,LDYTLKCMFD202002M,LDYTLKCMFD202101M,LDYTLKCMFD202102M,LDYTLKCMFD202201M,LDYTLKCMFD202202M,LDYTLKCMFD202301M,LDYTLKCMFD9904M";
        //两弹一星协同视图会议
        String LDYPTOTAL = "LDYTLKCPFD0914M,LDYTLKCPFD9908M,LDYTLKCPFDLAST2019M";
        //两弹一星协同视图报纸
        String LDYNTOTAL = "LDYTLKCCND0005M,LDYTLKCCND0911M,LDYTLKCCND2006M,LDYTLKCCND2007M,LDYTLKCCND2008M,LDYTLKCCND2010M,LDYTLKCCNDHISM,LDYTLKCCNDLAST2011M,LDYTLKCCNDLAST2012ADDM,LDYTLKCCNDLAST2012M,LDYTLKCCNDLAST2013M,LDYTLKCCNDLAST2014M,LDYTLKCCNDLAST2015M,LDYTLKCCNDLAST2016M,LDYTLKCCNDLAST2017M,LDYTLKCCNDLAST2018M,LDYTLKCCNDLAST2019M,LDYTLKCCNDLAST2020M,LDYTLKCCNDLAST2021M,LDYTLKCCNDLAST2022M,LDYTLKCCNDLAST2023M";
        if (numType.equals(1)) {//1数据量
            if (knowledgeStatisticsBo.getHyflCode().equals("3")) {
                map.put("书籍", 0L);
                map.put("图片", 0L);
                map.put("音视频", 0L);
                knowledgeStatisticsBo.setHyflCode("");
                knowledgeStatisticsBo.setBdDataTable("MYDXBDQKXQ");
                map.put("文献", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
            } else {
                if (!knowledgeStatisticsBo.getHyflCode().equals("")) {
                    knowledgeStatisticsBo.setHyflCode("ca" + knowledgeStatisticsBo.getHyflCode());
                }
                if (knowledgeStatisticsBo.getHyflCode().equals("ca1")) {
                    knowledgeStatisticsBo.setHyflCode("");
                    knowledgeStatisticsBo.setBdDataTable(LDYJTOTAL+","+LDYDTOTAL+","+LDYMTOTAL+","+LDYPTOTAL+","+LDYNTOTAL);
                    Long bdlxydNum=knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo);
                    knowledgeStatisticsBo.setHyflCode("ca1");
                    knowledgeStatisticsBo.setBdDataTable("MYDXWX");
                    Long bdwxNum =knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo);
                    map.put("文献", bdwxNum+bdlxydNum);
                } else if (knowledgeStatisticsBo.getHyflCode().equals("ca2")) {
                    knowledgeStatisticsBo.setBdDataTable("MYDXWX");
                    map.put("文献", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                } else {
                    knowledgeStatisticsBo.setBdDataTable("MYDXWX"+","+LDYJTOTAL+","+LDYDTOTAL+","+LDYMTOTAL+","+LDYPTOTAL+","+LDYNTOTAL);
                    map.put("文献", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                }
                knowledgeStatisticsBo.setBdDataTable("MYDXTP");
                map.put("图片", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable("MYDXSP");
                map.put("音视频", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));

                if (knowledgeStatisticsBo.getHyflCode().equals("ca1")) {
                    knowledgeStatisticsBo.setHyflCode("");
                    knowledgeStatisticsBo.setBdDataTable("LDYXBD");
                    map.put("书籍", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                } else if (knowledgeStatisticsBo.getHyflCode().equals("ca2")) {
                    knowledgeStatisticsBo.setHyflCode("");
                    knowledgeStatisticsBo.setBdDataTable("512WCDZWXK,WJMYTS");
                    map.put("书籍", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                } else {
                    knowledgeStatisticsBo.setBdDataTable("MYDXBDQK,LDYXBD,512WCDZWXK,WJMYTS");
                    map.put("书籍", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                }
            }
        } else {//2下载量 3浏览量
            List<String> resourceCodes = new ArrayList<>();
            if (knowledgeStatisticsBo.getHyflCode().equals("3")) {//党校期刊
                map.put("书籍", 0L);
                map.put("图片", 0L);
                map.put("音视频", 0L);
                knowledgeStatisticsBo.setHyflCode("");
                resourceCodes.add("MYDXBDQKQX");
                knowledgeStatisticsBo.setResourceCodes(resourceCodes);
                map.put("文献", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
            } else {
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(("MYDXWX"+","+LDYJTOTAL+","+LDYDTOTAL+","+LDYMTOTAL+","+LDYPTOTAL+","+LDYNTOTAL).split(",")));
                map.put("文献", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                resourceCodes.clear();//清空条件
                resourceCodes.add("MYDXTP");
                knowledgeStatisticsBo.setResourceCodes(resourceCodes);
                map.put("图片", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                resourceCodes.clear();//清空条件

                resourceCodes.add("MYDXSP");
                knowledgeStatisticsBo.setResourceCodes(resourceCodes);
                map.put("音视频", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                resourceCodes.clear();//清空条件

                if (knowledgeStatisticsBo.getHyflCode().equals("1")) {
                    resourceCodes.add("LDYXBD");
                    knowledgeStatisticsBo.setResourceCodes(resourceCodes);
                    map.put("书籍", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                    resourceCodes.clear();//清空条件
                } else if (knowledgeStatisticsBo.getHyflCode().equals("2")) {
                    resourceCodes.add("512WCDZWXK");
                    resourceCodes.add("WJMYTS");
                    knowledgeStatisticsBo.setResourceCodes(resourceCodes);
                    map.put("书籍", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                    resourceCodes.clear();//清空条件
                } else {
                    resourceCodes.add("MYDXBDQK");
                    resourceCodes.add("LDYXBD");
                    resourceCodes.add("512WCDZWXK");
                    resourceCodes.add("WJMYTS");
                    knowledgeStatisticsBo.setResourceCodes(resourceCodes);
                    map.put("书籍", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                    resourceCodes.clear();//清空条件
                }
            }

        }

        return map;
    }

    @Override
    public Map<String, Long> commonKnowledgeCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo) {
        if (knowledgeStatisticsBo.getDataNumCode() == null) {
            knowledgeStatisticsBo.setDataNumCode(1);
        }
        if (knowledgeStatisticsBo.getHyflCode() == null) {
            knowledgeStatisticsBo.setHyflCode("");
        }
        Map<String, Long> map = new HashMap<>();//创建HashMap对象
        //HyflCode行业分类代码：1 协同创新 2应急管理 3党校期刊
        //DataNumCode数据请求类型：数据类型  1数量 2下载量 3浏览量
        Integer numType = knowledgeStatisticsBo.getDataNumCode();
        //期刊、会议、博硕、报纸
        //公共知识表
        /**
         * 报纸新闻表 应急   YJTNLAST2022，  协同 报纸XTXN0022   ， 应急YJTN0022  ，   两弹一星（协同创新）
         * 会议： 应急YJTPLAST2022，  协同XTXP8922  ，   应急YJTP7921
         * 期刊：  应急YJTJLAST2022 ，  协同XTXJ7922  ， 应急YJTJ7922，  ，CJFX7911 CJFXLAST(党校期刊)
         * 硕博：协同（博士）XTXD0220  ， 协同（硕士）XTXM0021  ， 应急（博士）YJTD0521  ，  应急（硕士）YJTM0121 ， 应急（博士）YJTDLAST2022，应急（硕士）YJTMLAST2022
         *
         * 两弹一星（协同创新）    LDYXTOTAL
         * 两弹一星（报纸：LDYNTOTAL，期刊：LDYJTOTAL、博士：LDYDTOTAL、硕士：LDYMTOTAL、会议：LDYPTOTAL）
         */
        String DXQK = "CJFX7911,CJFXLAST";//党校期刊
        String XTBZ = "XTXN0022";//协同报纸
        String YJBZ = "YJTNLAST2022,YJTN0022";//应急报纸
        String XTHY = "XTXP8922,YJTN0022";//协同会议
        String YJHY = "YJTPLAST2022,YJTP7921";//应急会议
        String XTQK = "XTXJ7922";//协同期刊
        String YJQK = "YJTJLAST2022,YJTJ7922";//应急期刊
        String XTBS = "XTXD0220,XTXM0021";//协同博硕士论文
        String YJBS = "YJTD0521,YJTM0121,YJTDLAST2022,YJTMLAST2022";//应急博硕士论文
        if (numType.equals(1)) {//获取数据总量
            if (!knowledgeStatisticsBo.getHyflCode().equals("") && !knowledgeStatisticsBo.getHyflCode().equals("3")) {
                knowledgeStatisticsBo.setHyflCode("ca" + knowledgeStatisticsBo.getHyflCode());
            }
            if (knowledgeStatisticsBo.getHyflCode().equals("3")) {//行业分类-党校期刊
                knowledgeStatisticsBo.setHyflCode("");
                knowledgeStatisticsBo.setBdDataTable(DXQK);
                map.put("期刊", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                map.put("会议", 0L);
                map.put("博硕", 0L);
                map.put("报纸", 0L);
            } else if (knowledgeStatisticsBo.getHyflCode().equals("ca1")) {//行业分类-协同创新
                knowledgeStatisticsBo.setBdDataTable(XTQK);
                map.put("期刊", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(XTHY);
                map.put("会议", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(XTBS);
                map.put("博硕", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(XTBZ);
                map.put("报纸", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
            } else if (knowledgeStatisticsBo.getHyflCode().equals("ca2")) {//行业分类-应急管理
                knowledgeStatisticsBo.setBdDataTable(YJQK);
                map.put("期刊", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(YJHY);
                map.put("会议", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(YJBS);
                map.put("博硕", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(YJBZ);
                map.put("报纸", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
            } else {//行业分类-全部
                knowledgeStatisticsBo.setBdDataTable(DXQK + "," + XTQK + "," + YJQK);
                map.put("期刊", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(XTHY + "," + YJHY);
                map.put("会议", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(XTBS + "," + YJBS);
                map.put("博硕", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setBdDataTable(XTBZ + "," + YJBZ);
                map.put("报纸", knowledgeStatisticsDao.knowledgeCountByHyflCode(knowledgeStatisticsBo));
            }

        } else {//获取2下载量 3浏览量
            if (knowledgeStatisticsBo.getHyflCode().equals("3")) {//党校期刊
                knowledgeStatisticsBo.setHyflCode("");
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(DXQK.split(",")));
                map.put("期刊", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                map.put("会议", 0L);
                map.put("博硕", 0L);
                map.put("报纸", 0L);
            } else if (knowledgeStatisticsBo.getHyflCode().equals("1")) {//行业分类-协同创新
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(XTQK.split(",")));
                map.put("期刊", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(XTHY.split(",")));
                map.put("会议", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(XTBS.split(",")));
                map.put("博硕", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(XTBZ.split(",")));
                map.put("报纸", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
            } else if (knowledgeStatisticsBo.getHyflCode().equals("2")) {//行业分类-应急管理
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(YJQK.split(",")));
                map.put("期刊", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(YJHY.split(",")));
                map.put("会议", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(YJBS.split(",")));
                map.put("博硕", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList(YJBZ.split(",")));
                map.put("报纸", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
            } else {//行业分类-全部
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList((DXQK + "," + XTQK + "," + YJQK).split(",")));
                map.put("期刊", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList((XTHY+ "," + YJHY).split(",")));
                map.put("会议", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList((XTBS + "," + YJBS).split(",")));
                map.put("博硕", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
                knowledgeStatisticsBo.setResourceCodes(Arrays.asList((XTBZ + "," + YJBZ).split(",")));
                map.put("报纸", numType == 2 ? knowledgeStatisticsMapper.knowledgeDownloadCountByHyflCode(knowledgeStatisticsBo) : knowledgeStatisticsMapper.knowledgeBrowseCountByHyflCode(knowledgeStatisticsBo));
            }
        }
        return map;
    }

    @Override
    public List<BrowseDownLoadStaticsEntity> knowledgeBrowseDownloadTop(KnowledgeStatisticsBo knowledgeStatisticsBo) {
        if (knowledgeStatisticsBo.getActionType() == null) {
            knowledgeStatisticsBo.setActionType(1);
        }
        if (knowledgeStatisticsBo.getActionTypeLimitNun() == null) {
            knowledgeStatisticsBo.setActionTypeLimitNun(10);
        } else {
            if (knowledgeStatisticsBo.getActionTypeLimitNun() > 100) {//最多100条
                knowledgeStatisticsBo.setActionTypeLimitNun(100);
            }
        }
        return knowledgeStatisticsMapper.knowledgeBrowseDownloadTop(knowledgeStatisticsBo);
    }


    @Override
    public Map<String, Long> dataBaseKnowledgeCount(KnowledgeStatisticsBo knowledgeStatisticsBo) {

        Map<String, Long> map = new HashMap<>();//创建HashMap对象
        if (knowledgeStatisticsBo.getHyflCode().equals("1")) {//协同创新
            InnovateDbBo innovateDbBo = new InnovateDbBo();
            innovateDbBo.setSortCode("ca101");//sortCode
            map.put("中央精神", innovateDao.commonSearchInnovateCount(innovateDbBo));
            innovateDbBo.setSortCode("ca10201");
            innovateDbBo.setSearchDirection(0);//CNKI协同创新
            map.put("历史演进", innovateDao.advSearchInnovateCount(innovateDbBo));
            innovateDbBo.setSortCode("ca10301");
            map.put("国内动态", innovateDao.commonSearchInnovateCount(innovateDbBo));
            innovateDbBo.setSortCode("ca1040");
            map.put("绵阳实践", innovateDao.commonSearchInnovateCount(innovateDbBo));
            innovateDbBo.setSortCode("ca10501");
            map.put("国际事业", innovateDao.commonSearchInnovateCount(innovateDbBo));
//            map.put("两弹一星",0L);


        } else if (knowledgeStatisticsBo.getHyflCode().equals("2")) {//应急管理
            TotalSearchEntityBo totalSearchEntityBo = new TotalSearchEntityBo();
            totalSearchEntityBo.setDataSource("应急管理文献");
            totalSearchEntityBo.setAdvSearchSql("行业分类代码 = 'ca201*'");
            map.put("热点关注", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("行业分类代码 = 'ca202*'");
            map.put("抗震救灾", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("行业分类代码 = 'ca203*'");
            map.put("灾后重建", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("行业分类代码 = 'ca204*'");
            map.put("发展振兴", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("行业分类代码 = 'ca205*'");
            map.put("研究成果", indexDao.GetTotalSearchCount(totalSearchEntityBo));

        } else {//3 党校期刊
            TotalSearchEntityBo totalSearchEntityBo = new TotalSearchEntityBo();
            totalSearchEntityBo.setDataSource("MYDXBDQK");
            totalSearchEntityBo.setAdvSearchSql("地区='华东片区'");
            map.put("华东片区", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("地区='中南片区'");
            map.put("中南片区", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("地区='西南片区'");
            map.put("西南片区", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("地区='西北片区'");
            map.put("西北片区", indexDao.GetTotalSearchCount(totalSearchEntityBo));
            totalSearchEntityBo.setAdvSearchSql("地区='东北片区'");
            map.put("东北片区", indexDao.GetTotalSearchCount(totalSearchEntityBo));

        }
        return map;
    }


    @Override
    public Map<String, List<KnowledgeStatisticYearBo>> knowledgeTypeYearList(KnowledgeStatisticsBo knowledgeStatisticsBo) {
        Map<String, List<KnowledgeStatisticYearBo>> knowledgeTypeYearMap = new HashMap<>();
        Integer yearNum=1;
        if (knowledgeStatisticsBo.getDataNumCode() != null && knowledgeStatisticsBo.getDataNumCode() == 1) {//知识类型分析
            if (knowledgeStatisticsBo.getYear() != null && knowledgeStatisticsBo.getYear() > 0) {
                yearNum = knowledgeStatisticsBo.getYear();
                knowledgeStatisticsBo.setYear(DateTime.now().year() - knowledgeStatisticsBo.getYear() + 1);
            } else {
                yearNum = 5;
                knowledgeStatisticsBo.setYear(DateTime.now().year() - 4);
            }
            Map<String, String> maps = new HashMap<>();
            maps.put("期刊", "YJTJLAST2022,XTXJ7922,YJTJ7922,CJFX7911,CJFXLAST,LDYJTOTAL");
            maps.put("硕博", "XTXD0220,XTXM0021,YJTD0521,YJTM0121,YJTDLAST2022,YJTMLAST2022,LDYDTOTAL,LDYMTOTAL");
            maps.put("报纸", "YJTNLAST2022,XTXN0022,YJTN0022,LDYNTOTAL");
            maps.put("会议", "YJTPLAST2022,XTXP8922,YJTP7921,CJFX7911,YJTPLAST2022,LDYPTOTAL");
            for (Map.Entry entry : maps.entrySet()) {
                knowledgeStatisticsBo.setBdDataTable(entry.getValue().toString());
                List<KnowledgeStatisticYearBo> qkYearNum = knowledgeStatisticsDao.knowledgeYearList(knowledgeStatisticsBo);
                if (qkYearNum != null && qkYearNum.size() > 0) {
                    Integer totalNum = 0;
                    for (KnowledgeStatisticYearBo item : qkYearNum) {
                        totalNum += item.getNum();
                    }
                    KnowledgeStatisticYearBo allNum = new KnowledgeStatisticYearBo();
                    allNum.setYear("all");
                    allNum.setNum(totalNum);
                    qkYearNum.add(allNum);
                }
                knowledgeTypeYearMap.put(entry.getKey().toString(), qkYearNum);

            }
        } else {//知识提交机构分析
            SimpleDateFormat sdfTemp = new SimpleDateFormat("yyyy-01-01 00:00:00");
            if (!(knowledgeStatisticsBo.getYear() != null && knowledgeStatisticsBo.getYear() > 0)) {
                knowledgeStatisticsBo.setYear(5);
            }
            yearNum = knowledgeStatisticsBo.getYear();
            //循环获取年数据
            for (int i = 0; i < yearNum; i++) {
                Integer year = 0;
                Calendar calendarStart = Calendar.getInstance();
                Calendar calendarEnd = Calendar.getInstance();
                calendarStart.add(Calendar.YEAR, -i);
                year = calendarStart.get(Calendar.YEAR);
                knowledgeStatisticsBo.setStarYear(sdfTemp.format(calendarStart.getTime()));
                calendarEnd.add(Calendar.YEAR, -i + 1);
                knowledgeStatisticsBo.setEndYear(sdfTemp.format(calendarEnd.getTime()));
                List<KnowledgeStatisticDeptBo> knowledgeStatisticDeptBos = knowledgeStatisticsMapper.knowledgeDeptUploadYearList(knowledgeStatisticsBo);
                for (KnowledgeStatisticDeptBo item : knowledgeStatisticDeptBos) {
                    List<KnowledgeStatisticYearBo> knowledgeStatisticYearBos = new ArrayList<>();
                    if (knowledgeTypeYearMap.containsKey(item.getDepartmentName())) {//判断该部门是否存在 存在将数据加入，不存在新增
                        knowledgeStatisticYearBos = knowledgeTypeYearMap.get(item.getDepartmentName());
                    }
                    KnowledgeStatisticYearBo bo = new KnowledgeStatisticYearBo();
                    bo.setYear(year.toString());
                    bo.setNum(item.getNum());
                    knowledgeStatisticYearBos.add(bo);
                    knowledgeTypeYearMap.put(item.getDepartmentName(), knowledgeStatisticYearBos);
                }
            }

        }
        //重新组装，将年不存在的设置为0
        for (Map.Entry<String, List<KnowledgeStatisticYearBo>> entry : knowledgeTypeYearMap.entrySet()) {
            String key = entry.getKey();
            List<KnowledgeStatisticYearBo> value = entry.getValue();
            for (int i = 0; i < yearNum; i++) {
                Integer year = 0;
                Calendar calendarStart = Calendar.getInstance();
                calendarStart.add(Calendar.YEAR, -i);
                year = calendarStart.get(Calendar.YEAR);
                String yearStr = year.toString();
                List<KnowledgeStatisticYearBo> list = value.stream().filter(KnowledgeStatisticYearBo -> KnowledgeStatisticYearBo.getYear().equals(yearStr)).collect(Collectors.toList());
                if (!(list != null && list.size() > 0)) {
                    KnowledgeStatisticYearBo mo = new KnowledgeStatisticYearBo();
                    mo.setYear(yearStr);
                    mo.setNum(0);
                    value.add(mo);
                }
            }
            value.sort(Comparator.comparing(KnowledgeStatisticYearBo::getYear));//年份升序
            //value.sort((o1,o2)->Integer.parseInt(o2.getYear())-Integer.parseInt(o1.getYear()));//年份降序
            entry.setValue(value);
        }
        return knowledgeTypeYearMap;
    }

    @Override
    public Map<String, Long> knowledgeDataUploadDeptCount() {
        Map<String, Long> map = new HashMap<>();
        TotalSearchEntityBo totalSearchEntityBo = new TotalSearchEntityBo();
        totalSearchEntityBo.setDataSource("协同创新总视图,应急管理文献,LDYXTOTAL,MYDXBDQK,CJFX7911,CJFXLAST");
        map.put("资源总数", indexDao.GetTotalSearchCount(totalSearchEntityBo));
        map.put("提交知识数", knowledgeStatisticsMapper.knowledgeUploadCount());
        map.put("机构数", knowledgeStatisticsMapper.knowledgeDeptCount());
        return map;
    }

    @Override
    public Map<String, List<KnowledgeStatisticYearBo>> knowledgeOperationAnalysisByDate(KnowledgeStatisticsBo knowledgeStatisticsBo) {
        Map<String, List<KnowledgeStatisticYearBo>> knowledgeTypeYearMap = new HashMap<>();
        List<KnowledgeStatisticYearBo> downloadNumBos = new ArrayList<>();
        List<KnowledgeStatisticYearBo> browseNumBos = new ArrayList<>();
        List<KnowledgeStatisticYearBo> searchNumBos = new ArrayList<>();
        Calendar calendarNow = Calendar.getInstance();
        Integer year = calendarNow.get(Calendar.YEAR);
        if (knowledgeStatisticsBo.getYear() != null && knowledgeStatisticsBo.getYear() > 0) {//判断是否有年，没有年则统计本年
            year = knowledgeStatisticsBo.getYear();
        }
        if (knowledgeStatisticsBo.getMonth() != null && knowledgeStatisticsBo.getMonth() > 0 && knowledgeStatisticsBo.getMonth() < 13) {//有月，统计月-日
            Integer days = 0;
            Integer month = knowledgeStatisticsBo.getMonth();
            SimpleDateFormat sdfTemp = new SimpleDateFormat(year + "-MM-dd 00:00:00");
            //获取年 月
            String monthStr = month < 10 ? "0" + month.toString() : month.toString();
            String startDateStr = year + "-" + monthStr + "-01 00:00:00";
            String endDateStr = "";
            Date dateStart = null;
            try {
                dateStart = sdfTemp.parse(startDateStr);
                // 将Date对象转换为Calendar对象
                Calendar calendarStart = Calendar.getInstance();
                calendarStart.setTime(dateStart);
                long milliseconds1 = calendarStart.getTimeInMillis();
                calendarStart.add(Calendar.MONTH, +1);
                long milliseconds2 = calendarStart.getTimeInMillis();
                days = Integer.parseInt((int) ((milliseconds2 - milliseconds1) / (24 * 60 * 60 * 1000)) + "");//相差天数
                for (Integer i = 1; i <= days; i++) {
                    String nowDayStr = i < 10 ? "0" + i : i.toString();
                    String nextDayStr = (i + 1) < 10 ? "0" + (i + 1) : (i + 1) + "".toString();
                    String startDateForStr = year + "-" + monthStr + "-" + nowDayStr + " 00:00:00";
                    String endDateForStr = i == days ? year + "-" + monthStr + "-" + nowDayStr + " 23:59:59" : year + "-" + monthStr + "-" + nextDayStr + " 00:00:00";
                    knowledgeStatisticsBo.setStarYear(startDateForStr);
                    knowledgeStatisticsBo.setEndYear(endDateForStr);
                    KnowledgeStatisticsOperationBo KnowledgeStatisticsOperationBos = knowledgeStatisticsMapper.knowledgeOperationAnalysisByDate(knowledgeStatisticsBo);
                    if (KnowledgeStatisticsOperationBos != null) {
                        KnowledgeStatisticYearBo downloadNumBo = new KnowledgeStatisticYearBo();
                        downloadNumBo.setNum(KnowledgeStatisticsOperationBos.getDownloadNum());
                        downloadNumBo.setYear(nowDayStr + "日");
                        KnowledgeStatisticYearBo browseNumBo = new KnowledgeStatisticYearBo();
                        browseNumBo.setNum(KnowledgeStatisticsOperationBos.getBrowseNum());
                        browseNumBo.setYear(nowDayStr + "日");
                        KnowledgeStatisticYearBo searchNumBo = new KnowledgeStatisticYearBo();
                        searchNumBo.setNum(KnowledgeStatisticsOperationBos.getSearchNum());
                        searchNumBo.setYear(nowDayStr + "日");
                        downloadNumBos.add(downloadNumBo);
                        browseNumBos.add(browseNumBo);
                        searchNumBos.add(searchNumBo);
                    }
                }
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        } else {
            for (Integer i = 1; i <= 12; i++) {//月份
                String nowMonthStr = i < 10 ? "0" + i : i.toString();
                String endMonthStr = (i + 1) < 10 ? "0" + (i + 1) : (i + 1) + "";
                String startDateStr = year + "-" + nowMonthStr + "-01 00:00:00";
                String endDateStr = i == 12 ? (year + 1) + "-01-01 00:00:00" : year + "-" + endMonthStr + "-01 00:00:00";
                knowledgeStatisticsBo.setStarYear(startDateStr);
                knowledgeStatisticsBo.setEndYear(endDateStr);
                KnowledgeStatisticsOperationBo KnowledgeStatisticsOperationBos = knowledgeStatisticsMapper.knowledgeOperationAnalysisByDate(knowledgeStatisticsBo);
                if (KnowledgeStatisticsOperationBos != null) {
                    KnowledgeStatisticYearBo downloadNumBo = new KnowledgeStatisticYearBo();
                    downloadNumBo.setNum(KnowledgeStatisticsOperationBos.getDownloadNum());
                    downloadNumBo.setYear(nowMonthStr + "月");
                    KnowledgeStatisticYearBo browseNumBo = new KnowledgeStatisticYearBo();
                    browseNumBo.setNum(KnowledgeStatisticsOperationBos.getBrowseNum());
                    browseNumBo.setYear(nowMonthStr + "月");
                    KnowledgeStatisticYearBo searchNumBo = new KnowledgeStatisticYearBo();
                    searchNumBo.setNum(KnowledgeStatisticsOperationBos.getSearchNum());
                    searchNumBo.setYear(nowMonthStr + "月");
                    downloadNumBos.add(downloadNumBo);
                    browseNumBos.add(browseNumBo);
                    searchNumBos.add(searchNumBo);
                }
            }
        }

        knowledgeTypeYearMap.put("下载量", downloadNumBos);
        knowledgeTypeYearMap.put("浏览量", browseNumBos);
        knowledgeTypeYearMap.put("检索量", searchNumBos);
        return knowledgeTypeYearMap;
    }
}
