package web.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import web.bo.IntegrationEntityBO;
import web.common.CutPageBean;
import web.dao.RetrievalDao;
import web.entity.IntegrationEntity;
import web.service.RetrievalService;
import web.utils.CommonConvertUtils;
import web.utils.FileUtil;

import javax.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class RetrievalServiceImpl implements RetrievalService {
    @Resource(name = "kbase")
    private RetrievalDao retrievalDao;

    @Value("${filePath.PicPath}")
    private String picPath;

    @Value("${filePath.VideoPath}")
    private String videoPath;


    @Value("${filePath.Document}")
    private String document;

    @Value("${filePath.CoverPic}")
    private String coverPic;

    //本地期刊封面
    @Value("${filePath.BDQKCover}")
    private String bdqkCover;

    //本地期刊PDF文件
    @Value("${filePath.BDQKPDF}")
    private String bdqkPdf;

    /*本地两弹一星书籍-封面路径*/
    @Value("${filePath.ldyxbdCoverPath}")
    private String ldyxBookCoverPath;

    /*本地两弹一星书籍-文件路径*/
    @Value("${filePath.ldyxbdfilepath}")
    private String ldyxBookFilePath;

    /*本地两弹一星文献-文件路径*/
    @Value("${filePath.ldyxWxFilePath}")
    private String ldyxWxFilePath;


    @Override
    public CutPageBean<IntegrationEntity> retrieval(IntegrationEntityBO integrationEntityBO) {
        String database = this.searchDirection(integrationEntityBO.getType(), integrationEntityBO.getTransportType());
        integrationEntityBO.setDatabase(database);

        //处理初级检索SQL
        String primarySearchSql = this.handlePrimarySearch(integrationEntityBO.getSearchType(), integrationEntityBO.getSearchValue());
        integrationEntityBO.setPrimarySearchSql(primarySearchSql);
        integrationEntityBO.setStart(integrationEntityBO.page, integrationEntityBO.pageSize);
        String code = "";
        if (database == "MYDXBDQK") {
            code = CommonConvertUtils.sqllistor("地区", integrationEntityBO.getCodeList());
        } else {
            code = CommonConvertUtils.sqllistor("行业分类代码", integrationEntityBO.getCodeList());
        }
        integrationEntityBO.setCode(code);
        CutPageBean<IntegrationEntity> cutPage = new CutPageBean<>();
        List<IntegrationEntity> list = retrievalDao.retrieval(integrationEntityBO);
        Long count = retrievalDao.getCount(integrationEntityBO);
        Integer counter = count.intValue();
        cutPage.initCutPage(counter, integrationEntityBO.pageSize, list);
        return cutPage;
    }

    @Override
    public CutPageBean<IntegrationEntity> retrievalQK(IntegrationEntityBO integrationEntityBO) {
        integrationEntityBO.setStart(integrationEntityBO.getPage(), integrationEntityBO.getPageSize());

        List<IntegrationEntity> list = retrievalDao.retrievalQkList(integrationEntityBO);
        int count = (int) retrievalDao.retrievalQkCount(integrationEntityBO);
        CutPageBean<IntegrationEntity> cutPage = new CutPageBean<>();
        cutPage.initCutPage(count, integrationEntityBO.pageSize, list);
        return cutPage;
    }

    @Override
    public String update(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO) {
        Boolean bl;
        String fileName = "";
        String pictureName = "";
        if (file != null && !file.isEmpty()) { //文件不为空，必须加条件判断
            bl = this.loadFile(file, filePicture);
            fileName = file.getOriginalFilename();
            integrationEntityBO.setFileName(fileName);
            pictureName = filePicture.getOriginalFilename();
            integrationEntityBO.setPictureAddress(pictureName);
            if (bl == false) {
                return "存在相同文件";
            }
        }

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        integrationEntityBO.setCurrentTime(dateFormat.format(date));
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        integrationEntityBO.setTime(dateFormat1.format(date));
        Boolean result = retrievalDao.update(integrationEntityBO);
        if (result == true) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }

    @Override
    public String updateQk(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO) {
        String filename = file.getOriginalFilename();
        String pictureName = filePicture.getOriginalFilename();
        String compile = integrationEntityBO.getCompile();
        //文件
        if (StringUtils.hasText(filename)) {
            Boolean uploadFile = this.loadFile(file, bdqkPdf, compile + ".pdf");
        }

        //封面 --注意需要改名,名字改为和编号一致+jpg
        if (StringUtils.hasText(pictureName)) {
            String picturePrefix = pictureName.split("\\.")[0];
            if (!picturePrefix.equalsIgnoreCase(compile)) {//封面的前缀和编号不一致,代表需要更改封面文件
                Boolean uploadCover = this.loadFile(filePicture, bdqkCover, compile + ".jpg");
            }
        }

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        integrationEntityBO.setCurrentTime(dateFormat.format(date));
        Boolean result = retrievalDao.updateQk(integrationEntityBO);
        if (result == true) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }


    @Override
    public String updateQkZk(MultipartFile file, IntegrationEntityBO integrationEntityBO) {
        String fileName = integrationEntityBO.getFileName();
        String compile = integrationEntityBO.getCompile();
        //修改前的文件需要删除
        String oldFileName = integrationEntityBO.getOldfileName();
        //上传新文件文件
        if (StringUtils.hasText(fileName)) {
            File bdqkZkPath = new File(bdqkPdf + "//" + compile);
            String fileSavePath = bdqkZkPath.toString();
            FileUtil.overrideFile(file, fileSavePath, fileName);
//            FileUtil.overrideFile(file, document, fileName);


            //删除修改前的老文件
            if (StringUtils.hasText(oldFileName)) {
                if (!fileName.equals(oldFileName))
                    FileUtil.deleteFile(fileSavePath + "\\" + oldFileName);
            }

        }
        //修改数据
        Boolean updateQkZk = retrievalDao.updateQkZk(integrationEntityBO);
        if (updateQkZk) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }

    @Override
    public String updateLdyx(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO) {
        String fileName = file.getOriginalFilename();
        String pictureName = filePicture.getOriginalFilename();
        String compile = integrationEntityBO.getCompile();
        //有文件
        if (StringUtils.hasText(fileName) || StringUtils.hasText(pictureName)) {
            //两弹一星书籍
            if ("3".equals(integrationEntityBO.getType()) && "1".equals(integrationEntityBO.getTransportType())) {
                if (file != null && !file.isEmpty()) {
                    FileUtil.overrideFile(file, ldyxBookFilePath, compile + ".pdf");
                    //文件名不变
                    integrationEntityBO.setFileName(compile + ".pdf");
                }
                if (StringUtils.hasText(pictureName)) {
                    FileUtil.overrideFile(filePicture, ldyxBookCoverPath, compile + ".jpg");
                }

                //两弹一星文献
            } else if ("3".equals(integrationEntityBO.getType()) && "0".equals(integrationEntityBO.getTransportType())) {
                if (file != null && !file.isEmpty()) {
                    FileUtil.overrideFile(file, ldyxWxFilePath, compile + ".pdf");
                    //文件名不变
                    integrationEntityBO.setFileName(compile + ".pdf");
                }

            }

        } else {
            String theFileName = integrationEntityBO.getFileName();
            //页面过来的文件名为空,代表取消了文件
            if (!StringUtils.hasText(theFileName)) {
                if ("3".equals(integrationEntityBO.getType()) && "1".equals(integrationEntityBO.getTransportType())) {
                    FileUtil.deleteFile(ldyxBookFilePath + "\\" + compile + ".pdf");
                } else if ("3".equals(integrationEntityBO.getType()) && "0".equals(integrationEntityBO.getTransportType())) {
                    FileUtil.deleteFile(ldyxWxFilePath + "\\" + compile + ".pdf");
                }

            }
        }


        //修改数据
        Boolean updateQkZk = retrievalDao.updateLdyx(integrationEntityBO);
        if (updateQkZk) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }

    @Override
    public String delete(IntegrationEntityBO integrationEntityBO) {
        Boolean delData = retrievalDao.delete(integrationEntityBO);
        if (delData) {//删除数据之后,删除文件
            //文件
            String fileName = integrationEntityBO.getFileName();
            //图片
            String pictureName = integrationEntityBO.getPictureName();
            //封面
            String coverName = integrationEntityBO.getCoverName();
            if (StringUtils.hasText(fileName)) { //删除文件
                FileUtil.deleteFile(document + pictureName);
            } else if (StringUtils.hasText(fileName)) {//删除图片
                FileUtil.deleteFile(picPath + fileName);
            } else if (StringUtils.hasText(coverName)) { //删除封面
                FileUtil.deleteFile(coverPic + fileName);
            }
        }
        if (delData) {
            return "删除成功";
        } else {
            return "删除失败";
        }
    }

    @Override
    public String deleteQk(IntegrationEntityBO integrationEntityBO) {
        Boolean result = retrievalDao.deleteQk(integrationEntityBO);
        //删除文件和封面
        String compile = integrationEntityBO.getCompile();
        String filePath = bdqkPdf + compile + ".pdf";
        String picturePath = bdqkCover + compile + ".jpg";
        FileUtil.deleteFile(filePath);
        FileUtil.deleteFile(picturePath);
        if (result == true) {
            return "删除成功";
        } else {
            return "删除失败";
        }
    }

    @Override
    public String deleteQkZk(IntegrationEntityBO integrationEntityBO) {
        String compile = integrationEntityBO.getCompile();
        //和期刊子库的删除方法通用
        Boolean result = retrievalDao.delete(integrationEntityBO);
        //删除文件
        String fileName = integrationEntityBO.getFileName();
        if (StringUtils.hasText(fileName)) {
//            String filePath = this.getFilePathByFileType(fileName);
//            String overFilePath = filePath + "\\" + fileName;
//            FileUtil.deleteFile(overFilePath);

            File bdqkZkPath = new File(bdqkPdf + "//" + compile + "//" + fileName);
            String filePath = bdqkZkPath.toString();
            FileUtil.deleteFile(filePath);

        }
        if (result == true) {
            return "删除成功";
        } else {
            return "删除失败";
        }
    }

    @Override
    public String deleteLdyx(IntegrationEntityBO integrationEntityBO) {
        //和期刊子库的删除方法通用
        Boolean result = retrievalDao.delete(integrationEntityBO);
        //删除文件和封面
        String fileName = integrationEntityBO.getFileName();
        String compile = integrationEntityBO.getCompile();
        String coverName = "";
        if (StringUtils.hasText(compile)) {
            coverName = compile + "jpg";
        }
        String filePath = "";
        if (StringUtils.hasText(fileName)) {
            filePath = ldyxBookFilePath + fileName;
            FileUtil.deleteFile(filePath);
        }

        String coverPath = "";
        if (StringUtils.hasText(coverName)) {
            coverPath = ldyxBookCoverPath + coverPath;
            FileUtil.deleteFile(coverPath);
        }

        if (result) {
            return "删除成功";
        } else {
            return "删除失败";
        }
    }

    @Override
    public String insert(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO) {
        Boolean bl;
        String fileName = "";
        String fileName2 = "";
        if (file != null && !file.isEmpty()) { //文件不为空
            bl = this.loadFile(file, filePicture);
            fileName = file.getOriginalFilename();
            integrationEntityBO.setFileName(fileName);
            fileName2 = filePicture.getOriginalFilename();
            integrationEntityBO.setPictureAddress(fileName2);
            if (bl == false) {
                return "存在相同文件";
            }
        }

        String database = this.searchDirection(integrationEntityBO.getType(), integrationEntityBO.getTransportType());
        integrationEntityBO.setDatabase(database);
        integrationEntityBO.setFileName(fileName);
        //获取当前日期
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        integrationEntityBO.setCurrentTime(dateFormat.format(date));
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        integrationEntityBO.setTime(dateFormat1.format(date));
        Boolean result = retrievalDao.insert(integrationEntityBO);
        if (result == true) {
            return "新增成功";
        } else {
            return "新增失败";
        }
    }

    @Override
    public String insertQk(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO) {
        if (!file.isEmpty() || !filePicture.isEmpty()) {
            Boolean b2 = this.loadFileOfBdqk(file, filePicture);
            if (b2 == false) {
                return "存在相同文件";
            }
        }

        String database = this.searchDirection(integrationEntityBO.getType(), integrationEntityBO.getTransportType());
        integrationEntityBO.setDatabase(database);
        //获取当前日期
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        integrationEntityBO.setCurrentTime(dateFormat.format(date));
        String fileName = file.getOriginalFilename();
        if (StringUtils.hasText(fileName)) {
            String compile = fileName.split("\\.")[0];
            integrationEntityBO.setCompile(compile);
        }
        Boolean result = retrievalDao.insertQk(integrationEntityBO);
        if (result == true) {
            return "新增成功";
        } else {
            return "新增失败";
        }
    }

    @Override
    public String insertQkZk(MultipartFile file, IntegrationEntityBO integrationEntityBO) {
        String compile = integrationEntityBO.getCompile();
        if (!StringUtils.hasText(compile)) return "新增失败";
        //新建数据
        integrationEntityBO.setFileName(file.getOriginalFilename());
        Boolean insertQkZk = retrievalDao.insertQkZk(integrationEntityBO);

        //上传文件
        String fileName = file.getOriginalFilename();
//        String filePath = this.getFilePathByFileType(fileName);
        File bdqkZkPath = new File(bdqkPdf + "//" + compile);
        String fileSavePath = bdqkZkPath.toString();
        boolean bl = FileUtil.overrideFile(file, fileSavePath, fileName);


        if (insertQkZk) {
            return "新增成功";
        } else {
            return "新增失败";
        }

    }

    @Override
    public String insertLdyxData(MultipartFile file, MultipartFile filePicture,
                                 IntegrationEntityBO integrationEntityBO) {
        String result = "";
        String boFileName = integrationEntityBO.getFileName();
        if (StringUtils.hasText(boFileName)) {
            String compile = boFileName.split("\\.")[0];
            integrationEntityBO.setCompile(compile);
        }
        String type = integrationEntityBO.getType();//数据库
        String TransportType = integrationEntityBO.getTransportType();//检索方向
        if (!StringUtils.hasText(type) || !StringUtils.hasText(TransportType)) {
            return null;
        }
        //数据库名
        String daCode = this.searchDirection(type, TransportType);
        integrationEntityBO.setDatabase(daCode);

        //获取当前日期
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        integrationEntityBO.setCurrentTime(dateFormat.format(date));


        switch (daCode) {
            case "LDYXBD_WX":
                if (!file.isEmpty()) {
                    Boolean insertLdyxWxFile = this.loadFileLdyx(file, filePicture, ldyxWxFilePath, "");
                    if (!insertLdyxWxFile) {
                        result = "存在相同文件";
                        break;
                    }
                }

                Boolean insertLdyxWx = retrievalDao.insertLdyxbd(integrationEntityBO);
                if (insertLdyxWx) {
                    result = "新增成功";
                } else {
                    result = "新增失败";
                }
                break;
            case "LDYXBD":
                if (!file.isEmpty() || !filePicture.isEmpty()) {
                    Boolean insertLdyxFile = this.loadFileLdyx(file, filePicture, ldyxBookFilePath, ldyxBookCoverPath);
                    if (!insertLdyxFile) {
                        result = "存在相同文件";
                        break;
                    }
                }


                Boolean insertLdyxBook = retrievalDao.insertLdyxbd(integrationEntityBO);
                if (insertLdyxBook) {
                    result = "新增成功";
                } else {
                    result = "新增失败";
                }
                break;
        }
        return result;
    }

    /**
     * 通过指定存储路径上传文件与图片(协同创新，应急管理)
     *
     * @param file 文件
     * @return
     */
    public Boolean loadFile(MultipartFile file, MultipartFile filePicture) {
        String fileName = file.getOriginalFilename();
        String fileName2 = filePicture.getOriginalFilename();
        File file1 = new File(picPath + "//" + fileName);
        File file2 = new File(videoPath + "//" + fileName);
        File file3 = new File(document + "//" + fileName);
        File file4 = new File(coverPic + "//" + fileName2);
        //判定是否存在相同文件file1.exists()
        if (file1.exists()) {
            return false;
//            return "存在相同文件";
        } else if (file2.exists()) {
//            return "存在相同文件";
            return false;
        } else if (file3.exists()) {
//            return "存在相同文件";
            return false;
        } else if (file4.exists()) {
//            return "存在相同文件";
            return false;
        }
//        String randDir = RandomUtils.getRandomString("pir");
        String filePath = null;
        if (fileName.split("\\.")[1].equals("jpg")) {
            filePath = new File(picPath).toString();
        } else if (fileName.split("\\.")[1].equals("mv") || fileName.split("\\.")[1].equals("mp4") || fileName.split("\\.")[1].equals("AVI")) {
            filePath = new File(videoPath).toString();
        } else if (fileName.split("\\.")[1].equals("pdf")) {
            filePath = new File(document).toString();
        }
        String filePath1 = null;
        if (fileName2.split("\\.")[1].equals("jpg")) {
            filePath1 = new File(coverPic).toString();
        }
        try {
            //执行保存(文件,目录,文件名)
            FileUtil.saveFile(file, filePath, fileName);
            FileUtil.saveFile(filePicture, filePath1, fileName2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * 通过指定存储路径上传文件与图片(党校期刊)
     *
     * @param file 文件
     * @return
     */
    public Boolean loadFileOfBdqk(MultipartFile file, MultipartFile filePicture) {
        String fileName = file.getOriginalFilename();
        String fileName3 = file.getOriginalFilename().split("\\.")[0];
        String fileName4 = filePicture.getOriginalFilename().split("\\.")[1];
        //封面名称为附件名+封面的后缀(编号名+jpg)
        String coverName = fileName3 + "." + fileName4;
        File cover = new File(bdqkCover + "//" + coverName);
        File pdf = new File(bdqkPdf + "//" + fileName);

        if (cover.exists()) {
            return false;
        } else if (pdf.exists()) {
            return false;
        }

        String filePath = "";
        if (StringUtils.hasText(fileName)) {
            filePath = new File(bdqkPdf).toString();
        }

        String coverPath = "";
        if (StringUtils.hasText(coverName)) {
            coverPath = new File(bdqkCover).toString();
        }

        try {
            //执行保存(文件,目录,文件名)
            FileUtil.saveFile(file, filePath, fileName);
            FileUtil.saveFile(filePicture, coverPath, coverName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * 上传封面 - 文件名需要改名为编号加pdf或jpg的
     *
     * @param file    文件
     * @param compile 编号(文件名为编号+jpg)
     * @return
     */
    public Boolean loadFile(MultipartFile file, String basePath, String compile) {
        //封面存放的路径
        String filePath = new File(basePath).toString();
        //删除原有封面文件,并上传新的封面文件
        boolean bl = FileUtil.overrideFile(file, filePath, compile);
        return bl;
    }

    /**
     * 更具文件名,获取文件存放路径
     *
     * @param fileName 文件名
     * @return
     */
    private String getFilePathByFileType(String fileName) {
        String filePath = "";
        String fileSuffix = fileName.split("\\.")[1];
        if (("mv").equalsIgnoreCase(fileSuffix) || ("mp4").equalsIgnoreCase(fileSuffix) || ("AVI").equalsIgnoreCase(fileSuffix)) {
            filePath = new File(videoPath).toString();
        } else if (("pdf").equalsIgnoreCase(fileSuffix)) {
            filePath = new File(document).toString();
        } else if (("jpg").equalsIgnoreCase(fileSuffix)) {
            filePath = new File(picPath).toString();
        }
        return filePath;
    }


    /**
     * 通过指定存储路径上传文件与图片(两弹一星专用)
     *
     * @param file          文件
     * @param filePicture   封面
     * @param fileSavePath  文件存放路径
     * @param coverSavePath 封面存放路径
     * @return
     */
    public Boolean loadFileLdyx(MultipartFile file, MultipartFile filePicture,
                                String fileSavePath, String coverSavePath) {
        //文件名
        String fileName = "";
        //文件路径
        String filePath = null;
        if (file != null &&  !file.isEmpty()) { //文件
            fileName = file.getOriginalFilename();
            File file1 = new File(fileSavePath + "//" + fileName);

            //判定是否存在相同文件
            if (file1.exists()) {
                return false;
            }

            //文件后缀
            String fileSuffix = fileName.split("\\.")[1];
            //根据文件后缀确定存放路径
            if (fileSuffix.equalsIgnoreCase("pdf")) {
                filePath = new File(fileSavePath).toString();
            }

        }

        if (filePicture != null &&  !filePicture.isEmpty()) { //封面
            fileName = fileName.split("\\.")[0]+".jpg";
            File file4 = new File(coverSavePath + "//" + fileName);
            if (file4.exists()) {
                return false;
            }
            //文件后缀
            String fileSuffix = fileName.split("\\.")[1];
            if (fileSuffix.equalsIgnoreCase("jpg")) {
                filePath = new File(coverSavePath).toString();
            }
        }

        try {
            //对应的本地路径
            File myLocalFile = new File(filePath);
            //文件的父文件夹
            File directory = myLocalFile.getParentFile();
            //文件的父级目录不存在
            if (!myLocalFile.exists()) {
                // 创建目录
                directory.mkdirs();
            }
            //执行保存(文件,目录,文件名)
            if (file != null && !file.isEmpty()) FileUtil.saveFile(file, filePath, fileName);
            if (filePicture != null && !filePicture.isEmpty()) FileUtil.saveFile(filePicture, filePath, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 页面检索方向
     *
     * @param type
     * @return
     */
    private String searchDirection(String type, String transportType) {
        if (!StringUtils.hasLength(type)) {
            return null;
        }
        if ("2".equals(type)) {
            return "MYDXBDQK";
        } else if ("0".equals(type) && "1".equals(transportType)) {
            return "MYDXTP";
        } else if ("0".equals(type) && "2".equals(transportType)) {
            return "MYDXSP";
        } else if ("0".equals(type) && "0".equals(transportType)) {
            return "MYDXWX";
        } else if ("1".equals(type) && "0".equals(transportType)) {
            return "MYDXWX";
        } else if ("1".equals(type) && "1".equals(transportType)) {
            return "MYDXTP";
        } else if ("1".equals(type) && "2".equals(transportType)) {
            return "MYDXSP";
        } else if ("3".equals(type) && "1".equals(transportType)) {
            return "LDYXBD";
        } else if ("3".equals(type) && "0".equals(transportType)) {
            return "LDYXBD_WX";
        } else {
            return null;
        }
    }

//    /**
//     * 处理初级检索集合
//     *
//     * @param
//     * @return
//     */
//    private String handlePrimarySearch(String searchType, String searchValue) {
//        if (!StringUtils.hasLength(searchValue)) {
//            return null;
//        }
//        if ("题名".equals(searchType)) {
//            return searchType + "% '" + searchValue + "*'";
//        } else if ("作者".equals(searchType)) {
//            return searchType + "% '" + searchValue + "*'";
//        } else if("机构".equals(searchType)) {
//            return searchType + "% '" + searchValue + "*'";
//        }else {
//            return null;
//        }
//    }

    /**
     * 处理初级检索集合
     *
     * @param
     * @return
     */
    private String handlePrimarySearch(String searchType, String searchValue) {
        if (!StringUtils.hasLength(searchValue)) {
            return null;
        }
        return searchType + "% '" + searchValue + "*'";
    }

//    /**
//     * 处理插入集合
//     *
//     * @param
//     * @return
//     */
//    private String handleInsert(String insertType) {
//        if (!StringUtils.hasLength(insertType)) {
//            return null;
//        }
//        if ("0".equals(insertType)) {
//            return "题名";
//        }  else if ("1".equals(insertType)) {
//            return "行业分类代码";
//        }  else if ("2".equals(insertType)) {
//            return "文件名";
//        } else if ("3".equals(insertType)) {
//            return "来源数据库";
//        } else if ("4".equals(insertType)) {
//            return "机构";
//        } else if ("5".equals(insertType)) {
//            return "作者";
//        } else {
//            return null;
//        }
//    }

}
