package web.service.impl;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import web.bo.CJFXLASTBo;
import web.common.CutPageBean;
import web.dao.JournalDatabaseFederatedSearchDao;
import web.entity.CJFXLAST;
import web.service.JournalDatabaseFederatedSearchService;

import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/28 16:25
 * @Version 1.0
 */
@Service
public class JournalDatabaseFederatedSearchServiceImpl implements JournalDatabaseFederatedSearchService {
    @Autowired
    private JournalDatabaseFederatedSearchDao journalDao;

    @Override
    public CutPageBean<CJFXLAST> complexSearchJournal(CJFXLASTBo cjfxlastBo) {
        cjfxlastBo.setStart(cjfxlastBo.getPage(), cjfxlastBo.getPageSize());
        //拼接期刊标识码Sql
        String sortCodeSql = this.getFXSortCode(cjfxlastBo.getSortCodeList());
        cjfxlastBo.setSortCodeSql(sortCodeSql);
        //设置更新时间
        cjfxlastBo.setTheUpdateDate(cjfxlastBo.getUpdateDate());
        //处理初级检索SQL
        String primarySearchSql = this.handlePrimarySearch(cjfxlastBo.getSearchType(), cjfxlastBo.getSearchValue());
        cjfxlastBo.setPrimarySearchSql(primarySearchSql);

        //处理高级检索List
        String advSearchSql = this.handleAdvSearchList(cjfxlastBo.getAdvSearchList());
        cjfxlastBo.setAdvSearchSql(advSearchSql);
        //查询结果
        CutPageBean<CJFXLAST> pageBean = new CutPageBean<>();
        List<CJFXLAST> list = journalDao.complexSearchJournalList(cjfxlastBo);
        int count = (int) journalDao.complexSearchJournalCount(cjfxlastBo);
        pageBean.initCutPage(count, cjfxlastBo.getPageSize(), list);
        return pageBean;
    }

    /**
     * 获拼接左侧勾选信息
     *
     * @param sortCodeList
     * @return
     */
    private String getFXSortCode(List<String> sortCodeList) {
        if (CollectionUtils.isEmpty(sortCodeList))
            return null;
        StringBuffer bf = new StringBuffer("(");
        String column = "期刊标识码";
        for (String sort : sortCodeList) {
            if (!"".equals(sort) && sort != null) {
                if (sort.split(",").length > 1) {
                    String[] split = sort.split(",");
                    for (int i = 0; i < split.length; i++) {
                        bf.append(column).append(" = ").append(split[i]).append("?").append("  or ");
                    }
                    continue;
                }
                bf.append(column).append(" = ").append(sort).append("?").append("  or ");
            }
        }
        if (bf.length() == 0) {
            return null;
        }
        String returnStr = bf.substring(0, bf.length() - 3) + ")";
        return returnStr;
    }


    /**
     * 处理初级检索集合
     *
     * @param
     * @return
     */
    private String handlePrimarySearch(String searchType, String searchValue) {
        if (!StringUtils.hasLength(searchValue)) {
            return null;
        }
        if ("篇名".equals(searchType)) {
            return searchType + "= '" + searchValue + "'";
        } else if ("作者".equals(searchType) || "主题".equals(searchType)) {
            return searchType + "% '" + searchValue + "'";
        } else {
            return searchType + "= '*" + searchValue + "'";
        }

    }

    /**
     * 处理高级检索集合
     *
     * @param AdvSearchList
     * @return
     */
    private String handleAdvSearchList(List<Map<String, String>> AdvSearchList) {

        if (CollectionUtils.isEmpty(AdvSearchList))
            return null;
        StringBuffer bf = new StringBuffer("(");
        for (int i = 0; i < AdvSearchList.size(); i++) {
            String key = AdvSearchList.get(i).get("key");
            String value = AdvSearchList.get(i).get("value");
            String logic = "";
            if (i < AdvSearchList.size() - 1) {
                logic = AdvSearchList.get(i + 1).get("logic");
            }
            //如果搜索词和搜索选项为空直接进行下一次循环
            if (!StringUtils.hasLength(key) || !StringUtils.hasLength(value)) {
                continue;
            }
            String symbol = this.choiceSymbol(key);
//                bf.append(key).append("='*").append(value).append("'");
            bf.append(key).append(symbol).append(value).append("'");
            //拼and和or
            if (StringUtils.hasLength(logic)) {
                bf.append(" ").append(logic).append(" ");
            }
        }
        if (bf.length() == 0) {
            return null;
        }
        String returnStr = bf + ")";
        //排除空货号的情况
        if ("()".equals(returnStr)) {
            returnStr = "";
        }
        //排除(题名 = 'xxx' or )和(题名 = 'xxx' and )  的情况
        if (returnStr.endsWith("or )")) {
            returnStr = returnStr.replace("or )", ")");
        } else if (returnStr.endsWith("and )")) {
            returnStr = returnStr.replace("and )", ")");
        }
        return returnStr;
    }

    /**
     * 不同的检索词使用不同的语法
     *
     * @param searchType
     * @return
     */
    public String choiceSymbol(String searchType) {
        if ("篇名".equals(searchType) || "题名".equals(searchType)) {
            return " = '";
        } else if ("作者".equals(searchType) || "主题".equals(searchType)) {
            return " % '";
        } else {
            return " = '*";
        }
    }

}
