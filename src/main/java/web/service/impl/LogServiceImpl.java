package web.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import web.bo.KnowledgeStatisticsBo;
import web.common.Result;
import web.dao.KnowledgeStatisticsDao;
import web.entity.Security_UserDetail;
import web.entity.UserDo;
import web.entity.admin.log.ViewDownloadDo;
import web.mapper.LogMysqlMapper;
import web.service.LogService;
import web.utils.IPutil;
import web.utils.RandomUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;


@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMysqlMapper logMysqlMapper;
    @Autowired
    private KnowledgeStatisticsDao knowledgeStatisticsDao;

    @Override
    public Result viewLog(ViewDownloadDo viewDownloadDo, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Security_UserDetail detail = (Security_UserDetail) authentication.getPrincipal();
        UserDo usr = detail.getUserDo();
        viewDownloadDo.setUser_id(usr.getAccount());
        viewDownloadDo.setUser_name(usr.getUser_name());
        viewDownloadDo.setIp(IPutil.getRemortIP(request));
        viewDownloadDo.setCreate_time(RandomUtils.nowStr(""));
        if(viewDownloadDo.getHyfl_code()==null ||viewDownloadDo.getHyfl_code()==0 )
        {
            viewDownloadDo.setHyfl_code(getHyfl_codeByKnowledge(viewDownloadDo.getResource_code() != null ? viewDownloadDo.getResource_code() : "", viewDownloadDo.getResource_id() != null ? viewDownloadDo.getResource_id() : ""));
        }
        Integer res = logMysqlMapper.insertViewLog(viewDownloadDo);
        if (1 == res) return Result.succ("插入浏览日志成功");
        return Result.fail("插入浏览日志失败");
    }

    @Override
    public Result downLoadLog(ViewDownloadDo viewDownloadDo, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Security_UserDetail detail = (Security_UserDetail) authentication.getPrincipal();
        UserDo usr = detail.getUserDo();
        viewDownloadDo.setUser_id(usr.getAccount());
        viewDownloadDo.setUser_name(usr.getUser_name());
        viewDownloadDo.setIp(IPutil.getRemortIP(request));
        viewDownloadDo.setCreate_time(RandomUtils.nowStr(""));
        if(viewDownloadDo.getHyfl_code()==null ||viewDownloadDo.getHyfl_code()==0 )
        {
            viewDownloadDo.setHyfl_code(getHyfl_codeByKnowledge(viewDownloadDo.getResource_code() != null ? viewDownloadDo.getResource_code() : "", viewDownloadDo.getResource_id() != null ? viewDownloadDo.getResource_id() : ""));
        }
        Integer res = logMysqlMapper.insertDownLoadLog(viewDownloadDo);
        if (1 == res) return Result.succ("插入下载日志成功");
        return Result.fail("插入下载日志失败");
    }

    @Override
    public Result anonymousView(ViewDownloadDo viewDownloadDo, HttpServletRequest request) {
        viewDownloadDo.setUser_id(null);
        viewDownloadDo.setUser_name("anonymous");
        viewDownloadDo.setIp(IPutil.getRemortIP(request));
        viewDownloadDo.setCreate_time(RandomUtils.nowStr(""));
        if(viewDownloadDo.getHyfl_code()==null ||viewDownloadDo.getHyfl_code()==0 )
        {
            viewDownloadDo.setHyfl_code(getHyfl_codeByKnowledge(viewDownloadDo.getResource_code() != null ? viewDownloadDo.getResource_code() : "", viewDownloadDo.getResource_id() != null ? viewDownloadDo.getResource_id() : ""));
        }
        Integer res = logMysqlMapper.insertViewLog(viewDownloadDo);
        if (1 == res) return Result.succ("插入匿名浏览日志成功");
        return Result.fail("插入匿名浏览日志失败");
    }
    @Override
    public Integer getHyfl_codeByKnowledge(String resource_code,String resource_id){
        if(resource_code==""||resource_id==""){
            return  0;
        }
        //行业分类代码 1 协同创新 2应急管理 3党校期刊  为空查询全部
        Integer hyfl_code=0;

        //党校期刊表  3
        String DXQK="CJFX7911,CJFXLAST,MYDXBDQK，MYDXBDQK";

        //应急管理   2
        String YJGL="YJTP7921,YJTJ7922,YJTDLAST2022,YJTPLAST2022,YJTM0121,YJTN0022,512WCDZWXK,WJMYTS,YJTMLAST2022,YJTD0521,YJTJLAST2022,YJTNLAST2022";

        //协同创新   1
        String XTCX="XTXP8922,XTXJ7922,XTXD0220,XTXM0021,XTXN0022,LDYXBD";



        //混合  行业分类代码   ca1:协同创新 ca2:应急管理
        String HX="MYDXTP,MYDXWX,MYDXSP,MYDXSPIDER_METADATA";  //主键 :SYS_FLD_SYSID


        //两弹一星协同视图期刊
        String LDYJTOTAL = "LDYTLKCCJDLAST1M,LDYTLKCCJDLAST2M,LDYTLKCJFD2000M,LDYTLKCJFD2001M,LDYTLKCJFD2002M,LDYTLKCJFD2003M,LDYTLKCJFD2004M,LDYTLKCJFD2005M,LDYTLKCJFD2006M,LDYTLKCJFD2007M,LDYTLKCJFD2008M,LDYTLKCJFD2009M,LDYTLKCJFD2010M,LDYTLKCJFD2011M,LDYTLKCJFD2012M,LDYTLKCJFD2013M,LDYTLKCJFD2014M,LDYTLKCJFD7984M,LDYTLKCJFD8589M,LDYTLKCJFD9093M,LDYTLKCJFD9495M,LDYTLKCJFD9697M,LDYTLKCJFD9899M,LDYTLKCJFDHIS2M,LDYTLKCJFDLASN2014M,LDYTLKCJFDLASN2015M,LDYTLKCJFDLASN2016M,LDYTLKCJFDLASN2017M,LDYTLKCJFDLASN2018M,LDYTLKCJFDLASN2019M,LDYTLKCJFDLASN2020M,LDYTLKCJFDLASN2021M,LDYTLKCJFDLASN2022M,LDYTLKCJFDLASN2023M,LDYTLKCJFDLAST2015M,LDYTLKCJFDLAST2016M,LDYTLKCJFDLAST2017M,LDYTLKCJFDLAST2018M,LDYTLKCJFDLAST2019M,LDYTLKCJFDLAST2020M,LDYTLKCJFDLAST2021M,LDYTLKCJFDLAST2022M,LDYTLKCJFDLAST2023M,LDYTLKCJFDN0508M,LDYTLKCJFDN0911M,LDYTLKCJFDN1214M,LDYTLKCJFDN7904M";
        //两弹一星协同视图博士
        String LDYDTOTAL = "LDYTLKCDFD0911M,LDYTLKCDFD1214M,LDYTLKCDFD9908M,LDYTLKCDFDLAST2015M,LDYTLKCDFDLAST2016M,LDYTLKCDFDLAST2017M,LDYTLKCDFDLAST2018M,LDYTLKCDFDLAST2019M,LDYTLKCDFDLAST2020M,LDYTLKCDFDLAST2021M,LDYTLKCDFDLAST2022M,LDYTLKCDFDLAST2023M";
        //两弹一星协同视图硕士
        String LDYMTOTAL = "LDYTLKCMFD0506M,LDYTLKCMFD2007M,LDYTLKCMFD2008M,LDYTLKCMFD2009M,LDYTLKCMFD2010M,LDYTLKCMFD2011M,LDYTLKCMFD2012M,LDYTLKCMFD201301M,LDYTLKCMFD201302M,LDYTLKCMFD201401M,LDYTLKCMFD201402M,LDYTLKCMFD201501M,LDYTLKCMFD201502M,LDYTLKCMFD201601M,LDYTLKCMFD201602M,LDYTLKCMFD201701M,LDYTLKCMFD201702M,LDYTLKCMFD201801M,LDYTLKCMFD201802M,LDYTLKCMFD201901M,LDYTLKCMFD201902M,LDYTLKCMFD202001M,LDYTLKCMFD202002M,LDYTLKCMFD202101M,LDYTLKCMFD202102M,LDYTLKCMFD202201M,LDYTLKCMFD202202M,LDYTLKCMFD202301M,LDYTLKCMFD9904M";
        //两弹一星协同视图会议
        String LDYPTOTAL = "LDYTLKCPFD0914M,LDYTLKCPFD9908M,LDYTLKCPFDLAST2019M";
        //两弹一星协同视图报纸
        String LDYNTOTAL = "LDYTLKCCND0005M,LDYTLKCCND0911M,LDYTLKCCND2006M,LDYTLKCCND2007M,LDYTLKCCND2008M,LDYTLKCCND2010M,LDYTLKCCNDHISM,LDYTLKCCNDLAST2011M,LDYTLKCCNDLAST2012ADDM,LDYTLKCCNDLAST2012M,LDYTLKCCNDLAST2013M,LDYTLKCCNDLAST2014M,LDYTLKCCNDLAST2015M,LDYTLKCCNDLAST2016M,LDYTLKCCNDLAST2017M,LDYTLKCCNDLAST2018M,LDYTLKCCNDLAST2019M,LDYTLKCCNDLAST2020M,LDYTLKCCNDLAST2021M,LDYTLKCCNDLAST2022M,LDYTLKCCNDLAST2023M";

        XTCX=XTCX+","+LDYJTOTAL+","+LDYDTOTAL+","+LDYMTOTAL+","+LDYPTOTAL+","+LDYNTOTAL;

        List<String> DXQKs= Arrays.asList(DXQK.split(","));
        List<String> YJGLs= Arrays.asList(YJGL.split(","));
        List<String> XTCXs= Arrays.asList(XTCX.split(","));
        List<String> HXs= Arrays.asList(HX.split(","));
        if(DXQKs.contains(resource_code)){
            hyfl_code=3;
        }else if(YJGLs.contains(resource_code)){
            hyfl_code=2;
        }else if(XTCXs.contains(resource_code)){
            hyfl_code=1;
        }else if(HXs.contains(resource_code)){
            KnowledgeStatisticsBo knowledgeStatisticsBo=new KnowledgeStatisticsBo();
            knowledgeStatisticsBo.setBdDataTable(resource_code);
            String sql=" SYS_FLD_SYSID="+resource_id;
            knowledgeStatisticsBo.setWhereSql(sql);
            List<String> hyfl_codes=knowledgeStatisticsDao.knowledgeCountByHyflCodeAndSql(knowledgeStatisticsBo);
            if(hyfl_codes!=null&&hyfl_codes.size()>0){
                String code=hyfl_codes.get(0).toString();
                if(code.startsWith("ca1")){
                    hyfl_code=1;
                }else if(code.startsWith("ca2")){
                    hyfl_code=2;
                }
            }
        }
        return  hyfl_code;

    }


}
