package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.ExpertEntityBO;
import web.bo.PopularArticlesBo;
import web.dao.PeriodicalIndexDao;
import web.service.PeriodicalIndexService;

import java.util.List;

@Service
public class PeriodicalIndexServiceImpl implements PeriodicalIndexService {
    @Autowired
    PeriodicalIndexDao periodicalIndexDao;

    @Override
    public List<PopularArticlesBo> popularArticles(PopularArticlesBo popularArticlesBo) {
        int i = (int)(Math.random()*40+1);
        popularArticlesBo.setRandomNumber(i);
        List<PopularArticlesBo> res = periodicalIndexDao.popularArticles(popularArticlesBo);
        return res;
    }

    @Override
    public List<CJFXBASEINFOBo> wholeAtiicles(CJFXBASEINFOBo cjfxbaseinfoBo) {
        List<CJFXBASEINFOBo> res = periodicalIndexDao.wholeAtiicles(cjfxbaseinfoBo);
        return  res;
    }

    @Override
    public List<CJFXLASTBo> partyResource(CJFXLASTBo cjfxlastBo) {
        String  code  = cjfxlastBo.getCode();
        String[] codes = code.split(",");
        StringBuilder stringBuilder= new StringBuilder();
        for (String s : codes) {
            stringBuilder.append("or 期刊标识码='"+s+"?' ");
        }
        String result= stringBuilder.substring(2,stringBuilder.length()-1);
        cjfxlastBo.setConditionSql(result);
        List<CJFXLASTBo> res = periodicalIndexDao.partyResource(cjfxlastBo);
        return  res;
    }

    @Override
    public List<ExpertEntityBO> getExpert(ExpertEntityBO expertBO) {
        List<ExpertEntityBO> res = periodicalIndexDao.getExpert(expertBO);
        return res;
    }

    @Override
    public List<ExpertEntityBO> getExpertOrg() {
        List<ExpertEntityBO> res = periodicalIndexDao.getExpertOrg();
        return res;
    }
}
