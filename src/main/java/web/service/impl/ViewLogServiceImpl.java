package web.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.ViewLogBo;
import web.common.CutPageBean;
import web.mapper.ViewLoglMapper;
import web.service.ViewLogService;

import java.util.List;


@Service
public class ViewLogServiceImpl implements ViewLogService {

    @Autowired
    private ViewLoglMapper viewLoglMapper;


    @Override
    public CutPageBean<ViewLogBo> getViewLog(ViewLogBo viewLogBo) {
        CutPageBean<ViewLogBo> cutPage = new CutPageBean<>();
        List<ViewLogBo> list = viewLoglMapper.getViewLog(viewLogBo);
        Long count =  viewLoglMapper.getViewLogCount(viewLogBo);
        Integer counter =count.intValue();
        cutPage.initCutPage(counter, viewLogBo.getPageSize(), list);
        return cutPage;
    }

    @Override
    public CutPageBean<ViewLogBo> getOtherViewLog(ViewLogBo viewLogBo) {
        CutPageBean<ViewLogBo> cutPage = new CutPageBean<>();
        List<ViewLogBo> list = viewLoglMapper.getOtherViewLog(viewLogBo);
        Long count =  viewLoglMapper.getOtherViewLogCount(viewLogBo);
        Integer counter =count.intValue();
        cutPage.initCutPage(counter, viewLogBo.getPageSize(), list);
        return cutPage;
    }

    @Override
    public CutPageBean<ViewLogBo> getOtherDownLog(ViewLogBo viewLogBo) {
        CutPageBean<ViewLogBo> cutPage = new CutPageBean<>();
        List<ViewLogBo> list = viewLoglMapper.getOtherDownLLog(viewLogBo);
        Long count =  viewLoglMapper.getOtherDownLLogCount(viewLogBo);
        Integer counter =count.intValue();
        cutPage.initCutPage(counter, viewLogBo.getPageSize(), list);
        return cutPage;
    }

    @Override
    public CutPageBean<ViewLogBo> getDownLog(ViewLogBo viewLogBo) {
        CutPageBean<ViewLogBo> cutPage = new CutPageBean<>();
        List<ViewLogBo> list = viewLoglMapper.getDownLLog(viewLogBo);
        Long count =  viewLoglMapper.getDownLLogCount(viewLogBo);
        Integer counter =count.intValue();
        cutPage.initCutPage(counter, viewLogBo.getPageSize(), list);
        return cutPage;
    }
}
