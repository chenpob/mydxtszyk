package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import web.bo.EmergencyEntityBo;
import web.bo.TypicalCharacterBo;
import web.common.CutPageBean;
import web.dao.EmergencyDatabaseFederatedSearchDao;
import web.dao.UtilDao;
import web.entity.*;
import web.service.EmergencyDatabaseFederatedSearchService;
import web.utils.FileUtil;
import web.utils.FileuploadUtils;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Description: 应急库Service实现
 *
 * @author BaiGe
 * @date: 2022/6/20 16:38
 * @Version 1.0
 */
@Service
public class EmergencyDatabaseFederatedSearchServiceImpl implements EmergencyDatabaseFederatedSearchService {

    @Autowired
    private EmergencyDatabaseFederatedSearchDao emergencyDao;

    @Autowired
    private UtilDao utilDao;

    @Override
    public CutPageBean<EmergencyEntity> complexSearchEmergency(EmergencyEntityBo emergencyEntity) {
        emergencyEntity.setStart(emergencyEntity.getPage(), emergencyEntity.getPageSize());
        CutPageBean<EmergencyEntity> cutPage = new CutPageBean<>();
        //左侧信息SQL
        String sortCodeSql = this.getSortCode(emergencyEntity.getSortCodeList());
        emergencyEntity.setSortCodeSql(sortCodeSql);

        List<EmergencyEntity> list = emergencyDao.complexSearchEmergencyList(emergencyEntity);
        int count = (int) emergencyDao.complexSearchEmergencyCount(emergencyEntity);

        cutPage.initCutPage(count, emergencyEntity.getPageSize(), list);
        return cutPage;
    }

    @Override
    public CutPageBean<TypicalCharacterEntity> getTypicalCharacter(TypicalCharacterBo entity) {
        entity.setStart(entity.getPage(), entity.getPageSize());
        CutPageBean<TypicalCharacterEntity> cutPage = new CutPageBean<>();
        List<TypicalCharacterEntity> list = emergencyDao.getTypicalCharacterList(entity);
        Long count = emergencyDao.getTypicalCharacterCount(entity);
        Integer num=count.intValue();
        cutPage.initCutPage(num, entity.getPageSize(),list);
        return cutPage;
    }

    @Override
    public CutPageBean<TypicalCharacterEntity> getLDYXList(TypicalCharacterBo entity) {
        entity.setStart(entity.getPage(), entity.getPageSize());
        CutPageBean<TypicalCharacterEntity> cutPage = new CutPageBean<>();
        List<TypicalCharacterEntity> list = emergencyDao.getLDYXList(entity);
        Long count = emergencyDao.getLDYXCount(entity);
        Integer num=count.intValue();
        cutPage.initCutPage(num, entity.getPageSize(),list);
        return cutPage;
    }

    @Override
    public List<TypicalCharacterEntity> getTypicalCharacterTopNum(TypicalCharacterBo entity) {
        List<TypicalCharacterEntity> list = emergencyDao.getTypicalCharacterTopNum(entity);
        return list;
    }

    @Override
    public List<HotWord> getHotWord(Integer limitNum) {
        List<HotWord> hotWord = emergencyDao.getHotWord(limitNum);
        return hotWord;
    }

    @Override
    public List<IndexPictureEntity> getCaptureCover(String sortCode, Integer topNum) {
        IndexPictureEntity entity=new IndexPictureEntity();
        entity.setTopNum(topNum);
        entity.setSortCode(sortCode);
        List<IndexPictureEntity> entityList = emergencyDao.getTopCapture(entity);
        return entityList;
    }

    /**
     * 获拼接左侧勾选信息
     *
     * @param sortCodeList
     * @return
     */
    private String getSortCode(List<String> sortCodeList) {
        if (CollectionUtils.isEmpty(sortCodeList))
            return null;
        StringBuffer bf = new StringBuffer("(");
        String column = "行业分类代码";
        for (String sort : sortCodeList) {
            if (!"".equals(sort) && sort != null) {
                bf.append(column).append(" = ").append(sort).append("  or ");
            }
        }
        if (bf.length() == 0) {
            return null;
        }
        String returnStr = bf.substring(0, bf.length() - 3) + ")";
        return returnStr;
    }

    @Override
    public void getPicture(String tableCode, String fileName, HttpServletResponse response) {
        String dbCode = tableCode.split("_")[0];
        String deScript = dbCode + "_DATABASE_DESCRIPT";
        String des = utilDao.getDes(deScript);
        String dbBasePath = des + "\\" + dbCode + "\\Pages";
        //文件存放的完整路径
        String overPath = FileUtil.calculateTpiAbsolutePath(fileName, dbBasePath);
        FileuploadUtils.download(overPath, response);
    }

    @Override
    public EmergencyEntity getEmergencyEntityIndex(String sysId) {
        return emergencyDao.getEmergencyEntityBySysId(sysId);
    }
}
