package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.*;
import web.dao.BigdataDao;
import web.dao.PeriodicalIndexDao;
import web.service.BigDataService;
import web.service.PeriodicalIndexService;
import web.utils.CommonConvertUtils;
import web.utils.FileUtil;

import java.util.List;

@Service
public class BigDataServiceImpl implements BigDataService {

    @Autowired
    BigdataDao bigdataDao;

    @Override
    public List<CJFXBASEINFOBo> rankList(CJFXBASEINFOBo cjfxbaseinfoBo) {
        List<CJFXBASEINFOBo> cjfxbaseinfoBos = bigdataDao.rankList(cjfxbaseinfoBo);
        return cjfxbaseinfoBos;
    }

    @Override
    public List<CJFXLASTBo> dataAnalysis(CJFXLASTBo cjfxlastBo) {
        List<CJFXLASTBo> cjfxlastBos = bigdataDao.dataAnalysis(cjfxlastBo);
        return cjfxlastBos;
    }

    @Override
    public List<PropertiesBo> circlePic() {
        String jsonResult = FileUtil.readJsonFile("DJ_CLS.json");
        List<PropertiesBo> propertiesBos = CommonConvertUtils.getJsonToList(jsonResult, PropertiesBo.class);
        for (PropertiesBo propertiesBo : propertiesBos) {
            String  code  = propertiesBo.getValue();
            String[] codes = code.split(",");
            StringBuilder stringBuilder= new StringBuilder();
            for (String s : codes) {
                stringBuilder.append("or 期刊标识码='"+s+"?' ");
            }
            String result= stringBuilder.substring(2,stringBuilder.length()-1);
            propertiesBo.setConditionSql(result);
            Long nubmer  = bigdataDao.getNumber(propertiesBo);
            propertiesBo.setValue(nubmer.toString());
        }
        return propertiesBos;
    }


}
