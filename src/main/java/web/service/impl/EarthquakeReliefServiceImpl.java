package web.service.impl;


import javafx.beans.binding.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import web.bo.*;
import web.common.CutPageBean;
import web.common.Result;
import web.dao.EarthquakeReliefDao;
import web.dao.KbaseDao;
import web.entity.*;
import web.mapper.UserMysqlMapper;
import web.service.EarthquakeReliefService;
import web.service.UserService;
import web.utils.PictureUtils;
import web.utils.RandomUtils;
import web.utils.ReUtils;
import web.utils.VideoUtils;

import javax.annotation.Resource;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/4/19 10:31
 * @Version 1.0
 */
@Service
public class EarthquakeReliefServiceImpl implements EarthquakeReliefService {

    @Value("${filePath.CoverPic}")
    private String CoverPic;

    @Value("${filePath.VideoPath}")
    private String VideoPath;

    @Autowired
    private EarthquakeReliefDao earthquakeReliefDao;

    @Override
    public List<StandardEntityBo> ReviewEmergency(StandardEntityBo standardEntityBo) {
        List<StandardEntityBo> list= earthquakeReliefDao.ReviewEmergency(standardEntityBo);
        return list;
    }

    @Override
    public List<PictureEntityBo> leaderIdea(PictureEntityBo pictureEntityBo) {
        List<PictureEntityBo> list= earthquakeReliefDao.leaderIdea(pictureEntityBo);
        return  list;
    }

    @Override
    public List<VIdeoResourBo> getVideo(VIdeoResourBo vIdeoResourBo) {
        List<VIdeoResourBo> list= earthquakeReliefDao.getVideo(vIdeoResourBo);
        for (VIdeoResourBo entityBo : list) {
            // 在生成截图。 并且把图片存在某个地方
                //这里进行截图反写
                if (entityBo.getPicAddress() == "" || entityBo.getPicAddress() == null) {
                    try{
                        // 1. 先获取视频的地址
                        String path = VideoPath+entityBo.getFileName();
                        String s = VideoUtils.fetchFrame(path);
                         String suffix = "png";//图片文件的后缀名。可以是jpg、gif
                        String filePath =  entityBo.getTitle()+"."+suffix;
                        String newFileName = CoverPic+filePath;//在Eclipse里面，生成的文件也在工程文件夹下
                        PictureUtils.base64StringToImage(s, newFileName, suffix);
//                        // 然后这里进行更新我们的哪条数据 进行反写
                        entityBo.setPicAddress(filePath);
                        Boolean b = earthquakeReliefDao.updateVIdeoResourBo(entityBo);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
        }
        return  list;
    }

    @Override
    public DetailEntity getDocumentDetail(DetailEntity entity) {
        return earthquakeReliefDao.getDocumentDetail(entity);
    }

    @Override
    public Boolean changeEnd(DetailEntity entity) {
        VIdeoResourBo vIdeoResourBo = new VIdeoResourBo();
        List<VIdeoResourBo> list= earthquakeReliefDao.getVideo(vIdeoResourBo);
        for (VIdeoResourBo ideoResourBo : list) {
            // 如果endwith MP4
            if (ideoResourBo.getFileName().endsWith(".mp4")){
                continue;
            }
          //  String newName = ideoResourBo.getFileName()
            //ideoResourBo.setFileName(newName);
            System.out.println(ideoResourBo.getFileName());
            System.out.println(ideoResourBo.getTitle());
            System.out.println(ideoResourBo.getSysId());
        }
        return true;
    }
}
