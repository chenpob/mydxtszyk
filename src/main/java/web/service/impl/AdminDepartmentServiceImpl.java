package web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import web.bo.admin.AdminDepartmentBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.AdminDepartmentPo;
import web.entity.Security_UserDetail;
import web.entity.UserDo;
import web.mapper.AdminDepartmentMapper;
import web.mapper.UserMysqlMapper;
import web.service.AdminDepartmentService;
import web.utils.RandomUtils;

import java.util.HashMap;
import java.util.List;

/**
 * @author cb
 * @Description TODO
 * @date 2023/12/9
 */
@Service
public class AdminDepartmentServiceImpl implements AdminDepartmentService {

    @Autowired
    private AdminDepartmentMapper deptMapper;
    @Override
    public CutPageBean<HashMap<String, Object>> cutPage(AdminDepartmentBo adminDepartmentBo) {
        List<HashMap<String, Object>> res = deptMapper.list(adminDepartmentBo);
        Integer totalCount = deptMapper.totalCount(adminDepartmentBo);
        CutPageBean<HashMap<String, Object>> cutPage = new CutPageBean<>();
        cutPage.initCutPage(totalCount, adminDepartmentBo.getPageSize(), res);
        return cutPage;
    }

    @Override
    public Result AddOne(AdminDepartmentPo adminDepartmentPo) {

        Integer accountExist = deptMapper.isDeptExist(adminDepartmentPo.getDepartmentName());
        if (accountExist != 0) return Result.fail("机构已存在");
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        Security_UserDetail user = (Security_UserDetail) authentication.getPrincipal();
        UserDo admin = user.getUserDo();
        adminDepartmentPo.setDel_flag(0);
        adminDepartmentPo.setCreate_by(admin.getAccount());
        adminDepartmentPo.setCreate_time(RandomUtils.nowStr(""));
        Integer res=0;
        if(adminDepartmentPo.getId()!=null){
            res = deptMapper.editDept(adminDepartmentPo);
       }
        else {
            res = deptMapper.addOne(adminDepartmentPo);
        }
        if (res>0)return Result.succ("新建成功!");
        return Result.fail("新建失败!");

    }

    @Override
    public Result EditOne(AdminDepartmentPo adminDepartmentPo) {
        Integer accountExist = deptMapper.isDeptExist(adminDepartmentPo.getDepartmentName());
        if (accountExist != 0) return Result.fail("机构已存在");
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        Security_UserDetail user = (Security_UserDetail) authentication.getPrincipal();
        UserDo admin = user.getUserDo();
        adminDepartmentPo.setUpdate_by(admin.getAccount());
        adminDepartmentPo.setUpdate_time(RandomUtils.nowStr(""));
        Integer res = deptMapper.editDept(adminDepartmentPo);
        if (res>0)return Result.succ("修改成功!");
        return Result.fail("修改失败!");
    }

    @Override
    public Result DeleteOne(AdminDepartmentPo adminDepartmentPo) {
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        Security_UserDetail user = (Security_UserDetail) authentication.getPrincipal();
        UserDo admin = user.getUserDo();
        adminDepartmentPo.setDelete_by(admin.getAccount());
        adminDepartmentPo.setDel_time(RandomUtils.nowStr(""));
        Integer res = deptMapper.deleteDept(adminDepartmentPo);
        if (res>0)return Result.succ("删除成功!");
        return Result.fail("删除失败!");
    }


}
