package web.service.impl;

import org.springframework.stereotype.Service;
import web.entity.admin.log.SearchDo;
import web.mapper.KeyWordLogMapper;
import web.service.KeyWordLogService;


import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/8/30 11:23
 * @Version 1.0
 */
@Service
public class KeyWordLogServiceImpl implements KeyWordLogService {
    @Resource
    private KeyWordLogMapper logMapper;
    @Override
    public List<SearchDo> cutPageKeyWord(Map map) {
        return logMapper.cutPageKeyWord(map);
    }
}
