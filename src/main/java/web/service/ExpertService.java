package web.service;


import web.bo.ExpertEntityBO;
import web.common.CutPageBean;
import web.entity.CodeTreeEntity;
import web.entity.DocumentEntity;
import web.entity.ExpertEntity;


import java.util.List;

public interface ExpertService {

    /**
     * 应急库整合检索
     * @param
     * @return
     */
    public CutPageBean<ExpertEntity> GetExpertData(ExpertEntityBO expertEntityBO);

    public CutPageBean<ExpertEntity> GetExpertData1(ExpertEntityBO expertEntityBO);

    public ExpertEntity GetExpert(String id);

    public List<CodeTreeEntity> selectRegion();

    public CutPageBean<DocumentEntity> selectDocument(String author,int page,int pageSize);

    List<ExpertEntity> ExpertResourceFive();
}
