package web.service;

import web.bo.*;

import java.util.List;

public interface BigDataService {


    List<CJFXBASEINFOBo> rankList(CJFXBASEINFOBo cjfxbaseinfoBo);

    List<CJFXLASTBo> dataAnalysis(CJFXLASTBo cjfxlastBo);

    List<PropertiesBo> circlePic();
}
