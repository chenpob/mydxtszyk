package web.service;

import web.common.Result;
import web.entity.UserDo;

public interface Security_LoginService {
    Result login(UserDo userDo);

    Result Phonelogin(String phone);
}
