package web.service;

import org.springframework.web.multipart.MultipartFile;
import web.bo.MyUploadRecordBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.admin.user.MyuploadEntity;

import javax.servlet.http.HttpServletResponse;

/**
 * MyUploadService
 */
public interface MyUploadService {

    /**
     *  条件搜索，个人资料列表
     * @param page 页码
     * @param pageSize 页容量
     * @param keyWord 搜索词
     * @param type 搜索条件
     * @return CutPageBean<MyuploadEntity>
     */
    CutPageBean<MyuploadEntity> getList(Integer page, Integer pageSize, String keyWord, String type);

    /**
     * 上传 源文件
     * @param file 源文件
     * @return 返回源文件的信息保存到mysql表
     */
    Result uploadFile(MultipartFile file);

    /**
     * 下载 源文件
     * @param id 源文件id
     */
    void downloadFile(Integer id, HttpServletResponse response);

    /**
     *  根据id，删除个人资料
     * @param id 个人资料id
     * @return Result
     */
    Result delete(Integer id);

    /**
     * 保存上传的文件记录信息
     * @param recordBo 上传的文件记录信息
     * @return Result
     */
    Result save(MyUploadRecordBo recordBo);

    /**
     * 修改上传的文件记录信息
     * @param recordBo 上传的文件记录信息
     * @return Result
     */
    Result edit(MyUploadRecordBo recordBo);

    /**
     * 删除上传的文件
     * @param pathFile 删除的文件路径
     * @return Result
     */
    Result deleteFile(String pathFile);

    /**
     * 根据id，获取信息
     * @param id 记录id
     * @return Result
     */
    Result detail(Integer id);

    /***
     *  判断 源文件是否存在
     * @param id 记录id
     * @return Result
     */
    Result existFile(Integer id);
}
