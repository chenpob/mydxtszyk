package web.service;

import web.bo.*;
import web.common.CutPageBean;
import web.entity.UserloginlogEntity;

import java.util.List;

public interface UserLoginLogService {


    List<UserloginlogEntityBo> LoginLogList(UserloginlogEntityBo userloginlogEntityBo);

    CutPageBean<UserloginlogEntity> getUserOtherLog(UserloginlogEntityBo userloginlogEntityBo);

}
