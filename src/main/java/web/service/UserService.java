package web.service;


import web.common.CutPageBean;
import web.common.Result;
import web.entity.*;
import web.entity.admin.log.LoginDo;
import web.entity.admin.user.DownloadsEntity;
import web.entity.admin.user.ResetPwdDo;

import java.util.HashMap;
import java.util.List;

;


/**
 * @Auther: xiongping22369
 * @Date: 2018/8/17 08:55
 * @Description:
 */

public interface UserService {

    User2 kbase(Long id);

    List<CodeTree> getTree();

    /***************************************************************/
    CutPageBean<HashMap<String, Object>> list(UserVo vo);

    CutPageBean<HashMap<String, Object>> list_org_like(UserVo vo);

//    Result addOne(UserDo userDo);

    Result adminAdd(UserDo userDo);

    Result adminEdit(UserDo userDo);

    Result selfAdd(UserDo userDo);

    Result selfEdit(UserDo userDo);

    Result deleteUser(UserDo userDo);

//    Boolean isAccountExist(String account);

//    Result resetPwd(HashMap<String, String> map);
    Result resetPwd(ResetPwdDo resetPwdDo);

    Result basicInfo();

    Result changePwd(UserDo userDo);

    CutPageBean<DownloadsEntity>  downloads(Integer page,Integer pageSize,String resource_title,String create_time);

    CutPageBean<DownloadsEntity>  preview(Integer page,Integer pageSize,String resource_title,String create_time);

    Boolean isPhoneExist(String phone);

    List<HashMap<String,Object>> getMenuListById(String id);
}
