package web.service;

import web.bo.KnowledgeStatisticDeptBo;
import web.bo.KnowledgeStatisticYearBo;
import web.bo.KnowledgeStatisticsBo;
import web.entity.BrowseDownLoadStaticsEntity;

import java.util.List;
import java.util.Map;

public interface KnowledgeStatisticsService {
    /**
     * 资源可视化分析-本地知识数据统计
     * @return
     */
    Map<String,Long> bdKnowledgeCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo);
    /**
     * 资源可视化分析-公共知识数据统计
     * @return
     */
    Map<String,Long> commonKnowledgeCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo);
    /**
     * 资源可视化分析-知识操作TOP统计
     * @return
     */
    List<BrowseDownLoadStaticsEntity> knowledgeBrowseDownloadTop(KnowledgeStatisticsBo knowledgeStatisticsBo);
    /**
     * 资源可视化分析-协同创新各库数据量统计
     * @return
     */
    Map<String,Long> dataBaseKnowledgeCount(KnowledgeStatisticsBo knowledgeStatisticsBo);

    /**
     资源可视化分析-知识类型分布统计-部门上传分布统计
     */
    Map<String,List<KnowledgeStatisticYearBo>> knowledgeTypeYearList(KnowledgeStatisticsBo knowledgeStatisticsBo);
    /**
     资源可视化分析-顶部数据总量
     */
    Map<String,Long> knowledgeDataUploadDeptCount();
    /**
     资源可视化分析-知识操作情况分析
     */
    Map<String,List<KnowledgeStatisticYearBo>> knowledgeOperationAnalysisByDate(KnowledgeStatisticsBo knowledgeStatisticsBo);
}
