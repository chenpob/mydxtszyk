package web.service;

import web.entity.admin.log.SearchDo;

import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/8/30 11:20
 * @Version 1.0
 */
public interface KeyWordLogService {
    /**
     * 关键词分页
     * @param map
     * @return
     */
    public List<SearchDo> cutPageKeyWord(Map map);
}
