package web.service;

import org.springframework.web.multipart.MultipartFile;
import web.bo.IntegrationEntityBO;
import web.common.CutPageBean;
import web.entity.IntegrationEntity;

public interface RetrievalService {
    public CutPageBean<IntegrationEntity> retrieval(IntegrationEntityBO integrationEntityBO);

    /**
     * 党校期刊检索专用
     *
     * @param integrationEntityBO
     * @return
     */
    public CutPageBean<IntegrationEntity> retrievalQK(IntegrationEntityBO integrationEntityBO);


    public String update(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO);

    /**
     * 修改党校期刊数据
     *
     * @param file
     * @param filePicture
     * @param integrationEntityBO
     * @return
     */
    public String updateQk(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO);

    /**
     * 修改期刊子库数据
     *
     * @param file
     * @param integrationEntityBO
     * @return
     */
    public String updateQkZk(MultipartFile file, IntegrationEntityBO integrationEntityBO);

    /**
     * 修改两弹一星
     * @param file
     * @param integrationEntityBO
     * @return
     */
    public String updateLdyx(MultipartFile file,MultipartFile filePicture, IntegrationEntityBO integrationEntityBO);

    public String delete(IntegrationEntityBO integrationEntityBO);

    public String deleteQk(IntegrationEntityBO integrationEntityBO);

    /**
     * 删除期刊子库数据,该数据只有pdf文件,没有封面
     *
     * @param integrationEntityBO
     * @return
     */
    public String deleteQkZk(IntegrationEntityBO integrationEntityBO);

    /**
     * 删除两弹一星数据
     * @param integrationEntityBO
     * @return
     */
    public String deleteLdyx(IntegrationEntityBO integrationEntityBO);

    public String insert(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO);

    public String insertQk(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO);

    /**
     * 新建党校期刊子库数据
     *
     * @param file
     * @param integrationEntityBO
     * @return
     */
    public String insertQkZk(MultipartFile file, IntegrationEntityBO integrationEntityBO);

    /**
     * 新建两弹一星数据
     *
     * @param file                文件
     * @param filePicture         封面
     * @param integrationEntityBO 实体记录
     * @return
     */
    public String insertLdyxData(MultipartFile file, MultipartFile filePicture, IntegrationEntityBO integrationEntityBO);


}
