package web.service;


import com.cnki.dataSource.DataSource;
import web.bo.PictureEntityBo;
import web.bo.StandardEntityBo;
import web.bo.VIdeoResourBo;
import web.bo.VideoEntityBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.*;

import java.util.HashMap;
import java.util.List;

;


/**
 * @Auther: xiongping22369
 * @Date: 2018/8/17 08:55
 * @Description:
 */

public interface EarthquakeReliefService {

    List<StandardEntityBo> ReviewEmergency(StandardEntityBo standardEntityBo);

    List<PictureEntityBo> leaderIdea(PictureEntityBo pictureEntityBo);

    List<VIdeoResourBo> getVideo(VIdeoResourBo videoEntityBo);

    DetailEntity getDocumentDetail(DetailEntity entity);

    Boolean changeEnd(DetailEntity entity);
}
