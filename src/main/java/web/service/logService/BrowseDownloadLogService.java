package web.service.logService;

import web.bo.BrowseDownLoadLogBo;
import web.entity.admin.BrowseDownLoadLogEntity;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/12 11:22
 * @Version 1.0
 */
public interface BrowseDownloadLogService {
    /**
     * 查询浏览下载日志
     * @return
     */
    List<BrowseDownLoadLogEntity> searchBrowseDownloadLog(BrowseDownLoadLogBo browseLogBo);
}
