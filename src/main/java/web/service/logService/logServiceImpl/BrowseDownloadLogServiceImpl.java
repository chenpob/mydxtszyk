package web.service.logService.logServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.bo.BrowseDownLoadLogBo;
import web.entity.admin.BrowseDownLoadLogEntity;
import web.mapper.BrowseDownloadLogMapper;
import web.service.logService.BrowseDownloadLogService;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/12 11:45
 * @Version 1.0
 */
@Service
public class BrowseDownloadLogServiceImpl  implements BrowseDownloadLogService {
    @Autowired
    private BrowseDownloadLogMapper browseDownloadLogMapper;
    @Override
    public List<BrowseDownLoadLogEntity> searchBrowseDownloadLog(BrowseDownLoadLogBo browseLogBo) {
        return browseDownloadLogMapper.searchBrowseDownloadList(browseLogBo);
    }
}
