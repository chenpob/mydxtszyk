package web.service;

import web.common.CutPageBean;
import web.common.Result;
import web.entity.AuthorityTree;
import web.entity.RoleVo;
import web.entity.UserVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface SystemManageService {
    boolean changeLoginVerificationCodeStatus(String status);

    CutPageBean<HashMap<String, Object>> grouplist(RoleVo vo);

    /*修改组禁用状态*/
    Result changeStatus(String id, String status);

   /*获取弹出层的成员列表*/
    CutPageBean<HashMap<String, Object>> userList(UserVo vo, String role, String type);

    /*获取弹出层的成员列表 另一种样式*/
    List<CutPageBean<HashMap<String, Object>>> userListNew(UserVo vo, String role, String type);

    /*获取弹出层的成员列表 第三种样式*/
    List<HashMap<String,String>> userListTwo(UserVo vo, String role, String type);
    ArrayList<String> userListTwoChoose(String role);

    /*弹出层移除已选成员*/
    Result moveMember(String user_id, String role_id);

    /*弹出层添加成员*/
    Result addMember(String user_id, String role_id, String type);

    /*新建或编辑权限信息*/
    Result addOrEditAuthority (Object dataObj);

    /*获取权限树*/
    List<AuthorityTree> getMenuTree(String role_id);

    /*删除权限组*/
    Result deleteAuthority(String role_id);

    Result saveCheckedDatasTwo(Object checkedDatas,String role);

}
