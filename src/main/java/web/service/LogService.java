package web.service;


import web.common.Result;
import web.entity.admin.log.ViewDownloadDo;

import javax.servlet.http.HttpServletRequest;


public interface LogService {

    Result viewLog(ViewDownloadDo viewDownloadDo, HttpServletRequest request);

    Result downLoadLog(ViewDownloadDo viewDownloadDo, HttpServletRequest request);

    Result anonymousView(ViewDownloadDo viewDownloadDo, HttpServletRequest request);

    Integer getHyfl_codeByKnowledge(String resource_code,String resource_id);
}
