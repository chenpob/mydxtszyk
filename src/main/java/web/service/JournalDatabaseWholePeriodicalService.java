package web.service;

import web.bo.CJFXBASEINFOBo;
import web.bo.ZkzsEntityBo;
import web.common.CutPageBean;
import web.entity.CJFXBASEINFO;

/**
 * Description:期刊库-整刊展示
 *
 * @author BaiGe
 * @date: 2022/7/4 10:19
 * @Version 1.0
 */
public interface JournalDatabaseWholePeriodicalService {
    /**
     * 整刊检索
     * @return
     */
    CutPageBean<CJFXBASEINFO> searchWholePeriodical(CJFXBASEINFOBo cjfxbaseinfoBo);

    CutPageBean<ZkzsEntityBo> ZkJssearch(ZkzsEntityBo zkzsEntityBo);
}
