package web.service;

import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.ExpertEntityBO;
import web.bo.PopularArticlesBo;

import java.util.List;

public interface PeriodicalIndexService {

    List<PopularArticlesBo> popularArticles(PopularArticlesBo popularArticlesBo);

    List<CJFXBASEINFOBo> wholeAtiicles(CJFXBASEINFOBo cjfxbaseinfoBo);

    List<CJFXLASTBo> partyResource(CJFXLASTBo cjfxbaseinfoBo);

    List<ExpertEntityBO> getExpert(ExpertEntityBO expertBO);

    List<ExpertEntityBO> getExpertOrg();
}
