package web.service;

import org.springframework.web.multipart.MultipartFile;
import web.bo.ScreenUploadRecordBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.admin.user.ScreenUploadEntity;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface ScreenUploadService {
    /**
     *  条件搜索，个人资料列表
     * @param page 页码
     * @param pageSize 页容量
     * @param keyWord 搜索词
     * @return CutPageBean<ScreenUploadEntity>
     */
    CutPageBean<ScreenUploadEntity> getList(Integer page, Integer pageSize, String keyWord);

    /**
     * 显示屏前端用
     * @return
     */
    List<ScreenUploadEntity> getListALL();

    /**
     *  删除
     * @param id 个人资料id
     * @return Result
     */
    Result delete(Integer id);

    /**
     * 保存
     * @param recordBo 上传的文件记录信息
     * @return Result
     */
    Result save(MultipartFile filePicture,ScreenUploadRecordBo recordBo);

    /**
     * 修改
     * @param recordBo 上传的文件记录信息
     * @return Result
     */
    Result edit(MultipartFile filePicture,ScreenUploadRecordBo recordBo);


    /**
     * 是否存在相同文件，不存在则保存
     * @param filePicture
     * @return
     */
    Boolean loadFile(MultipartFile filePicture);
}

