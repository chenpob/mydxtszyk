package web.service;


import web.bo.ViewLogBo;
import web.common.CutPageBean;

public interface ViewLogService {


    CutPageBean<ViewLogBo> getViewLog(ViewLogBo viewLogBo);

    CutPageBean<ViewLogBo> getOtherViewLog(ViewLogBo viewLogBo);

    CutPageBean<ViewLogBo> getOtherDownLog(ViewLogBo viewLogBo);

    CutPageBean<ViewLogBo> getDownLog(ViewLogBo viewLogBo);
}
