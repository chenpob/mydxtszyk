package web.service;

import web.bo.AddFrequencyBo;
import web.bo.IntegrationEntityBO;
import web.bo.TotalSearchEntityBo;
import web.bo.VIdeoResourBo;
import web.common.CutPageBean;
import web.entity.IntegrationEntity;
import web.entity.StandardEntity;

import java.util.List;

public interface IndexService {
    public List<StandardEntity> GetIndexData(String code, String top);

    public List<StandardEntity> GetPicture(String code, String top);

    public List<VIdeoResourBo> GetVideo(VIdeoResourBo videoEntityBo);

    public CutPageBean<IntegrationEntity> Integration(IntegrationEntityBO integrationEntityBO);



    public void AddFrequency(AddFrequencyBo addFrequencyBo);

    public void BrowseFrequency(AddFrequencyBo addFrequencyBo);

    CutPageBean<TotalSearchEntityBo> totalSearch(TotalSearchEntityBo totalSearchEntityBo);
}
