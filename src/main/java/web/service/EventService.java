package web.service;

import web.bo.*;

import java.util.List;

public interface EventService {


    List<String> getEventDate(EventDetailBo eventDetailBo);

    List<EventDetailBo> getEventByYear(EventDetailBo eventDetailBo);

    List<EventDetailBo> getEventById(EventDetailBo eventDetailBo);

    List<EveBo> getEventList(EveBo eveBo);

    EveBo getBigEventById(EveBo eveBo);
}
