package web.service;

import web.bo.CJFXLASTBo;
import web.common.CutPageBean;
import web.entity.CJFXLAST;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/28 16:23
 * @Version 1.0
 */
public interface JournalDatabaseFederatedSearchService {
    /**
     * 期刊库-整合检索
     * @param cjfxlastBo
     * @return
     */
    CutPageBean<CJFXLAST> complexSearchJournal(CJFXLASTBo cjfxlastBo);
}
