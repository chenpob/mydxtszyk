package web.service.innovate;

import web.bo.Innovate.InnovateDbBo;
import web.common.CutPageBean;
import web.entity.Innovate.InnovateDbEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Description:国内动态页面Service
 *
 * @author BaiGe
 * @date: 2022/7/12 18:47
 * @Version 1.0
 */
public interface InnovateService {

    /**
     * 展示最新几条的数据
     *
     * @param innovateDbBo
     * @return
     */
    List<InnovateDbEntity> commonSearchTopInnovate(InnovateDbBo innovateDbBo);

    /**
     * 普通检索
     *
     * @param innovateDbBo
     * @return
     */
    CutPageBean<InnovateDbEntity> commonSearchInnovate(InnovateDbBo innovateDbBo);

    /**
     * 整合检索
     *
     * @param innovateDbBo
     * @return
     */
    CutPageBean<InnovateDbEntity> advSearchInnovate(InnovateDbBo innovateDbBo);

    CutPageBean<InnovateDbEntity> commonadvSearch(InnovateDbBo innovateDbBo);

    /**
     * 通用文件接口
     *
     * @param innovateDbBo
     * @param response
     */
    Object commonFileInterface(InnovateDbBo innovateDbBo, HttpServletResponse response) throws IOException, InterruptedException;


    /**
     * 理论研究分类专用接口
     *
     * @param innovateDbBo
     * @return
     */
    List<InnovateDbEntity> onlyForTheoryResearch(InnovateDbBo innovateDbBo);

    /**
     * 详情页
     *
     * @param fileName
     * @return
     */
    InnovateDbEntity indexInnovate(String fileName, String tableName, String sysId);

    /**
     * 获取协同创新图片
     *
     * @param sortCode
     * @return
     */
    List<InnovateDbEntity> getCaptureCover(String sortCode, Integer topNum);
}
