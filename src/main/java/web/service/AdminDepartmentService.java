package web.service;

import web.bo.admin.AdminDepartmentBo;
import web.common.CutPageBean;
import web.common.Result;
import web.entity.AdminDepartmentPo;
import web.entity.UserDo;


import java.util.HashMap;
import java.util.List;

/**
 * @author cb
 * @Description TODO
 * @date 2023/12/9
 */
public interface AdminDepartmentService {
    CutPageBean<HashMap<String, Object>> cutPage(AdminDepartmentBo adminDepartmentBo);

    /**
     * 查询所有部门
     * @return
     */
    Result AddOne(AdminDepartmentPo adminDepartmentPo);

    Result EditOne(AdminDepartmentPo adminDepartmentPo);


    Result DeleteOne(AdminDepartmentPo adminDepartmentPo);




}
