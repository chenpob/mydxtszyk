package web.service;

import web.bo.EmergencyEntityBo;
import web.bo.TypicalCharacterBo;
import web.common.CutPageBean;
import web.entity.EmergencyEntity;
import web.entity.HotWord;
import web.entity.IndexPictureEntity;
import web.entity.Innovate.InnovateDbEntity;
import web.entity.TypicalCharacterEntity;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Description: 应急库Service
 *
 * @author BaiGe
 * @date: 2022/6/20 16:33
 * @Version 1.0
 */
public interface EmergencyDatabaseFederatedSearchService {

    /**
     * 应急库整合检索
     * @param emergencyEntity 应急库实体bo
     * @return
     */
    CutPageBean<EmergencyEntity> complexSearchEmergency(EmergencyEntityBo emergencyEntity);

    /**
     * 获取图片-用文件下载的形式
     * @param tableCode
     * @param fileName
     * @param response
     */
    void getPicture(String tableCode, String fileName, HttpServletResponse response);

    /**
     * 获取应急库详情
     * @param sysId 主键id
     * @return
     */
    EmergencyEntity getEmergencyEntityIndex(String sysId);

    /**
     * 获得典型人物
     * @param entity 典型人物Bo
     * @return 分页集合
     */
    public CutPageBean<TypicalCharacterEntity> getTypicalCharacter(TypicalCharacterBo entity);

    /**
     * 获得两弹一星人物
     * @param entity 典型人物Bo
     * @return 分页集合
     */
    public CutPageBean<TypicalCharacterEntity> getLDYXList(TypicalCharacterBo entity);



    /**
     * 获得典型人物- 显示首页几条
     * @param entity
     * @return
     */
    public List<TypicalCharacterEntity> getTypicalCharacterTopNum(TypicalCharacterBo entity);

    /**
     * 获取热词
     * @param limitNum
     * @return
     */
    List<HotWord> getHotWord(Integer limitNum);

    /**
     * 获取应急管理 抓取图片
     * @param sortCode
     * @return
     */
    List<IndexPictureEntity> getCaptureCover(String sortCode, Integer topNum);

}
