package web.service;

import web.common.Result;

import java.util.HashMap;

public interface CommonService {
    HashMap<String,Object> getInitJson(String account);

    Result captchaRequire();
}
