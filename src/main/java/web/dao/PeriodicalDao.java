package web.dao;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import web.bo.PeriodicalBo;
import web.entity.PeriodicalEntity;
import web.entity.PeriodicalStageEntity;
import web.entity.PeriodicalYearEntity;

import java.util.List;

public interface PeriodicalDao  {
    public List<PeriodicalEntity> GetPeriodicalData(PeriodicalBo periodicalBo);

    public List<String> GetCount(PeriodicalBo periodicalBo);

    public List<PeriodicalEntity> GetPeriodical(PeriodicalBo periodicalBo);

    public List<PeriodicalStageEntity> PeriodicalStage(PeriodicalBo periodicalBo);

    public List<PeriodicalYearEntity> PeriodicalYear(PeriodicalBo periodicalBo);

    public Long Get512Count(PeriodicalBo periodicalBo);

    public List<PeriodicalEntity> Get512WWK(PeriodicalBo periodicalBo);
}
