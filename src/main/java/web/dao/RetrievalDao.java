package web.dao;

import web.bo.IntegrationEntityBO;
import web.bo.MYDXYJKEntityBo;
import web.common.CutPageBean;
import web.entity.IntegrationEntity;

import java.util.List;

public interface RetrievalDao {
    public List<IntegrationEntity> retrieval(IntegrationEntityBO integrationEntityBO);

    public Long getCount(IntegrationEntityBO integrationEntityBO);

    /**
     * 党校期刊检索专用-list
     * @param integrationEntityBO
     * @return
     */
    public List<IntegrationEntity> retrievalQkList(IntegrationEntityBO integrationEntityBO);

    /**
     * 党校期刊检索专用-Count
     * @param integrationEntityBO
     * @return
     */
    public long retrievalQkCount(IntegrationEntityBO integrationEntityBO);

    public Boolean update(IntegrationEntityBO integrationEntityBO);

    public Boolean updateQk(IntegrationEntityBO integrationEntityBO);

    public Boolean updateQkZk(IntegrationEntityBO integrationEntityBO);

    public Boolean updateLdyx(IntegrationEntityBO integrationEntityBO);

    public Boolean delete(IntegrationEntityBO integrationEntityBO);

    public Boolean deleteQk(IntegrationEntityBO integrationEntityBO);

    public Boolean insert(IntegrationEntityBO integrationEntityBO);

    public Boolean insertQk(IntegrationEntityBO integrationEntityBO);

    /**
     * 新增期刊子库数据
     * @param integrationEntityBO
     * @return
     */
    public Boolean insertQkZk(IntegrationEntityBO integrationEntityBO);

    /**
     * 两弹一星插入数据 --包含两弹一星图片库和两弹一星文献库
      * @param integrationEntityBO
     * @return
     */
    public Boolean insertLdyxbd(IntegrationEntityBO integrationEntityBO);
}
