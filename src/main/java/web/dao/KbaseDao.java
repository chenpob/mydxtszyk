package web.dao;


import web.entity.CodeTree;
import web.entity.User2;

import java.util.List;

public interface KbaseDao {
     User2 kbase(Long id);

     List<CodeTree> getTree();
}
