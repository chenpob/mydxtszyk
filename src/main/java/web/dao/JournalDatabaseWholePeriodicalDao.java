package web.dao;

import web.bo.CJFXBASEINFOBo;
import web.bo.ZkzsEntityBo;
import web.entity.CJFXBASEINFO;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/4 10:25
 * @Version 1.0
 */
public interface JournalDatabaseWholePeriodicalDao {

    /**
     * 整刊检索-list
     * @param cjfxbaseinfoBo
     * @return
     */
    List<CJFXBASEINFO> searchWholePeriodicalList(CJFXBASEINFOBo cjfxbaseinfoBo);

    /**
     * 整刊检索-count
     * @param cjfxbaseinfoBo
     * @return
     */
    long searchWholePeriodicalCount(CJFXBASEINFOBo cjfxbaseinfoBo);

    List<ZkzsEntityBo> ZkJssearchList(ZkzsEntityBo zkzsEntityBo);

    List<String> ZkJssearchListCount(ZkzsEntityBo zkzsEntityBo);
}
