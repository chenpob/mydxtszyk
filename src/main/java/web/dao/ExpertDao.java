package web.dao;


import web.bo.DocumentEntityBo;
import web.bo.ExpertEntityBO;
import web.entity.CodeTreeEntity;
import web.entity.DocumentEntity;
import web.entity.ExpertEntity;



import java.util.List;


public interface ExpertDao {
    public List<ExpertEntity> GetExpertData(ExpertEntityBO expertEntityBO);

    public ExpertEntityBO GetName(ExpertEntityBO expertEntityBO);

    public  ExpertEntity GetExpert(ExpertEntityBO expertEntityBO);

    public Long GetCount(ExpertEntityBO expertEntityBO);

    public long GetCount1(DocumentEntityBo documentEntityBo);

    public List<CodeTreeEntity> selectRegion();

    public List<DocumentEntity> selectDocument(DocumentEntityBo documentEntityBo);


    List<ExpertEntity> ExpertResourceFive();
}
