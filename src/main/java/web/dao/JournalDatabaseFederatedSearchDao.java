package web.dao;

import web.bo.CJFXLASTBo;
import web.entity.CJFXLAST;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/28 16:26
 * @Version 1.0
 */
public interface JournalDatabaseFederatedSearchDao {

    /**
     * 期刊库-整合检索List
     * @param cjfxlastBo
     * @return
     */
    List<CJFXLAST> complexSearchJournalList(CJFXLASTBo cjfxlastBo);

    /**
     * 期刊库-整合检索Count
     * @param cjfxlastBo
     * @return
     */
    long complexSearchJournalCount(CJFXLASTBo cjfxlastBo);
}
