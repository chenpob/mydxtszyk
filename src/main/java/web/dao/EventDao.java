package web.dao;

import web.bo.EveBo;
import web.bo.EventDetailBo;

import java.util.List;

public interface EventDao {

    List<String> getEventDate(EventDetailBo eventDetailBo);

    List<EventDetailBo> getEventByYear(EventDetailBo eventDetailBo);

    List<EventDetailBo> getEventById(EventDetailBo eventDetailBo);

    List<EveBo> getEventList(EveBo eveBo);

    EveBo getBigEventById(EveBo eveBo);
}
