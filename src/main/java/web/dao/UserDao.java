package web.dao;


import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import web.entity.User;

import java.util.HashMap;
import java.util.List;

@Mapper
@Component
public interface UserDao {

     @DataSource("cnki_sc")
     List<HashMap<String,Object>> list();
}
