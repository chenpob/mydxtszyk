package web.dao;


import com.cnki.dataSource.DataSource;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import web.bo.PictureEntityBo;
import web.bo.StandardEntityBo;
import web.bo.VIdeoResourBo;
import web.bo.VideoEntityBo;
import web.entity.DetailEntity;

import java.util.HashMap;
import java.util.List;

@Mapper
@Component
public interface EarthquakeReliefDao {

     List<StandardEntityBo> ReviewEmergency(StandardEntityBo standardEntityBo);

     List<PictureEntityBo> leaderIdea(PictureEntityBo pictureEntityBo);

     List<VIdeoResourBo> getVideo(VIdeoResourBo vIdeoResourBo);

     Boolean updateVIdeoResourBo(VIdeoResourBo entityBo);

     DetailEntity getDocumentDetail(DetailEntity entity);

     Boolean updateVIdeoResourBoById(VIdeoResourBo ideoResourBo);
}
