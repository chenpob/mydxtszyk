package web.dao;

import web.bo.EmergencyEntityBo;
import web.bo.Innovate.InnovateDbBo;
import web.bo.TypicalCharacterBo;
import web.entity.EmergencyEntity;
import web.entity.HotWord;
import web.entity.IndexPictureEntity;
import web.entity.Innovate.InnovateDbEntity;
import web.entity.TypicalCharacterEntity;

import java.util.List;

/**
 * Description: 应急库Dao
 *
 * @author BaiGe
 * @date: 2022/6/21 14:57
 * @Version 1.0
 */
public interface EmergencyDatabaseFederatedSearchDao {
    /**
     * 整合检索-list
     * @param emergencyEntity
     * @return
     */
    List<EmergencyEntity> complexSearchEmergencyList(EmergencyEntityBo emergencyEntity);

    /**
     * 整合检索-条数
     * @param emergencyEntity
     * @return
     */
    long complexSearchEmergencyCount(EmergencyEntityBo emergencyEntity);

    /**
     * 根据sysId 获取应急库详情
     * @param sysId 主键id
     * @return
     */
    EmergencyEntity getEmergencyEntityBySysId(String sysId);

    /**
     * 获取典型人物_list
     * @param entity
     * @return
     */
    List<TypicalCharacterEntity> getTypicalCharacterList(TypicalCharacterBo entity);

    /**
     * 获取典型人物_count
     * @param entity
     * @return
     */
    Long getTypicalCharacterCount(TypicalCharacterBo entity);


    /**
     * 获取两弹一星人物_list
     * @param entity
     * @return
     */
    List<TypicalCharacterEntity> getLDYXList(TypicalCharacterBo entity);

    /**
     * 获取两弹一星人物_count
     * @param entity
     * @return
     */
    Long getLDYXCount(TypicalCharacterBo entity);

    /**
     * 获取首页五个典型人物
     * @param entity
     * @return
     */
    List<TypicalCharacterEntity> getTypicalCharacterTopNum(TypicalCharacterBo entity);

    /**
     * 获取热词
     * @param limitNum
     * @return
     */
    List<HotWord> getHotWord(Integer limitNum);

    /**
     * 获取前五条抓取数据
     * @return
     */
    List<IndexPictureEntity> getTopCapture(IndexPictureEntity pictureEntity);
}
