package web.dao.innovate;

import web.bo.Innovate.InnovateDbBo;
import web.entity.Innovate.InnovateDbEntity;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/13 10:09
 * @Version 1.0
 */
public interface InnovateDao {

    /**
     * 最新几条的数据
     * @param innovateDbBo
     * @return
     */
    List<InnovateDbEntity> commonSearchTopInnovate(InnovateDbBo innovateDbBo);

    /**
     * 通用检索-list
     * @param innovateDbBo
     * @return
     */
    List<InnovateDbEntity> commonSearchInnovateList(InnovateDbBo innovateDbBo);

    //通用高级检索
    List<InnovateDbEntity> commonadvSearch(InnovateDbBo innovateDbBo);

    long commonadvSearchCount(InnovateDbBo innovateDbBo);

    /**
     * 通用检索-条数
     * @param innovateDbBo
     * @return
     */
    long commonSearchInnovateCount(InnovateDbBo innovateDbBo);

    /**
     * 高级检索-list
     * @param innovateDbBo
     * @return
     */
    List<InnovateDbEntity> advSearchInnovateList(InnovateDbBo innovateDbBo);

    /**
     * 高级检索-条数
     * @param innovateDbBo
     * @return
     */
     long advSearchInnovateCount(InnovateDbBo innovateDbBo);

    /**
     * 理论研究分类专用接口
     * @param innovateDbBo
     * @return
     */
    List<InnovateDbEntity> onlyForTheoryResearch(InnovateDbBo innovateDbBo);

    /**
     * 详情页
     * @param innovateDbEntity
     * @return
     */
    InnovateDbEntity indexInnovate(InnovateDbEntity innovateDbEntity);

    /**
     * 详情页-抓取库专用
     * @param innovateDbEntity
     * @return
     */
    InnovateDbEntity indexInnovateForGrab(InnovateDbEntity innovateDbEntity);

    /**
     * 获取前五条抓取数据
     * @return
     */
    List<InnovateDbEntity> getTopCapture(InnovateDbBo innovateDbBo);




}