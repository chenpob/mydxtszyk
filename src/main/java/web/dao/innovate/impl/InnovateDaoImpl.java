package web.dao.innovate.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.Innovate.InnovateDbBo;
import web.dao.innovate.InnovateDao;
import web.entity.Innovate.InnovateDbEntity;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/13 10:17
 * @Version 1.0
 */
@Repository
public class InnovateDaoImpl implements InnovateDao {

    @Override
    public List<InnovateDbEntity> commonSearchTopInnovate(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.commonSearchTopInnovate";
        return sqlSession.selectList(statementId, innovateDbBo, false);
    }

    @Override
    public List<InnovateDbEntity> commonSearchInnovateList(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.commonSearchInnovateList";
        return sqlSession.selectList(statementId, innovateDbBo, false);
    }



    @Override
    public List<InnovateDbEntity> commonadvSearch(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.commonadvSearch";
        return sqlSession.selectList(statementId, innovateDbBo, false);
    }

    @Override
    public long commonadvSearchCount(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.commonadvSearchCount";
        return sqlSession.getCount(statementId, innovateDbBo);
    }


    @Override
    public long commonSearchInnovateCount(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.commonSearchInnovateCount";
        return sqlSession.getCount(statementId, innovateDbBo);
    }

    @Override
    public List<InnovateDbEntity> advSearchInnovateList(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.advSearchInnovateList";
        return sqlSession.selectList(statementId, innovateDbBo,false);
    }

    @Override
    public long advSearchInnovateCount(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.advSearchInnovateCount";
        return sqlSession.getCount(statementId, innovateDbBo);
    }



    @Override
    public List<InnovateDbEntity> onlyForTheoryResearch(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.onlyForTheoryResearch";
        return sqlSession.selectList(statementId, innovateDbBo,false);
    }

    @Override
    public InnovateDbEntity indexInnovate(InnovateDbEntity innovateDbEntity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.indexInnovate";
        return sqlSession.selectOne(statementId, innovateDbEntity,false);
    }

    @Override
    public InnovateDbEntity indexInnovateForGrab(InnovateDbEntity innovateDbEntity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.indexInnovateForGrab";
        return sqlSession.selectOne(statementId, innovateDbEntity,false);
    }

    @Override
    public List<InnovateDbEntity> getTopCapture(InnovateDbBo innovateDbBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.innovate.InnovateDao.getTopCapture";
        return sqlSession.selectList(statementId, innovateDbBo,false);
    }
}
