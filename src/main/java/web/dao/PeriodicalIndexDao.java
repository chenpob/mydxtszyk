package web.dao;

import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.ExpertEntityBO;
import web.bo.PopularArticlesBo;

import java.util.List;

public interface PeriodicalIndexDao {
    List<PopularArticlesBo> popularArticles(PopularArticlesBo popularArticlesBo);

    List<CJFXBASEINFOBo> wholeAtiicles(CJFXBASEINFOBo cjfxbaseinfoBo);

    List<CJFXLASTBo> partyResource(CJFXLASTBo cjfxlastBo);

    List<ExpertEntityBO> getExpert(ExpertEntityBO expertBO);

    List<ExpertEntityBO> getExpertOrg();
}
