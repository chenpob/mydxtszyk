package web.dao;

import web.common.Result;
import web.entity.Periodical.fullPeriodicalDisplay.Cover_YearEntity;
import web.entity.Periodical.fullPeriodicalDisplay.IndexEntity;
import web.entity.Periodical.fullPeriodicalDisplay.argsObj.SelectObj;

import java.util.HashMap;
import java.util.List;

public interface FullPeriodicalDisplayDao {
    Cover_YearEntity defaultsearch(String pykm);

    List<String> defaultsearchYears(String pykm);

    List<String> selectyear(SelectObj selectObj);

    HashMap<String,Object> selectqi(SelectObj selectObj);

    List<IndexEntity> selectbdqk(SelectObj selectObj);
}
