package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.*;
import web.dao.EarthquakeReliefDao;
import web.dao.ExpertDao;
import web.entity.CodeTreeEntity;
import web.entity.DetailEntity;
import web.entity.DocumentEntity;
import web.entity.ExpertEntity;

import java.util.List;
@Repository(value = "EarthquakeReliefIDaompl")
public class EarthquakeReliefIDaompl implements EarthquakeReliefDao {

    @Override
    public List<StandardEntityBo> ReviewEmergency(StandardEntityBo standardEntityBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EarthquakeReliefDao.ReviewEmergency";
        return sqlSession.selectList(statementId, standardEntityBo, false);
    }

    @Override
    public List<PictureEntityBo> leaderIdea(PictureEntityBo pictureEntityBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EarthquakeReliefDao.leaderIdea";
        return sqlSession.selectList(statementId, pictureEntityBo, false);
    }

    @Override
    public List<VIdeoResourBo> getVideo(VIdeoResourBo vIdeoResourBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EarthquakeReliefDao.getVideo";
        return sqlSession.selectList(statementId, vIdeoResourBo, false);
    }

    @Override
    public Boolean updateVIdeoResourBo(VIdeoResourBo entityBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EarthquakeReliefDao.updateVIdeoResourBo";
        return sqlSession.executeUpdate(statementId, entityBo);
    }

    @Override
    public DetailEntity getDocumentDetail(DetailEntity entity) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EarthquakeReliefDao.getDocumentDetail";
        return sqlSession.selectOne(statementId, entity,false);
    }

    @Override
    public Boolean updateVIdeoResourBoById(VIdeoResourBo ideoResourBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EarthquakeReliefDao.updateVIdeoResourBoById";
        return sqlSession.executeUpdate(statementId, ideoResourBo);
    }
}
