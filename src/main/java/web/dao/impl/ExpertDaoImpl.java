package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.DocumentEntityBo;
import web.bo.ExpertEntityBO;
import web.dao.ExpertDao;
import web.entity.CodeTreeEntity;
import web.entity.DocumentEntity;
import web.entity.ExpertEntity;



import java.util.List;

@Repository(value = "kbaseExpert")
public class ExpertDaoImpl implements ExpertDao {
    @Override
    public List<ExpertEntity> GetExpertData(ExpertEntityBO expertEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.GetExpertData";
        return sqlSession.selectList(statementId, expertEntityBO, false);
    }

    @Override
    public ExpertEntityBO GetName(ExpertEntityBO expertEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.GetExpertData1";
        return sqlSession.selectOne(statementId, expertEntityBO, false);
    }

    @Override
    public Long GetCount(ExpertEntityBO expertEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.GetCount";
        return sqlSession.getCount(statementId, expertEntityBO);
    }

    @Override
    public long GetCount1(DocumentEntityBo documentEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.GetCount1";
        return sqlSession.getCount(statementId, documentEntityBo);
    }


    @Override
    public ExpertEntity GetExpert(ExpertEntityBO expertEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.GetExpert";
        return sqlSession.selectOne(statementId, expertEntityBO, false);
    }

    @Override
    public List<CodeTreeEntity> selectRegion() {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.SelectRegion";
        return sqlSession.selectList(statementId, null, false);
    }

    @Override
    public List<DocumentEntity> selectDocument(DocumentEntityBo documentEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.SelectDocument";
        return sqlSession.selectList(statementId, documentEntityBo, false);
    }

    @Override
    public List<ExpertEntity> ExpertResourceFive() {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ExpertDao.ExpertResourceFive";
        return sqlSession.selectList(statementId, null, false);
    }


}
