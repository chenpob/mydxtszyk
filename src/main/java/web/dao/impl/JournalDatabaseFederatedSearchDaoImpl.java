package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.CJFXLASTBo;
import web.dao.JournalDatabaseFederatedSearchDao;
import web.entity.CJFXLAST;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/6/28 16:28
 * @Version 1.0
 */
@Repository
public class JournalDatabaseFederatedSearchDaoImpl implements JournalDatabaseFederatedSearchDao {
    @Override
    public List<CJFXLAST> complexSearchJournalList(CJFXLASTBo cjfxlastBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.JournalDatabaseFederatedSearchDao.complexSearchJournalList";
        return sqlSession.selectList(statementId, cjfxlastBo, false);
    }

    @Override
    public long complexSearchJournalCount(CJFXLASTBo cjfxlastBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.JournalDatabaseFederatedSearchDao.complexSearchJournalCount";
        return sqlSession.getCount(statementId, cjfxlastBo);
    }
}

