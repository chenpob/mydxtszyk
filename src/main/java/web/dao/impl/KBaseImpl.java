package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.dao.KbaseDao;
import web.entity.CodeTree;
import web.entity.User2;

import java.util.List;


@Repository(value = "kbase123")
public class KBaseImpl implements KbaseDao {
    @Override
    public User2 kbase(Long id) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseDao.aaa";
        return sqlSession.selectOne(statementId, id, false);
    }

    @Override
    public List<CodeTree> getTree() {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseDao.getTree";
        return sqlSession.selectList(statementId, null, false);
    }
}
