package web.dao.impl;
import cnki.tpi.kbatis.sqlsession.SqlSession;
import org.springframework.stereotype.Repository;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import web.bo.CJFXLASTBo;
import web.bo.KnowledgeStatisticYearBo;
import web.bo.KnowledgeStatisticsBo;
import web.dao.KnowledgeStatisticsDao;

import java.util.List;
import java.util.Map;

@Repository
public class KnowledgeStatisticsDaoImpl implements KnowledgeStatisticsDao {
    @Override
    public long knowledgeCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KnowledgeStatisticsDao.knowledgeCountByHyflCode";
        return sqlSession.getCount(statementId, knowledgeStatisticsBo);
    }

    @Override
    public List<KnowledgeStatisticYearBo> knowledgeYearList(KnowledgeStatisticsBo knowledgeStatisticsBo){
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KnowledgeStatisticsDao.knowledgeYearList";
        return sqlSession.selectList(statementId,knowledgeStatisticsBo,false);
    }

    @Override
    public List<String> knowledgeCountByHyflCodeAndSql(KnowledgeStatisticsBo knowledgeStatisticsBo){
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KnowledgeStatisticsDao.knowledgeCountByHyflCodeAndSql";
        return sqlSession.selectList(statementId,knowledgeStatisticsBo,false);
    }
}
