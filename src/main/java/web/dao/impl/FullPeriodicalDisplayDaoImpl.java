package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.common.Result;
import web.dao.FullPeriodicalDisplayDao;
import web.entity.Periodical.fullPeriodicalDisplay.CoverEntity;
import web.entity.Periodical.fullPeriodicalDisplay.Cover_YearEntity;
import web.entity.Periodical.fullPeriodicalDisplay.IndexEntity;
import web.entity.Periodical.fullPeriodicalDisplay.argsObj.SelectObj;

import java.util.HashMap;
import java.util.List;

@Repository
public class FullPeriodicalDisplayDaoImpl implements FullPeriodicalDisplayDao {
    @Override
    public Cover_YearEntity defaultsearch(String pykm) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String sql_headerInfo = "web.dao.FullPeriodicalDisplayDao.defaultsearch";
        Cover_YearEntity cover_yearEntity = sqlSession.selectOne(sql_headerInfo, pykm, false);

//        if(cover_yearEntity == null){
//            return null;
//        }
//        String sql_years = "web.dao.FullPeriodicalDisplayDao.years";
//        List<String> years = sqlSession.selectList(sql_years,pykm,false);
//
//        cover_yearEntity.setYears(years);
        return cover_yearEntity;
    }

    @Override
    public List<String> defaultsearchYears(String pykm) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String sql_years = "web.dao.FullPeriodicalDisplayDao.years";
        List<String> years = sqlSession.selectList(sql_years, pykm, false);
        return years;
    }

    @Override
    public List<String> selectyear(SelectObj selectObj) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.FullPeriodicalDisplayDao.selectyear";
        List<String> qi = sqlSession.selectList(statementId, selectObj, false);
        return qi;
    }

    @Override
    public HashMap<String, Object> selectqi(SelectObj selectObj) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.FullPeriodicalDisplayDao.selectqi";
        List<IndexEntity> index = sqlSession.selectList(statementId, selectObj, false);

        String sql_qiCount = "web.dao.FullPeriodicalDisplayDao.qiCount";
        Integer count = sqlSession.selectOne(sql_qiCount, selectObj, false);

        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("index", index);
        stringObjectHashMap.put("count", count);
        return stringObjectHashMap;
    }

    @Override
    public  List<IndexEntity> selectbdqk(SelectObj selectObj) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.FullPeriodicalDisplayDao.selectbdqi";
        List<IndexEntity> index = sqlSession.selectList(statementId, selectObj, false);



        return index;
    }
}
