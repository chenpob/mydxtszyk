package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.IntegrationEntityBO;
import web.common.CutPageBean;
import web.dao.RetrievalDao;
import web.entity.IntegrationEntity;

import java.util.List;

@Repository(value = "kbase")
public class RetrievalDaoImpl implements RetrievalDao {

    @Override
    public List<IntegrationEntity> retrieval(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.Retrieval";
        return  sqlSession.selectList(statementId,integrationEntityBO,false);
    }

    @Override
    public Long getCount(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.GetCount";
        return sqlSession.getCount(statementId, integrationEntityBO);
    }

    @Override
    public List<IntegrationEntity> retrievalQkList(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.RetrievalQkList";
        return  sqlSession.selectList(statementId,integrationEntityBO,false);
    }

    @Override
    public long retrievalQkCount(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.RetrievalQkCount";
        return  sqlSession.getCount(statementId,integrationEntityBO);
    }

    @Override
    public Boolean update(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.update";
        return sqlSession.executeUpdate(statementId, integrationEntityBO);
    }

    @Override
    public Boolean updateQk(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.updateQk";
        return sqlSession.executeUpdate(statementId, integrationEntityBO);
    }

    @Override
    public Boolean updateQkZk(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.updateQkZk";
        return sqlSession.executeUpdate(statementId, integrationEntityBO);
    }

    @Override
    public Boolean updateLdyx(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.updateLdyx";
        return sqlSession.executeUpdate(statementId, integrationEntityBO);
    }

    @Override
    public Boolean delete(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.delete";
        return sqlSession.executeDelete(statementId, integrationEntityBO);
    }

    @Override
    public Boolean deleteQk(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.deleteQk";
        return sqlSession.executeDelete(statementId, integrationEntityBO);
    }

    @Override
    public Boolean insert(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.insert";
        return sqlSession.executeInsert(statementId, integrationEntityBO);
    }

    @Override
    public Boolean insertQk(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.insertQk";
        return sqlSession.executeInsert(statementId, integrationEntityBO);
    }

    @Override
    public Boolean insertQkZk(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.insertQkZk";
        return sqlSession.executeInsert(statementId, integrationEntityBO);
    }

    @Override
    public Boolean insertLdyxbd(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseRetrievalDao.insertLdyxbd";
        return sqlSession.executeInsert(statementId, integrationEntityBO);
    }
}
