package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.PeriodicalBo;
import web.dao.PeriodicalDao;
import web.entity.PeriodicalEntity;
import web.entity.PeriodicalStageEntity;
import web.entity.PeriodicalYearEntity;

import java.util.List;

@Repository(value = "kbasePeriodical")
public class PeriodicalDaoImpl implements PeriodicalDao {
    @Override
    public List<PeriodicalEntity> GetPeriodicalData(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.GetPeriodicalData";
        return sqlSession.selectList(statementId,periodicalBo,false);
    }

    @Override
    public List<String> GetCount(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.GetCount";
        return sqlSession.selectList(statementId, periodicalBo,false);
    }

    @Override
    public List<PeriodicalEntity> GetPeriodical(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.Periodical";
        return sqlSession.selectList(statementId,periodicalBo,false);
    }


    @Override
    public Long Get512Count(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.Get512Count";
        return sqlSession.getCount(statementId, periodicalBo);
    }

    @Override
    public List<PeriodicalEntity> Get512WWK(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.Get512WWK";
        return sqlSession.selectList(statementId,periodicalBo,false);
    }

    @Override
    public List<PeriodicalYearEntity> PeriodicalYear(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.PeriodicalYear";
        return sqlSession.selectList(statementId,periodicalBo,false);
    }

    @Override
    public List<PeriodicalStageEntity> PeriodicalStage(PeriodicalBo periodicalBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalDao.PeriodicalStage";
        return sqlSession.selectList(statementId,periodicalBo,false);
    }
}
