package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import web.bo.MYDXYJKEntityBo;
import web.dao.ResouceDao;
import web.dao.UtilDao;
import web.entity.MyDescript;
import web.entity.UtilsEntity;

import java.util.List;

@Repository("ResouceImpl")
public class ResouceImpl implements ResouceDao {


    @Override
    public List<MYDXYJKEntityBo> getResouce(MYDXYJKEntityBo mydxyjkEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ResouceDao.getResouce";
        return sqlSession.selectList(statementId, mydxyjkEntityBo, false);
    }

    @Override
    public Boolean UpdatemydxyjkEntityBo(MYDXYJKEntityBo entityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.ResouceDao.UpdatemydxyjkEntityBo";
        return sqlSession.executeUpdate(statementId, entityBo);
    }
}
