package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.CJFXBASEINFOBo;
import web.bo.ZkzsEntityBo;
import web.dao.JournalDatabaseWholePeriodicalDao;
import web.entity.CJFXBASEINFO;

import java.util.List;

/**
 * Description:
 *
 * @author BaiGe
 * @date: 2022/7/4 11:23
 * @Version 1.0
 */
@Repository
public class JournalDatabaseWholePeriodicalDaoImpl implements JournalDatabaseWholePeriodicalDao {
    @Override
    public List<CJFXBASEINFO> searchWholePeriodicalList(CJFXBASEINFOBo cjfxbaseinfoBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.JournalDatabaseWholePeriodicalDao.searchWholePeriodicalList";
        return sqlSession.selectList(statementId, cjfxbaseinfoBo, false);
    }

    @Override
    public long searchWholePeriodicalCount(CJFXBASEINFOBo cjfxbaseinfoBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.JournalDatabaseWholePeriodicalDao.searchWholePeriodicalCount";
        return sqlSession.getCount(statementId, cjfxbaseinfoBo);
    }

    @Override
    public List<ZkzsEntityBo> ZkJssearchList(ZkzsEntityBo zkzsEntityBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.JournalDatabaseWholePeriodicalDao.ZkJssearchList";
        return sqlSession.selectList(statementId, zkzsEntityBo, false);
    }

    @Override
    public List<String> ZkJssearchListCount(ZkzsEntityBo zkzsEntityBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.JournalDatabaseWholePeriodicalDao.ZkJssearchListCount";
        return sqlSession.selectList(statementId, zkzsEntityBo,false);
    }


}
