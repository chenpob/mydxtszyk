package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.*;
import web.dao.EventDao;
import web.dao.IndexDao;
import web.entity.IntegrationEntity;
import web.entity.StandardEntity;

import java.util.List;

@Repository("kbaseEvent")
public class EventDaoImpl implements EventDao {


    @Override
    public List<String> getEventDate(EventDetailBo eventDetailBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EventDao.getEventDate";
        return sqlSession.selectList(statementId,eventDetailBo,false);
    }

    @Override
    public List<EventDetailBo> getEventByYear(EventDetailBo eventDetailBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EventDao.getEventByYear";
        return sqlSession.selectList(statementId,eventDetailBo,false);
    }

    @Override
    public List<EventDetailBo> getEventById(EventDetailBo eventDetailBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EventDao.getEventById";
        return sqlSession.selectList(statementId,eventDetailBo,false);
    }

    @Override
    public List<EveBo> getEventList(EveBo eveBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EventDao.getEventList";
        return sqlSession.selectList(statementId,eveBo,false);
    }

    @Override
    public EveBo getBigEventById(EveBo eveBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EventDao.getBigEventById";
        return sqlSession.selectOne(statementId,eveBo,false);
    }
}
