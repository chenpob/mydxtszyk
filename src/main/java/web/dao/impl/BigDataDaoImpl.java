package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.*;
import web.dao.BigdataDao;
import web.dao.PeriodicalIndexDao;

import java.util.List;

@Repository
public class BigDataDaoImpl implements BigdataDao {

    @Override
    public List<CJFXBASEINFOBo> rankList(CJFXBASEINFOBo cjfxbaseinfoBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.BigdataDao.rankList";
        return sqlSession.selectList(statementId, cjfxbaseinfoBo, false);
    }

    @Override
    public List<CJFXLASTBo> dataAnalysis(CJFXLASTBo cjfxlastBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.BigdataDao.dataAnalysis";
        return sqlSession.selectList(statementId, cjfxlastBo, false);
    }

    @Override
    public Long getNumber(PropertiesBo propertiesBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.BigdataDao.getNumber";
        return sqlSession.getCount(statementId, propertiesBo);
    }
}
