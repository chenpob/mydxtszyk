package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.*;
import web.common.CutPageBean;
import web.dao.IndexDao;
import web.entity.*;

import java.util.List;

@Repository(value = "kbaseIndex")
public class IndexDaoImpl implements IndexDao {

    @Override
    public List<StandardEntity> GetIndexData(StandardEntityBo standardEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.GetIndexData";
      return   sqlSession.selectList(statementId,standardEntityBo,false);
    }

    @Override
    public List<StandardEntity> GetPicture(StandardEntityBo standardEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.GetPicture";
        return   sqlSession.selectList(statementId,standardEntityBo,false);
    }


    @Override
    public List<IntegrationEntity> Integration(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.Integration";
        return   sqlSession.selectList(statementId,integrationEntityBO,false);
    }



    @Override
    public Long GetCount(IntegrationEntityBO integrationEntityBO) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.GetCount";
        return sqlSession.getCount(statementId, integrationEntityBO);
    }

    @Override
    public void AddFrequency(AddFrequencyBo addFrequencyBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.AddFrequency";
        sqlSession.executeUpdate(statementId, addFrequencyBo);
    }

    @Override
    public void AddFrequency1(AddFrequencyBo addFrequencyBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.AddFrequency1";
        sqlSession.executeUpdate(statementId, addFrequencyBo);
    }

    @Override
    public void BrowseFrequency(AddFrequencyBo addFrequencyBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.BrowseFrequency";
        sqlSession.executeUpdate(statementId, addFrequencyBo);
    }

    @Override
    public List<TotalSearchEntityBo> totalSearch(TotalSearchEntityBo totalSearchEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.totalSearch";
        return  sqlSession.selectList(statementId,totalSearchEntityBo,false);
    }

    @Override
    public Long GetTotalSearchCount(TotalSearchEntityBo totalSearchEntityBo) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.KbaseIndexDao.GetTotalSearchCount";
        return sqlSession.getCount(statementId, totalSearchEntityBo);
    }
}
