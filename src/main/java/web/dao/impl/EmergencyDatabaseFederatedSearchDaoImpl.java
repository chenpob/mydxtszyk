package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.EmergencyEntityBo;
import web.bo.TypicalCharacterBo;
import web.dao.EmergencyDatabaseFederatedSearchDao;
import web.entity.EmergencyEntity;
import web.entity.HotWord;
import web.entity.IndexPictureEntity;
import web.entity.TypicalCharacterEntity;

import java.util.List;

/**
 * Description: 应急库Dao实现
 *
 * @author BaiGe
 * @date: 2022/6/21 15:00
 * @Version 1.0
 */
@Repository
public class EmergencyDatabaseFederatedSearchDaoImpl  implements EmergencyDatabaseFederatedSearchDao {

    @Override
    public List<EmergencyEntity> complexSearchEmergencyList(EmergencyEntityBo emergencyEntity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.complexSearchEmergencyList";
        return sqlSession.selectList(statementId, emergencyEntity, false);
    }

    @Override
    public long complexSearchEmergencyCount(EmergencyEntityBo emergencyEntity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.complexSearchEmergencyCount";
        return sqlSession.getCount(statementId, emergencyEntity);
    }

    @Override
    public EmergencyEntity getEmergencyEntityBySysId(String sysId) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getEmergencyEntityBySysId";
        return sqlSession.selectOne(statementId, sysId, false);
    }
    @Override
    public List<TypicalCharacterEntity> getTypicalCharacterList(TypicalCharacterBo entity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getTypicalCharacterList";
        return sqlSession.selectList(statementId, entity, false);
    }

    @Override
    public Long getTypicalCharacterCount(TypicalCharacterBo entity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getTypicalCharacterCount";
        return sqlSession.getCount(statementId, entity);
    }

    @Override
    public List<TypicalCharacterEntity> getTypicalCharacterTopNum(TypicalCharacterBo entity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getTypicalCharacterTopNum";
        List<TypicalCharacterEntity> list = sqlSession.selectList(statementId, entity, false);
        return list;
    }

    @Override
    public List<TypicalCharacterEntity>  getLDYXList(TypicalCharacterBo entity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getLDYXList";
        return sqlSession.selectList(statementId, entity, false);
    }

    @Override
    public Long getLDYXCount(TypicalCharacterBo entity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getLDYXCount";
        return sqlSession.getCount(statementId, entity);
    }

    @Override
    public List<HotWord> getHotWord(Integer limitNum) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getHotWord";
        return sqlSession.selectList(statementId, limitNum, false);
    }

    @Override
    public List<IndexPictureEntity> getTopCapture(IndexPictureEntity pictureEntity) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.EmergencyDatabaseDao.getTopCapture";
        return sqlSession.selectList(statementId, pictureEntity, false);
    }
}
