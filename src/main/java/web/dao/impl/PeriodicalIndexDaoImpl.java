package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Repository;
import web.bo.CJFXBASEINFOBo;
import web.bo.CJFXLASTBo;
import web.bo.ExpertEntityBO;
import web.bo.PopularArticlesBo;
import web.dao.PeriodicalIndexDao;

import java.util.List;

@Repository
public class PeriodicalIndexDaoImpl implements PeriodicalIndexDao {
    @Override
    public List<PopularArticlesBo> popularArticles(PopularArticlesBo popularArticlesBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalIndexDao.popularArticles";
        List<PopularArticlesBo> res = sqlSession.selectList(statementId, popularArticlesBo, false);
        return res;
    }

    @Override
    public List<CJFXBASEINFOBo> wholeAtiicles(CJFXBASEINFOBo cjfxbaseinfoBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalIndexDao.wholeAtiicles";
        List<CJFXBASEINFOBo> res = sqlSession.selectList(statementId, cjfxbaseinfoBo, false);
        return res;
    }

    @Override
    public List<CJFXLASTBo> partyResource(CJFXLASTBo cjfxlastBo) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalIndexDao.partyResource";
        List<CJFXLASTBo> res = sqlSession.selectList(statementId, cjfxlastBo, false);
        return res;
    }

    @Override
    public List<ExpertEntityBO> getExpert(ExpertEntityBO expertBO) {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalIndexDao.getExpert";
        List<ExpertEntityBO> res = sqlSession.selectList(statementId, expertBO, false);
        return res;
    }

    @Override
    public List<ExpertEntityBO> getExpertOrg() {
        SqlSession sqlSession = DataSourceUtil.getSqlSession();
        String statementId = "web.dao.PeriodicalIndexDao.getExpertOrg";
        List<ExpertEntityBO> res = sqlSession.selectList(statementId, null, false);
        return res;
    }
}
