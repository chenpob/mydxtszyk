package web.dao.impl;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import web.dao.UtilDao;

@Component
public class UtilDaoImpl implements UtilDao {

    @Override
    public String getDes(@RequestBody String dbCode) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.UtilDao.getDes";
        return sqlSession.selectOne(statementId, dbCode, false);
    }

    @Override
    public String getFilePah(String resultDes) {
        SqlSession sqlSession =  DataSourceUtil.getSqlSession();
        String statementId = "web.dao.UtilDao.getFilePah";
        return sqlSession.selectOne(statementId, resultDes, false);
    }
}
