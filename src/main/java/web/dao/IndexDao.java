package web.dao;

import web.bo.AddFrequencyBo;
import web.bo.IntegrationEntityBO;
import web.bo.StandardEntityBo;
import web.bo.TotalSearchEntityBo;
import web.entity.IntegrationEntity;
import web.entity.StandardEntity;

import java.util.List;

public interface IndexDao {
    public List<StandardEntity> GetIndexData(StandardEntityBo standardEntityBo);

    public List<StandardEntity> GetPicture(StandardEntityBo standardEntityBo);

    public List<IntegrationEntity> Integration(IntegrationEntityBO integrationEntityBO);

    public Long GetCount(IntegrationEntityBO integrationEntityBO);

    public void AddFrequency(AddFrequencyBo addFrequencyBo);

    public void AddFrequency1(AddFrequencyBo addFrequencyBo);

    public void BrowseFrequency(AddFrequencyBo addFrequencyBo);

    List<TotalSearchEntityBo> totalSearch(TotalSearchEntityBo totalSearchEntityBo);

    Long GetTotalSearchCount(TotalSearchEntityBo totalSearchEntityBo);


}
