package web.dao;


import web.bo.KnowledgeStatisticYearBo;
import web.bo.KnowledgeStatisticsBo;

import java.util.List;
import java.util.Map;

public interface KnowledgeStatisticsDao{
    /**
     * 根据行业分类获取本地文献数量 协同创新、应急管理、党校期刊 Count
     * @param knowledgeStatisticsBo
     * @return
     */
    long knowledgeCountByHyflCode(KnowledgeStatisticsBo knowledgeStatisticsBo);

    List<KnowledgeStatisticYearBo> knowledgeYearList(KnowledgeStatisticsBo knowledgeStatisticsBo);

    List<String> knowledgeCountByHyflCodeAndSql(KnowledgeStatisticsBo knowledgeStatisticsBo);
}
