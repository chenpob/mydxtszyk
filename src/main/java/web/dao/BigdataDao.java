package web.dao;

import web.bo.*;

import java.util.List;

public interface BigdataDao {

    List<CJFXBASEINFOBo> rankList(CJFXBASEINFOBo cjfxbaseinfoBo);

    List<CJFXLASTBo> dataAnalysis(CJFXLASTBo cjfxlastBo);

    Long getNumber(PropertiesBo propertiesBo);
}
