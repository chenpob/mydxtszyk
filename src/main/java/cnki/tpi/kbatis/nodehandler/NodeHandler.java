package cnki.tpi.kbatis.nodehandler;

import cnki.tpi.kbatis.sqlnode.iface.SqlNode;
import org.dom4j.Element;

import java.util.List;

public interface NodeHandler {
    /**
     * 处理子标签
     *
     * @param nodeToHandler
     * @param contents
     */
    void nodeToHandler(Element nodeToHandler, List<SqlNode> contents);
}
