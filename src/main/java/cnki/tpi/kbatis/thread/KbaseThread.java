package cnki.tpi.kbatis.thread;

import cnki.tpi.kbatis.sqlsession.SqlSession;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * ClassName: KbaseThread
 * Description:
 *
 * @author 小黄
 * date: 2020/5/27 14:21
 */
@Component
@Scope("prototype")//多例
@NoArgsConstructor
@AllArgsConstructor
@Data
public class KbaseThread implements Runnable {

    public SqlSession sqlSession = DataSourceUtil.getSqlSession();

    private LongTextDto longTextDto;

    public KbaseThread(LongTextDto longTextDto) {
        this.longTextDto = longTextDto;
    }

    @Override
    public void run() {
        sqlSession.updateLongText(longTextDto.getSql(), longTextDto.getMap());
    }
}
