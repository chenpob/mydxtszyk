package cnki.tpi.kbatis.thread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * ClassName: LongTextDto
 * Description:
 *
 * @author 小黄
 * date: 2020/5/27 14:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LongTextDto {

    private String sql;

    private Map<String, String> map;

}
