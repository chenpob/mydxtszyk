package cnki.tpi.kbatis.io;

import java.io.InputStream;
import java.io.Reader;

/**
 * @ClassName Resources
 * @Description 读取配置文件
 * @Author 小黄
 * @Date 2019/11/10 11:06
 * @Version 1.0
 */
public class Resources {

    public static InputStream getResourcesAsStream(String location) {
        return Resources.class.getClassLoader().getResourceAsStream(location);
    }

    public static Reader getResourcesAsReader(String location) {
        return null;
    }

}
