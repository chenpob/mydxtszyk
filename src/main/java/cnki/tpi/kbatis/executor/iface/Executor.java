package cnki.tpi.kbatis.executor.iface;


import cnki.tpi.kbatis.config.Configuration;
import cnki.tpi.kbatis.config.MappedStatement;
import java.util.List;
public interface Executor {
    /**
     * 查询结果返回List<T>
     *
     * @param mappedStatement
     * @param configuration
     * @param param
     * @param <T>
     * @return
     */
    <T> List<T> queryList(MappedStatement mappedStatement, Configuration configuration, Object param, Boolean isHightLight);

    /**
     * 查询符合条件的记录数
     *
     * @param mappedStatement
     * @param configuration
     * @param param
     * @return
     */
    Long queryRecordNumber(MappedStatement mappedStatement, Configuration configuration, Object param);

    /**
     * 执行statement.execute(sql)方法
     *
     * @param mappedStatementById
     * @param configuration
     * @param param
     * @return
     */
    Boolean execute(MappedStatement mappedStatementById, Configuration configuration, Object param);

}
