package cnki.tpi.kbatis.executor;


import cnki.tpi.kbatis.config.Configuration;
import cnki.tpi.kbatis.config.MappedStatement;
import cnki.tpi.kbatis.executor.iface.Executor;
import cnki.tpi.kbatis.sqlsource.BoundSql;

import java.util.List;

/**
 * @ClassName BaseExecutor
 * @Description
 * @Author 小黄
 * @Date 2019/11/16 10:07
 * @Version 1.0
 */
public abstract class BaseExecutor implements Executor {

    @Override
    public <T> List<T> queryList(MappedStatement mappedStatement, Configuration configuration, Object param, Boolean isHightLight) {
        BoundSql boundSql = mappedStatement.getSqlSource().getBoundSql(param);
        List<Object> results = excuteQueryList(mappedStatement, configuration, param, boundSql, isHightLight);
        return (List<T>) results;
    }

    @Override
    public Long queryRecordNumber(MappedStatement mappedStatement, Configuration configuration, Object param) {
        BoundSql boundSql = mappedStatement.getSqlSource().getBoundSql(param);
        return excuteQueryRecordNumber(mappedStatement, configuration, param, boundSql);
    }

    @Override
    public Boolean execute(MappedStatement mappedStatement, Configuration configuration, Object param) {
        BoundSql boundSql = mappedStatement.getSqlSource().getBoundSql(param);
        return execute(mappedStatement, configuration, param, boundSql);
    }

    /**
     * 查询结果返回List<T>
     *
     * @param mappedStatement
     * @param configuration
     * @param param
     * @param boundSql
     * @return
     */
    public abstract List<Object> excuteQueryList(MappedStatement mappedStatement, Configuration configuration, Object param, BoundSql boundSql, Boolean isHightLight);

    /**
     * 查询记录数
     *
     * @param mappedStatement
     * @param configuration
     * @param param
     * @param boundSql
     * @return
     */
    public abstract Long excuteQueryRecordNumber(MappedStatement mappedStatement, Configuration configuration, Object param, BoundSql boundSql);

    /**
     * 执行statement.execute(sql)方法
     *
     * @param mappedStatement
     * @param configuration
     * @param param
     * @param boundSql
     * @return
     */
    protected abstract Boolean execute(MappedStatement mappedStatement, Configuration configuration, Object param, BoundSql boundSql);

}
