package cnki.tpi.kbatis.executor;


import cnki.tpi.kbatis.config.Configuration;
import cnki.tpi.kbatis.config.MappedStatement;
import cnki.tpi.kbatis.executor.iface.Executor;

import java.util.List;

/**
 * @ClassName CacheExecutor
 * @Description
 * @Author 小黄
 * @Date 2019/11/15 20:18
 * @Version 1.0
 */
public class CacheExecutor implements Executor {
    private Executor delegate;

    public CacheExecutor(Executor delegate) {
        this.delegate = delegate;
    }

    @Override
    public <T> List<T> queryList(MappedStatement mappedStatement, Configuration configuration, Object param, Boolean isHightLight) {
        return delegate.queryList(mappedStatement, configuration, param, isHightLight);
    }

    @Override
    public Long queryRecordNumber(MappedStatement mappedStatement, Configuration configuration, Object param) {
        return delegate.queryRecordNumber(mappedStatement, configuration, param);
    }

    @Override
    public Boolean execute(MappedStatement mappedStatement, Configuration configuration, Object param) {
        return delegate.execute(mappedStatement, configuration, param);
    }

}
