package cnki.tpi.kbatis.sqlsource;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName BoundSql
 * @Description 封装参数
 * @Author 小黄
 * @Date 2019/11/12 22:17
 * @Version 1.0
 */
public class BoundSql {

    /**
     * 可以执行的 sql
     */
    private String sql;

    /**
     * 参数集合
     */
    private List<ParameterMapping> parameterMappings = Arrays.asList();

    public BoundSql(String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void setParameterMappings(List<ParameterMapping> parameterMappings) {
        this.parameterMappings = parameterMappings;
    }
}
