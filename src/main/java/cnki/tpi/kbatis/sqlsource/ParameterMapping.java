package cnki.tpi.kbatis.sqlsource;

/**
 * @ClassName ParameterMapping
 * @Description
 * @Author 小黄
 * @Date 2019/11/12 22:21
 * @Version 1.0
 */
public class ParameterMapping {
    private String name;
    private Class<?> type;

    public ParameterMapping(String content) {
        this.name = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }
}
