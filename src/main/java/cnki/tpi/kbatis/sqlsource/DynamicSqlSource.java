package cnki.tpi.kbatis.sqlsource;


import cnki.tpi.kbatis.sqlnode.DynamicContext;
import cnki.tpi.kbatis.sqlnode.iface.SqlNode;
import cnki.tpi.kbatis.sqlsource.iface.SqlSource;

/**
 * @ClassName DynamicSqlSource
 * @Description
 * @Author 小黄
 * @Date 2019/11/12 22:13
 * @Version 1.0
 */
public class DynamicSqlSource implements SqlSource {

    private SqlNode rootSqlNode;

    public DynamicSqlSource(SqlNode rootSqlNode) {
        this.rootSqlNode = rootSqlNode;
    }

    @Override
    public BoundSql getBoundSql(Object paramObject) {
        DynamicContext context = new DynamicContext(paramObject);
        // 将SqlNode处理成一条SQL语句
        rootSqlNode.apply(context);
        // 该SQL语句，此时还包含#{}，不包含${}
        String sql = context.getSql();
        SqlSourceParser sourceParse = new SqlSourceParser();
        SqlSource sqlSource = sourceParse.parse(sql);
        return sqlSource.getBoundSql(paramObject);
    }
}
