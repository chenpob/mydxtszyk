package cnki.tpi.kbatis.sqlsource;


import cnki.tpi.kbatis.sqlnode.DynamicContext;
import cnki.tpi.kbatis.sqlnode.iface.SqlNode;
import cnki.tpi.kbatis.sqlsource.iface.SqlSource;

/**
 * @ClassName RawSqlSource
 * @Description
 * @Author 小黄
 * @Date 2019/11/12 22:14
 * @Version 1.0
 */
public class RawSqlSource implements SqlSource {
    private SqlSource sqlSource;

    public RawSqlSource(SqlNode rootSqlNode) {
        DynamicContext context = new DynamicContext(null);
        // 将SqlNode处理成一条SQL语句
        rootSqlNode.apply(context);
        // 该SQL语句，此时还包含#{}，不包含${}
        String sql = context.getSql();
        SqlSourceParser sourceParse = new SqlSourceParser();
        sqlSource = sourceParse.parse(sql);
    }

    @Override
    public BoundSql getBoundSql(Object paramObject) {
        return sqlSource.getBoundSql(paramObject);
    }
}
