package cnki.tpi.kbatis.sqlsource;


import cnki.tpi.kbatis.sqlsource.iface.SqlSource;
import cnki.tpi.kbatis.utils.GenericTokenParser;
import cnki.tpi.kbatis.utils.ParameterMappingTokenHandler;

/**
 * @ClassName SqlSourceParser
 * @Description
 * @Author 小黄
 * @Date 2019/11/16 15:15
 * @Version 1.0
 */
public class SqlSourceParser {
    public SqlSource parse(String sql) {
        ParameterMappingTokenHandler tokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser tokenParser = new GenericTokenParser("#{", "}", tokenHandler);
        String parse = tokenParser.parse(sql);
        return new StaticSqlSource(parse, tokenHandler.getParameterMappings());
    }
}
