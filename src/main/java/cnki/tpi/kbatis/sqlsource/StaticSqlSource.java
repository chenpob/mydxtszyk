package cnki.tpi.kbatis.sqlsource;

import cnki.tpi.kbatis.sqlsource.iface.SqlSource;

import java.util.List;

/**
 * @ClassName StaticSqlSource
 * @Description
 * @Author 小黄
 * @Date 2019/11/16 15:17
 * @Version 1.0
 */
public class StaticSqlSource implements SqlSource {
    private String sql;

    private List<ParameterMapping> parameterMappings;

    public StaticSqlSource(String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
    }

    @Override
    public BoundSql getBoundSql(Object paramObject) {
        return new BoundSql(sql, parameterMappings);
    }
}
