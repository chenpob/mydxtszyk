package cnki.tpi.kbatis.sqlsource.iface;


import cnki.tpi.kbatis.sqlsource.BoundSql;

/**
 * @ClassName SqlSource
 * @Description
 * @Author 小黄
 * @Date 2019/11/12 21:10
 * @Version 1.0
 */
public interface SqlSource {

    /**
     * 获取 JDBC 可以执行的 SQL
     *
     * @return
     */
    BoundSql getBoundSql(Object paramObject);
}
