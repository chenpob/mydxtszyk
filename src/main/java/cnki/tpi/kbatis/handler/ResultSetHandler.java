package cnki.tpi.kbatis.handler;

import cn.hutool.core.date.DateUtil;
import cnki.tpi.kbatis.annotation.Column;
import cnki.tpi.kbatis.config.MappedStatement;
import cnki.tpi.kbatis.utils.DataSourceUtil;
import cnki.tpi.kbatis.utils.HighLightUtil;
import cnki.tpi.util.StrUtil;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

/**
 * @ClassName ResultSetHandler
 * @Description
 * @Author 小黄
 * @Date 2019/11/16 15:56
 * @Version 1.0
 */
public class ResultSetHandler {
    private ResultSet resultSet;
    private MappedStatement mappedStatement;
    private List<Object> results;

    public ResultSetHandler(ResultSet resultSet, MappedStatement mappedStatement, List<Object> results) {
        this.resultSet = resultSet;
        this.mappedStatement = mappedStatement;
        this.results = results;
    }

    public void handleResultSet() throws Exception {
        Class<?> resultTypeClass = mappedStatement.getResultTypeClass();
        if (resultTypeClass == java.util.Date.class) {
            while (resultSet.next()) {
                String date = resultSet.getString(1);
                results.add(DateUtil.parseDate(date));
            }
        } else if (resultTypeClass == java.lang.String.class) {
            while (resultSet.next()) {
                String s = resultSet.getString(1);
                results.add(s);
            }
        } else if (resultTypeClass == java.lang.Integer.class) {
            while (resultSet.next()) {
                String i = resultSet.getString(1);
                results.add(StrUtil.checkStringNull(i) ? Integer.valueOf(i) : Integer.valueOf(0));
            }
        } else {
            List<Field> fields = DataSourceUtil.getFields(resultTypeClass);
            // 没遍历一次是一行数据，对应一个映射对象
            while (resultSet.next()) {
                Object result = resultTypeClass.newInstance();
                for (Field field : fields) {
                    //暴力反射
                    field.setAccessible(true);
                    Object value = new Object();
                    //获取字段column上对应的值
                    Column[] columns = field.getAnnotationsByType(Column.class);
                    if (columns != null && columns.length > 0) {
                        for (int i = 0; i < columns.length; i++) {
                            Column column = columns[i];
                            String columnValue = column.value();
                            try {
                            if (StrUtil.checkStringNull(columnValue)) {

                                    String valueStr = resultSet.getString(columnValue);


                                    if (StrUtil.checkStringNull(valueStr)) {
                                        if (field.getType() == String.class) {
                                            value = HighLightUtil.contentHighLight(valueStr, "###", "!!!");
                                        } else if (field.getType() == java.util.Date.class) {
                                            value = DateUtil.parseDate(valueStr);
                                        } else if (field.getType() == Integer.class) {
                                            value = Integer.parseInt(valueStr);
                                        } else if (field.getType() == Timestamp.class) {
                                            value = Timestamp.valueOf(valueStr);
                                        } else if (field.getType() == Double.class) {
                                            value = Double.parseDouble(valueStr);
                                        } else if (field.getType() == BigDecimal.class) {
                                            value = BigDecimal.valueOf(Long.parseLong(valueStr));
                                        }
                                       // break;
                                    } else {
                                        if (i == columns.length - 1) {
                                            if (field.getType() == String.class) {
                                                value = "";
                                            } else if (field.getType() == Integer.class) {
                                                value = Integer.valueOf(0);
                                            } else if (field.getType() == Double.class) {
                                                value = Double.valueOf(0);
                                            } else if (field.getType() == BigDecimal.class) {
                                                value = BigDecimal.ZERO;
                                            } else {
                                                value = null;
                                            }
                                        } else {
                                            continue;
                                        }
                                    }
                                }
                            }
                            catch (Exception e){
                                continue;
                            }
                            field.set(result, value);// 设置值
                        }
                    }
                }
                results.add(result);
            }
        }
    }

}
