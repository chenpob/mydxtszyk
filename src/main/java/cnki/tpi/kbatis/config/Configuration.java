package cnki.tpi.kbatis.config;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Configuration
 * @Description
 * @Author 小黄
 * @Date 2019/11/10 11:05
 * @Version 1.0
 */
@Data
public class Configuration {
    private String driver;
    private String url;
    private String username;
    private String password;
    private Map<String, MappedStatement> map = new HashMap<String, MappedStatement>();

    public void addMappedStatement(String statementId, MappedStatement mappedStatement) {
        this.map.put(statementId, mappedStatement);
    }

    public MappedStatement getMappedStatementById(String statementId) {
        return map.get(statementId);
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
