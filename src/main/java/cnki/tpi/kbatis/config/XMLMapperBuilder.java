package cnki.tpi.kbatis.config;

import org.dom4j.Element;

import java.util.List;

/**
 * @ClassName XMLMapperBuilder
 * @Description 解析映射文件
 * @Author 小黄
 * @Date 2019/11/12 20:25
 * @Version 1.0
 */
public class XMLMapperBuilder {
    private Configuration configuration;

    public XMLMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * 解析 <mapper/> 标签
     *
     * @param rootElement
     */
    public void parse(Element rootElement) {
        // 唯一标识
        String namespace = rootElement.attributeValue("namespace");
        List<Element> selectElements = rootElement.elements("sql");
        if (selectElements.size() > 0) {
            for (Element selectElement : selectElements) {
                XMLStatementBuilder statementBuilder = new XMLStatementBuilder(configuration);
                statementBuilder.parseStatement(selectElement, namespace);
            }
        }
    }
}
