package cnki.tpi.kbatis.sqlnode;

import cnki.tpi.kbatis.sqlnode.iface.SqlNode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @ClassName MixedSqlNode
 * @Description 封装所有的 SqlNode 节点信息，方便处理
 * @Author 小黄
 * @Date 2019/11/14 21:01
 * @Version 1.0
 */
public class MixedSqlNode implements SqlNode {
    @Setter
    @Getter
    private List<SqlNode> sqlNodes;

    public MixedSqlNode(List<SqlNode> sqlNodes) {
        this.sqlNodes = sqlNodes;
    }

    @Override
    public void apply(DynamicContext context) {
        for (SqlNode sqlNode : sqlNodes) {
            sqlNode.apply(context);
        }
    }
}
