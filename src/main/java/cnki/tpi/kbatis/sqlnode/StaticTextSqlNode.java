package cnki.tpi.kbatis.sqlnode;


import cnki.tpi.kbatis.sqlnode.iface.SqlNode;

/**
 * @ClassName staticTextSqlNode
 * @Description
 * @Author 小黄
 * @Date 2019/11/14 21:35
 * @Version 1.0
 */
public class StaticTextSqlNode implements SqlNode {
    private String sqlText;

    public StaticTextSqlNode(String text) {
        this.sqlText = text;
    }

    @Override
    public void apply(DynamicContext context) {
        context.appendSql(sqlText);
    }
}
