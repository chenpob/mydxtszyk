package cnki.tpi.kbatis.sqlnode.iface;


import cnki.tpi.kbatis.sqlnode.DynamicContext;

/**
 * 封装了不同的 sql 脚本信息，提供 sql 脚本处理功能
 */
public interface SqlNode {

    void apply(DynamicContext context);
}
