package cnki.tpi.kbatis.sqlnode;

import cnki.tpi.kbatis.sqlnode.iface.SqlNode;
import cnki.tpi.kbatis.utils.OgnlUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName ifSqlNode
 * @Description
 * @Author 小黄
 * @Date 2019/11/14 21:47
 * @Version 1.0
 */
public class ifSqlNode implements SqlNode {
    @Getter
    @Setter
    private String text;
    @Getter
    @Setter
    private SqlNode sqlNode;

    public ifSqlNode(String text, SqlNode sqlNode) {
        this.text = text;
        this.sqlNode = sqlNode;
    }

    @Override
    public void apply(DynamicContext context) {
        boolean flag = OgnlUtils.evaluateBoolean(text, context.getBindings().get("_parameter"));
        if (flag) {
            sqlNode.apply(context);
        }
    }
}
