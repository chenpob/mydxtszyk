package cnki.tpi.kbatis.sqlnode;


import cnki.tpi.kbatis.sqlnode.iface.SqlNode;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: WhereSqlNode
 * Description:
 *
 * @author 小黄
 * date: 2019/11/26 14:43
 */
public class WhereSqlNode extends TrimSqlNode {

    private static List<String> prefixList = Arrays.asList("AND ", "OR ", "AND\n", "OR\n", "AND\r", "OR\r", "AND\t", "OR\t");

    public WhereSqlNode(SqlNode contents) {
        super(contents, "WHERE", prefixList, null, null);
    }

}