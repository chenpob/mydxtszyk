package cnki.tpi.kbatis.sqlnode;


import cnki.tpi.kbatis.sqlnode.iface.SqlNode;
import cnki.tpi.kbatis.utils.GenericTokenParser;
import cnki.tpi.kbatis.utils.OgnlUtils;
import cnki.tpi.kbatis.utils.SimpleTypeRegistry;
import cnki.tpi.kbatis.utils.TokenHandler;

/**
 * @ClassName TextSqlNode
 * @Description
 * @Author 小黄
 * @Date 2019/11/12 22:04
 * @Version 1.0
 */
public class TextSqlNode implements SqlNode {
    private String testSql;

    public TextSqlNode(String testSql) {
        this.testSql = testSql;
    }


    @Override
    public void apply(DynamicContext context) {
        GenericTokenParser tokenParser = new GenericTokenParser("${", "}", new BindingTokenParser(context));
        String sql = tokenParser.parse(testSql);
        context.appendSql(sql);
    }

    public boolean isDynamic() {
        if (testSql.indexOf("${") > -1) {
            return true;
        }
        return false;
    }

    private static class BindingTokenParser implements TokenHandler {
        private DynamicContext context;

        public BindingTokenParser(DynamicContext context) {
            this.context = context;
        }

        /**
         * expression：比如说${username}，那么expression就是username username也就是Ognl表达式
         */
        @Override
        public String handleToken(String expression) {
            Object paramObject = context.getBindings().get("_parameter");
            if (paramObject == null) {
                // context.getBindings().put("value", null);
                return "";
            } else if (SimpleTypeRegistry.isSimpleType(paramObject.getClass())) {
                // context.getBindings().put("value", paramObject);
                return String.valueOf(paramObject);
            }

            // 使用Ognl api去获取相应的值
            Object value = OgnlUtils.getValue(expression, context.getBindings());
            String srtValue = value == null ? "" : String.valueOf(value);
            return srtValue;
        }

    }
}
