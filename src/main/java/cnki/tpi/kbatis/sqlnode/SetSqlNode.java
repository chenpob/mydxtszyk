package cnki.tpi.kbatis.sqlnode;


import cnki.tpi.kbatis.sqlnode.iface.SqlNode;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: SetSqlNode
 * Description:
 *
 * @author 小黄
 * date: 2019/11/26 10:03
 */
public class SetSqlNode extends TrimSqlNode {

    private static List<String> suffixList = Arrays.asList(",");

    public SetSqlNode(SqlNode contents) {
        super(contents, "SET", null, null, suffixList);
    }
}
