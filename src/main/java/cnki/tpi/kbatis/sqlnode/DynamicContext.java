package cnki.tpi.kbatis.sqlnode;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName DynamicContext
 * @Description 封装 sql 文本信息和入参对象
 * @Author 小黄
 * @Date 2019/11/12 21:57
 * @Version 1.0
 */
public class DynamicContext {
    private StringBuffer sb = new StringBuffer();

    private Map<String, Object> bindings = new HashMap<String, Object>();

    public DynamicContext(Object parameter) {
        this.bindings.put("_parameter", parameter);
    }

    public String getSql() {
        return sb.toString();
    }

    public void appendSql(String sql) {
        sb.append(sql);
        sb.append(" ");
    }

    public Map<String, Object> getBindings() {
        return bindings;
    }

    public void bind(String name, Object value) {
        bindings.put(name, value);
    }
}
