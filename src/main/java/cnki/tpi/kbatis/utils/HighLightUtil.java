package cnki.tpi.kbatis.utils;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 高亮
 */
public class HighLightUtil {

    public static String contentHighLight(String content, String prefix, String posfix) {
        if (content.contains(prefix) && content.contains(posfix)) {
            content = content.replaceAll(prefix, "<span style=\"color:#c00\">");
            content = content.replaceAll(posfix, "</span>");
            return content;
        } else {
            Pattern pattern = Pattern.compile(prefix);
            StringBuffer buf = new StringBuffer();
            Matcher findMatcher = pattern.matcher(content);
            int number = 0;
            while (findMatcher.find()) {
                number++;
                if (number % 2 == 1) {//奇数
                    findMatcher.appendReplacement(buf, "<span style=\"color:#c00\">");
                } else {
                    findMatcher.appendReplacement(buf, "</span>");
                }
            }
            findMatcher.appendTail(buf);
            String s = buf.toString();
            if (s.contains(posfix)) {
                s = s.replaceAll(posfix, "</span>");
            }
            return s;
        }
    }

    public static String cancelHighlight(String content) {
        if (!StringUtils.isEmpty(content)) {
            content = content.replaceAll("<span style=\"color:#c00\">", "");
            content = content.replaceAll("</span>", "");
        }
        return content;
    }

   /* public static void main(String[] args) throws Exception {
        String  s = "<span style=\"color:red\">广州</span>南站</span>广州</span>市轨道交通衔接工程 (<span style=\"color:red\">广州</span>市轨道交通二十二号线  </span>广州</span>南站) 施工图设计 第四篇 车站 <span style=\"color:red\">广州</span>南站 第一册 车站建筑 第一分册 主体建筑施工图";
        String s1 = cancelHighlight(s);
        System.out.println(s1);
       String s = "###广州###南站###广州###市轨道交通衔接工程 (###广州###市轨道交通二十二号线  ###广州###南站) 施工图设计 第四篇 车站 ###广州###南站 第一册 车站建筑 第一分册 主体建筑施工图";
        String s1 = contentHighLight(s, "###", "###");
        System.out.println(s1);

        try {
            // read file content from file
            StringBuffer sb = new StringBuffer("");
            //FileReader reader = new FileReader("E:\\smart_word\\a.txt");
           File file = new File("E:\\smart_word\\5001-6000.txt");
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8"); //或GB2312,GB18030
            BufferedReader br = new BufferedReader(isr);
            //BufferedReader br = new BufferedReader(reader);
            String str = null;
            while ((str = br.readLine()) != null) {
                //str = new String(str.getBytes("GBK"), "UTF-8");
                str = str.replaceAll(";", "\n");
                sb.append(str);
            }
            br.close();
            isr.close();
            //reader.close();


            // write string to file
            FileWriter writer = new FileWriter("E:\\smart_word\\abcefg.txt");
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(sb.toString());
            bw.close();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

}
