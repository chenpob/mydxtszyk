package cnki.tpi.kbatis.sqlsession;


import cnki.tpi.kbatis.config.Configuration;

/**
 * @ClassName DefaultSqlSessionFactory
 * @Description
 * @Author 小黄
 * @Date 2019/11/15 19:55
 * @Version 1.0
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {
    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
