package cnki.tpi.kbatis.sqlsession;
import cnki.tpi.kbatis.config.Configuration;
import cnki.tpi.kbatis.config.XMLConfigBuilder;
import java.io.InputStream;
import java.io.Reader;

/**
 * @ClassName SqlSessionFactoryBuilder
 * @Description
 * @Author 小黄
 * @Date 2019/11/15 19:50
 * @Version 1.0
 */
public class SqlSessionFactoryBuilder {
    private Configuration configuration;

    public SqlSessionFactory build(InputStream inputStream) {
        XMLConfigBuilder configBuilder = new XMLConfigBuilder();
        configuration = configBuilder.parse(inputStream);
        return build();
    }

    public SqlSessionFactory build(Reader reader) {
        return null;
    }

    public SqlSessionFactory build() {
        return new DefaultSqlSessionFactory(configuration);
    }
}
