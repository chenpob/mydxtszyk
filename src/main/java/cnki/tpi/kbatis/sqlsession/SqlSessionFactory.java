package cnki.tpi.kbatis.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();
}
