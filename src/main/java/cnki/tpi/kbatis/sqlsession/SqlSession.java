package cnki.tpi.kbatis.sqlsession;

import java.util.List;
import java.util.Map;

/**
 * @ClassName SqlSession
 * @Description
 * @Author 小黄
 * @Date 2019/11/15 19:57
 * @Version 1.0
 */
public interface SqlSession {
    <T> T selectOne(String statementId, Object param, Boolean isHighLight);

    <T> List<T> selectList(String statementId, Object param, Boolean isHighLight);

    Long getCount(String statementId, Object param);

    Boolean executeInsert(String statementId, Object param);

    /**
     * 插入/更新数据中有超长文本字段(一般对应前端富文本框的那些字段)
     * 此方法通过插入时加锁,然后使用select max(id) form tableName的方式获取到当前插入数据的id,适合id设置为auto的情况
     *
     * @param statementId
     * @param param
     * @param selectCondition 如果为update更新操作, 一定要通过selectCondition传入获取该记录的查询条件 eg: "ID = 1"
     * @return
     */
    Boolean executeWithLongText(String statementId, Object param, String selectCondition);

    Boolean executeDelete(String statementId, Object param);

    Boolean executeUpdate(String statementId, Object param);

    void updateLongText(String sql, Map<String, String> map);

    /**
     * 调用该方法必须加锁
     *
     * @param fieldName
     * @param tableName
     * @return
     */
    Integer getMaxId(String fieldName, String tableName);
}
