package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_pis_cls implements Serializable {
    private static final long serialVersionUID = -727940998325770249L;
    private String username;//
    private String type;//
    private String name;//
    private String grade;//
    private String childflag;//
    private String syscode;//
    private String sortnum;//
    private String sysid;//

}
