package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_auto_anno implements Serializable {
    private static final long serialVersionUID = 529337717602444183L;
    private String db_code;//
    private String template_name;//
    private String vsm_field;//
    private String vsm_domain;//
    private String max_feature_num;//
    private String vsm_plan;//

}
