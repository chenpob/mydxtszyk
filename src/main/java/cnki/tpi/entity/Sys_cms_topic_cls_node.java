package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_topic_cls_node implements Serializable {
    private static final long serialVersionUID = -1461777374273475160L;
    private String sysid;//
    private String clscode;//
    private String nodename;//
    private String nodecode;//
    private String grade;//
    private String childflag;//
    private String sortno;//
    private String nodestatus;//
    private String syscode;//
}
