package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_option implements Serializable {
    private static final long serialVersionUID = -8076716411163699872L;
    private String name;// 名称  
    private String dbname;// 数据库中文名  
    private String dbcode;// 数据库编码  
    private String value;// 验证sql语句  
    private String reservedata;// reservedata  
    private String sortnum;// 排序  
    private String flag;// 标志  
    private String sysid;// 编号  
    private String displayvalue;// 验证sql的显示语句  
    private String displayreservedata;// displayreservedata

}
