package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database_field implements Serializable {
    private static final long serialVersionUID = 6307154303350969614L;
    private String databasecode;//
    private String field;//
    private String alias;//
    private String datatype;//
    private String datalength;//
    private String indextype;//
    private String indexproperty;//
    private String caption;//
    private String defaultvalue;//
    private String optionalvalue;//
    private String texttype;//
    private String nullenable;//
    private String required;//
    private String fieldvalueunique;//
    private String sortnumber;//
    private String element;//
    private String element1;//
    private String element2;//
    private String qualifier;//
    private String scheme;//
    private String lang;//
    private String issystemfield;//
    private String sysid;//
    private String indexfldtype;//
}
