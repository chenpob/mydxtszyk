package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class SearchLog implements Serializable {
    private static final long serialVersionUID = -8426517013999021301L;
    private Integer searchLogId;//查询记录id
    private Integer sysOperateLogId;
    private String userId;//用户id
    private String userName;//用户名称
    private String ip;//用户登录ip
    private String dbId;//访问数据库id
    private String dbCode;//访问数据库code
    private String str_Query;//页面显示查询条件
    private String str_Order;//页面显示排序
    private String searchType;//查询类型（单库，跨库）
    private String classCode;
    private String topicCls;
    private String topicNode;
    private Integer searchLogStatus;
    private Timestamp operateTime;//操作时间
    private Timestamp createTime;//创建时间
    private String str_Search;//访问检索页所需数据结构

}
