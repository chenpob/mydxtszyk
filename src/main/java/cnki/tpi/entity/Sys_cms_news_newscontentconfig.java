package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_news_newscontentconfig implements Serializable {
    private static final long serialVersionUID = -1674191163315809965L;
    private String sysid;// 编号,
    private String dbid;// 数据库id,
    private String articlenum;// 显示条数,
    private String articlelength;// 每条显示文字数量,
    private String showtype;// 首页显示类型,
    private String ispub;// 是否显示,
    private String recommendsql;// 推荐表达式,
    private String imageurl;// 图片路径,
    private String columnname;//
    private String namelength;//
    private String briffield;//
    private String location;//
    private String sortno;//
}
