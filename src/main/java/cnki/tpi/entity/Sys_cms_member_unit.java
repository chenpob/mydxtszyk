package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_member_unit implements Serializable {
    private static final long serialVersionUID = 3980901131445181775L;
    private String doiprefix;//
    private String unitname;//
    private String unitshouxie;//
    private String unitcode;//
    private String username;//
    private String password;//
    private String ip;//
    private String port;//
    private String doiurl;//
    private String contract;//
    private String telephone;//
    private String fax;//
    private String email;//
    private String postcode;//
    private String address;//
    private String localunitflag;//
    private String notes;//
    private String sysid;//

}
