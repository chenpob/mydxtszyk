package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_ip implements Serializable {
    private static final long serialVersionUID = 2767117657325679918L;
    private String urid;//
    private String sip;//
    private String eip;//
    private String flag;//
    private String sysid;//
}
