package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_topic_cls implements Serializable {
    private static final long serialVersionUID = 7417690846445508411L;
    private String sysid;//
    private String clscode;//
    private String clsname;//
    private String sortno;//
    private String clsstatus;//
}
