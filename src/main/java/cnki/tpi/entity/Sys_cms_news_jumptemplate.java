package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_news_jumptemplate implements Serializable {
    private static final long serialVersionUID = 7210794787196526265L;
    private String sysid;// 编号,
    private String dbid;// 数据库id,
    private String templatetype;// 模板类型,
    private String articlelength;// 文字数量,
    private String ispub;// 是否显示,
    private String sortnmu;// 排序值,
    private String columnname;// 栏目显示名称,
    private String location;//
}
