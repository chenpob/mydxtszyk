package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database_cls_db implements Serializable {
    private static final long serialVersionUID = 2014060536393006099L;
    private String clsid;
    private String clscode;
    private String databasecode;
    private String dbid;
}
