package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_publishtype implements Serializable {
    private static final long serialVersionUID = -5703700435814336090L;
    private String name;// 模板名称  
    private String code;// 模板代码  
    private String descript;// 描述  
    private String sysid;// 编号  
}
