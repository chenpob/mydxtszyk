package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_us_fieldmap implements Serializable {
    private static final long serialVersionUID = 8825276029862752527L;
    private String sysid;//
    private String usfieldid;//
    private String dbid;//
    private String fieldid;//
}
