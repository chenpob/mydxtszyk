package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_cls_template implements Serializable {
    private static final long serialVersionUID = 1510962855382556317L;
    private String name;//
    private String code;//
    private String descript;//
    private String sortnumber;//
    private String sysid;//
}
