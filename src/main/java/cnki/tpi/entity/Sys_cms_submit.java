package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_submit implements Serializable {
    private static final long serialVersionUID = 1777643431628346338L;
    private String name;// name  
    private String dbname;// 数据库中文名  
    private String dbcode;// 数据库编码  
    private String fieldname;// 字段名  
    private String fielddisplayname;// 字段显示名  
    private String fieldtype;// 字段类型  
    private String defaultvalue;// 默认值  
    private String isallownull;// 是否允许为空  
    private String isonlyone;// 字段唯一性  
    private String descript;// 字段描述  
    private String type;// 类别  
    private String sortnum;// 排序  
    private String sysid;// 编号  
}
