package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_librarynumconfig implements Serializable {
    private static final long serialVersionUID = -6160624984678516449L;
    private String dbname;// 数据库中文名  
    private String dbcode;// 数据库编码  
    private String librarynumvalue;// 组成馆藏号字段  
    private String flownumvalue;// 生成流水号字段  
    private String split;// 馆藏号分隔符  
    private String sysid;// 编号  
}
