package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Sys_cms_user implements Serializable {
    private static final long serialVersionUID = -3400335817943012128L;
    private String username = "";//
    private String realname = "";//
    private String password = "";//
    private String usertype = "";//
    private String securitytype = "";//
    private String securityvalue = "";//
    private String begindate = "";//
    private String enddate = "";//
    private String stopflag = "";//
    //审核状态（20181116java版新增注册审核功能使用）
    private String checkstate = "";
    //新增身份证号
    private String idnumber = "";
    //新增性别
    private String sex = "";//
    private String unit = "";//
    private String unitsysid = "";//
    private String dept = "";//
    private String deptsysid = "";//
    private String unitdeptcode = "";//
    private String telephone = "";//
    private String email = "";//
    private String postcode = "";//
    private String address = "";//
    private String descript = "";//
    private String sysid;//
    private String requestip;
    private List<Sys_cms_user_group> usergroup;//隶属组
    private List<Sys_cms_user_acl_db_right> userright;//数据库权限
    private List<Sys_cms_user_ip> userip;//查询IP限制0有效1无效
    private List<Sys_cms_user_acl_db_right> groupRight;
    private String verificationCode;//登录时的验证码
    private String isFront;//登录时判断是否后台登录入口

}
