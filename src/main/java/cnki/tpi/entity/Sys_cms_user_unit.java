package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_unit implements Serializable {
    private static final long serialVersionUID = -6919533399126373336L;
    private String unitname;//
    private String unitcode;//
    private String contract;//
    private String telephone;//
    private String fax;//
    private String email;//
    private String postcode;//
    private String address;//
    private String uniturl;//
    private String unittype;//
    private String notes;//
    private String sysid;//
}
