package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_yearconfig implements Serializable {
    private static final long serialVersionUID = -7180288207698526277L;
    private String dbname;// 数据库中文名  
    private String dbcode;// 数据库编码  
    private String yearvalue;// 年限值  
    private String yearfield;// 年限字段  
    private String sysid;// 编号  
}
