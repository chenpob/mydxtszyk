package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resource_template_file_format implements Serializable {
    private static final long serialVersionUID = -6118408515728339876L;
    private String type;//
    private String name;//
    private String mimetype;//
    private String jsmeta;//
    private String descript;//
    private String sysid;//
}
