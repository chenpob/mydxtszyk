package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class DBConfig implements Serializable {
    private static final long serialVersionUID = 2174349986297673989L;
    private String dbid;//数据库id,
    private String cnname;//数据库名称,
    private String dbcode;//数据库编码,
    private String dbdescript;//数据库描述,
    private String classid;//分类号,
    private String searchfields;//检索字段,
    private String dbowner;//数据库拥有者,
    private String dblanguage;//语种,
    private String defaultselected;//是否选中,
    private String sortnum;//排序,
    private String sysid;
}
