package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_parameter implements Serializable {
    private static final long serialVersionUID = 6203401577880624335L;
    private String mdoiPrefix;//
    private String mdoiNaSegent;//
    private String mdoiNaName;//
    private String mdoiNaCode;//
    private String ghrIP;//
    private String ghrPort;//
    private String ghrAccount;//
    private String ghrPassword;//
}
