package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_field implements Serializable {
    private static final long serialVersionUID = 3926746966114933934L;
    private String urid;//
    private String databasecode;//
    private String fieldname;//
    private String flag;//
    private String sysid;//
}
