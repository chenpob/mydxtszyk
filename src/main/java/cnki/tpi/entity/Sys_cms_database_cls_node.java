package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Sys_cms_database_cls_node implements Serializable {
    private static final long serialVersionUID = 8270004794301536042L;
    private String clsid;//
    private String name;//
    private String code;//
    private String grade;//
    private String childflag;//
    private String syscode;//
    private String sortnumber;//
    private String sysid;//
    private List<Sys_cms_database_cls_db> clsDbs;
}
