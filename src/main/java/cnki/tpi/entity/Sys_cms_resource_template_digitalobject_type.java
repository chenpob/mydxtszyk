package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resource_template_digitalobject_type implements Serializable {
    private static final long serialVersionUID = -1165543882815656941L;
    private String objtypecode;//
    private String objtypedescription;//
    private String sysid;//
}
