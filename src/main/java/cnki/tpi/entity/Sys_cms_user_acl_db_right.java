package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Sys_cms_user_acl_db_right implements Serializable {
    private static final long serialVersionUID = -5743209879417731116L;
    private String urid;//
    private String dbid;//
    private String rightvalue;//
    private String rightfieldname;//
    private String rightclsvalue;//
    private String downloadcountofday;//
    private String maxdownloadcount;//
    private String securitytype;//
    private String flag;//
    private String sysid;//
    //用于存放管理权限数值
    private List<String> rightList;
    //用于存放字段权限数值
    private List<String> fieldList;
}
