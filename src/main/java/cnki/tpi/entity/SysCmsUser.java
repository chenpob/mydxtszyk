package cnki.tpi.entity;

import cnki.tpi.kbatis.annotation.Column;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysCmsUser implements Serializable {
    private static final long serialVersionUID = -4054851835793952138L;
    @Column("sysid")
    private int sysid;
    @Column("username")
    private String username;
    @Column("password")
    private String password;
}
