package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_menuconfig implements Serializable {
    private static final long serialVersionUID = 1741417415279401196L;
    private String name;//
    private String url;//
    private String grade;//
    private String childflag;//
    private String syscode;//
    private String sortnum;//
    private String sysid;//

}
