package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_emailconfig implements Serializable {
    private static final long serialVersionUID = -1444826972742064643L;
    private String dbname;//数据库中文名  
    private String dbcode;//数据库编码  
    private String emailfield;//存储邮箱地址的字段  
    private String subjectlist;//主题列表  
    private String contentlist;//内容列表  
    private String smtpaddress;//邮件服务器地址  
    private String emailaddress;//发送人email地址  
    private String emailuser;//邮件用户姓名  
    private String emailpwd;//邮件用户密码  
    private String sysid;//编号  
}
