package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_pis_subject implements Serializable {
    private static final long serialVersionUID = 1022629929560940871L;
    private String username;// 用户名,
    private String syscode;// 所属导航树节点,
    private String subject;// 主题,
    private String ksql;// ksql语句,
    private String collectiontime;// 收藏日期,
    private String dbcode;// 数据库code,
    private String id;//
}
