package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resource_template implements Serializable {
    private static final long serialVersionUID = 5531099255666630242L;
    private String sysid;//
    private String resourcecode;//
    private String resourcename;//
    private String resourcedescript;//
    private String fileformat;//
    private String sortnum;//
}
