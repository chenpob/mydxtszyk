package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database_cls implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;//
    private String code;//
    private String descript;//
    private String sortnumber;//
    private String sysid;//

}
