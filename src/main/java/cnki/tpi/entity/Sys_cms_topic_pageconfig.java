package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_topic_pageconfig implements Serializable {
    private static final long serialVersionUID = 2826277961416977193L;
    private String sysid;//
    private String dbcode;//
    private String topiccode;//
    private String nodecode;//
    private String articlenum;//
    private String articlelength;//
    private String showtype;//
    private String ispub;//
    private String columnname;//
    private String location;//
    private String status;//
    private String dbname;//
    private String sortno;//
    private String clscode;//
}
