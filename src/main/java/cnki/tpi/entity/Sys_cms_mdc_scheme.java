package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_mdc_scheme implements Serializable {
    private static final long serialVersionUID = -8372779988731813655L;
    private String element;//
    private String minoccurs;//
    private String maxoccurs;//
    private String element1;//
    private String minoccurs1;//
    private String maxoccurs1;//
    private String element1default;//
    private String element1values;//
    private String element2;//
    private String minoccurs2;//
    private String maxoccurs2;//
    private String element2default;//
    private String element2values;//
    private String datatype;//
    private String qualifier;//
    private String qrequired;//
    private String qdefault;//
    private String scheme;//
    private String srequired;//
    private String sdefault;//
    private String lang;//
    private String lrequired;//
    private String ldefault;//
    private String syscode;//
    private String grade;//
    private String haschildflag;//
    private String sysid;//

}
