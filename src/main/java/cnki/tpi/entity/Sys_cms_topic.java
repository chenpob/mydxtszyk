package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_topic implements Serializable {
    private static final long serialVersionUID = 765280688655909015L;
    private String sysid;//
    private String topicname;//
    private String topiccode;//
    private String topicstatus;//
    private String topicpage;//
    private String topicpic;//
}
