package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class DBUsfield implements Serializable {
    private static final long serialVersionUID = -7064309155230077437L;
    private String id;//
    private String showfieldname;//   显示字段名称,
    private String realfieldname;//   实际字段名称,
    private String fieldtype;//
    private String datatype;//
    private String fieldid;//
    private String sortno;//
    private String width;//
    private String fieldtypetail;//
    private String datatypetail;//
    private String sorttype;//

}
