package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_security implements Serializable {
    private static final long serialVersionUID = 2163417776105468909L;
    private String name;//
    private String type;//
    private String value;//
    private String descript;//
    private String sysid;//
}
