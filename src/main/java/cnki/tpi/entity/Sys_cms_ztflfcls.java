package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_ztflfcls implements Serializable {
    private static final long serialVersionUID = -5773524673847877722L;
    private String sys_fld_class_name;//
    private String sys_fld_class_code;//
    private String sys_fld_class_grade;//
    private String sys_fld_child_flag;//
    private String sys_fld_sys_code;//
    private String sys_fld_parent_code;//
    private String sys_fld_prevsilibing_code;//
    private String sys_fld_nextsilibing_code;//
    private String sys_fld_child_sortsn;//
    private String sys_fld_sysid;//
    private String sys_fld_record_count;//
}
