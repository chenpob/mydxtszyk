package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_topic_cls_query implements Serializable {
    private static final long serialVersionUID = 385176626897208257L;
    private String sysid;//
    private String nodecode;//
    private String dbcode;//
    private String query;//
    private String sort;//
    private String status;//
    private String dbname;//
    private String clscode;//
}
