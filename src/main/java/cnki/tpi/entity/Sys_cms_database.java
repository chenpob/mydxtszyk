package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database implements Serializable {
    private static final long serialVersionUID = -8012638393596825841L;
    private String databasecode;//数据库代码
    private String databasename;//数据库名称
    private String databasetype;//数据库类型
    private String createdate;//建库日期
    private String owner;//创建人
    private String password;//数据库密码
    private String unit;//创建单位
    private String publisher;//出版者
    private String copyright;//版权所有者
    private String databasedescript;//简介
    private String databasepath;//数据库路径
    private String databaseurl;//url
    private String version;//版本
    private String databasepubflag;//数据库是否发布
    private String recordcheckpubflag;//记录是否检查后发布
    private String visibleflag;//数据库是否可见
    private String sortsn;//序号
    private String unitdbcode;//单位数据库代码
    private String localtpitype;//是否为tpi自建数据库
    private String secrity;//数据库密级
    private String sysid;//系统id
    private String recordcount;//记录个数
    private String datasize;//数据库容量
    private String updateperiod;//更新周期
    private String datasource;//数据来源
    private String lang;//语种
    private String timerange;//时间范围
    private String spacerange;//空间范围
    private String metabz;//元数据标准
    private String servermode;//服务模式
    private String servertime;//服务时间
    private String serverstate;//服务状态
    private String syzn;//使用指南
    private String lxfs;//联系方式
    private String sydx;//适用对象
    private String syxk;//使用许可
    private String qxsm;//权限声明
    private String sffs;//收费方式
    private String price;//价格
    private String deleterecordtype;//删除标志
    private String openurlflag;//是否支持openurl
    private String havertserflag;//数据可收割标志
    private String filepathtype;//文件路径类型
    private String filepath;//用户自定义原文路径
    private String filenametype;//文件名命名类型
    private String filenamefield;//文件名的字段列表
    private String webdatafileext;//虚拟目录文件扩展名
    private String webdatavirtualname;//虚拟目录
    private String fileformat;//数据库的文件格式
    private String templateid;//模版id
    private String styleid;//模版样式id
    private String knetdatabase;//关联数据库名
    private String yearfield;//年字段
    private String datastartyear;//数据开始年
    private String dataendyear;//数据结束年
    private String issmalllanguage;//是否小语种数据库
    private String papertype;//数字对象存储方式
    private String paperfieldvalue;//数字对象构造值
    private String datetimeflag;//是否加时间
    private String showinfirstpageflag;//是否在首页列表中显示
    private String ordertype;//数据默认的排序方式
    private String isunicode;//
    private String searchtype;//检索类型
    private String vip;//虚拟库ip
    private String vport;//虚拟库端口
    private String vusername;//虚拟库用户名
    private String vpassword;//虚拟库用户名
    private String vviewname;//虚拟库名称)
    //数据库操作权限(非表字段，展示数据用)
    private String urid;//
    private String dbid;//
    private String rightvalue;//
    private String rightfieldname;//
    private String rightclsvalue;//
    private String downloadcountofday;//
    private String maxdownloadcount;//
    private String securitytype;//
    private String flag;//

}
