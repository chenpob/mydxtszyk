package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_group implements Serializable {
    private static final long serialVersionUID = -3070944906336499728L;
    private String userid;//
    private String groupid;//
    private String sysid;//
}
