package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database_publish_field implements Serializable {
    private static final long serialVersionUID = -6207143509857641710L;
    private String sysid;//编号  
    private String dbid;//数据库id  
    private String fieldtype;//字段发布类型  
    private String fielddispname;//字段显示名称  
    private String fieldproperty;//字段的显示类型  
    private String fieldname;//原字段名称  
    private String briefwidth;//概览字段宽度,
    private String ishidden;// 

}
