package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class DBClass implements Serializable {
    private static final long serialVersionUID = -910438146123457419L;
    private String classid;  // 分类编号  
    private String classname;// 分类名称  
    private String classcode;// 分类编码  
    private String sortnum;  // 排序号    

}
