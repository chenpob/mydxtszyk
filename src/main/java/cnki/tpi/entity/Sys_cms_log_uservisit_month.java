package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_log_uservisit_month implements Serializable {
    private static final long serialVersionUID = 4123766298278742806L;
    private String sysid;//
    private String logusertype;//
    private String loguserid;//
    private String logusername;//
    private String logrolename;//
    private String logdatabase;//
    private String logdownloadnum;//
    private String logelementarysearchnum;//
    private String logrankingsearchnum;//
    private String logprofessionalsearchnum;//
    private String logmuldbsearchnum;//
    private String lognavigationnum;//
    private String loglogonsuccessnum;//
    private String loglogonfailnum;//
    private String logbrowsenum;//
    private String logotheroperatenum;//
    private String logvisitdate;//
    private String logtotalmoney;//
    private String logtotalticket;//
    private String logdetailbrowsenum;//
    private String loglogoutfailnum;//
    private String loglogoutsuccessnum;//
    private String logdownloadfailnum;//
    private String loguserip;//
    private String logarticleid;//
    private String logdatabasecode;//
}
