package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_log implements Serializable {
    private static final long serialVersionUID = -7739767711891041014L;
    private String sysid;//
    private String logusertype;//
    private String logtype;//
    private String loguserid;//
    private String logusername;//
    private String logdatabase;//
    private String logtime;//
    private String loguserip;//
    private String logcontent;//
    private String logyear;//
    private String logmonth;//
    private String logday;//
    private String logrolename;//
    private String logobjectname;//
    private String logactionid;//
    private String logobjectdescript;//
    private String logfilename;//
    private String logarticleid;//
    private String logdatabasecode;//

}
