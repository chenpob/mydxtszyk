package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resource_template_digitalobject implements Serializable {
    private static final long serialVersionUID = 7860649503041735613L;
    private String resourcetemplateid;//
    private String objtypecode;//
    private String objtypedescription;//
    private String sysid;//
}
