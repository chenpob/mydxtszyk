package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resource_type implements Serializable {
    private static final long serialVersionUID = 2002574476495162937L;
    private String resourcetypeid;//
    private String objtypecode;//
    private String objtypedescription;//
    private String sysid;//
}
