package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_mdc_translation implements Serializable {
    private static final long serialVersionUID = -3166097774153621992L;
    private String english;//
    private String chinese;//
    private String sysid;//
}
