package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_errorinfoconfig implements Serializable {
    private static final long serialVersionUID = 7338267164403388823L;
    private String errorinfolist;//审核不合格原因错误列表
    private String sysid;//编号

}
