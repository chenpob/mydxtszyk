package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_group implements Serializable {
    private static final long serialVersionUID = 7106012781660314962L;
    private String name;//
    private String type;//
    private String descript;//
    private String sysid;//
}
