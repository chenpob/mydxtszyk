package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database_descript implements Serializable {
    private static final long serialVersionUID = -2412176993098294654L;
    private String databasecode;//数据库代码
    private String databasename;//数据库名称
    private String databasetype;//数据库类型
    private String createdate;//建库日期
    private String owner;//创建人
    private String password;//数据库密码
    private String unit;//创建单位
    private String databasedescript;//简介
    private String databasepath;//数据库路径
    private String databaseurl;//url
    private String version;//版本
    private String databasepubflag;//数据库是否发布
    private String recordcheckpubflag;//记录是否检查后发布
    private String visibleflag;//数据库是否可见
    private String sortsn;//序号
    private String unitdbcode;//单位数据库代码
    private String localtpitype;//是否为tpi自建数据库
    private String secrity;//数据库密级
    private String sysid;//系统id
    private String datawxzl;//文献种类
    private String datastartyear;//数据开始年
    private String dataendyear;//数据结束年
    private String recordcount;//记录个数
    private String datasize;//数据库容量
    private String mediatype;//媒体类型
    private String searchfielddict;//检索字段相关词典
    private String searchfield;//检索字段
    private String browsefield;//概览字段
    private String browsefieldlen;//概览字段宽度
    private String detailfield;//细览字段
    private String viewname;//视图
    private String firstalpha;//首字母
    private String zt;//载体
    private String metadata;//元数据项
    private String publicfield;//公共字段
    private String publicfieldtitle;//显示信息字段
    private String subjectclass;//学科分类
    private String clcclass;//中图分类法
    private String resourceclass;//资源分类
    private String uri;//uri
    private String updatedatetime;//最新更新时间
    private String updateperiod;//更新周期
    private String publisher;//出版者
    private String copyright;//版权所有者
    private String fileformat;//文件格式
    private String filetype;//文件保存类型
    private String filepath;//文件保存路径
    private String virtualpathname;//虚拟目录
    private String datasource;//数据来源
    private String lang;//语种
    private String timerange;//时间范围
    private String spacerange;//空间范围
    private String metabz;//元数据标准
    private String servermode;//服务模式
    private String servertime;//服务时间
    private String serverstate;//服务状态
    private String syzn;//使用指南
    private String lxfs;//联系方式
    private String sydx;//适用对象
    private String syxk;//使用许可
    private String qxsm;//权限声明
    private String sffs;//收费方式
    private String price;//价格
}
