package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_news_muldbsearchconfig implements Serializable {
    private static final long serialVersionUID = -5684392180994568426L;
    private String sysid;// 编号
    private String dbinfo;// 数据库信息
    private String fieldinfo;// 字段信息

}
