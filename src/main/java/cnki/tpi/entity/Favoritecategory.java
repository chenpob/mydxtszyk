package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

//收藏分类
@Data
public class Favoritecategory implements Serializable {
    private static final long serialVersionUID = -6361588795878555093L;
    private Integer favoriteCategoryID;//id
    private String userCode;
    private String categoryName;
    private Integer categoryStatus;
    private Integer sortNo;

}
