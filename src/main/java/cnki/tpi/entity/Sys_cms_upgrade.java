package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_upgrade implements Serializable {
    private static final long serialVersionUID = -2425909876491413579L;
    private String type;//
    private String version;//
    private String upgrade_date;//
}
