package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_hztopy implements Serializable {
    private static final long serialVersionUID = 2612616320517817342L;
    private String dbname;// 数据库中文名
    private String dbcode;// 数据库编码
    private String hztopy;// 汉字对应拼音
    private String sysid;// 编号
}
