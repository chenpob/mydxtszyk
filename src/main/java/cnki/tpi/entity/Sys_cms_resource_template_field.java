package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resource_template_field implements Serializable {
    private static final long serialVersionUID = 4634369880826630891L;
    private String resourcetemplateid;//
    private String field;//
    private String alias;//
    private String datatype;//
    private String datalength;//
    private String indexfldtype;//
    private String indextype;//
    private String indexproperty;//
    private String caption;//
    private String defaultvalue;//
    private String optionalvalue;//
    private String texttype;//
    private String nullenable;//
    private String required;//
    private String fieldvalueunique;//
    private String sortnumber;//
    private String element;//
    private String element1;//
    private String element2;//
    private String qualifier;//
    private String scheme;//
    private String lang;//
    private String issystemfield;//
    private String sysid;//
}
