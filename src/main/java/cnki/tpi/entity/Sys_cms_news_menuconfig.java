package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_news_menuconfig implements Serializable {
    private static final long serialVersionUID = -3416622792164822730L;
    private String sysid;//
    private String name;//
    private String url;//
    private String sort;//
    private String type;//
    private String menutype;//
}
