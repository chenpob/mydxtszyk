package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_cls_template_node implements Serializable {
    private static final long serialVersionUID = -4791951887806625472L;
    private String clsid;//
    private String name;//
    private String code;//
    private String grade;//
    private String childflag;//
    private String syscode;//
    private String sortnumber;//
    private String sysid;//

}
