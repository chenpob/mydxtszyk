package cnki.tpi.entity;
//收藏

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class Favoritefolder implements Serializable {
    private static final long serialVersionUID = 3572704081844437189L;
    private Integer favoriteFolderID;//id
    private Integer favoriteCategoryID;//收藏分类id
    private String userCode;//用户code
    private String dbCode;
    private String dbName;
    private Integer recordID;
    private String recordTitle;
    private Timestamp favoriteDate;
    private Integer favoriteStatus;

}
