package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_acl_db_right_field implements Serializable {
    private static final long serialVersionUID = -1056988027253989540L;
    private String urid;//
    private String databasecode;//
    private String fieldname;//
    private String rightvalue;//
    private String flag;//
    private String sysid;//
}
