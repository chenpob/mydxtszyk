package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class MultiSearchGroup implements Serializable {
    private static final long serialVersionUID = 4442209488090638899L;
    private String groupname;
    private int countnumber;

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return groupname.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        MultiSearchGroup o = (MultiSearchGroup) obj;
        return groupname.equals(o.groupname);
    }
}
