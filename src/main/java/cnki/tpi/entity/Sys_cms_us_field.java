package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_us_field implements Serializable {
    private static final long serialVersionUID = 792087561168483607L;
    private String sysid;//
    private String name;//
    private String datatype;//
}
