package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_pis_collection implements Serializable {
    private static final long serialVersionUID = -2463622882726081687L;
    private String username;// 用户名
    private String syscode;// 所属导航树节点
    private String subject;// 主题
    private String collectiontime;// 收藏日期
    private String dbcode;// 数据库编码
    private String id;//
    private String sysid;// 原始记录系统编号
}
