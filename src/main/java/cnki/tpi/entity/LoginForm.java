package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginForm implements Serializable {
    private static final long serialVersionUID = -1341741860967578223L;
    private String username;
    private String password;
}
