package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_publishtype_style implements Serializable {
    private static final long serialVersionUID = 1731799757216246170L;
    private String templateid;// 模板id  
    private String csscode;// 样式风格代码  
    private String cssname;// 样式风格名称  
    private String cssvisual;// 样式存放路径  
    private String descript;// 样式风格描述  
    private String sysid;// 编号

}
