package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_resourcetype_profile implements Serializable {
    private static final long serialVersionUID = -8433413440303895354L;
    private String sysid;//
    private String resourcecode;//
    private String resourcename;//
    private String resourcedescript;//
    private String fileformat;//
    private String briffield;//
    private String columnname;//
    private String namelength;//
}
