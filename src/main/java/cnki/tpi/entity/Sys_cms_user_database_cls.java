package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_database_cls implements Serializable {
    private static final long serialVersionUID = -7352419997993420900L;
    private String urid;//
    private String databasecode;//
    private String databaseclscode;//
    private String sysclasscode;//
    private String sysclsid;//
    private String flag;//
    private String sysid;//
}
