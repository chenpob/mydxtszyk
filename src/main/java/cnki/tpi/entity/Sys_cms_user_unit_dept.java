package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_user_unit_dept implements Serializable {
    private static final long serialVersionUID = -2371491515962205096L;
    private String unitid;//
    private String deptname;//
    private String deptcode;//
    private String haschildflag;//
    private String grade;//
    private String syscode;//
    private String sortnum;//
    private String sysid;//
}
