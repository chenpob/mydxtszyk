package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_publish_theme implements Serializable {
    private static final long serialVersionUID = 848971640263784990L;
    private String sysid;//
    private String themename;//
    private String themeprofile;//
}
