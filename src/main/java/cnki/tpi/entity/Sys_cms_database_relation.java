package cnki.tpi.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sys_cms_database_relation implements Serializable {
    private static final long serialVersionUID = -4404546986907059141L;
    private String sysid;//
    private String dbcode;//
    private String dbfield;//
    private String relationdbcode;//
    private String relationdbfield;//
    private String status;//
    private String sortno;//
    private String title;//
}
