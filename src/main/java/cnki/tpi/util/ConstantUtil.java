package cnki.tpi.util;

/**
 * 静态常量处理
 *
 * @author lpz
 */
public class ConstantUtil {
    public static String COMMA = ",";
    public static String SEPARATED_SYMBOLS = "~";
    public static String ARROW = "---";
    public static String XIEGANG = "/";
    public static String HYPHEN = "-";
    public static String UNDERLINE = "_";
    public static String SEMICOLON = ";";
    public static String COLON = ":";
    public static String PARENTTHESES_LEFT = "(";
    public static String PARENTTHESES_RIGHT = ")";


    //操作成功或失败返回值
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    public static final String REPEAT = "repeat";//重复
    public static final String DATEERROR = "dateError";//日期选择错误

    /*******单库发布 开始***********/
    public static final String DATABASE_PUBLISH_FIELDTYPE_ONE = "1";//1=检索字段
    public static final String DATABASE_PUBLISH_FIELDTYPE_TWO = "2";//2=排序字段
    public static final String DATABASE_PUBLISH_FIELDTYPE_THREE = "3";//3=概览字段
    public static final String DATABASE_PUBLISH_FIELDTYPE_FOURE = "4";//4=细缆字段
    public static final String DATABASE_PUBLISH_FIELDTYPE_FIVE = "5";

    public static final String DATABASE_PUBLISH_YES = "1";//1=已发布
    public static final String DATABASE_PUBLISH_NO = "0";//0=未发布
    /*******单库发布 结束***********/

    /*******数据库分类设置 开始***********/
    public static String DBCLASSID = "数据库分类代码已存在";//dbclassID存在
    public static String DBCLASSNAME = "数据库分类名称已存在";//dbclassNAME存在
    public static String DBCLASSIDANDNAME = "数据库分类代码，名称都已存在";//dbclass都存在
    public static int DATA1 = 1;//data=1代表输入的id存在
    public static int DATA2 = 2;//data=2代表输入的name存在
    public static int DATA3 = 3;//data=3代表输入的id和name存在
    public static int DATA0 = 0;//data=0代表输入的id和name都不存在
    public static String SETFLAG = "1";//单库映射已设置
    public static String UNSETFLAG = "0";//单库映射已未设置
    public static String DBFIELD = "该数据库字段已映射";//dbclassID存在
    public static String PUBLISHFIELD = "该公共字段已映射";//dbclassNAME存在
    /*******数据库分类设置 结束***********/

    //树的根节点id
    public static String TREE_ROOT_ID = "0";
    //树根节点父id
    public static String TREE_ROOT_PID = "-1";
    //根节点默认层级
    public static String TREE_ROOT_GRADE = "0";
    //有子节点标记
    public static String HASCHILDFLAG_YES = "1";
    //无子节点标记
    public static String HASCHILDFLAG_NO = "0";
    //节点为部门标记
    public static String NODE_FLAG_DEPT = "0";
    //节点为人员标记
    public static String NODE_FLAG_USER = "1";

    //用户标记
    public static String USER_FLAG = "0";
    //用户组标记
    public static String GROUP_FLAG = "1";
    //默认组类型
    public static String GROUP_DEFAULT_TYPE = "4";

    //首页栏目配置
    public static String COLUMNMSG = "该数据库已经设置过此模板";
    //栏目类型图片新闻栏目
    public static String COLUMNDATA1 = "1";
    //栏目类型文章新闻栏目
    public static String COLUMNDATA2 = "2";
    //栏目类型首页推荐栏目
    public static String COLUMNDATA3 = "3";
    //栏目类型站点公告栏目
    public static String COLUMNDATA4 = "4";
    //栏目类型多媒体推荐栏目
    public static String COLUMNDATA5 = "5";
    //栏目类型首页跳转图片模板
    public static String COLUMNDATA12 = "12";
    //栏目类型首页跳转文字模板
    public static String COLUMNDATA13 = "13";
    //栏目类型文字描述
    public static String COLUMNDATA1_MSG = "图片新闻栏目";
    public static String COLUMNDATA2_MSG = "文章新闻栏目";
    public static String COLUMNDATA3_MSG = "首页推荐栏目";
    public static String COLUMNDATA4_MSG = "站点公告栏目";
    public static String COLUMNDATA5_MSG = "多媒体推荐栏目";
    public static String COLUMNDATA12_MSG = "首页跳转图片模板";
    public static String COLUMNDATA13_MSG = "首页跳转文字模板";
    //首页栏目位置
    public static String LOCATION_RIGHT = "1";
    public static String LOCATION_LEFT = "0";

    public static final String SESSION_SECURITY_CODE = "sessionSecCode";
    public static final String SESSION_USER = "sessionUser";
    public static final String SYSNAME = "tpiweb";    //系统名称路径

    //所有字段
    public static final String ALL_FIELD = "所有字段";
    //所有导航
    public static final String ALL_NAV = "所有导航";
    //每日下载量(不限制默认为0)
    public static final String DOWNLOAD_COUNT_TODAY = "0";
    //最大下载量(不限制默认为0)
    public static final String MAX_DOWNLOAD_COUNT = "0";
    //安全方式(默认为0)
    public static final String SECURITY_TYPE = "0";
    //系统字段
    public static final String IS_SYSTEM_FIELD = "1";
    //非系统字段
    public static final String NOT_SYSTEM_FIELD = "0";
    //数据库分类导航后缀
    public static final String DB_CLS_SUFFIX = "_CLS";
    //数据库分类导航树节点后缀
    public static final String DB_CLS_NODE_SUFFIX = "_CLS_NODE";
    //数据库数据后缀
    public static final String DB_METADATA_SUFFIX = "_METADATA";
    //数据库数据后缀
    public static final String DB_METADATA_FILE_SUFFIX = "_METADATA_FILE";
    //前台每页默认显示条数
    public static final int DEFAULT_NUM_PAGE = 10;
    public static final int DEFAULT_NUM_PAGE_12 = 12;
    public static final int DEFAULT_NUM_PAGE_15 = 15;
    //文件格式
    //图片
    public static String FILE_IMAGE = "JPG,BMP,EPS,GIF,MIF,MIFF,PNG,TIF,TIFF,SVG,WMF,JPE,JPEG,DIB,ICO,TGA,CUT,PIC";
    //音频
    public static String FILE_SOUND = "MP3,AAC,WAV,WMA,CDA,FLAC,M4A,MID,MKA,MP2,MPA,MPC,APE,OFR,OGG,RA,WV,TTA,AC3,DTS";
    //视频
    public static String FILE_VIDEO = "AVI,ASF,WMV,AVS,FLV,MKV,MOV,3GP,MP4,MPG,MPEG,DAT,OGM,VOB,RM,RMVB,TS,TP,IFO,NSV";
    //预览附件
    public static String FILE_BROWSE = "PDF,CAJ";
    //文件格式标记
    public static String FILE_UNTITLED_NUM = "0";
    public static String FILE_IMAGE_NUM = "1";
    public static String FILE_SOUND_NUM = "2";
    public static String FILE_VIDEO_NUM = "3";
    public static String FILE_BROWSE_NUM = "4";
    public static String FILE_OTHER_NUM = "5";
    //收藏中的分类管理 删除是9，未删除是1
    public static final String CATEGORYSTAUTS_DELETE = "9";
    public static final String CATEGORYSTAUTS = "1";
    public static final String CATEGORY_NAME = "该分类名称已存在";
    public static final int CATEGORY_SORTNUM = 0;
    public static final int CHECK_PASS_FLAG = 2;//数据审核通过

    //首页默认访问路径
    //public static final String DEFAULT_HOME_PAGE="home/index";
    public static final String DEFAULT_HOME_PAGE = "system/index";
    public static final String LOGIN_PAGE = "home/login";

    //数据库类型是音频
    public static final String DATABASETYPE_SOUND = "6";
    //数据库类型是视频
    public static final String DATABASETYPE_VIDEO = "4";
    //数据已发布
    public static final String DATABAS_IS_PUBLISH = "1";
    //数据库可见
    public static final String DATABAS_IS_SHOW = "1";
    //数据库降序
    public static final String DESC = "DESC";
    public static final String DESC_NUM = "0";
    //数据库升序
    public static final String ASC = "ASC";
    public static final String ASC_NUM = "1";
    //数据库是否需要审核后才能查看0代表不需要，1代表需要
    public static final String CHECK_PUB_FLAG_YES = "1";
    public static final String CHECK_PUB_FLAG_NO = "1";

    //开关标记
    public static final String ON = "on";
    public static final String OFF = "off";
    //停用标记（0未停用，1停用）
    public static final String STOPFLAG_0 = "0";
    public static final String STOPFLAG_1 = "1";
    //审核状态（0待审、1通过、2不通过）
    public static final String CHCEKSTATE_0 = "0";
    public static final String CHCEKSTATE_1 = "1";
    public static final String CHCEKSTATE_2 = "2";
    //密级类型
    public static final String SERCURITY_0 = "0";
    public static final String SERCURITY_1 = "1";
    public static final String SERCURITY_2 = "2";
    //登陆日志状态
    public static final String LOGIN_0 = "0";//过期
    public static final String LOGIN_1 = "1";//登陆成功
    public static final String LOGIN_2 = "2";//停用
    public static final String LOGIN_3 = "3";//失败
    public static final String LOGIN_4 = "4";//ip地址不在范围内

    //匿名用户
    public static final String ANONY = "ANONY";
    //匿名用户组sysid
    public static final String ANONYMOUS_SYSID = "5";
    //系统管理员
    public static final String SA = "SA";
    //下载量，浏览量自增1
    public static int DOWNLOADRATE = 1;
    //浏览历史 状态及资源
    public static int STATUSANDSOURCE = 0;
    //前台登陆入口标记
    public static String ISFRONT1 = "1";
    //后台登陆入口标记
    public static String ISFRONT0 = "0";
    //系统管理员用户组sysyid
    public static String SYSTEM_GROUP_ID = "1";
    //浏览下载记录 无
    public static String BROWSE_DOWNLOAD_HISTORY = "无";
    //细览页必须查询字段
    public static String MUST_QUERY_THE_FIELD = "SYS_FLD_VSM,SYS_FLD_DOWNLOADRATE,SYS_FLD_BROWSERATE";
    //细览相似文献模板分类（图片模板）
    public static String TEMPLATE_CLASSIFICATION = "62,63,64,65";
    //浏览记录，下载记录，收藏列表，标题最大长度
    public static int MAX_LENGTH = 300;
    //默认排序
    public static String SORTNUM = "0";
    //栏目设置是否可见1可见，0不可见。
    public static String ISPUB_YES = "1";
    public static String ISPUB_NO = "0";
    //是否删除成功 0成功,1不成功
    public static int ISSUCESS_YES = 0;
    public static int ISSUCESS_NO = 1;
    //统计Start
    //默认值
    public static String ACCESSDEFAULTS = "0";
    //小计及总计字段初始值
    public static int ACCESS_DEFAULTS = 0;
    //小计
    public static String SUBTOTAL = "小计";
    //总计
    public static String TOTALALL = "总计";
    //分类的全部
    public static String ALLCLASSIFICATION = "全部";
    //全部分类
    public static String ALLAF = "allaf";
    //其他
    public static String OTHERCLASSIFICATION = "其他";
    //其他分类
    public static String OTHERAF = "otheraf";
    //username的全部
    public static String ALLUSERNAME = "全部";
    //全部分类
    public static String ALLNAME = "allname";
    //GRADE
    public static String GRADE = "1";
    //查询不到dbid默认添加-1
    public static int DBIDDEFAULTS = -1;
    //统计end
    //该库已删除
    public static String THE_DATABASE_HAS_BEEN_DELETED = "该库已删除";
    //分类导航名称已存在
    public static String CLASSIFIED_NAVIGATION_NAME = "分类导航名称已存在";
    //专题库名称已存在
    public static String THEMATIC_NAME = "专题库名称已存在";
    //1代表未删除
    public static String DELETE = "1";

    //左侧-文字列表
    public static String THEMATIC_NUM_1 = "1";
    //左侧-图片列表
    public static String THEMATIC_NUM_2 = "2";
    //左侧-图片文字列表
    public static String THEMATIC_NUM_3 = "3";
    //左侧-图片轮播
    public static String THEMATIC_NUM_4 = "4";
    //右侧-图片文字列表
    public static String THEMATIC_NUM_5 = "5";
    //右侧-单图片
    public static String THEMATIC_NUM_6 = "6";
    //右侧-图片轮播
    public static String THEMATIC_NUM_7 = "7";

    //左侧-文字列表
    public static String THEMATIC_NAME_1 = "左侧-文字列表";
    //左侧-图片列表
    public static String THEMATIC_NAME_2 = "左侧-图片列表";
    //左侧-图片文字列表
    public static String THEMATIC_NAME_3 = "左侧-图片文字列表";
    //左侧-图片轮播
    public static String THEMATIC_NAME_4 = "左侧-图片轮播";
    //右侧-图片文字列表
    public static String THEMATIC_NAME_5 = "右侧-图片文字列表";
    //右侧-单图片
    public static String THEMATIC_NAME_6 = "右侧-单图片";
    //右侧-图片轮播
    public static String THEMATIC_NAME_7 = "右侧-图片轮播";

    //审核不通过原因名称
    public static String ERROR_CONFIG_NAME = "该审核不通过原因已存在";
    //localhost 登录 得到的本机ip
    public static String LOCALHOSTIPV6 = "0:0:0:0:0:0:0:1";
    //本机ip
    public static String LOCALHOSTIPV4 = "127.0.0.1";
    //用户及用户组默认起始ip
    public static String SIP = "0.0.0.0";
    //用户及用户组默认终止ip
    public static String EIP = "255.255.255.255";

    //redis字典key前缀
    public final static String DICTIONARY_KEY_PREFIX = "dictionary_";

    //sql相关度常量
    public final static String RELEVANT = "RELEVANT";

    //本地库
    public final static String SEARCH_TYPE_0 = "0";
    //虚拟库/并行库
    public final static String SEARCH_TYPE_1 = "1";
    //采集库
    public final static String SEARCH_TYPE_2 = "2";
    //计算文件夹的旧版本生成方式
    public final static String GET_DIR_WAY_0 = "0";
    //计算文件夹的新版本生成方式(默认)
    public final static String GET_DIR_WAY_1 = "1";

    //kbatis location
    public final static String KBATIS_LOCATION = "kbatis-config.xml";

}
