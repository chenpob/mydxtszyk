package cnki.tpi.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * 字符串操作工具类
 *
 * @author lpz
 */
public class StrUtil {
    //判断字符串是否为空或者为null
    public static boolean checkStringNull(String str) {
        if (str != null && !str.trim().equals("") && !str.equals("null")) {
            return true;
        } else {
            return false;
        }
    }

    //判断Object是否为空
    public static boolean checkObjNull(Object obj) {
        if (obj != null) {
            return checkStringNull(obj.toString());
        } else {
            return false;
        }
    }

    /**
     * 去除字符串前后的,
     *
     * @param str
     * @return
     */
    public static String disposeStr(String str) {
        if (StrUtil.checkStringNull(str)) {
            if (str.startsWith(",")) {
                str = str.substring(1);
            }
            if (str.endsWith(",")) {
                str = str.substring(0, str.length() - 1);
            }
        }
        return str;
    }

    /**
     * 去除字符串前后的字符
     *
     * @param str
     * @return
     */
    public static String disposeSymbol(String str, String symbol) {
        if (StrUtil.checkStringNull(str)) {
            if (str.startsWith(symbol)) {
                str = str.substring(symbol.length());
            }
            if (str.endsWith(symbol)) {
                str = str.substring(0, str.length() - symbol.length());
            }
        }
        return str;
    }


    /**
     * 保证字符串首尾以,结束
     *
     * @param str
     * @return
     */
    public static String addCommaToStr(String str) {
        if (StrUtil.checkStringNull(str)) {
            if (!str.startsWith(",")) {
                str = "," + str;
            }
            if (!str.endsWith(",")) {
                str = str + ",";
            }
        }
        return str;
    }


    /**
     * 保证字符串首尾以,结束
     *
     * @param str
     * @return
     */
    public static String addCommaToStrOrNull(String str) {
        if (StrUtil.checkStringNull(str)) {
            if (!str.startsWith(",")) {
                str = "," + str;
            }
            if (!str.endsWith(",")) {
                str = str + ",";
            }
        } else {
            return null;
        }
        return str;
    }


    public static String disposeArrow(String str) {
        if (StrUtil.checkStringNull(str)) {
            if (str.startsWith(ConstantUtil.ARROW)) {
                str = str.substring(3);
            }
            if (str.endsWith(ConstantUtil.ARROW)) {
                str = str.substring(0, str.length() - 3);
            }
        }
        return str;
    }

    /**
     * 保证字符串末尾以~结束
     *
     * @param str
     * @return
     */
    public static String addArrowToStr(String str) {
        if (StrUtil.checkStringNull(str)) {
            if (!str.endsWith(ConstantUtil.ARROW)) {
                str = str + ConstantUtil.ARROW;
            }
        }
        return str;
    }

    /**
     * 获取登录人的ip
     *
     * @param request
     * @return
     */
    public final static String getIpAddress(HttpServletRequest request) throws IOException {
        // 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址  

        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
        } else if (ip.length() > 15) {
            String[] ips = ip.split(",");
            for (int index = 0; index < ips.length; index++) {
                String strIp = (String) ips[index];
                if (!("unknown".equalsIgnoreCase(strIp))) {
                    ip = strIp;
                    break;
                }
            }
        }
        return ip;
    }


    public static boolean isContain(List list, String str) {
        boolean flag = false;
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (flag) {
                    break;
                }
                if (str.equals(String.valueOf(list.get(i)))) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    public static boolean isContain(String[] strings, String str) {
        boolean flag = false;
        if (strings != null && strings.length > 0) {
            for (String string : strings) {
                if (str.equals(string)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 将数组转化为+拼接的字符串
     *
     * @param arr
     * @return
     */
    public static String convertArrToStr(String arr[]) {
        String str = "";
        if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if (i == arr.length - 1) {
                    str += " '" + arr[i] + "' ";
                } else {
                    str += " '" + arr[i] + "' +";
                }
            }
        }
        return str;
    }

    /**
     * 将用字符隔开的字符串转化为list
     *
     * @param str
     * @return
     */
    public static List<String> convertStrToList(String str, String symbol) {
        str = disposeSymbol(str, symbol);
        String[] arr = null;
        if (checkStringNull(str)) {
            arr = str.split(symbol);
        }
        List<String> list = new ArrayList<String>();
        if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if (StrUtil.checkStringNull(arr[i])) {
                    list.add(arr[i]);
                }
            }
        }
        return list;
    }

    /**
     * 将用字符隔开的字符串转化为list<Integer>
     *
     * @param str
     * @return
     */
    public static List<Integer> convertIntegerStrToList(String str, String symbol) {
        str = disposeSymbol(str, symbol);
        String[] arr = null;
        if (checkStringNull(str)) {
            arr = str.split(symbol);
        }
        List<Integer> list = new ArrayList<>();
        if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if (StrUtil.checkStringNull(arr[i])) {
                    list.add(Integer.valueOf(arr[i]));
                }
            }
        }
        return list;
    }


    /**
     * 去除字符串前后的/
     *
     * @param str
     * @return
     */
    public static String disposeStrXieGang(String str) {
        if (StrUtil.checkStringNull(str)) {
            if (str.startsWith(ConstantUtil.XIEGANG)) {
                str = str.substring(1);
            }
            if (str.endsWith(ConstantUtil.XIEGANG)) {
                str = str.substring(0, str.length() - 1);
            }
        }
        return str;
    }

    /**
     * 字符串编码转换的实现方法
     *
     * @param str        待转换编码的字符串
     * @param newCharset 目标编码
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String changeCharset(String str, String newCharset)
            throws UnsupportedEncodingException {
        if (str != null) {
            //用默认字符编码解码字符串。  
            byte[] bs = str.getBytes();
            //用新的字符编码生成字符串  
            return new String(bs, newCharset);
        }
        return null;
    }

    /**
     * 字符串编码转换的实现方法
     *
     * @param str        待转换编码的字符串
     * @param oldCharset 原编码
     * @param newCharset 目标编码
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String changeCharset(String str, String oldCharset, String newCharset)
            throws UnsupportedEncodingException {
        if (str != null) {
            //用旧的字符编码解码字符串。解码可能会出现异常。  
            byte[] bs = str.getBytes(oldCharset);
            //用新的字符编码生成字符串  
            return new String(bs, newCharset);
        }
        return null;
    }

    /**
     * 部门编码加零处理
     *
     * @param str
     * @return
     */
    public static String addZero(String str) {
        if (checkStringNull(str)) {
            int length = str.length();
            for (int i = length; i < 4; i++) {
                str = "0" + str;
            }
        }
        return str;
    }

    /**
     * 得到同一层级的部门编码
     * 最大的编码+1后转为字符串
     *
     * @param deptCode
     * @return
     */
    public static String getDeptCode(String deptCode) {
        Integer i = Integer.valueOf(deptCode);
        i += 1;
        return addZero(String.valueOf(i));
    }

    /**
     * 得到父级别的syscode
     *
     * @param sysCode
     * @param deptCode
     * @return
     */
    public static String getParentSysCode(String sysCode, String deptCode) {
        String str = sysCode.substring(0, sysCode.lastIndexOf(deptCode));
        if (!checkStringNull(str)) {
            return "0";
        }
        return str;
    }


    /**
     * 将字符编码转换成GBK码
     */
    public static String toGbk(String str) throws UnsupportedEncodingException {
        return changeCharset(str, "GBK");
    }

    /**
     * 将字符编码转换成ISO-8859-1码
     */
    public static String toIso88591(String str) throws UnsupportedEncodingException {
        return changeCharset(str, "ISO_8859_1");
    }

    /**
     * 将字符编码转换成UTF-8码
     */
    public static String toUtf8(String str) throws UnsupportedEncodingException {
        return changeCharset(str, "UTF-8");
    }

    /**
     * 将字符编码转换成US-ASCII码
     */
    public static String toAscii(String str) throws UnsupportedEncodingException {
        return changeCharset(str, "US-ASCII");
    }


    /**
     * 判断url 是否在哪里
     */
    public static Boolean isContain(ArrayList<String> arrayList , String url) {
        for (String s : arrayList) {
            if (s.equals(url)){
                return true;
            }
        }
        return false;
    }

    public static void main(String args[]) {
        String str = "01";
        System.out.println(addZero(str));
        System.out.println(getDeptCode(str));
        System.out.println(getParentSysCode("0001", "0001"));
    }


}
