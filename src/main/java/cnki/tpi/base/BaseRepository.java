package cnki.tpi.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseRepository {
    @Autowired
    protected IDataContext context;
    protected final static Logger logger = LoggerFactory.getLogger(BaseRepository.class);
    //protected SqlSession sqlSession = DataSourceUtil.getSqlSession();
}
