package cnki.tpi.base;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DataSourceMysqlConfig {

    @Bean(name = "mysqlDs")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.mysql") // application.yml中对应属性的前缀
    public DataSource dataSource1() {
        return DataSourceBuilder.create().build();
    }

    //档案数据库
    @Bean(name = "archivesDs")
    @ConfigurationProperties(prefix = "spring.datasource.archives.mysql") // application.yml中对应属性的前缀
    public DataSource dataSource2() {
        return DataSourceBuilder.create().build();
    }

    //OA已办数据库
    @Bean(name = "alreadydoneDs")
    @ConfigurationProperties(prefix = "spring.datasource.alreadydone.mysql") // application.yml中对应属性的前缀
    public DataSource dataSource3() {
        return DataSourceBuilder.create().build();
    }

}
