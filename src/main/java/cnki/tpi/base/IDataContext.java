package cnki.tpi.base;

import java.util.List;
import java.util.Map;

import kbase.struct.TPI_DIRINFO;
import kbase.struct.TPI_DISKINFO;

public interface IDataContext {
    <T> List<T> excuteQuery(String sql, Class<T> t);

    List<Map<String, String>> excuteQueryList(String sql);

    List<Map<String, Object>> excuteQueryListObj(String sql);

    int excuteQueryRecordNumber(String sql);

    <T> List<T> excuteQuery(String sql, List list, Class<T> t);

    Boolean excuteDelete(String sql);

    Boolean excuteCreateDataBaseOrTable(String paramString);

    Boolean excuteInsertList(List<String> sqlList);

    int excuteSql(String sql);

    Boolean excuteUpdate(String sql);

    Boolean excuteUpdateList(List<String> sqlList);

    int updateAndReturnSysid(String sql);

    /**
     * 仅返回一个对象
     *
     * @param sql
     * @param t
     * @return
     */
    <T> T excuteQueryReturnOne(String sql, Class<T> t);

    /**
     * 查找list,并且高亮显示
     *
     * @param sql
     * @return
     */
    List<Map<String, String>> excuteQueryList_light(String sql);

    /**
     * 跨库检索数量查询
     *
     * @param dbList
     * @param arr
     * @param string
     * @return
     */
    List<Map<String, String>> getMultiCountByDb(List<Map<String, String>> dbList, List<String> arr, String string);

    /**
     * 文本
     *
     * @param sql
     * @param columnLabel
     * @param content
     */
    void updateLongText(String sql, String columnLabel, String content);

    /**
     * @param fieldName 字段名称
     * @param tableName 表名称
     * @return
     */
    Integer getMaxId(String fieldName, String tableName);

    /**
     * @param tableName 表名称
     * @param condition 检索条件
     * @return
     */
    Integer getTableCountByCondition(String tableName, String condition);

    /**
     * 通过实例化对象数据插入表记录
     *
     * @param tableName 待导入记录的表名
     * @param list      实体类对象集合
     * @return
     */
    <T> Boolean insertRecData(String tableName, List<T> list);

    /**
     * 通过实例化对象数据插入表记录
     *
     * @param tableName 待导入记录的表名
     * @param t         实体类对象
     * @return
     */
    <T> Boolean insertRecData(String tableName, T t);

    /**
     * 通过REC格式数据插入表记录
     *
     * @param tableName 待导入记录的表名
     * @param content   REC格式内容，如： <REC><key1>=value1<key2>=value2
     * @return
     */
    Boolean insertRecData(String tableName, String content);

    /**
     * 通过REC文件插入表记录
     *
     * @param tableName 待导入记录的表名
     * @param filePath  REC文件路径
     * @return
     */
    Boolean insertRecFile(String tableName, String filePath);

    /**
     * 通过实例化对象数据更新表记录
     *
     * @param columnLabel 更新的主键列，唯一性由用户保证
     * @param tableName   待更新记录的表名
     * @param list        实体类对象集合
     * @return
     */
    <T> Boolean updateRecData(String columnLabel, String tableName, List<T> list);

    /**
     * 通过实例化对象数据更新表记录
     *
     * @param columnLabel 更新的主键列，唯一性由用户保证
     * @param tableName   待更新记录的表名
     * @param t           实体类对象
     * @return
     */
    <T> Boolean updateRecData(String columnLabel, String tableName, T t);

    /**
     * 通过REC格式数据更新表记录
     *
     * @param columnLabel 更新的主键列，唯一性由用户保证
     * @param tableName   待更新记录的表名
     * @param content     REC格式内容，如： <REC><key1>=value1<key2>=value2
     * @return
     */
    Boolean updateRecData(String columnLabel, String tableName, String content);

    boolean KBase_MakeDir(String path);

    boolean KBase_FileExists(String path);

    boolean KBase_RemoveDir(String path);

    TPI_DIRINFO[] KBase_GetServerDirList(String path);

    TPI_DISKINFO[] KBase_GetServerDiskInfo();

    /**
     * 分词
     *
     * @param content 分词内容
     * @return 分词结果
     */
    String wordSegment(String content);

    /**
     * 分词
     *
     * @param content          分词内容
     * @param isKeepSingleWord 是否保留单字，若否则去除单字返回词干数组，不管是否都会自动过滤特殊字符
     * @return 分词结果
     */
    List<String> wordSegment(String content, boolean isKeepSingleWord);

    /**
     * 得到相似词列表
     *
     * @param inputKeyWord 输入的关键词
     * @return 得到结果 [相似词][相似度][词频][|][相似词][相似度][词频]...
     */
    String getRelevantWords(String inputKeyWord);

    /**
     * 得到相似词列表
     *
     * @param inputKeyWord       输入的关键词
     * @param maxRelevantWord    要求最多返回相似词的数量
     * @param seperatorChar      两个词之间的分隔符号
     * @param isWordFreqFollowed 是否输出词频信息
     * @return 得到结果 [相似词][相似度][词频][|][相似词][相似度][词频]...
     */
    String getRelevantWords(String inputKeyWord, int maxRelevantWord, int seperatorChar, boolean isWordFreqFollowed);

}
